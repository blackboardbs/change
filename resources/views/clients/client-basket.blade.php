@extends('clients.show')

@section('tab-content')
    <div class="col-lg-5">
        <div class="card">
            <div class="card-header">
                Client Basket
            </div>
            <div class="card-body" id="left_div">
                <ul>
                    <dt>
                        First Name
                    </dt>
                    <dd>
                        @if($client->first_name)
                            {{$client->first_name}}
                        @else
                            <small><i>No first names captured</i></small>
                        @endif
                    </dd>
                    <dt>
                        Surname
                    </dt>
                    <dd>
                        @if($client->last_name)
                            {{$client->last_name}}
                        @else
                            <small><i>No surname captured</i></small>
                        @endif
                    </dd>
                    <dt>
                        Initials
                    </dt>
                    <dd>
                        @if($client->initials)
                            {{$client->initials}}
                        @else
                            <small><i>No initials captured</i></small>
                        @endif
                    </dd>
                    <dt>
                        ID/Passport number
                    </dt>
                    <dd>
                        @if($client->id_number)
                            {{$client->id_number}}
                        @else
                            <small><i>No ID/Passport number captured</i></small>
                        @endif
                    </dd>
                    <dt>
                        Cellphone number
                    </dt>
                    <dd>
                        @if($client->contact)
                            {{$client->contact}}
                        @else
                            <small><i>No contact number captured</i></small>
                        @endif
                    </dd>
                    <dt>
                        Email
                    </dt>
                    <dd>
                        @if($client->email)
                            <a href="mailto:{{$client->email}}">{{$client->email}}</a><a href="javascript:void(0)" onclick="sendClientEmail('{{$client->id}}','{!! $client->email !!}')" class="btn btn-sm btn-secondary ml-3">Request Client Feedback</a>
                        @else
                            <small><i>No email captured</i></small><a href="javascript:void(0)" onclick="sendClientEmail('{{$client->id}}','{!! $client->email !!}')" class="btn btn-sm btn-secondary ml-3">Request Client Feedback</a>
                        @endif
                    </dd>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-sm-7">
        <div class="card">
            <div class="card-header">
                Client Activities
            </div>

            <div class="card-body col-sm-12" id="right_div">
                @foreach($client_basket_activities as $key => $activity)
                    @if(count($activity))
                        {{--<div class="card col-md-12">--}}
                        <div class="cb">
                            <strong>{{$key}}</strong>
                        </div>
                        <div class="cb-body">
                                <ul>
                                    @forelse($activity as $name)
                                        <li>{{$name->name}}</li>
                                    @empty
                                        <li><i>No activities linked to client</i></li>
                                    @endforelse
                                </ul>
                        </div>
                        {{--     </div>--}}
                    @endif
                @endforeach
                {{--<div id="fader"></div>--}}
            </div>

        </div>
    </div>
@endsection

@section('extra-js')
    {{--<script src="https://code.highcharts.com/highcharts.js"></script>--}}
    <script src="https://code.highcharts.com/modules/no-data-to-display.js"></script>
    <script src="https://rawgit.com/highcharts/rounded-corners/master/rounded-corners.js"></script>
    <script>
        $(window).load(function() {
            $('#right_div').css({'height': $('#left_div').innerHeight(),'max-height': $('#left_div').innerHeight(), 'overflow': 'auto'});
        });
    </script>
@endsection

@section('extra-css')
    <style>
        .cb {
            background: linear-gradient(-45deg, #ee7752, #e73c7e, #ee7752, #e73c7e);
            background-size: 400% 400%;
            animation: gradient 15s ease infinite;
            padding: 0.5rem;
            border-radius: 3px;
            color: #f9f9f9;
        }
        .cb-body{
            background: linear-gradient(-45deg, rgba(238, 119, 82, .3), rgba(231, 60, 126, .3), rgba(238, 119, 82, .3), rgba(231, 60, 126, .3));
            background-size: 400% 400%;
            animation: gradient 15s ease infinite;
            padding: 1rem;
        }
        .cb-body>ul{
            padding: 0!important;
            list-style: none;
            margin: 0;
        }
        .cb-body>ul>li{
            padding: 3px 3px 3px 20px!important ;
            background: rgba(255, 255, 255, .5);
            border-bottom: 2px solid rgb(255, 255, 255);
            color: #777;
        }
        /*#fader{
            background: rgb(255,255,255);
            background: linear-gradient(0deg, rgba(255,255,255,1) 35%, rgba(0,212,255,0) 100%);
            height: 3rem;
            width: 96.5%;
            position: absolute;
            bottom: 0;
            z-index: 1000;
        }*/

        @keyframes gradient {
            0% {
                background-position: 0% 50%;
            }
            50% {
                background-position: 100% 50%;
            }
            100% {
                background-position: 0% 50%;
            }
        }
    </style>
@endsection

