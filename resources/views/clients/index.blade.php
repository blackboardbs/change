@extends('adminlte.default')

@section('title') Clients @endsection

@section('header')
    <div class="container-fluid container-title row">
        <div class="col-sm-1 pr-0"><h3>@yield('title')</h3></div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
    <form class="mt-3">

        <div class="form-row">
            <div class="form-group col-md-3">
                <label for="f">From: first contact date</label>
                {{Form::date('f',old('f'),['class'=>'form-control form-control-sm'])}}
            </div>
            <div class="form-group col-md-3">
                <label for="t">To: first contact date </label>
                {{Form::date('t',old('t'),['class'=>'form-control form-control-sm'])}}
            </div>
            <div class="form-group col-md-3">
                <label for="q">Matching</label>
                <div class="input-group input-group-sm">
                    <div class="input-group-prepend">
                        <div class="input-group-text">
                            <i class="fa fa-search"></i>
                        </div>
                    </div>
                    {{Form::text('q',old('query'),['class'=>'form-control form-control-sm','placeholder'=>'Search...'])}}
                </div>
            </div>
            <div class="form-group col-md-3">
                <label for="">&nbsp;</label>
                <div>
                    <button type="submit" class="btn btn-sm btn-secondary ml-2 mr-2"><i class="fa fa-search"></i> Search</button>
                    <a href="{{route('clients.index')}}" class="btn btn-sm btn-info"><i class="fa fa-eraser"></i> Clear</a>
                </div>
            </div>
        </div>
    </form>
    <hr>

    <div class="table-responsive">
        <table class="table table-bordered table-hover table-sm">
            <thead class="btn-dark">
            <tr>
                <th nowrap>@sortablelink('company', 'Name')</th>
                <th nowrap>@sortablelink('email', 'Email')</th>
                <th nowrap>@sortablelink('cell', 'Cellphone Number')</th>
                <th nowrap>@sortablelink('process', 'Process')</th>
                <th nowrap>@sortablelink('section', 'Section')</th>
                <th nowrap>@sortablelink('created_at', 'Created Date ')</th>
                <th nowrap>@sortablelink('created_by', 'Created By')</th>
            </tr>
            </thead>
            <tbody>
            @php $related_parties_count = 0; @endphp
            @forelse($clients as $client)
                @if(!isset($client["related_party_id"]))
                    <tr>
                        <td><a href="{{route('clients.show',[$client["id"],$client["process_id"],$client["step_id"]])}}">{{(isset($client["company"] ) && $client["company"] != ' ' ? $client["company"]  : 'Not Captured')}}</a></td>
                        <td>{{!is_null($client["email"]) ? $client["email"] : ''}}</td>
                        <td>{{!is_null($client["contact"]) ? $client["contact"] : ''}}</td>
                        <td>@foreach($client["process"] as $processes)
                                <a href="{{route('clients.progress',$client['id'])}}/{{$processes['process']['id']}}/{{$processes['step']['id']}}">{{$processes['process']['name']}}</a><br />
                            @endforeach
                        </td>
                        <td>@foreach($client["process"] as $processes)
                                <a href="{{route('clients.progress',$client['id'])}}/{{$processes['process']['id']}}/{{$processes['step']['id']}}">{{$processes['step']['name']}}</a><br />
                            @endforeach
                        </td>
                        <td>{{$client["completed_at"]}}</td>
                        <td><a href="{{route('profile',$client["introducer"])}}">{{$client["introducer"]}}</a></td>
                    </tr >
                @else
                    @if(request()->clientf != 'primary')
                    @php $related_parties_count++ @endphp
                    <tr class="bg-gray-light">
                        <td><a href="{{route('relatedparty.related_index',$client["client_id"])}}">{{(isset($client["company"]) && $client["company"] != '' ? $client["company"] : 'Not Captured')}}</a></td>
                        <td>{{!is_null($client["email"]) ? $client["email"] : ''}}</td>
                        <td>{{!is_null($client["contact"]) ? $client["contact"] : ''}}</td>
                        <td>{{!is_null($client["process"]) ? $client["process"] : ''}}</td>
                        <td>{{!is_null($client["section"]) ? $client["section"] : ''}}</td>
                        <td>{{$client["completed_at"]}}</td>
                        <td><a href="{{route('profile',$client["introducer"])}}">{{$client["introducer"]}}</a></td>
                    </tr>
                    @endif()

                @endif
            @empty
                <tr>
                    <td colspan="100%" class="text-center"><small class="text-muted">No clients match those criteria.</small></td>
                </tr>
            @endforelse
            </tbody>

        </table>
    </div>

    <small class="text-muted">Found <b>{{count($clients)}}</b> clients matching those criteria.</small>
    </div>
@endsection
@section('extra-css')
    <link rel="stylesheet" href="{{asset('chosen/chosen.min.css')}}">
@endsection
@section('extra-js')
    <script>
        $('select').on('change', function () {
            $(this).closest('form').submit();
        });

        $('input[type=radio][name=clientf]').change(function() {
            $(this).closest('form').submit();
        });
    </script>
@endsection