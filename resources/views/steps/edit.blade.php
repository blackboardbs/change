@extends('adminlte.default')

@section('title') Edit step @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <a href="{{route('processes.show',$step->process)}}" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</a>
    </div>
@endsection

@section('content')
<div class="container-fluid">
    <div class="card mt-3">
        <div class="card-body">
            <dt>
                Process
            </dt>
            <dd>
                {{$step->process->name}}
            </dd>
            <dt>
                Office
            </dt>
            <dd>
                {{$step->process->office->area->region->division->name}} / {{$step->process->office->area->region->name}} / {{$step->process->office->area->name}} / {{$step->process->office->name}}
            </dd>
        </div>
    </div>

    {{Form::open(['url' => route('steps.update',$step), 'method' => 'put','class'=>'mt-3 mb-3'])}}

    {{Form::label('name', 'Name')}}
    {{Form::hidden('process_id',$process_id,['class'=>'form-control'])}}
    {{Form::text('name',$step->name,['class'=>'form-control'. ($errors->has('name') ? ' is-invalid' : ''),'placeholder'=>'Name'])}}
    @foreach($errors->get('name') as $error)
        <div class="invalid-feedback">
            {{ $error }}
        </div>
    @endforeach

    {{Form::label('colour', 'Colour')}}
    <div class="col-lg-4 text-center">
        <div class="form-control">
            <input name="step_colour" type="text" style="background-color: {{$step->colour}}" value="{{$step->colour}}" class="step_colour form-control form-control-sm" title="Step colour"/>
        </div>
    </div>

    {{Form::label('group', 'Group Step')}}
    <div class="col-lg-4 text-left">
        <div>
            <input name="group_step" id="group_step" type="checkbox" {{($step->group == 1 ? "checked" : '')}} />
        </div>
    </div>

    <hr>

    {{Form::label('activities', 'Activities')}}
    <blackboard-process-editor  ref="step_a"
            :black-activities="{{$activities}}"

    ></blackboard-process-editor>

    <div class="blackboard-fab mr-3 mb-3">
        <button type="submit" class="btn btn-primary btn-lg submitbtn"><i class="fa fa-save"></i> Save</button>
    </div>

    {{Form::close()}}
</div>
@endsection
@section('extra-js')

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.3.3/css/bootstrap-colorpicker.min.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.3.3/js/bootstrap-colorpicker.min.js"></script>
    <style>
        .colorpicker-2x .colorpicker-saturation {
            width: 200px;
            height: 200px;
        }

        .colorpicker-2x .colorpicker-hue,
        .colorpicker-2x .colorpicker-alpha {
            width: 30px;
            height: 200px;
        }

        .colorpicker-2x .colorpicker-color,
        .colorpicker-2x .colorpicker-color div {
            height: 30px;
        }
    </style>
    <script>
        $(function () {
            if($("#group_step").is(':checked')){
                vm.$refs.step_a.disen();
            };
            $("#group_step").on('click',function () {
                vm.$refs.step_a.disen();
            });
            // Basic instantiation:
            $('.step_colour').colorpicker({
                align: 'center',
                input: $('.step_colour2'),
                format: 'rgba',
                customClass: 'colorpicker-2x',
                sliders: {
                    saturation: {
                        maxLeft: 200,
                        maxTop: 200
                    },
                    hue: {
                        maxTop: 200
                    },
                    alpha: {
                        maxTop: 200
                    }
                }
            }).on('changeColor', function(event) {
                $('.step_colour').css('background-color', event.color.toString());

            });
        });
    </script>
@endsection