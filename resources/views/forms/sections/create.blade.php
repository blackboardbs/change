@extends('adminlte.default')

@section('title') Create Form Section @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        {{--<a href="{{route('processes.show',$process)}}" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</a>--}}
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        {{--<div class="card mt-3">
            <div class="card-body">
                <dt>
                    Process
                </dt>
                <dd>
                    {{$process->name}}
                </dd>
                <dt>
                    Office
                </dt>
                <dd>
                    {{$process->office->area->region->division->name}} / {{$process->office->area->region->name}} / {{$process->office->area->name}} / {{$process->office->name}}
                </dd>
            </div>
        </div>--}}

        {{Form::open(['url' => route('form_section.store',$forms), 'method' => 'post','class'=>'mt-3 mb-3'])}}

        {{Form::label('name', 'Name')}}
        {{Form::text('name',old('name'),['class'=>'form-control form-control-sm'. ($errors->has('name') ? ' is-invalid' : ''),'placeholder'=>'Name'])}}
        @foreach($errors->get('name') as $error)
            <div class="invalid-feedback">
                {{ $error }}
            </div>
        @endforeach

        <hr>

        {{Form::label('fields', 'Fields')}}
        <blackboard-forms-editor></blackboard-forms-editor>

        <div class="blackboard-fab mr-3 mb-3">
            <button type="submit" class="btn btn-primary btn-lg submitbtn"><i class="fa fa-save"></i> Save</button>
        </div>

        {{Form::close()}}
    </div>
@endsection
@section('extra-js')

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

@endsection