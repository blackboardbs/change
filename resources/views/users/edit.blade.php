@extends('adminlte.default')

@section('title') Edit User @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <a href="{{route('users.index')}}" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</a>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />
    {{Form::open(['url' => route('users.update',$user), 'method' => 'put','files'=>true])}}

    <div class="form-group mt-3">
        {{Form::label('first_name', 'First Name')}}
        {{Form::text('first_name',$user->first_name,['class'=>'form-control'. ($errors->has('first_name') ? ' is-invalid' : ''),'placeholder'=>'First Name'])}}
        @foreach($errors->get('first_name') as $error)
            <div class="invalid-feedback">
                {{ $error }}
            </div>
        @endforeach
    </div>

    <div class="form-group">
        {{Form::label('last_name', 'Last Name')}}
        {{Form::text('last_name',$user->last_name,['class'=>'form-control'. ($errors->has('last_name') ? ' is-invalid' : ''),'placeholder'=>'Last Name'])}}
        @foreach($errors->get('last_name') as $error)
            <div class="invalid-feedback">
                {{ $error }}
            </div>
        @endforeach
    </div>

    <div class="form-group">
        {{Form::label('email', 'Email')}}
        {{Form::text('email',$user->email,['class'=>'form-control'. ($errors->has('email') ? ' is-invalid' : ''),'placeholder'=>'Email'])}}
        @foreach($errors->get('email') as $error)
            <div class="invalid-feedback">
                {{ $error }}
            </div>
        @endforeach
    </div>

    <div class="form-group">
        {{Form::label('avatar', 'Display Picture')}}
        {{Form::file('avatar',['class'=>'form-control'. ($errors->has('avatar') ? ' is-invalid' : ''),'placeholder'=>'Avatar','onchange'=>"document.getElementById('blackboard-preview-large').src = window.URL.createObjectURL(this.files[0]); document.getElementById('blackboard-preview-small').src = window.URL.createObjectURL(this.files[0])"])}}
        @foreach($errors->get('avatar') as $error)
            <div class="invalid-feedback">
                {{ $error}}
            </div>
        @endforeach
        <small id="avatar" class="form-text text-muted">
            Images must be a square and will be resized to 200x200
        </small>
        <br>
        <img src="{{route('avatar',['q'=>$user->avatar])}}" id="blackboard-preview-large" class="blackboard-avatar blackboard-avatar-profile"/>
        <img src="{{route('avatar',['q'=>$user->avatar])}}" id="blackboard-preview-small" class="blackboard-avatar blackboard-avatar-navbar-img ml-3"/>
    </div>

    <div class="form-group">
        {{Form::label('role', 'Role')}}
        {{Form::select('role[]',$roles,$user_roles,['class'=>'form-control'. ($errors->has('role') ? ' is-invalid' : ''),'multiple'])}}
        @foreach($errors->get('role') as $error)
            <div class="invalid-feedback">
                {{ $error }}
            </div>
        @endforeach
        <small class="form-text text-muted">
            Hold <kbd>Ctrl</kbd> to select multiple entries
        </small>
    </div>

    <div class="form-group">
        {{Form::label('division', 'Division')}}
        {{Form::select('division[]',$divisions,$user_divisions,['class'=>'form-control'. ($errors->has('division') ? ' is-invalid' : ''),'multiple'])}}
        @foreach($errors->get('division') as $error)
            <div class="invalid-feedback">
                {{ $error }}
            </div>
        @endforeach
        <small class="form-text text-muted">
            Hold <kbd>Ctrl</kbd> to select multiple entries
        </small>
    </div>

    <div class="form-group">
        {{Form::label('region', 'Region')}}
        {{Form::select('region[]',$regions,$user_regions,['class'=>'form-control'. ($errors->has('region') ? ' is-invalid' : ''),'multiple'])}}
        @foreach($errors->get('region') as $error)
            <div class="invalid-feedback">
                {{ $error }}
            </div>
        @endforeach
        <small class="form-text text-muted">
            Hold <kbd>Ctrl</kbd> to select multiple entries
        </small>
    </div>

    <div class="form-group">
        {{Form::label('area', 'Area')}}
        {{Form::select('area[]',$areas,$user_areas,['class'=>'form-control'. ($errors->has('area') ? ' is-invalid' : ''),'multiple'])}}
        @foreach($errors->get('area') as $error)
            <div class="invalid-feedback">
                {{ $error }}
            </div>
        @endforeach
        <small class="form-text text-muted">
            Hold <kbd>Ctrl</kbd> to select multiple entries
        </small>
    </div>

    <div class="form-group">
        {{Form::label('office', 'Office')}}
        {{Form::select('office[]',$offices,$user_offices,['class'=>'form-control'. ($errors->has('office') ? ' is-invalid' : ''),'multiple'])}}
        @foreach($errors->get('office') as $error)
            <div class="invalid-feedback">
                {{ $error }}
            </div>
        @endforeach
        <small class="form-text text-muted">
            Hold <kbd>Ctrl</kbd> to select multiple entries
        </small>
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-sm">Save</button>
    </div>

    {{Form::close()}}
    </div>
@endsection