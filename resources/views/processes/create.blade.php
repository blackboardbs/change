@extends('adminlte.default')

@section('title') Create {{$type_name}} @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <a href="{{route('processes.index')}}" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</a>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
    {{Form::open(['url' => route('processes.store'), 'method' => 'post','class'=>'mt-3 mb-3','autocomplete' => 'off'])}}
    <input type="hidden" name="process_type_id" id="process_type_id" value="{{isset($process_type_id)?$process_type_id:1}}"/>
    {{Form::label('name', 'Name')}}
    {{Form::text('name',old('name'),['class'=>'form-control form-control-sm'. ($errors->has('name') ? ' is-invalid' : ''),'placeholder'=>'Name'])}}
    @foreach($errors->get('name') as $error)
        <div class="invalid-feedback">
            {{ $error }}
        </div>
    @endforeach

    {{Form::label('office', 'Office')}}
    {{Form::select('office',$offices,old('office'),['class'=>'form-control form-control-sm'. ($errors->has('office') ? ' is-invalid' : '')])}}
    @foreach($errors->get('office') as $error)
        <div class="invalid-feedback">
            {{ $error }}
        </div>
    @endforeach

    <div class="row mt-3">
        <div class="col-lg-4 text-center">
            <label><i class="fa fa-circle" style="color:rgba(242,99,91,.7)"></i> Not started</label>
            <div class="form-control">
                <input name="not_started_colour" type="text" style="background-color: rgba(242,99,91,.7)" value="rgba(242,99,91,.7)" class="not_started_color form-control form-control-sm" title="Stage colour: Not started"/>
            </div>
        </div>
        <div class="col-lg-4 text-center">
            <label><i class="fa fa-circle" style="color:rgba(252,182,61,.7)"></i> Started</label>
            <div class="form-control">
                <input name="started_colour" type="text" style="background-color: rgba(252,182,61,.7)" value="rgba(252,182,61,.7)" class="started_colour form-control form-control-sm" title="Stage colour: Started"/>
            </div>
        </div>
        <div class="col-lg-4 text-center">
            <label><i class="fa fa-circle" style="color:rgba(50,193,75,.7)"></i> Completed</label>
            <div class="form-control">
                <input name="completed_colour" type="text" style="background-color: rgba(50,193,75,.7)" value="rgba(50,193,75,.7)" class="completed_colour form-control form-control-sm" title="Stage colour: Completed"/>
            </div>
        </div>
    </div>

    <div class="blackboard-fab mr-3 mb-3">
        <button type="submit" class="btn btn-primary btn-lg"><i class="fa fa-save"></i> Save</button>
    </div>

    {{-- todo notifications --}}

    {{Form::close()}}
    </div>
@endsection
@section('extra-js')

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.3.3/css/bootstrap-colorpicker.min.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.3.3/js/bootstrap-colorpicker.min.js"></script>
    <style>
        .colorpicker-2x .colorpicker-saturation {
            width: 200px;
            height: 200px;
        }

        .colorpicker-2x .colorpicker-hue,
        .colorpicker-2x .colorpicker-alpha {
            width: 30px;
            height: 200px;
        }

        .colorpicker-2x .colorpicker-color,
        .colorpicker-2x .colorpicker-color div {
            height: 30px;
        }
    </style>
    <script>
        $(function () {
            // Basic instantiation:
            $('.not_started_color').colorpicker({
                align: 'center',
                input: $('.not_started_color2'),
                format: 'rgba',
                customClass: 'colorpicker-2x',
                sliders: {
                    saturation: {
                        maxLeft: 200,
                        maxTop: 200
                    },
                    hue: {
                        maxTop: 200
                    },
                    alpha: {
                        maxTop: 200
                    }
                }
            }).on('changeColor', function(event) {
                $('.not_started_color').css('background-color', event.color.toString());

            });

            $('.started_colour').colorpicker({
                align: 'center',
                format: 'rgba',
                customClass: 'colorpicker-2x',
                sliders: {
                    saturation: {
                        maxLeft: 200,
                        maxTop: 200
                    },
                    hue: {
                        maxTop: 200
                    },
                    alpha: {
                        maxTop: 200
                    }
                }
            }).on('changeColor', function(event) {
                $('.started_colour').css('background-color', event.color.toString());

            });

            $('.completed_colour').colorpicker({
                align: 'center',
                format: 'rgba',
                customClass: 'colorpicker-2x',
                sliders: {
                    saturation: {
                        maxLeft: 200,
                        maxTop: 200
                    },
                    hue: {
                        maxTop: 200
                    },
                    alpha: {
                        maxTop: 200
                    }
                }
            }).on('changeColor', function(event) {
                $('.completed_colour').css('background-color', event.color.toString());

            });
        });
    </script>
@endsection