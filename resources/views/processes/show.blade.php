@extends('adminlte.default')

@section('title') {{$process->name}} @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <a href="{{route('processes.index')}}" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</a>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
    <div class="row mt-3">
        <div class="col-lg-9">
            <div class="card">
                    <div class="card-body">
                        <ul class="mr-5">
                            <dt>
                                Office
                            </dt>
                            <dd>
                                {{$process->office->area->region->division->name}} / {{$process->office->area->region->name}} / {{$process->office->area->name}} / {{$process->office->name}}
                            </dd>
                            <dt>
                                Colours
                            </dt>
                            <dd>
                                <div class="row text- ml-0 mr-0">
                                    <div class="col-lg-4 pt-2 pb-2">
                                    {{--<div class="col-lg-4 pt-2 pb-2" style="background-color: {{$process->getStageHex(0)}}">--}}
                                        <b>Not started</b>
                                        <br>
                                        {{--{{$process->getStageHex(0,false)}}--}}
                                    </div>
                                    <div class="col-lg-4 pt-2 pb-2">
                                    {{--<div class="col-lg-4 pt-2 pb-2" style="background-color: {{$process->getStageHex(1)}}">--}}
                                        <b>Started</b>
                                        <br>
                                        {{--{{$process->getStageHex(1,false)}}--}}
                                    </div>
                                    <div class="col-lg-4 pt-2 pb-2">
                                    {{--<div class="col-lg-4 pt-2 pb-2" style="background-color: {{$process->getStageHex(2)}}">--}}
                                        <b>Completed</b>
                                        <br>
                                        {{--{{$process->getStageHex(2,false)}}--}}
                                    </div>
                                </div>
                            </dd>
                            <dt>
                                Steps
                            </dt>
                            <dd>
                                <div class="table-responsive ">
                                    <table class="table table-bordered table-sm table-hover">
                                        <thead class="btn-dark">
                                        <tr>
                                            <th>Name</th>
                                            <th>Activities</th>
                                            <th class="last">Move</th>
                                            <th class="last">Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @forelse($process->steps as $step)
                                            <tr>
                                                <td>{{$step->name}}</td>
                                                <td>{{$step->activities()->count()}}</td>
                                                <td class="last">
                                                    <a href="#" title="Move activity up" onclick="document.querySelector('.moveform-up-{{$step->id}}').submit()"><i class="fa fa-arrow-up"></i></a>
                                                    |
                                                    <a href="#" title="Move activity down" onclick="document.querySelector('.moveform-down-{{$step->id}}').submit()"><i class="fa fa-arrow-down"></i></a>
                                                    {{Form::open(['url' => route('steps.move',$step), 'method' => 'post','class'=>'moveform-up-'.$step->id])}}
                                                    {{Form::hidden('direction','up')}}
                                                    {{Form::close()}}
                                                    {{Form::open(['url' => route('steps.move',$step), 'method' => 'post','class'=>'moveform-down-'.$step->id])}}
                                                    {{Form::hidden('direction','down')}}
                                                    {{Form::close()}}
                                                </td>
                                                <td class="last">
                                                    <a href="{{route('steps.edit',$step)}}" class="btn btn-success btn-sm">Edit</a>
                                                    <a href="#" onclick="document.querySelector('.deleteform-{{$step->id}}').submit()" class="btn btn-danger btn-sm">Delete</a>
                                                    {{Form::open(['url' => route('steps.destroy',$step), 'method' => 'delete','class'=>'deleteform-'.$step->id])}}
                                                    {{Form::close()}}
                                                </td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="100%" class="text-center">
                                                    <small class="text-muted">No Steps created yet.</small>
                                                </td>
                                            </tr>
                                        @endforelse
                                        <tr>
                                            <td class="text-center" colspan="100%">
                                                <a href="{{route('steps.create',$process)}}" class="btn btn-sm btn-success">
                                                    Add
                                                </a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </dd>
                        </ul>
                    </div>

            </div>
        </div>

        <div class="col-lg-3">
            <div class="card">
                <div class="card-header">
                    Actions
                </div>
                <div class="card-body">
                    @if(auth()->user()->can('admin'))
                        <a href="{{route('processes.edit',$process)}}" class="btn btn-sm btn-block btn-outline-primary"><i class="fa fa-pencil"></i> Edit</a>

                        <br>

                        {{Form::open(['url' => route('processes.destroy',['process' => $process,'processid' => $process]).'?t='.$process_type_id, 'method' => 'delete'])}}
                        <button type="submit" class="btn btn-block btn-sm btn-outline-danger"><i class="fa fa-trash"></i> Delete</button>
                        {{Form::close()}}
                    @else
                        <button type="button" class="btn btn-block btn-sm btn-outline-primary disabled" disabled title="You do not have permission to do that"><i class="fa fa-pencil"></i> Edit</button>

                        <br>

                        <button type="button" class="btn btn-block btn-sm btn-outline-danger disabled" disabled title="You do not have permission to do that"><i class="fa fa-trash"></i> Delete</button>
                    @endif
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection