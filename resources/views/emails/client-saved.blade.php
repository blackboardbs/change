@component('mail::message')
# Introduction

The client saved.

@component('mail::button', ['url' => route('clients.show', [$client,$process_id,$step_id])])
Viev Client
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
