<?php

namespace App\Mail;

use App\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ClientBasketEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $client;
    public $process_id;
    public $step_id;
    public $password;
    public function __construct(Client $client,$process_id,$step_id,$password)
    {
        $this->client = $client;
        $this->process_id = $process_id;
        $this->step_id = $step_id;
        $this->password = $password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Please complete')->view('emails.clientbasket')->with(['clientid'=>$this->client->id,'process_id'=>$this->process_id,'step_id'=>$this->step_id,'password'=>$this->password]);
    }
}
