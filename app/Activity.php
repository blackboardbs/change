<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Activity extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function actionable()
    {
        return $this->morphTo();
    }

    public function related_party()
    {
        return $this->morphTo();
    }

    public function getTypeName()
    {
        //activity type hook
        switch ($this->actionable_type) {
            case 'App\ActionableText':
                return 'text';
                break;
            case 'App\ActionableTextarea':
                return 'textarea';
                break;
            case 'App\ActionableTemplateEmail':
                return 'template_email';
                break;
            case 'App\ActionableDocumentEmail':
                return 'document_email';
                break;
            case 'App\ActionableDocument':
                return 'document';
                break;
            case 'App\ActionableDropdown':
                return 'dropdown';
                break;
            case 'App\ActionableDate':
                return 'date';
                break;
            case 'App\ActionableBoolean':
                return 'boolean';
                break;
            case 'App\ActionableNotification':
                return 'notification';
                break;
            case 'App\ActionableMultipleAttachment':
                return 'multiple_attachment';
                break;
            default:
                return 'error';
                break;
        }
    }

    public function getRelatedPartyTypeName()
    {
        //activity type hook
        switch ($this->actionable_type) {
            case 'App\RelatedPartyText':
                return 'text';
                break;
            case 'App\RelatedPartyTextarea':
                return 'textarea';
                break;
            case 'App\RelatedPartyTemplateEmail':
                return 'template_email';
                break;
            case 'App\RelatedPartyDocumentEmail':
                return 'document_email';
                break;
            case 'App\RelatedPartyDocument':
                return 'document';
                break;
            case 'App\RelatedPartyDropdown':
                return 'dropdown';
                break;
            case 'App\RelatedPartyDate':
                return 'date';
                break;
            case 'App\RelatedPartyBoolean':
                return 'boolean';
                break;
            case 'App\RelatedPartyNotification':
                return 'notification';
                break;
            case 'App\RelatedPartyMultipleAttachment':
                return 'multiple_attachment';
                break;
            default:
                return 'error';
                break;
        }
    }

    public function getTypeDisplayName()
    {
        //activity type hook
        switch ($this->actionable_type) {
            case 'App\ActionableText':
                return 'Free text';
                break;
            case 'App\ActionableTextarea':
                return 'Free textarea';
                break;
            case 'App\ActionableTemplateEmail':
                return 'Letter Email';
                break;
            case 'App\ActionableDocumentEmail':
                return 'Document Email';
                break;
            case 'App\ActionableDocument':
                return 'Document';
                break;
            case 'App\ActionableBoolean':
            case 'App\ActionableDropdown':
                return 'Dropdown';
                break;
            case 'App\ActionableDate':
                return 'Date';
                break;
            case 'App\ActionableNotification':
                return 'Notification';
                break;
            case 'App\ActionableMultipleAttachment':
                return 'Multiple Attachment';
                break;
            default:
                return 'error';
                break;
        }
    }

    public function getRelatedPartyTypeDisplayName()
    {
        //activity type hook
        switch ($this->actionable_type) {
            case 'App\RelatedPartyText':
                return 'Free text';
                break;
            case 'App\RelatedPartyTextarea':
                return 'Free textarea';
                break;
            case 'App\RelatedPartyTemplateEmail':
                return 'Letter Email';
                break;
            case 'App\RelatedPartyDocumentEmail':
                return 'Document Email';
                break;
            case 'App\RelatedPartyDocument':
                return 'Document';
                break;
            case 'App\RelatedPartyBoolean':
            case 'App\RelatedPartyDropdown':
                return 'Dropdown';
                break;
            case 'App\RelatedPartyDate':
                return 'Date';
                break;
            case 'App\RelatedPartyNotification':
                return 'Notification';
                break;
            case 'App\RelatedPartyMultipleAttachment':
                return 'Multiple Attachment';
                break;
            default:
                return 'error';
                break;
        }
    }

    public function dependant(){
        return $this->belongsTo(Activity::class,'dependant_activity_id');
    }

    public function steps(){
        return $this->hasOne(Step::class,'process_id');
    }

    public function comments()
    {
        return $this->hasMany('App\ActivityComment', 'activity_id');
    }
}