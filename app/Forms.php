<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Forms extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function sections()
    {
        return $this->hasMany('App\FormSection', 'form_id')->orderBy('order');
    }

    public function getClientDetailsInputs($form_id)
    {
        $sections = FormSection::with('form_section_inputs')->where('form_id',$form_id)->get();

        //dd($sections);
        $client_detail = [];

        foreach ($sections as $section) {
            $process_progress = [];

            $section_array = [
                'id' => $section->id,
                'name' => $section->name,
                'order' => $section->order,
                'stage' => 0,
                'inputs' => []
            ];

            array_push($process_progress,$section_array);

            foreach ($section->form_section_inputs as $input) {

                $input_array = [
                    'id' => $input->id,
                    'name' => $input->name,
                    'order' => $input->order,
                    'type' => $input->getFormTypeName(),
                    'type_display' => $input->getFormTypeDisplayName(),
                    'stage' => 0,
                    'due_date' => 0,
                    'dependant_activity_id' => $input->dependant_activity_id,
                    'tooltip' => $input->tooltip
                ];

                if ($input_array['type'] == 'dropdown') {
                    $input_array['dropdown_items'] = $input->input->items->pluck('name', 'id')->toArray();
                    $input_array['dropdown_values'] = [];
                }

                $process_progress[0]['inputs'][$input->id] = $input_array;
            }

            $client_detail[$section->id][$section->name] =$process_progress;
        }

        return $client_detail;
    }

    public function getClientDetailsInputValues($client_id,$form_id)
    {
        $sections = FormSection::with(['form_section_inputs.input.data' => function ($query) use ($client_id) {
            $query->where('client_id', $client_id);
        }])->where('form_id',$form_id)->get();

        //dd($sections);
        $client_detail = [];

        foreach ($sections as $section) {
            $process_progress = [];

            $section_array = [
                'id' => $section->id,
                'name' => $section->name,
                'order' => $section->order,
                'stage' => 0,
                'inputs' => []
            ];

            array_push($process_progress,$section_array);

            foreach ($section->form_section_inputs as $input) {

                $input_array = [
                    'id' => $input->id,
                    'name' => $input->name,
                    'order' => $input->order,
                    'type' => $input->getFormTypeName(),
                    'type_display' => $input->getFormTypeDisplayName(),
                    'stage' => 0,
                    'due_date' => 0,
                    'dependant_activity_id' => $input->dependant_activity_id,
                    'tooltip' => $input->tooltip
                ];

                if ($input_array['type'] == 'dropdown') {

                    $input_array['dropdown_items'] = $input->input->items->pluck('name', 'id')->toArray();
                    $input_array['dropdown_values'] = $input->input->valuess->where('client_id',$client_id)->pluck('form_input_dropdown_item_id', 'id')->toArray();
                    //dd($activity_array);
                }

                if (isset($input->input['data'][0])) {
                   // dd($input->input['data']);
                    $data_index = count($input->input['data']) -1;
//                dd($activity->input['data'][$data_index]->data);
                    switch ($input_array['type']) {
                        //get last not zero
                        case 'boolean':
                            $input_array['value'] = $input->input['data'][$data_index]->data;
                            $input_array['crdate'] = Carbon::parse($input->input['data'][$data_index]->created_at)->format('Y-m-d');
                            break;
                        case 'date':
                            $input_array['value'] = $input->input['data'][$data_index]->data;
                            $input_array['crdate'] = Carbon::parse($input->input['data'][$data_index]->created_at)->format('Y-m-d');
                            break;
                        case 'text':
                            $input_array['value'] = $input->input['data'][$data_index]->data;
                            $input_array['crdate'] = Carbon::parse($input->input  ['data'][$data_index]->created_at)->format('Y-m-d');
                            break;
                        case 'textarea':
                            $input_array['value'] = $input->input['data'][$data_index]->data;
                            $input_array['crdate'] = Carbon::parse($input->input  ['data'][$data_index]->created_at)->format('Y-m-d');
                            break;
                        case 'dropdown':
                            $input_array['value'] = $input->input['data'][$data_index]->form_input_dropdown_item_id;
                            $input_array['crdate'] = Carbon::parse($input->input['data'][$data_index]->created_at)->format('Y-m-d');
                            break;
                    }
                }
                $process_progress[0]['inputs'][$input->id] = $input_array;
            }

            $client_detail[$section->id][$section->name] =$process_progress;
        }
//dd($client_detail);
        return $client_detail;
    }
}
