<?php

namespace App\Http\Controllers;

use App\ActionableBooleanData;
use App\ActionableDateData;
use App\ActionableDocumentData;
use App\ActionableDropdownData;
use App\ActionableTextareaData;
use App\ActionableTextData;
use App\ActionsAssigned;
use App\Activity;
use App\ActivityInClientBasket;
use App\ActivityLog;
use App\Client;
use App\ClientVerification;
use App\Config;
use App\Document;
use App\EmailTemplate;
use App\HelperFunction;
use App\Log;
use App\Mail\AssignedConsultantNotify;
use App\Mail\ClientBasketEmail;
use App\Mail\ClientSaved;
use App\Step;
use App\User;
use App\Template;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Process;


class ClientBasketController extends Controller
{
    public function show(Request $request, $client_id,$process_id,$step_id)
    {
        $client = Client::find($client_id);

        $process = Process::find($process_id);

        //$client_process = ClientProcess::where('client_id',$client->id)->where('process_id',$process->id)->first();

        $step = Step::find($step_id);

        $activity_step = $step;

        $action_id = '';

        $parameters = app('App\Http\Controllers\ClientController')->clientProcessProgress($client,$process,$step,$request,$activity_step,$action_id);

        return view('clients.client-basket')->with($parameters);
    }

    public function includeActivityInClientBasket(Request $request)
    {
        $activity = ActivityInClientBasket::where('client_id', $request->client_id)->where('activity_id', $request->activity_id)->first()?? new ActivityInClientBasket();
        $activity->client_id = $request->client_id;
        $activity->activity_id = $request->activity_id;
        $activity->process_id = $request->process_id;
        $activity->in_client_basket = $request->status;
        $activity->save();

        $act = Activity::find($request->activity_id, ['name']);
        if($request->has('all') && $request->input('all') == 1) {
            $message = $request->status ? 'All activities were successfully included in the basket' : 'All activities were successfully removed from the basket';
        } else {
            $message = $request->status ? $act->name . ' included in the basket successfully' : $act->name . ' removed from the basket successfully';
        }
        return response()->json(['success'=> $message]);
    }

    public function clientProgress(Request $request,$client_id, $process_id, $step_id)
    {

        $is_valid = 0;

        $email = ($request->has('email') ? $request->input('email') : null);
        $client = ($request->has('client') ? $request->input('client') : null);
        $password= ($request->has('pwd') ? $request->input('pwd') : null);

        $client_verification = ClientVerification::where('login_ip_address',request()->ip())->where('last_session',Session::getId())->where('client_id',$client_id)->first();

        if(!$client_verification){
            $is_valid = 0;

            $verification = ClientVerification::where('client_id',$client)->where('email',$email)->where('password',$password)->first();

            if(!$verification){
                $is_valid = 0;
            } else {

                $v = ClientVerification::find($verification->id);
                $v->last_session = Session::getId();
                $v->login_ip_address = request()->ip();
                $v->save();

                $is_valid = 1;
            }
        } else {
            $is_valid = 1;
        }
        //$client_verification = ClientVerification::where('client_id',$client)->where('email',$email)->where('password',$password)->get();

        if($request->session()->get('path_route') != null) {

            $path = '1';
            $path_route = $request->session()->get('path_route');
        } else {
            $request->session()->forget('path_route');
            $path = '0';
            $path_route = '';
        }

        //dd($step["activities"]);
        $client = Client::withTrashed()->find($client_id);

        $client->with('process.office.area.region.division');

        $step = Step::find($client->step_id);
        $client_progress = $client->process->getStageHex(0);

        if($client->step_id == $step->id)
            $client_progress = $client->process->getStageHex(1);

        if($client->step_id > $step->id)
            $client_progress = $client->process->getStageHex(2);

        $steps = Step::where('process_id', $client->process_id)->orderBy('order','asc')->get();
        $c_step_order = Step::where('id',$client->step_id)->withTrashed()->first();
//dd($step["activities"]);
        $step_data = [];
        foreach ($steps as $a_step):
            $progress_color = $client->process->getStageHex(0);
            $step_stage = 0;

            if($c_step_order->order == $a_step->order) {
                $progress_color = $client->process->getStageHex(1);
                $step_stage = 1;
            }


            if($c_step_order->order > $a_step->order) {
                $progress_color = $client->process->getStageHex(2);
                $step_stage = 2;
            }


            $tmp_step = [
                'id' => $a_step->id,
                'name' => $a_step->name,
                'process_id' => $a_step->process_id,
                'progress_color' => $progress_color,
                'order' => $a_step->order,
                'stage' => $step_stage
            ];

            array_push($step_data, $tmp_step);

        endforeach;


        $max_step = Step::orderBy('order','desc')->where('process_id', $client->process_id)->first();


        $n_step = Step::select('id')->orderBy('order','asc')->where('process_id', $client->process_id)->where('order','>',$step->order)->whereNull('deleted_at')->first();

        $next_step = $step->id;

        if($next_step == $max_step->id)
            $next_step = $max_step->id;
        else
            $next_step = (isset($n_step->id) ? $n_step->id : $step->id);
        $template_email_options = EmailTemplate::orderBy('name')->pluck('name', 'id');

        $activity_comment = array();

        $in_basket = ActivityInClientBasket::select('activity_id')->where('client_id',$client_id)->where('process_id',$process_id)->where('in_client_basket',1)->get();

        $activities = $client->getClientBasketActivities($process_id);

        $helper = new HelperFunction();
        $cb = $helper->clientBucketActivityIds($steps, $client, $client->process_id);

        $steps_names_in_basket = $steps->keyBy('name')->filter(function ($step) use ($process_id){ return $step->process_id == $process_id; })->map(function ($step) use ($cb){
            return $step->activities->filter(function ($activity) use ($cb){
                return in_array($activity->id, $cb);
            });
        })->filter(function ($step){
            return count($step) > 0;
        });

        $parameters = [
            'config'=> Config::first(),
            'activities' => $activities,
            'clientid' => $client->id,
            'client' => $client,
            'activity_comment' => $activity_comment,
            'step' => $step,
            'active' => $step,
            'max_step' => $max_step->id,
            'next_step' => $next_step,
            'process_progress' => $client->getProcessStepProgress($step),
            'view_process_dropdown' => $client->clientProcessIfActivitiesExist(),
            'steps' => $step_data,
            'users' => User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id'),
            'documents' => Document::orderBy('name')->where('client_id', $client->id)->orWhere('client_id', null)->pluck('name', 'id'),
            'document_options' => Document::orderBy('name')->where('client_id', $client->id)->orWhere('client_id', null)->pluck('name', 'id'),
            'templates' => Template::where('template_type_id','2')->orderBy('name')->pluck('name', 'id'),
            'client_progress' => $client_progress,
            'template_email_options' => $template_email_options,
            'path' => $path,
            'path_route' => $path_route,
            'qa_complete' => '',
            'client_basket_activities'=>$cb,
            'process_id' => $process_id,
            'in_basket' => collect($in_basket)->toArray(),
            'is_valid' => $is_valid,
            'steps_names_in_basket' => $steps_names_in_basket->keys()->toArray()
        ];

        return view('clients.clientactivity')->with($parameters);
    }

    public function clientStoreProgress(Client $client,Request $request,$process_id,$step_id){
        if($request->has('step_id') && $request->input('step_id') != ''){
            $log = new Log;
            $log->client_id = $client->id;
            $log->user_id = 0;
            $log->save();

            $id = $client->id;
            $steps = Step::with(['activities.actionable.data' => function ($query) use ($id) {
                $query->where('client_id', $id);
            }])->get();

            foreach ($steps as $step) {
                $all_activities_completed = false;
                foreach ($step->activities as $activity) {

                    if (is_null($request->input($activity->id))) {
                        if ($request->input('old_' . $activity->id) != $request->input($activity->id)) {

                            if (is_array($request->input($activity->id))) {

                                $old = explode(',', $request->input('old_' . $activity->id));
                                $diff = array_diff($old, $request->input($activity->id));
                                //dd($diff);

                                foreach ($request->input($activity->id) as $key => $value) {
                                    $activity_log = new ActivityLog;
                                    $activity_log->log_id = $log->id;
                                    $activity_log->activity_id = $activity->id;
                                    $activity_log->activity_name = $activity->name;
                                    $activity_log->old_value = $request->input('old_' . $activity->id);
                                    $activity_log->new_value = $value;
                                    $activity_log->save();
                                }
                            } else {
                                $old = $request->input('old_' . $activity->id);

                                $activity_log = new ActivityLog;
                                $activity_log->log_id = $log->id;
                                $activity_log->activity_id = $activity->id;
                                $activity_log->activity_name = $activity->name;
                                $activity_log->old_value = $request->input('old_' . $activity->id);
                                $activity_log->new_value = $request->input($activity->id);
                                $activity_log->save();
                            }

                            switch ($activity->actionable_type) {
                                case 'App\ActionableBoolean':
                                    ActionableBooleanData::where('actionable_boolean_id', $activity->actionable_id)->where('client_id', $client->id)->delete();

                                    break;
                                case 'App\ActionableDate':
                                    ActionableDateData::where('actionable_date_id', $activity->actionable_id)->where('client_id', $client->id)->delete();

                                    break;
                                case 'App\ActionableText':

                                    ActionableTextData::where('actionable_text_id', $activity->actionable_id)->where('client_id', $client->id)->delete();

                                    break;
                                case 'App\ActionableTextarea':
                                    ActionableTextareaData::where('actionable_textarea_id', $activity->actionable_id)->where('client_id', $client->id)->delete();

                                    break;
                                case 'App\ActionableDropdown':
                                    ActionableDropdownData::where('actionable_dropdown_id', $activity->actionable_id)->where('client_id', $client->id)->delete();

                                    break;
                                default:
                                    //todo capture defaults
                                    break;
                            }
                        }
                    }

                    if ($request->has($activity->id) && !is_null($request->input($activity->id))) {
                        //If value did not change, do not save it again or add it to log
                        if ($request->input('old_' . $activity->id) == $request->input($activity->id)) {
                            continue;
                        }
                        if (is_array($request->input($activity->id))) {

                            $old = explode(',', $request->input('old_' . $activity->id));
                            $diff = array_diff($old, $request->input($activity->id));
                            //dd($diff);

                            foreach ($request->input($activity->id) as $key => $value) {
                                $activity_log = new ActivityLog;
                                $activity_log->log_id = $log->id;
                                $activity_log->activity_id = $activity->id;
                                $activity_log->activity_name = $activity->name;
                                $activity_log->old_value = $request->input('old_' . $activity->id);
                                $activity_log->new_value = $value;
                                $activity_log->save();
                            }
                        } else {
                            $old = $request->input('old_' . $activity->id);

                            $activity_log = new ActivityLog;
                            $activity_log->log_id = $log->id;
                            $activity_log->activity_id = $activity->id;
                            $activity_log->activity_name = $activity->name;
                            $activity_log->old_value = $request->input('old_' . $activity->id);
                            $activity_log->new_value = $request->input($activity->id);
                            $activity_log->save();
                        }

                        switch ($activity->actionable_type) {
                            case 'App\ActionableBoolean':
                                ActionableBooleanData::where('client_id', $client->id)->where('actionable_boolean_id', $activity->actionable_id)->where('data', $old)->delete();

                                ActionableBooleanData::insert([
                                    'data' => $request->input($activity->id),
                                    'actionable_boolean_id' => $activity->actionable_id,
                                    'client_id' => $client->id,
                                    'user_id' => auth()->id(),
                                    'duration' => 120,
                                    'created_at' => now()
                                ]);
                                break;
                            case 'App\ActionableDate':
                                ActionableDateData::where('client_id', $client->id)->where('actionable_date_id', $activity->actionable_id)->where('data', $old)->delete();

                                ActionableDateData::insert([
                                    'data' => $request->input($activity->id),
                                    'actionable_date_id' => $activity->actionable_id,
                                    'client_id' => $client->id,
                                    'user_id' => auth()->id(),
                                    'duration' => 120,
                                    'created_at' => now()
                                ]);
                                break;
                            case 'App\ActionableText':
                                ActionableTextData::where('client_id', $client->id)->where('actionable_text_id', $activity->actionable_id)->where('data', $old)->delete();

                                ActionableTextData::insert([
                                    'data' => $request->input($activity->id),
                                    'actionable_text_id' => $activity->actionable_id,
                                    'client_id' => $client->id,
                                    'user_id' => auth()->id(),
                                    'duration' => 120,
                                    'created_at' => now()
                                ]);
                                break;
                            case 'App\ActionableTextarea':
                                ActionableTextareaData::where('client_id', $client->id)->where('actionable_textarea_id', $activity->actionable_id)->where('data', $old)->delete();

                                $replace1 = str_replace('&nbsp;', ' ', $request->input($activity->id));
                                $replace2 = str_replace('&ndash;', '-', $replace1);
                                $replace3 = str_replace('&bull;', '- ', $replace2);
                                $replace4 = str_replace('&ldquo;', '"', $replace3);
                                $replace5 = str_replace('&rdquo;', '"', $replace4);
                                $replace6 = str_replace("&rsquo;", "'", $replace5);
                                $replace7 = str_replace("&lsquo;", "'", $replace6);

                                $data = $replace7;

                                ActionableTextareaData::insert([
                                    'data' => $data,
                                    'actionable_textarea_id' => $activity->actionable_id,
                                    'client_id' => $client->id,
                                    'user_id' => auth()->id(),
                                    'duration' => 120,
                                    'created_at' => now()
                                ]);
                                break;
                            case 'App\ActionableDropdown':
                                foreach ($request->input($activity->id) as $key => $value) {
                                    if (in_array($value, $old, true)) {

                                    } else {
                                        ActionableDropdownData::insert([
                                            'actionable_dropdown_id' => $activity->actionable_id,
                                            'actionable_dropdown_item_id' => $value,
                                            'client_id' => $client->id,
                                            'user_id' => auth()->id(),
                                            'duration' => 120,
                                            'created_at' => now()
                                        ]);
                                    }

                                    if (!empty($diff)) {
                                        ActionableDropdownData::where('client_id', $client->id)->where('actionable_dropdown_id', $activity->actionable_id)->whereIn('actionable_dropdown_item_id', $diff)->delete();
                                    }


                                }
                                break;
                            default:
                                //todo capture defaults
                                break;
                        }

                    }
                }
            }
            //Handle files
            foreach($request->files as $key => $file):
                $file_activity = Activity::find($key);
                switch($file_activity->actionable_type){
                    case 'App\ActionableDocument':
                        $afile = $request->file($key);
                        $name = Carbon::now()->format('Y-m-d')."-".strtotime(Carbon::now()).".".$afile->getClientOriginalExtension();
                        $stored = $afile->storeAs('documents', $name);

                        $document = new Document;
                        $document->name = $file_activity->name;
                        $document->file = $name;
                        $document->user_id = 0;
                        $document->client_id = $client->id;
                        $document->save();

                        ActionableDocumentData::insert([
                            'actionable_document_id' => $file_activity->actionable_id,
                            'document_id' => $document->id,
                            'client_id' => $client->id,
                            'user_id' => auth()->id(),
                            'duration' => 120
                        ]);
                        break;
                    default:
                        //todo capture detaults
                        break;
                }

            endforeach;

        }

        Mail::to(array_values(array_filter([$client->consultant->email??null, $client->introducer->email??null])))
            ->send(new ClientSaved($client,$process_id,$step_id));
        return redirect()->back()->with(['flash_success' => "Details successfully captured."]);
    }

    public function sendClientEmail(Request $request){

        $client = Client::where('id',$request->input('client_id'))->first();



        $err = 0;

        $process_id = $request->input('process_id');
        $step_id = $request->input('step_id');

        foreach ($request->input('emails') as $email) {
            if($email) {
                $password = $this->generateRandomString(8);

                $cv = new ClientVerification();
                $cv->client_id = $client->id;
                $cv->email = $email;
                $cv->password = $password;
                $cv->save();


                Mail::to(trim($email))->send(new ClientBasketEmail($client,$process_id,$step_id,$password));
                if (Mail::failures()) {
                    $err++;
                }
            }
        }


        // check for failures
        if ($err != 0) {
            return response()->json(['message' => 'Error']);
        }

            return response()->json(['message' => 'Success']);

    }

    function generateRandomString($length = 8) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
