<?php

namespace App\Http\Controllers;

use App\ActionableBoolean;
use App\ActionableDate;
use App\ActionableDocument;
use App\ActionableDocumentEmail;
use App\ActionableDropdown;
use App\ActionableDropdownItem;
use App\ActionableMultipleAttachment;
use App\ActionableNotification;
use App\ActionableTemplateEmail;
use App\ActionableText;
use App\ActionableTextarea;
use App\RelatedPartyText;
use App\RelatedPartyTextarea;
use App\RelatedPartyDropdown;
use App\RelatedPartyBoolean;
use App\RelatedPartyDate;
use App\RelatedPartyDropdownItem;
use App\RelatedPartyDocument;
use App\RelatedPartyDocumentEmail;
use App\RelatedPartyMultipleAttachment;
use App\RelatedPartyNotification;
use App\RelatedPartyTemplateEmail;
use App\Activity;
use App\Process;
use App\Role;
use App\Step;
use App\Http\Requests\StoreStepRequest;
use App\Http\Requests\UpdateStepRequest;
use Illuminate\Http\Request;

class StepController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create(Process $process)
    {
        return view('steps.create')->with(['process' => $process->load('office.area.region.division')]);
    }

    public function store(Process $process, StoreStepRequest $request)
    {
        $step = new Step;
        $step->name = $request->input('name');
        $step->colour = $request->input('step_colour');
        $step->process_id = $process->id;
        $step->order = Step::where('process_id', $process->id)->max('order') + 1;
        $step->group = ($request->has('group_step') && $request->input('group_step') == 'on' ? 1 : 0);
        $step->save();

        $step_id = $step->id;

        //loop over each activity input
        foreach ($request->input('activities') as $activity_key => $activity_input) {
            $activity = new Activity();

            if($process->process_type_id == 2){
                $actionable = $this->createRelatedParty($activity_input['type']);

                $activity->name = $activity_input['name'];
                $activity->order = $activity_key + 1;
                $activity->actionable_id = $actionable->id;
                $activity->actionable_type = $this->getRelatedPartyType($activity_input['type']);
                $activity->kpi = (isset($activity_input['kpi']) && $activity_input['kpi'] == "on") ? 1 : null;
                $activity->comment = (isset($activity_input['comment']) && $activity_input['comment'] == "on") ? 1 : null;
                $activity->value = (isset($activity_input['value']) && $activity_input['value'] == "on") ? 1 : null;
                $activity->procedure= (isset($activity_input['procedure']) && $activity_input['procedure'] == "on") ? 1 : null;
                $activity->client_activity = (isset($activity_input['client']) && $activity_input['client'] == "on") ? 1 : null;
                $activity->step_id = $step->id;
                $activity->threshold = (isset($activity_input['threshold']) ? $this->getThresholdAsSeconds($activity_input['threshold_time'], $activity_input['threshold_type']) : '');
                $activity->weight = (isset($activity_input['weight']) ? $activity_input['weight'] : 0);
                $activity->tooltip = $activity_input['tooltip'];
                $activity->show_tooltip = (isset($activity_input['show_tooltip']) && $activity_input['show_tooltip'] == "on") ? 1 : 0;
                //todo add dependant activities
                $activity->user_id = (!isset($activity_input['user']) || $activity_input['user'] == 0) ? null : $activity_input['user'];
                $activity->grouped = (isset($activity_input['grouping']) && $activity_input['grouping'] == "on") ? 1 : 0;
                $activity->grouping = (isset($activity_input['grouping_value'])) ? $activity_input['grouping_value'] : null;
                $activity->report = (isset($activity_input['report']) && $activity_input['report'] == "on") ? 1 : 0;
                $activity->client_bucket = (isset($activity_input['client_bucket']) && $activity_input['client_bucket'] == "on") ? 1 : 0;
                $activity->position = (isset($activity_input['position']) ? $activity_input['position'] : null);
                $activity->save();

                //if activity is a dropdown type
                if ($activity_input['type'] == 'dropdown') {

                    //only add dropdown items if there is input
                    if (isset($activity_input['dropdown_items'])) {

                        //loop over each dropdown item
                        foreach ($activity_input['dropdown_items'] as $dropdown_item) {
                            $actionable_dropdown_item = new RelatedPartyDropdownItem;
                            $actionable_dropdown_item->related_party_dropdown_id = $actionable->id;
                            $actionable_dropdown_item->name = $dropdown_item;
                            $actionable_dropdown_item->save();
                        }
                    }
                }
            } else {
                $actionable = $this->createActionable($activity_input['type']);

                $activity->name = $activity_input['name'];
                $activity->order = $activity_key + 1;
                $activity->actionable_id = $actionable->id;
                $activity->actionable_type = $this->getActionableType($activity_input['type']);
                $activity->kpi = (isset($activity_input['kpi']) && $activity_input['kpi'] == "on") ? 1 : null;
                $activity->comment = (isset($activity_input['comment']) && $activity_input['comment'] == "on") ? 1 : null;
                $activity->value = (isset($activity_input['value']) && $activity_input['value'] == "on") ? 1 : null;
                $activity->procedure= (isset($activity_input['procedure']) && $activity_input['procedure'] == "on") ? 1 : null;
                $activity->client_activity = (isset($activity_input['client']) && $activity_input['client'] == "on") ? 1 : null;
                $activity->step_id = $step_id;
                $activity->threshold = (isset($activity_input['threshold']) ? $this->getThresholdAsSeconds($activity_input['threshold_time'], $activity_input['threshold_type']) : '');
                $activity->weight = (isset($activity_input['weight']) ? $activity_input['weight'] : 0);
                $activity->tooltip = $activity_input['tooltip'];
                $activity->show_tooltip = (isset($activity_input['show_tooltip']) && $activity_input['show_tooltip'] == "on") ? 1 : 0;
                //todo add dependant activities
                $activity->user_id = (!isset($activity_input['user']) || $activity_input['user'] == 0) ? null : $activity_input['user'];
                $activity->grouped = (isset($activity_input['grouping']) && $activity_input['grouping'] == "on") ? 1 : 0;
                $activity->grouping = (isset($activity_input['grouping_value'])) ? $activity_input['grouping_value'] : null;
                $activity->report = (isset($activity_input['report']) && $activity_input['report'] == "on") ? 1 : 0;
                $activity->client_bucket = (isset($activity_input['client_bucket']) && $activity_input['client_bucket'] == "on") ? 1 : 0;
                $activity->position = (isset($activity_input['position']) ? $activity_input['position'] : null);
                $activity->save();

                //if activity is a dropdown type
                if ($activity_input['type'] == 'dropdown') {

                    //only add dropdown items if there is input
                    if (isset($activity_input['dropdown_items'])) {

                        //loop over each dropdown item
                        foreach ($activity_input['dropdown_items'] as $dropdown_item) {
                            $actionable_dropdown_item = new ActionableDropdownItem;
                            $actionable_dropdown_item->actionable_dropdown_id = $actionable->id;
                            $actionable_dropdown_item->name = $dropdown_item;
                            $actionable_dropdown_item->save();
                        }
                    }
                }
            }
        }
        return redirect(route('processes.show', $process))->with('flash_success', 'Process updated successfully.');
    }

    public function edit(Step $step)
    {
        $step->load('process.office.area.region.division', 'activities');

        $activities_array = [];
        foreach ($step->activities as $activity) {
            $threshold = [
                'time' => $activity->threshold,
                'type' => 'seconds'
            ];

            if ($activity->threshold > 60) {
                $threshold = [
                    'time' => $activity->threshold / 60,
                    'type' => 'minutes'
                ];
            }

            if ($activity->threshold > 60 * 60) {
                $threshold = [
                    'time' => $activity->threshold / (60 * 60),
                    'type' => 'hours'
                ];
            }

            if ($activity->threshold > 60 * 60 * 24) {
                $threshold = [
                    'time' => $activity->threshold / (60 * 60 * 24),
                    'type' => 'days'
                ];
            }
//dd($step);
            if($step->process->process_type_id == 1) {
                $activity_array = [
                    'id' => $activity->id,
                    'name' => $activity->name,
                    'type' => $activity->getTypeName(),
                    'client' => $activity->client_activity,
                    'kpi' => ($activity->kpi ==1 ? true : false),
                    'comment' => $activity->comment,
                    'value' => $activity->value,
                    'procedure' => $activity->procedure,
                    'is_dropdown_items_shown' => false,
                    'tooltip' => $activity->tooltip,
                    'show_tooltip' => ($activity->show_tooltip == 1 ? true : false),
                    'is_tooltip_shown' => ($step["tooltip"] != null ? true : false),
                    'is_grouping_items_shown' => ($step["group"] == "1" ? true : false),
                    'is_default_value_shown' => ($activity->default_value != null ? true : false),
                    'grouping_value' => ($activity->grouping != null ? $activity->grouping : 0),
                    'use_default_value' => ($activity->default_value != null ? 1 : ''),
                    'default_value' => ($activity->default_value != null ? $activity->default_value : ''),
                    'weight' => $activity->weight,
                    'dropdown_item' => '',
                    'dropdown_items' => [],
                    'threshold' => $threshold,
                    'user' => $activity->user_id ?? 0,
                    'report' => ($activity->report == "1" ? true : false),
                    'client_bucket' => ($activity->client_bucket == "1" ? true : false),
                    'position' => $activity->position
                ];

                if ($activity->getTypeName() == 'dropdown') {
                    $activity_array['dropdown_items'] = $activity->actionable->items->pluck('name')->toArray();
                }
            } else {
                $activity_array = [
                    'id' => $activity->id,
                    'name' => $activity->name,
                    'type' => $activity->getRelatedPartyTypeName(),
                    'client' => $activity->client_activity,
                    'kpi' => ($activity->kpi ==1 ? true : false),
                    'comment' => $activity->comment,
                    'value' => $activity->value,
                    'procedure' => $activity->procedure,
                    'tooltip' => $activity->tooltip,
                    'is_dropdown_items_shown' => false,
                    'show_tooltip' => ($activity->show_tooltip == 1 ? true : false),
                    'is_tooltip_shown' => ($step["show_tooltip"] != null ? true : false),
                    'is_grouping_items_shown' => ($step->group == "1" ? true : false),
                    'is_default_value_shown' => ($activity->default_value != null ? true : false),
                    'grouping_value' => ($activity->grouping != null ? $activity->grouping : 0),
                    'use_default_value' => ($activity->default_value != null ? 1 : ''),
                    'default_value' => ($activity->default_value != null ? $activity->default_value : ''),
                    'weight' => $activity->weight,
                    'dropdown_item' => '',
                    'dropdown_items' => [],
                    'threshold' => $threshold,
                    'user' => $activity->user_id ?? 0,
                    'report' => ($activity->report == "1" ? true : false),
                    'client_bucket' => ($activity->client_bucket == "1" ? true : false),
                ];

                if ($activity->getRelatedPartyTypeName() == 'dropdown') {
                    $activity_array['dropdown_items'] = $activity->actionable->items->pluck('name')->toArray();
                }
            }



            array_push($activities_array, $activity_array);
        }

//dd($activities_array);
        $paramaters = [
            'step' => $step,
            'process_id' => $step->process->id,
            'activities' => json_encode($activities_array),
            'roles' => Role::orderBy('name')->get()
        ];


        return view('steps.edit')->with($paramaters);
    }

    public function update(Step $step, UpdateStepRequest $request)
    {
//dd($request->input());
        $process = Process::where('id',$request->input('process_id'))->first()->process_type_id;
        $existing_step = Step::where('name',$step->name)->first();


        if($existing_step != null){
            $step_id = $existing_step->id;


            $step->name = $request->input('name');
            $step->colour = $request->input('step_colour');
            $step->group = ($request->has('group_step') && $request->input('group_step') == 'on' ? 1 : 0);
            $step->save();
        }
        //dd($request->input('activities'));
        $pactivities = array();
        if($request->input("activities") != null) {
            foreach ($request->input("activities") as $activities) {
                //dd($activities);
                array_push($pactivities, $activities["id"]);
            }
        }
        Activity::where('step_id',$step->id)->whereNotIn('id',$pactivities)->delete();


        //loop over each activity input
        if($request->input("activities") != null) {
            //check if Related Party activities
            if(isset($process) && $process == '2'){
                foreach ($request->input('activities') as $activity_key => $activity_input) {

                    $activity = $step->activities()->where('id', $activity_input['id'])->get()->first();
                    if(isset($activity_input['type'])) {
                        $activity_type = $step->activities()->where('id', $activity_input['id'])->where('actionable_type', $this->getActionableType($activity_input['type']))->get()->first();
                    } else {
                        $activity_type = $step->activities()->where('id', $activity_input['id'])->where('actionable_type', $this->getActionableType($activity_input['hidden_type']))->get()->first();
                    }

                    //if there is a previous activity matching the name and type, reactivate it else create a new one
                    if (!$activity) {
                        $new_activity = true;
                        if (!$activity_type) {
                            $new_activity_type = true;
                            $activity = new Activity();
                            $actionable = $this->createRelatedParty($activity_input['type']);
                        } else {
                            $new_activity_type = false;
                            $activity->restore();
                            $actionable = $activity->actionable;
                        }

                    } else {
                        $new_activity = false;
                        if (!$activity_type) {
                            $new_activity_type = true;
                            $activity = Activity::find($activity_input['id']);
                            $actionable = $this->createRelatedParty($activity_input['type']);
                        } else {
                            $new_activity_type = false;
                            $activity->restore();
                            $actionable = $activity->actionable;
                        }

                    }

                    $activity->name = $activity_input['name'];
                    $activity->order = $activity_key + 1;
                    $activity->actionable_id = (isset($actionable->id) ? $actionable->id : $actionable);
                    $activity->actionable_type = $this->getRelatedPartyType($activity_input['type']);
                    $activity->kpi = (isset($activity_input['kpi']) && $activity_input['kpi'] == "on") ? 1 : null;
                    $activity->comment = (isset($activity_input['comment']) && $activity_input['comment'] == "on") ? 1 : null;
                    $activity->value = (isset($activity_input['value']) && $activity_input['value'] == "on") ? 1 : null;
                    $activity->procedure = (isset($activity_input['procedure']) && $activity_input['procedure'] == "on") ? 1 : null;
                    $activity->client_activity = (isset($activity_input['client']) && $activity_input['client'] == "on") ? 1 : null;
                    $activity->step_id = $step->id;
                    $activity->threshold = (isset($activity_input['threshold']) ? $this->getThresholdAsSeconds($activity_input['threshold_time'], $activity_input['threshold_type']) : null);
                    $activity->weight = (isset($activity_input['weight']) ? $activity_input['weight'] : 0);
                    $activity->tooltip = $activity_input['tooltip'];
                    $activity->show_tooltip = (isset($activity_input['show_tooltip']) && $activity_input['show_tooltip'] == "on" ? 1 : 0);
                    //todo add dependant activities
                    $activity->user_id = (!isset($activity_input['user']) || $activity_input['user'] == 0) ? null : $activity_input['user'];
                    $activity->grouped = (isset($activity_input['grouping']) && $activity_input['grouping'] == "on") ? 1 : 0;
                    $activity->grouping = (isset($activity_input['grouping_value'])) ? $activity_input['grouping_value'] : null;
                    $activity->default_value = (isset($activity_input['default_value'])) ? $activity_input['default_value'] : null;
                    $activity->report = (isset($activity_input['report']) && $activity_input['report'] == "on") ? 1 : 0;
                    $activity->client_bucket = (isset($activity_input['client_bucket']) && $activity_input['client_bucket'] == "on") ? 1 : 0;
                    $activity->position = (isset($activity_input['position']) ? $activity_input['position'] : null);
                    $activity->save();


                    //if activity is a dropdown type
                    if ($activity_input['type'] == 'dropdown') {

                        //delete all previous dropdown items
                        RelatedPartyDropdownItem::where('related_party_dropdown_id', (isset($actionable->id) ? $actionable->id : $actionable))->delete();


                        //only add dropdown items if there is input
                        if (isset($activity_input['dropdown_items'])) {

                            //loop over each dropdown item
                            foreach ($activity_input['dropdown_items'] as $dropdown_item) {

                                //if this is a reactivated activity, search for all old dropdowns
                                if (!$new_activity_type) {

                                    //find if there already a dropdown item for that activity
                                    $item = $actionable->items()->withTrashed()->where('name', $dropdown_item)->get()->first();

                                    //if there is a previous dropdown item, reactivate it else create a new one
                                    if (!$item) {
                                        $actionable_dropdown_item = new RelatedPartyDropdownItem;
                                        $actionable_dropdown_item->related_party_dropdown_id = $actionable->id;
                                        $actionable_dropdown_item->name = $dropdown_item;
                                        $actionable_dropdown_item->save();
                                    } else {
                                        $item->restore();
                                    }

                                } // otherwise create a new dropdown item without searching
                                else {
                                    $actionable_dropdown_item = new RelatedPartyDropdownItem;
                                    $actionable_dropdown_item->related_party_dropdown_id = (isset($actionable->id) ? $actionable->id : $actionable);
                                    $actionable_dropdown_item->name = $dropdown_item;
                                    $actionable_dropdown_item->save();
                                }
                            }
                        }
                    }
                }
            } else {
                foreach ($request->input('activities') as $activity_key => $activity_input) {

                    $activity = $step->activities()->where('id', $activity_input['id'])->get()->first();
                    if(isset($activity_input['type'])) {
                        $activity_type = $step->activities()->where('id', $activity_input['id'])->where('actionable_type', $this->getActionableType($activity_input['type']))->get()->first();
                    } else {
                        $activity_type = $step->activities()->where('id', $activity_input['id'])->where('actionable_type', $this->getActionableType($activity_input['hidden_type']))->get()->first();
                    }

                    //if there is a previous activity matching the name and type, reactivate it else create a new one
                    if (!$activity) {
                        $new_activity = true;
                        if (!$activity_type) {
                            $new_activity_type = true;
                            $activity = new Activity();
                            $actionable = $this->createActionable($activity_input['type']);
                        } else {
                            $new_activity_type = false;
                            $activity->restore();
                            $actionable = $activity->actionable;
                        }

                    } else {
                        $new_activity = false;
                        if (!$activity_type) {
                            $new_activity_type = true;
                            $activity = Activity::find($activity_input['id']);
                            $actionable = $this->createActionable($activity_input['type']);
                        } else {
                            $new_activity_type = false;
                            $activity->restore();
                            $actionable = $activity->actionable;
                        }

                    }

                    $activity->name = $activity_input['name'];
                    $activity->order = $activity_key + 1;
                    $activity->actionable_id = (isset($actionable->id) ? $actionable->id : $actionable);
                    $activity->actionable_type = $this->getActionableType($activity_input['type']);
                    $activity->kpi = (isset($activity_input['kpi']) && $activity_input['kpi'] == "on") ? 1 : null;
                    $activity->comment = (isset($activity_input['comment']) && $activity_input['comment'] == "on") ? 1 : null;
                    $activity->value = (isset($activity_input['value']) && $activity_input['value'] == "on") ? 1 : null;
                    $activity->procedure = (isset($activity_input['procedure']) && $activity_input['procedure'] == "on") ? 1 : null;
                    $activity->client_activity = (isset($activity_input['client']) && $activity_input['client'] == "on") ? 1 : null;
                    $activity->step_id = $step->id;
                    $activity->threshold = (isset($activity_input['threshold']) ? $this->getThresholdAsSeconds($activity_input['threshold_time'], $activity_input['threshold_type']) : null);
                    $activity->weight = (isset($activity_input['weight']) ? $activity_input['weight'] : 0);
                    $activity->tooltip = $activity_input['tooltip'];
                    $activity->show_tooltip = (isset($activity_input['show_tooltip']) && $activity_input['show_tooltip'] == "on" ? 1 : 0);
                    //todo add dependant activities
                    $activity->user_id = (!isset($activity_input['user']) || $activity_input['user'] == 0) ? null : $activity_input['user'];
                    $activity->grouped = (isset($activity_input['grouping']) && $activity_input['grouping'] == "on") ? 1 : 0;
                    $activity->grouping = (isset($activity_input['grouping_value'])) ? $activity_input['grouping_value'] : null;
                    $activity->default_value = (isset($activity_input['default_value'])) ? $activity_input['default_value'] : null;
                    $activity->report = (isset($activity_input['report']) && $activity_input['report'] == "on") ? 1 : 0;
                    $activity->client_bucket = (isset($activity_input['client_bucket']) && $activity_input['client_bucket'] == "on") ? 1 : 0;
                    $activity->position = (isset($activity_input['position']) ? $activity_input['position'] : null);
                    $activity->save();


                    //if activity is a dropdown type
                    if ($activity_input['type'] == 'dropdown') {

                        //delete all previous dropdown items
                        ActionableDropdownItem::where('actionable_dropdown_id', (isset($actionable->id) ? $actionable->id : $actionable))->delete();


                        //only add dropdown items if there is input
                        if (isset($activity_input['dropdown_items'])) {

                            //loop over each dropdown item
                            foreach ($activity_input['dropdown_items'] as $dropdown_item) {

                                //if this is a reactivated activity, search for all old dropdowns
                                if (!$new_activity_type) {

                                    //find if there already a dropdown item for that activity
                                    $item = $actionable->items()->withTrashed()->where('name', $dropdown_item)->get()->first();

                                    //if there is a previous dropdown item, reactivate it else create a new one
                                    if (!$item) {
                                        $actionable_dropdown_item = new ActionableDropdownItem;
                                        $actionable_dropdown_item->actionable_dropdown_id = $actionable->id;
                                        $actionable_dropdown_item->name = $dropdown_item;
                                        $actionable_dropdown_item->save();
                                    } else {
                                        $item->restore();
                                    }

                                } // otherwise create a new dropdown item without searching
                                else {
                                    $actionable_dropdown_item = new ActionableDropdownItem;
                                    $actionable_dropdown_item->actionable_dropdown_id = (isset($actionable->id) ? $actionable->id : $actionable);
                                    $actionable_dropdown_item->name = $dropdown_item;
                                    $actionable_dropdown_item->save();
                                }
                            }
                        }
                    }
                }
            }
        }
        return redirect(route('processes.show', $step->process_id))->with('flash_success', 'Process updated successfully.');
    }

    public function destroy(Step $step)
    {
        $process_id = $step->process_id;

        $step->delete();

        return redirect(route('processes.show', $process_id))->with('flash_success', 'Process updated successfully.');
    }

    public function move(Step $step, Request $request)
    {
        //To do - set order properly

        if ($request->input('direction') == 'up') {
            $next_step = Step::where('process_id', $step->process->id)->where('order', '<', $step->order)->orderBy('order', 'desc')->first();

            if ($next_step) {
                $old_order = $step ->order;
                $new_order = $next_step ->order;

                $step->order = $new_order;
                $next_step->order = $old_order;
                $step->save();
            }
        }

        if ($request->input('direction') == 'down') {
            $next_step = Step::where('process_id', $step->process->id)->where('order', '>', $step->order)->orderBy('order', 'asc')->first();

            if ($next_step) {
                $old_order = $step ->order;
                $new_order = $next_step ->order;

                $step->order = $new_order;
                $next_step->order = $old_order;
                $step->save();
            }
        }

        return redirect(route('processes.show', $step->process))->with('flash_success', 'Process updated successfully.');
    }

    public function getActionableType($type)
    {
        //activity type hook
        switch ($type) {
            case 'text':
                return 'App\ActionableText';
                break;
            case 'textarea':
                return 'App\ActionableTextarea';
                break;
            case 'template_email':
                return 'App\ActionableTemplateEmail';
                break;
            case 'document_email':
                return 'App\ActionableDocumentEmail';
                break;
            case 'document':
                return 'App\ActionableDocument';
                break;
            case 'dropdown':
                return 'App\ActionableDropdown';
                break;
            case 'date':
                return 'App\ActionableDate';
                break;
            case 'boolean':
                return 'App\ActionableBoolean';
                break;
            case 'notification':
                return 'App\ActionableNotification';
                break;
            case 'multiple_attachment':
                return 'App\ActionableMultipleAttachment';
            default:
                abort(500, 'Error');
                break;
        }
    }

    public function getRelatedPartyType($type)
    {
        //activity type hook
        switch ($type) {
            case 'text':
                return 'App\RelatedPartyText';
                break;
            case 'textarea':
                return 'App\RelatedPartyTextarea';
                break;
            case 'dropdown':
                return 'App\RelatedPartyDropdown';
                break;
            case 'date':
                return 'App\RelatedPartyDate';
                break;
            case 'boolean':
                return 'App\RelatedPartyBoolean';
                break;
            case 'template_email':
                return 'App\RelatedPartyTemplateEmail';
                break;
            case 'document_email':
                return 'App\RelatedPartyDocumentEmail';
                break;
            case 'document':
                return 'App\RelatedPartyDocument';
                break;
            case 'notification':
                return 'App\RelatedPartyNotification';
                break;
            case 'multiple_attachment':
                return 'App\RelatedPartyMultipleAttachment';
            default:
                abort(500, 'Error');
                break;
        }
    }

    public function getThresholdAsSeconds($time, $type)
    {
        switch ($type) {
            case 'seconds':
                return $time;
                break;
            case 'minutes':
                return $time * 60;
                break;
            case 'hours':
                return $time * 60 * 60;
                break;
            case 'days':
                return $time * 60 * 60 * 24;
                break;
            default:
                return $time;
                break;
        }
    }

    public function createActionable($type)
    {
        //activity type hook
        switch ($type) {
            case 'text':
                return ActionableText::create();
                break;
            case 'textarea':
                return ActionableTextarea::create();
                break;
            case 'template_email':
                return ActionableTemplateEmail::create();
                break;
            case 'document_email':
                return ActionableDocumentEmail::create();
                break;
            case 'document':
                return ActionableDocument::create();
                break;
            case 'dropdown':
                return ActionableDropdown::create();
                break;
            case 'date':
                return ActionableDate::create();
                break;
            case 'boolean':
                return ActionableBoolean::create();
                break;
            case 'notification':
                return ActionableNotification::create();
                break;
            case 'multiple_attachment':
                return ActionableMultipleAttachment::create();
            default:
                abort(500, 'Error');
                break;
        }
    }

    public function createRelatedParty($type)
    {
        //activity type hook
        switch ($type) {
            case 'text':
                return RelatedPartyText::create();
                break;
            case 'textarea':
                return RelatedPartyTextarea::create();
                break;
            case 'dropdown':
                return RelatedPartyDropdown::create();
                break;
            case 'date':
                return RelatedPartyDate::create();
                break;
            case 'boolean':
                return RelatedPartyBoolean::create();
                break;
            case 'template_email':
                return RelatedPartyTemplateEmail::create();
                break;
            case 'document_email':
                return RelatedPartyDocumentEmail::create();
                break;
            case 'document':
                return RelatedPartyDocument::create();
                break;
            case 'notification':
                return RelatedPartyNotification::create();
                break;
            case 'multiple_attachment':
                return RelatedPartyMultipleAttachment::create();
            default:
                abort(500, 'Error');
                break;
        }
    }
}