<?php

namespace App\Http\Controllers;

use App\ActionableBooleanData;
use App\ActionableDateData;
use App\ActionableDropdownData;
use App\ActionableDropdownItem;
use App\ActionableMultipleAttachment;
use App\ActionableMultipleAttachmentData;
use App\ActionableNotificationData;
use App\ActionableDocumentData;
use App\ActionableTextData;
use App\ActionableTextareaData;
use App\ActionableTemplateEmail;
use App\ActionableTemplateEmailData;
use App\ActionableDocumentEmailData;
use App\ActionActivities;
use App\Actions;
use App\ActionsAssigned;
use App\Activity;
use App\ActivityComment;
use App\ActivityInClientBasket;
use App\ActivityLog;
use App\Client;
use App\ClientActivity;
use App\ClientComment;
use App\ClientCRFForm;
use App\ClientProcess;
use App\ClientUser;
use App\Committee;
use App\Config;
use App\Document;
use App\EmailSignature;
use App\EmailTemplate;
use App\Events\NotificationEvent;
use App\Exports\ClientExport;
use App\FormInputCheckboxData;
use App\FormInputDropdownData;
use App\FormInputRadioData;
use App\FormLog;
use App\Forms;
use App\FormSection;
use App\HelperFunction;
use App\Http\Requests\StoreClientFormRequest;
use App\Http\Requests\StoreClientRequest;
use App\Http\Requests\StoreFollowRequest;
use App\Http\Requests\UpdateClientFormRequest;
use App\Http\Requests\UpdateClientRequest;
use App\Log;
use App\Mail\AssignedConsultantNotify;
use App\Mail\MoveToProdNotify;
use App\Mail\MoveToQANotify;
use App\Mail\QACompleteNotify;
use App\Notification;
use App\Process;
use App\Project;
use App\Referrer;
use App\RelatedPartiesTree;
use App\RelatedParty;
use App\RelatedPartyDateData;
use App\RelatedPartyDropdownData;
use App\RelatedPartyDropdownItem;
use App\Step;
use App\StepsRelatedPartiesLink;
use App\Template;
use App\TriggerType;
use App\UserNotification;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Notifications\Action;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\TemplateMail;
use Maatwebsite\Excel\Excel;
use PhpOffice\PhpWord\TemplateProcessor;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\User;
use App\ClientForm;
use Barryvdh\Snappy\Facades\SnappyPdf as PDF;
use App\Mail\ClientNotifyMail;
use App\BusinessUnits;
use App\Presentation;
use App\FormInputTextData;
use App\FormInputBooleanData;
use App\FormInputDateData;
use App\FormInputTextareaData;

class ClientController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['sendtemplate', 'sendnotification']);
        $this->middleware('auth:api')->only(['sendtemplate', 'sendnotification']);
        $this->middleware('permission:maintain_client')->except(['create', 'store']);

        $this->progress_colours = [
            'not_started' => 'background-color: rgba(64,159,255, 0.15)',
            'started' => 'background-color: rgba(255,255,70, 0.15)',
            'progress_completed' => 'background-color: rgba(60,255,70, 0.15)',
        ];
    }

    public function index(Request $request)
    {

        $np = 0;
        $qa = 0;

        $c_array = array();
        $client_arr = array();

        $clients = new Client();
        $clients->unHide();

        $deleted_rps = RelatedPartiesTree::select('related_party_id')->get();

        $clients = $clients->with(['processes','referrer', 'process.steps.activities.actionable.data', 'introducer', 'consultant', 'committee', 'project'])->select('*', DB::raw('CONCAT(first_name," ",COALESCE(`last_name`,"")) as full_name'),
                        DB::raw('ROUND(DATEDIFF(completed_at, created_at), 0) as completed_days'),
                        DB::raw('CAST(AES_DECRYPT(`hash_company`, "Qwfe345dgfdg") AS CHAR(50)) hash_company'),
                        DB::raw('CAST(AES_DECRYPT(`hash_first_name`, "Qwfe345dgfdg") AS CHAR(50)) hash_first_name'),
                        DB::raw('CAST(AES_DECRYPT(`hash_last_name`, "Qwfe345dgfdg") AS CHAR(50)) hash_last_name'),
                        DB::raw('CAST(AES_DECRYPT(`hash_cif_code`, "Qwfe345dgfdg") AS CHAR(50)) hash_cif_code'),
                        DB::raw('CAST(`case_number` AS CHAR(50)) AS case_number')
            );

        if (\auth()->user()->is('consultant')){
            $clients = $clients->where('consultant_id', \auth()->id());
        }

        if ($request->has('q') && $request->input('q') != '') {

            $clients = $clients->having('hash_company', 'like', "%" . $request->input('q') . "%")
                ->orHaving('hash_first_name', 'like', "%" . $request->input('q') . "%")
                ->orHaving('hash_last_name', 'like', "%" . $request->input('q') . "%")
                ->orHaving('hash_cif_code', 'like', "%" . $request->input('q') . "%")
                ->orHaving('case_number', 'like', "%" . $request->input('q') . "%");
        }

        if($request->has('c') && $request->input('c') == 'yes') {
            $clients = $clients->whereNotNull('completed_at');
        } elseif($request->has('c') && $request->input('c') == 'no') {
            $clients = $clients->whereNull('completed_at');
        }

        if ($request->has('f') && $request->input('f') != '') {
            $clients = $clients->where('instruction_date','>=',$request->input('f'));
        }

        if ($request->has('t') && $request->input('t') != '') {
            $clients = $clients->where('instruction_date','<=',$request->input('t'));
        }

        $clients = $clients->get();

        foreach ($clients as $client) {

            /*$r = new ClientProcess();
            $r->client_id = $client->id;
            $r->process_id = $client->process_id;
            $r->step_id = $client->step_id;
            $r->save();*/


                $data = [];

                foreach ($client->processes as $process){
                    array_push($data,[
                        'process'=>['id'=>$process->process_id,'name'=>$process->process->name],
                        'step'=>['id'=>$process->step_id,'name'=>$process->step->name]]);
                }


                $client_arr[$client->id][] = [
                    'id' => $client->id,
                    'email'=>$client->email,
                    'contact'=>$client->contact,
                    'process_id'=>$client->process_id,
                    'step_id'=>$client->step_id,
                    'process'=>$data,
                    'section'=>$client->step->name,
                    'company' => ($client->company != null && strtolower($client->hash_company) != 'n/a' && strtolower($client->company) != 'n/a'  ? $client->company : $client->first_name . ' ' . $client->last_name),
                    'avatar' => isset($client->introducer)?$client->introducer->avatar:null,
                    'introducer' => $client->introducer->first_name.' '.$client->introducer->last_name,
                    'completed_at'=> ($client->completed_at != null ? Carbon::parse($client->completed_at       )->format('Y-m-d') : ''),
                ];

                //$total++;
            }




        $deleted_rps = RelatedPartiesTree::select('related_party_id')->get();

        $related_parties = RelatedParty::with(['client','referrer', 'process.steps2.activities.actionable.data', 'introducer', 'committee', 'project'])->select('*', DB::raw('CONCAT(first_name," ",COALESCE(`last_name`,"")) as full_name'),
            DB::raw('ROUND(DATEDIFF(completed_at, created_at), 0) as completed_days'),
            DB::raw('CAST(AES_DECRYPT(`hash_company`, "Qwfe345dgfdg") AS CHAR(50)) hash_company'),
            DB::raw('CAST(AES_DECRYPT(`hash_first_name`, "Qwfe345dgfdg") AS CHAR(50)) hash_first_name'),
            DB::raw('CAST(AES_DECRYPT(`hash_last_name`, "Qwfe345dgfdg") AS CHAR(50)) hash_last_name'),
            DB::raw('CAST(AES_DECRYPT(`hash_cif_code`, "Qwfe345dgfdg") AS CHAR(50)) hash_cif_code'),
            DB::raw('CAST(`case_number` AS CHAR(50)) AS case_number')
        )->whereHas('client', function ($q) use ($request){


            if($request->has('c') && $request->input('c') == 'yes') {
                $q->whereNotNull('completed_at');
            } elseif($request->has('c') && $request->input('c') == 'no') {
                $q->whereNull('completed_at');
            }
        })->whereIn('id',collect($deleted_rps)->toArray());

        if ($request->has('q') && $request->input('q') != '') {

            $related_parties = $related_parties->having('hash_company', 'like', "%" . $request->input('q') . "%")
                ->orHaving('hash_first_name', 'like', "%" . $request->input('q') . "%")
                ->orHaving('hash_last_name', 'like', "%" . $request->input('q') . "%")
                ->orHaving('hash_cif_code', 'like', "%" . $request->input('q') . "%")
                ->orHaving('case_number', 'like', "%" . $request->input('q') . "%");
        }

        if ($request->has('trigger_type') && $request->input('trigger_type') != '') {
            $related_parties = $related_parties->whereTriggerTypeId($request->input('trigger_type'));
        }

        if ($request->has('f') && $request->input('f') != '') {
            $related_parties = $related_parties->where('instruction_date','>=',$request->input('f'));
        }

        if ($request->has('t') && $request->input('t') != '') {
            $related_parties = $related_parties->where('instruction_date','<=',$request->input('t'));
        }

        $related_parties = $related_parties->get();

        foreach ($related_parties as $related_party) {
            if($related_party){
                $rp_data = [];

                $client_arr[$related_party->client_id][] = [
                    'related_party_id' => $related_party->id,
                    'email'=>$related_party->email,
                    'company' => ($related_party->company != null ? $related_party->company : $related_party->first_name . ' ' . $related_party->last_name),
                    'id' => $related_party->id,
                    'contact' => $related_party->contact,
                    'process' => $related_party->process->name,
                    'section' => $related_party->step->name,
                    'client_id' => $related_party->client_id,
                    'introducer' => $related_party->client->introducer->first_name.' '.$related_party->client->introducer->last_name,
                    'instruction_date' => $related_party->instruction_date,
                    'completed_at' => ($related_party->client->created_at != null ? Carbon::parse($related_party->client->created_at)->format('Y-m-d') : '')
                ];
            }
            //$total++;
        }

        if ($request->has('step') && $request->input('step') != 'all' && $request->input('step') != '') {
            if ($request->input('step') == 1000) {
                if (($request->has('f') && $request->input('f') != '') && ($request->has('t') && $request->input('t') != '')){
                    return redirect('clients?c=yes&f='.$request->input('f').'&t='.$request->input('t'));
                } else {
                    return redirect('clients?c=yes');
                }
            }
            if ($request->input('step') == 1001) {
                return redirect('clients?qa=yes');
            }
        }


            foreach ($client_arr as $key => $value) {
                foreach ($value as $key2 => $value2) {
                    array_push($c_array, $value2);
                }
            }

            if ($request->has('sort') && $request->input('sort') == 'investigation_completed_date') {
                if ($request->has('direction') && $request->input('direction') == 'asc') {
                    $c_array = collect($c_array)->sortBy('investigation_completed_date')->values();
                }

                if ($request->has('direction') && $request->input('direction') == 'desc') {
                    $c_array = collect($c_array)->sortByDesc('investigation_completed_date')->values();
                }
            }

            if ($request->has('sort') && $request->input('sort') == 'qa_end_date') {
                if ($request->has('direction') && $request->input('direction') == 'asc') {
                    $c_array = collect($c_array)->sortBy('qa_end_date')->values();
                }

                if ($request->has('direction') && $request->input('direction') == 'desc') {
                    $c_array = collect($c_array)->sortByDesc('qa_end_date')->values();
                }
            }

            if ($request->has('sort') && $request->input('sort') == 'committee_date') {
                if ($request->has('direction') && $request->input('direction') == 'asc') {
                    $c_array = collect($c_array)->sortBy('committee_date')->values();
                }

                if ($request->has('direction') && $request->input('direction') == 'desc') {
                    $c_array = collect($c_array)->sortByDesc('committee_date')->values();
                }
            }

            if ($request->has('sort') && $request->input('sort') == 'exit_committee_decision') {
                if ($request->has('direction') && $request->input('direction') == 'asc') {
                    $c_array = collect($c_array)->sortBy('exit_committee_decision')->values();
                }

                if ($request->has('direction') && $request->input('direction') == 'desc') {
                    $c_array = collect($c_array)->sortByDesc('exit_committee_decision')->values();
                }
            }

            if ($request->has('sort') && $request->input('sort') == 'current_work_queue') {
                if ($request->has('direction') && $request->input('direction') == 'asc') {
                    $c_array = collect($c_array)->sortBy('current_work_queue')->values();
                }

                if ($request->has('direction') && $request->input('direction') == 'desc') {
                    $c_array = collect($c_array)->sortByDesc('current_work_queue')->values();
                }
            }

            if ($request->has('sort') && $request->input('sort') == 'company') {
                if ($request->has('direction') && $request->input('direction') == 'asc') {
                    $c_array = collect($c_array)->sortBy('company')->values();
                }

                if ($request->has('direction') && $request->input('direction') == 'desc') {
                    $c_array = collect($c_array)->sortByDesc('company')->values();
                }
            }

            if ($request->has('sort') && $request->input('sort') == 'case_number') {
                if ($request->has('direction') && $request->input('direction') == 'asc') {
                    $c_array = collect($c_array)->sortBy('case_number')->values();
                }

                if ($request->has('direction') && $request->input('direction') == 'desc') {
                    $c_array = collect($c_array)->sortByDesc('case_number')->values();
                }
            }

            if ($request->has('sort') && $request->input('sort') == 'cif_code') {
                if ($request->has('direction') && $request->input('direction') == 'asc') {
                    $c_array = collect($c_array)->sortBy('cif_code')->values();
                }

                if ($request->has('direction') && $request->input('direction') == 'desc') {
                    $c_array = collect($c_array)->sortByDesc('cif_code')->values();
                }
            }

            if ($request->has('sort') && $request->input('sort') == 'committee') {
                if ($request->has('direction') && $request->input('direction') == 'asc') {
                    $c_array = collect($c_array)->sortBy('committee')->values();
                }

                if ($request->has('direction') && $request->input('direction') == 'desc') {
                    $c_array = collect($c_array)->sortByDesc('committee')->values();
                }
            }

            if ($request->has('sort') && $request->input('sort') == 'out_of_scope') {
                if ($request->has('direction') && $request->input('direction') == 'asc') {
                    $c_array = collect($c_array)->sortBy('out_of_scope')->values();
                }

                if ($request->has('direction') && $request->input('direction') == 'desc') {
                    $c_array = collect($c_array)->sortByDesc('out_of_scope')->values();
                }
            }

            if ($request->has('sort') && $request->input('sort') == 'completed_at') {
                if ($request->has('direction') && $request->input('direction') == 'asc') {
                    $c_array = collect($c_array)->sortBy('completed_at')->values()->all();
                }

                if ($request->has('direction') && $request->input('direction') == 'desc') {
                    $c_array = collect($c_array)->sortByDesc('completed_at')->values()->all();
                }
            }

            if ($request->has('sort') && $request->input('sort') == 'project') {
                if ($request->has('direction') && $request->input('direction') == 'asc') {
                    $c_array = collect($c_array)->sortBy('project')->values();
                }

                if ($request->has('direction') && $request->input('direction') == 'desc') {
                    $c_array = collect($c_array)->sortByDesc('project')->values();
                }
            }

            if ($request->has('sort') && $request->input('sort') == 'consultant') {
                if ($request->has('direction') && $request->input('direction') == 'asc') {
                    $c_array = collect($c_array)->sortBy('consultant')->values();
                }

                if ($request->has('direction') && $request->input('direction') == 'desc') {
                    $c_array = collect($c_array)->sortByDesc('consultant')->values();
                }
            }

            if ($request->has('sort') && $request->input('sort') == 'instruction_date') {
                if ($request->has('direction') && $request->input('direction') == 'asc') {
                    $c_array = collect($c_array)->sortBy('instruction_date')->values();
                }

                if ($request->has('direction') && $request->input('direction') == 'desc') {
                    $c_array = collect($c_array)->sortByDesc('instruction_date')->values();
                }
            }

            if ($request->has('sort') && $request->input('sort') == 'trigger_type') {
                if ($request->has('direction') && $request->input('direction') == 'asc') {
                    $c_array = collect($c_array)->sortBy('trigger_type')->values();
                }

                if ($request->has('direction') && $request->input('direction') == 'desc') {
                    $c_array = collect($c_array)->sortByDesc('trigger_type')->values();
                }
            }

        if ($request->has('committee_decision') && $request->committee_decision != ''){
            $c_array = collect($c_array)->filter(function($client) use($request){
                //return $client["exit_committee_decision"] >= $request->committee_decision;
                return false !== stristr($client["exit_committee_decision"], $request->committee_decision);
            })->values();
            //return collect($client_arr)->whereLike('exit_committee_decision', '%'.$request->committee_decision.'%')->values();
        }

        if ($request->has('committee_date_f') && $request->committee_date_f != ''){
            $c_array = collect($c_array)->filter(function($client) use($request){
                return $client["committee_date"] >= $request->committee_date_f && isset($client["committee_date"]);
            })->values();
        }

        if ($request->has('committee_date_t') && $request->committee_date_t != ''){
            $c_array = collect($c_array)->where('committee_date', '<=', $request->committee_date_t)->filter(function($client) use($request){
                return $client["committee_date"] <= $request->committee_date_t && isset($client["committee_date"]);
            })->values();
        }


        $parameters = [
            'np' => $np,
            'qa' => $qa,
            'clients' => (isset($client_arr) ? (empty($c_array) ? $client_arr : $c_array) : []),
            'processes' => Process::orderBy('name')->where('process_type_id','1')->pluck('name', 'id')->prepend('All processes', 'all'),
            'steps' => Step::orderBy('process_id')->whereNotIn('process_id',[10,11,13,14])->orderBy('order')->withTrashed()->pluck('name', 'id')->prepend('All steps', ''),
            'assigned_user' => Client::all()->keyBy('consultant_id')->map(function ($consultant){
                return isset($consultant->consultant)?$consultant->consultant->first_name.' '.$consultant->consultant->last_name:null;
            })->sort(),
            'trigger_type_dropdown' => Client::with('trigger')->whereHas('trigger')->get(['trigger_type_id'])->keyBy('trigger_type_id')->map(function ($project){
                return $project->trigger->name??null;
            }),
            'committee_decision' => ActionableDropdownItem::where('actionable_dropdown_id', 20)->pluck('name', 'name'),
        ];

        return view('clients.index')->with($parameters);
    }

    public function create()
    {
        //TODO show process steps on process selection

        $last_process = Client::where('referrer_id', auth()->id())->orderBy('created_at', 'desc')->first();

        if ($last_process) {
            $last_process = $last_process->process_id;
        } else {
            $last_process = '';
        }

        $configs = Config::first();
        $processes = Process::where('process_type_id','1')->orderBy('name')->pluck('name', 'id')->prepend('Please Select','0');
        $referrers = Referrer::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id');
        $businessunits = BusinessUnits::all()->pluck('name', 'id');
        $triggertype= TriggerType::all()->pluck('name', 'id')->prepend('Select','0');
        $project = Project::where('name','!=','')->whereNotNull('name')->get();
        $committee = Committee::where('name','!=','')->whereNotNull('name')->pluck('name','id')->prepend('Select','0');

        $inputs = new Forms();
        $forms = $inputs->getClientDetailsInputs(2);
//dd($forms);
        $parameters = [
            'config' => $configs,
            'processes' => $processes,
            'last_process' => $last_process,
            'referrers' => $referrers,
            'businessunits' => $businessunits,
            'triggertype' => $triggertype,
            'project' => $project,
            'committee' => $committee,
            'projects_down_down' => $project->keyBy('id')->map(function($proj){ return $proj->name; })->push('other'),
            'forms' => $forms
        ];

        return view('clients.create')->with($parameters);
    }

    public function store(StoreClientRequest $request)
    {
        $client = new Client;
        $client->first_name = $request->input('first_name');
        $client->last_name = $request->input('last_name');
        $client->initials = $request->input('initials');
        $client->id_number = $request->input('id_number');
        $client->email = $request->input('email');
        $client->contact = $request->input('contact');
        $client->introducer_id = auth()->id();
        $client->office_id = auth()->user()->office()->id ?? 1;
        $client->process_id = $request->input('process');
        $client->step_id = Step::where('process_id',$request->input('process'))->orderBy('order','asc')->first()->id;
        $client->needs_approval = !auth()->user()->can('maintain_client');
        if(Auth::user()->is('consultant')){
            $client->consultant_id = Auth::id();
            $client->assigned_date = now();
        }

        $client->save();

        Client::where('id',$client->id)->update([
            'hash_first_name' => DB::raw("AES_ENCRYPT('".addslashes($request->input('first_name'))."','Qwfe345dgfdg')"),
            'hash_last_name' => DB::raw("AES_ENCRYPT('".addslashes($request->input('last_name'))."','Qwfe345dgfdg')"),
            'hash_id_number' => DB::raw("AES_ENCRYPT('".addslashes($request->input('id_number'))."','Qwfe345dgfdg')"),
            'hash_email' => DB::raw("AES_ENCRYPT('".addslashes($request->input('company_email'))."','Qwfe345dgfdg')"),
            'hash_contact' => DB::raw("AES_ENCRYPT('".addslashes($request->input('contact'))."','Qwfe345dgfdg')")
        ]);

        $cp = new ClientProcess();
        $cp->client_id = $client->id;
        $cp->process_id = $request->input('process');
        $cp->step_id = Step::where('process_id',$request->input('process'))->orderBy('order','asc')->first()->id;
        $cp->save();

        $extra = Forms::where('id',2)->first();

        if($extra) {
            $sections = FormSection::with('form_section_inputs.input')->where('form_id', $extra->id)->get();

            foreach ($sections as $section) {
                foreach ($section->form_section_inputs as $activity) {

                    if ($request->has($activity->id) && !is_null($request->input($activity->id))) {

                        switch ($activity->input_type) {
                            case 'App\FormInputBoolean':
                                FormInputBooleanData::insert([
                                    'data' => $request->input($activity->id),
                                    'form_input_boolean_id' => $activity->input_id,
                                    'client_id' => $client->id,
                                    'user_id' => auth()->id(),
                                    'duration' => 120,
                                    'created_at' => now()
                                ]);
                                break;
                            case 'App\FormInputDate':
                                FormInputDateData::insert([
                                    'data' => $request->input($activity->id),
                                    'form_input_date_id' => $activity->input_id,
                                    'client_id' => $client->id,
                                    'user_id' => auth()->id(),
                                    'duration' => 120,
                                    'created_at' => now()
                                ]);
                                break;
                            case 'App\FormInputText':
                                FormInputTextData::insert([
                                    'data' => $request->input($activity->id),
                                    'form_input_text_id' => $activity->input_id,
                                    'client_id' => $client->id,
                                    'user_id' => auth()->id(),
                                    'duration' => 120,
                                    'created_at' => now()
                                ]);
                                break;
                            case 'App\FormInputTextarea':
                                FormInputTextareaData::insert([
                                    'data' => $request->input($activity->id),
                                    'form_input_textarea_id' => $activity->input_id,
                                    'client_id' => $client->id,
                                    'user_id' => auth()->id(),
                                    'duration' => 120,
                                    'created_at' => now()
                                ]);
                                break;
                            case 'App\FormInputDropdown':
                                foreach ($request->input($activity->id) as $key => $value) {
                                    FormInputDropdownData::insert([
                                        'form_input_dropdown_id' => $activity->input_id,
                                        'form_input_dropdown_item_id' => $value,
                                        'client_id' => $client->id,
                                        'user_id' => auth()->id(),
                                        'duration' => 120,
                                        'created_at' => now()
                                    ]);
                                }
                                break;
                            default:
                                //todo capture defaults
                                break;
                        }

                    }
                }
            }
        }


        //$adminuser = User::whereHas('roles', function($q){$q->where('name', 'admin');})->get()->toArray();

        //Mail::to($adminuser)->send(new ClientNotifyMail($client));
        return redirect(route('clients.index'))->with('flash_success', 'Client captured successfully');
        /*return redirect(route('clients.show', $client->id))->with('flash_success', 'Client captured successfully');*/
    }

    /**
     * Display client edit form with parameters
     *
     * @param Client $client
     * @return void
     */
    public function edit(Client $client,$process_id,$step_id)
    {
        $last_process = Client::where('referrer_id', auth()->id())->orderBy('created_at', 'desc')->first();

        if ($last_process) {
            $last_process = $last_process->process_id;
        } else {
            $last_process = '';
        }

        $processes = Process::orderBy('name')->pluck('name', 'id');
        $referrers = Referrer::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id');
        $businessunits = BusinessUnits::all()->pluck('name', 'id');
        $triggertype= TriggerType::all()->pluck('name', 'id')->prepend('Select','0');
        $project = Project::where('name','!=','')->whereNotNull('name')->get();
        $committee = Committee::where('name','!=','')->whereNotNull('name')->pluck('name','id')->prepend('Select','0');
        $smart_id = $this->smartID($client);

        $form = Forms::find(2);

        if($form) {
            $forms = $form->getClientDetailsInputValues($client->id, $form->id);
        } else {
            $forms =[];
        }

        $parameters = [
            'client' => $client,
            'processes' => $processes,
            'last_process' => $last_process,
            'referrers' => $referrers,
            'businessunits' => $businessunits,
            'triggertype' => $triggertype,
            'project' => $project,
            'committee' => $committee,
            'process_id' => $process_id,
            'step' => Step::where('id',$step_id)->first()->toArray(),
            'projects_down_down' => $project->keyBy('id')->map(function($proj){ return $proj->name; })->push('other'),
            'date_of_birth' => $smart_id['date'],
            'gender' => $smart_id['gender'],
            'citizenship' => $smart_id['citizenship'],
            'forms'=>$forms
        ];

        return view('clients.edit')->with($parameters);
    }

    /**
     * Update client details from Request
     *
     * @param UpdateClientRequest $request
     * @param Client $client
     * @return void
     */
    public function update(UpdateClientRequest $request, Client $client)
    {


        $mclient = Client::find($client->id);
        $mclient->first_name = $request->input('first_name');
        $mclient->last_name = $request->input('last_name');
        $mclient->initials = $request->input('initials');
        $mclient->id_number = $request->input('id_number');
        $mclient->email = $request->input('email');
        $mclient->contact = $request->input('contact');
        $mclient->save();

        Client::where('id',$client->id)->update([
            'hash_first_name' => DB::raw("AES_ENCRYPT('".addslashes($request->input('first_name'))."','Qwfe345dgfdg')"),
            'hash_last_name' => DB::raw("AES_ENCRYPT('".addslashes($request->input('last_name'))."','Qwfe345dgfdg')"),
            'hash_id_number' => DB::raw("AES_ENCRYPT('".addslashes($request->input('id_number'))."','Qwfe345dgfdg')"),
            'hash_email' => DB::raw("AES_ENCRYPT('".addslashes($request->input('company_email'))."','Qwfe345dgfdg')"),
            'hash_contact' => DB::raw("AES_ENCRYPT('".addslashes($request->input('contact'))."','Qwfe345dgfdg')")
        ]);

        $forms = FormSection::where('form_id',2)->get();

        if($forms) {
            foreach ($forms as $form) {
                $id = $client->id;
                $form_section = FormSection::find($form->id);
                $form_section = $form_section->load(['form_section_inputs.input.data' => function ($query) use ($id) {
                    $query->where('client_id', $id);
                }]);

                $all_activities_completed = false;
                foreach ($form_section->form_section_inputs as $activity) {
                    if (is_null($request->input($activity->id))) {
                        if ($request->input('old_' . $activity->id) != $request->input($activity->id)) {

                            if (is_array($request->input($activity->id))) {

                                $old = explode(',', $request->input('old_' . $activity->id));
                                $diff = array_diff($old, $request->input($activity->id));
                                //dd($diff);

                            } else {
                                $old = $request->input('old_' . $activity->id);

                            }

                            switch ($activity->actionable_type) {
                                case 'App\FormInputBoolean':
                                    FormInputBooleanData::where('input_boolean_id', $activity->actionable_id)->where('client_id', $client->id)->delete();

                                    break;
                                case 'App\FormInputDate':
                                    FormInputDateData::where('input_date_id', $activity->actionable_id)->where('client_id', $client->id)->delete();

                                    break;
                                case 'App\FormInputText':
                                    FormInputTextData::where('input_text_id', $activity->actionable_id)->where('client_id', $client->id)->delete();

                                    break;
                                case 'App\FormInputTextarea':
                                    FormInputTextareaData::where('input_textarea_id', $activity->actionable_id)->where('client_id', $client->id)->delete();

                                    break;
                                case 'App\FormInputDropdown':
                                    FormInputDropdownData::where('input_dropdown_id', $activity->actionable_id)->where('client_id', $client->id)->delete();

                                    break;
                                default:
                                    //todo capture defaults
                                    break;
                            }
                        }
                    }

                    if ($request->has($activity->id) && !is_null($request->input($activity->id))) {
                        //If value did not change, do not save it again or add it to log
                        if ($request->input('old_' . $activity->id) == $request->input($activity->id)) {
                            continue;
                        }
                        if (is_array($request->input($activity->id))) {

                            $old = explode(',', $request->input('old_' . $activity->id));
                            $diff = array_diff($old, $request->input($activity->id));
                            //dd($diff);

                        } else {
                            $old = $request->input('old_' . $activity->id);
                        }

                        switch ($activity->input_type) {
                            case 'App\FormInputBoolean':
                                FormInputBooleanData::where('client_id', $client->id)->where('Form_input_boolean_id', $activity->input_id)->where('data', $old)->delete();

                                FormInputBooleanData::insert([
                                    'data' => $request->input($activity->id),
                                    'form_input_boolean_id' => $activity->input_id,
                                    'client_id' => $client->id,
                                    'user_id' => auth()->id(),
                                    'duration' => 120,
                                    'created_at' => now()
                                ]);
                                break;
                            case 'App\FormInputDate':
                                FormInputDateData::insert([
                                    'data' => $request->input($activity->id),
                                    'form_input_date_id' => $activity->input_id,
                                    'client_id' => $client->id,
                                    'user_id' => auth()->id(),
                                    'duration' => 120,
                                    'created_at' => now()
                                ]);
                                break;
                            case 'App\FormInputText':

                                FormInputTextData::insert([
                                    'data' => $request->input($activity->id),
                                    'form_input_text_id' => $activity->input_id,
                                    'client_id' => $mclient->id,
                                    'user_id' => auth()->id(),
                                    'duration' => 120,
                                    'created_at' => now()
                                ]);
                                break;
                            case 'App\FormInputTextarea':

                                FormInputTextareaData::insert([
                                    'data' => $request->input($activity->id),
                                    'form_input_textarea_id' => $activity->input_id,
                                    'client_id' => $client->id,
                                    'user_id' => auth()->id(),
                                    'duration' => 120,
                                    'created_at' => now()
                                ]);
                                break;
                            case 'App\FormInputDropdown':
                                foreach ($request->input($activity->id) as $key => $value) {
                                    if (in_array($value, $old, true)) {

                                    } else {
                                        FormInputDropdownData::insert([
                                            'form_input_dropdown_id' => $activity->input_id,
                                            'form_input_dropdown_item_id' => $value,
                                            'client_id' => $client->id,
                                            'user_id' => auth()->id(),
                                            'duration' => 120,
                                            'created_at' => now()
                                        ]);
                                    }

                                    if (!empty($diff)) {
                                        FormInputDropdownData::where('client_id', $client->id)->where('form_input_dropdown_id', $activity->input_id)->whereIn('form_input_dropdown_item_id', $diff)->delete();
                                    }
                                }
                                break;
                            default:
                                //todo capture defaults
                                break;
                        }

                    }
                }
            }
        }

        return redirect()->back()->with(['flash_success' => "Client updated successfully."]);
    }

    public function destroy($id)
    {
        Client::destroy($id);
        return redirect()->route("clients.index")->with('flash_success','Client deleted successfully');

    }

    public function restore($id)
    {
        Client::onlyTrashed()->where('id',$id)->restore();

        return redirect('clients')->with('flash_success','Client successfully restored');

    }
/** **/
    public function show(Request $request,$client_id,$process_id,$step_id)
    {
        $client = Client::find($client_id);

        $process = Process::find($process_id);

        $client_process = ClientProcess::where('client_id',$client->id)->where('process_id',$process->id)->first();

        if($step_id != null) {
            $step = Step::find($step_id);
            //dd('');
        } else {
            $s = Step::where('process_id',$process_id)->orderBy('order')->first();
            $step = Step::find($s->id);
        }
//dd($process_id);
        $activity_step = $step;

        $action_id = null;
        $smart_id = $this->smartID($client);

        $parameters = $this->clientProcessProgress($client,$process,$step,$request,$activity_step, $action_id);
        $parameters['date_of_birth'] = $smart_id['date'];
        $parameters['gender'] = $smart_id['gender'];
        $parameters['citizenship'] = $smart_id['citizenship'];

        return view('clients.details')->with($parameters);
    }

    private function smartID(Client $client){
        if (isset($client->id_number)){
            $partial_d_o_b = substr($client->id_number, 0,2).'-'.substr($client->id_number, 2,2).'-'.substr($client->id_number, 4,2);
            $date_of_birth = ((substr($client->id_number, 0,2) > 45) && (substr($client->id_number, 0,2) <= 99))?'19'.$partial_d_o_b:'20'.$partial_d_o_b;
            $gender = (substr($client->id_number,6,4) < 5000) ? "Female" : '';
            $gender = (substr($client->id_number,6,4) >= 5000) ? "Male":'';
            $citizenship = substr($client->id_number, 10,1) ? "Permanent Resident" : "SA Citizen";
        }else{
            $date_of_birth = '';
            $gender = '';
            $citizenship = '';
        }

        return [
            'date' => $date_of_birth,
            'gender' => $gender,
            'citizenship' => $citizenship
        ];

    }

    public function progress(Client $client)
    {
        $client->with('process.office.area.region.division');

        $parameters = [
            'client' => $client,
            'process_progress' => $client->getProcessProgress(),
            'steps' => Step::all(),
            'documents' => Document::orderBy('name')->where('client_id', $client->id)->orWhere('client_id', null)->pluck('name', 'id'),
            'templates' => Template::orderBy('name')->pluck('name', 'id')
        ];

        return view('clients.progress')->with($parameters);
    }
/** **/
    public function stepProgress(Request $request,$client_id, $process_id, $step_id)
    {
        $client = Client::find($client_id);

        $process = Process::find($process_id);

        $client_process = ClientProcess::where('client_id',$client->id)->where('process_id',$process->id)->first();

        $step = Step::find($step_id);

        $activity_step = $step;

        $action_id = null;

        $parameters = $this->clientProcessProgress($client,$process,$step,$request,$activity_step, $action_id);

        return view('clients.stepprogress')->with($parameters);
    }
/** **/
    public function storeProgress(Client $client, Request $request)
    {

        if($request->has('step_id') && $request->input('step_id') != ''){
            $log = new Log;
            $log->client_id = $client->id;
            $log->user_id = auth()->id();
            $log->save();

            $id = $client->id;
            $step = Step::find($request->input('step_id'));
            $step->load(['activities.actionable.data' => function ($query) use ($id) {
                $query->where('client_id', $id);
            }]);

            $all_activities_completed = false;
            foreach ($step->activities as $activity) {
                if(is_null($request->input($activity->id))){
                    if($request->input('old_'.$activity->id) != $request->input($activity->id)){

                        if(is_array($request->input($activity->id))){

                            $old = explode(',',$request->input('old_'.$activity->id));
                            $diff = array_diff($old,$request->input($activity->id));
                            //dd($diff);

                            foreach($request->input($activity->id) as $key => $value) {
                                $activity_log = new ActivityLog;
                                $activity_log->log_id = $log->id;
                                $activity_log->activity_id = $activity->id;
                                $activity_log->activity_name = $activity->name;
                                $activity_log->old_value = $request->input('old_' . $activity->id);
                                $activity_log->new_value = $value;
                                $activity_log->save();
                            }
                        } else {
                            $old = $request->input('old_'.$activity->id);

                            $activity_log = new ActivityLog;
                            $activity_log->log_id = $log->id;
                            $activity_log->activity_id = $activity->id;
                            $activity_log->activity_name = $activity->name;
                            $activity_log->old_value = $request->input('old_'.$activity->id);
                            $activity_log->new_value = $request->input($activity->id);
                            $activity_log->save();
                        }

                        switch ($activity->actionable_type) {
                            case 'App\ActionableBoolean':
                                ActionableBooleanData::where('actionable_boolean_id',$activity->actionable_id)->where('client_id',$client->id)->delete();

                                break;
                            case 'App\ActionableDate':
                                ActionableDateData::where('actionable_date_id',$activity->actionable_id)->where('client_id',$client->id)->delete();

                                break;
                            case 'App\ActionableText':

                                ActionableTextData::where('actionable_text_id',$activity->actionable_id)->where('client_id',$client->id)->delete();

                                break;
                            case 'App\ActionableTextarea':
                                ActionableTextareaData::where('actionable_textarea_id',$activity->actionable_id)->where('client_id',$client->id)->delete();

                                break;
                            case 'App\ActionableDropdown':
                                ActionableDropdownData::where('actionable_dropdown_id',$activity->actionable_id)->where('client_id',$client->id)->delete();

                                break;
                            default:
                                //todo capture defaults
                                break;
                        }
                    }
                }

                if ($request->has($activity->id) && !is_null($request->input($activity->id))) {
                    //If value did not change, do not save it again or add it to log
                    if($request->input('old_'.$activity->id) == $request->input($activity->id)){
                        continue;
                    }
                    if(is_array($request->input($activity->id))){

                        $old = explode(',',$request->input('old_'.$activity->id));
                        $diff = array_diff($old,$request->input($activity->id));
                        //dd($diff);

                        foreach($request->input($activity->id) as $key => $value) {
                            $activity_log = new ActivityLog;
                            $activity_log->log_id = $log->id;
                            $activity_log->activity_id = $activity->id;
                            $activity_log->activity_name = $activity->name;
                            $activity_log->old_value = $request->input('old_' . $activity->id);
                            $activity_log->new_value = $value;
                            $activity_log->save();
                        }
                    } else {
                        $old = $request->input('old_'.$activity->id);

                        $activity_log = new ActivityLog;
                        $activity_log->log_id = $log->id;
                        $activity_log->activity_id = $activity->id;
                        $activity_log->activity_name = $activity->name;
                        $activity_log->old_value = $request->input('old_'.$activity->id);
                        $activity_log->new_value = $request->input($activity->id);
                        $activity_log->save();
                    }

                    //activity type hook
                    //dd($request);
                    switch ($activity->actionable_type) {
                        case 'App\ActionableBoolean':
                            ActionableBooleanData::where('client_id',$client->id)->where('actionable_boolean_id',$activity->actionable_id)->where('data',$old)->delete();

                            ActionableBooleanData::insert([
                                'data' => $request->input($activity->id),
                                'actionable_boolean_id' => $activity->actionable_id,
                                'client_id' => $client->id,
                                'user_id' => auth()->id(),
                                'duration' => 120,
                                'created_at' => now()
                            ]);
                            break;
                        case 'App\ActionableDate':
                            ActionableDateData::where('client_id',$client->id)->where('actionable_date_id',$activity->actionable_id)->where('data',$old)->delete();

                            ActionableDateData::insert([
                                'data' => $request->input($activity->id),
                                'actionable_date_id' => $activity->actionable_id,
                                'client_id' => $client->id,
                                'user_id' => auth()->id(),
                                'duration' => 120,
                                'created_at' => now()
                            ]);
                            break;
                        case 'App\ActionableText':
                            ActionableTextData::where('client_id',$client->id)->where('actionable_text_id',$activity->actionable_id)->where('data',$old)->delete();

                            ActionableTextData::insert([
                                'data' => $request->input($activity->id),
                                'actionable_text_id' => $activity->actionable_id,
                                'client_id' => $client->id,
                                'user_id' => auth()->id(),
                                'duration' => 120,
                                'created_at' => now()
                            ]);
                            break;
                        case 'App\ActionableTextarea':
                            ActionableTextareaData::where('client_id',$client->id)->where('actionable_textarea_id',$activity->actionable_id)->where('data',$old)->delete();

                            $replace1 = str_replace('&nbsp;',' ',$request->input($activity->id));
                            $replace2 = str_replace('&ndash;','-',$replace1);
                            $replace3 = str_replace('&bull;', '- ',$replace2);
                            $replace4 = str_replace('&ldquo;','"',$replace3);
                            $replace5 = str_replace('&rdquo;','"',$replace4);
                            $replace6 = str_replace("&rsquo;","'",$replace5);
                            $replace7 = str_replace("&lsquo;","'",$replace6);

                            $data = $replace7;

                            ActionableTextareaData::insert([
                                'data' => $data,
                                'actionable_textarea_id' => $activity->actionable_id,
                                'client_id' => $client->id,
                                'user_id' => auth()->id(),
                                'duration' => 120,
                                'created_at' => now()
                            ]);
                            break;
                        case 'App\ActionableDropdown':
                            foreach($request->input($activity->id) as $key => $value){
                                if(in_array($value,$old,true)) {

                                } else {
                                    ActionableDropdownData::insert([
                                        'actionable_dropdown_id' => $activity->actionable_id,
                                        'actionable_dropdown_item_id' => $value,
                                        'client_id' => $client->id,
                                        'user_id' => auth()->id(),
                                        'duration' => 120,
                                        'created_at' => now()
                                    ]);
                                }

                                if(!empty($diff)){
                                    ActionableDropdownData::where('client_id',$client->id)->where('actionable_dropdown_id',$activity->actionable_id)->whereIn('actionable_dropdown_item_id',$diff)->delete();
                                }



                            }
                            break;
                        /*case 'App\ActionableMultipleAttachment':
                            ActionableMultipleAttachmentData::insert([
                                'email' => $request->input($activity->id),
                                'template_id' => $request->input('template_email_'.$activity->id),
                                'actionable_ma_id' => $activity->actionable_id,
                                'client_id' => $client->id,
                                'user_id' => auth()->id(),
                                'duration' => 120
                            ]);
                            break;*/
                        default:
                            //todo capture defaults
                            break;
                    }

                }
            }

            $client_process_progress = ClientProcess::where('client_id',$client->id)->where('process_id',$request->input('process_id'))->first();

            //Move process step to the next step if all activities completed
            $max_step = Step::orderBy('order','desc')->where('process_id', $request->input('process_id'))->first();

            $n_step = Step::select('id')->orderBy('order','asc')->where('process_id', $request->input('process_id'))->where('order','>',$step->order)->whereNull('deleted_at')->first();

            $load_next_step = false;
            //dd($n_step);
            if($client->isStepActivitiesCompleted($step) && $client_process_progress->step_id != $max_step["id"] && $step->id != $max_step["id"]){
                $client = Client::find($client->id);
                $client->step_id = $n_step->id;
                $client->save();

                $cpp = ClientProcess::find($client_process_progress->id);
                $cpp->step_id = $n_step->id;
                $cpp->save();

                $load_next_step = true;
            }
            if($client->isStepActivitiesCompleted($step) && $step->id == $max_step["id"]){
                $client = Client::find($client->id);
                $client->step_id = $max_step['id'];
                $client->save();

                $cpp = ClientProcess::find($client_process_progress->id);
                $cpp->step_id = $max_step['id'];
                $cpp->save();

                $load_next_step = false;
            }
            if($client->step_id == $request->input('step_id')){
                $client = Client::find($client->id);
                $client->step_id = $step->id;
                $client->save();

                $cpp = ClientProcess::find($client_process_progress->id);
                $cpp->step_id = $step->id;
                $cpp->save();

                $load_next_step = false;
            }

            //Handle files
            foreach($request->files as $key => $file):
                $file_activity = Activity::find($key);
                switch($file_activity->actionable_type){
                    case 'App\ActionableDocument':
                        $afile = $request->file($key);
                        $name = Carbon::now()->format('Y-m-d')."-".strtotime(Carbon::now()).".".$afile->getClientOriginalExtension();
                        $stored = $afile->storeAs('documents', $name);

                        $document = new Document;
                        $document->name = $file_activity->name;
                        $document->file = $name;
                        $document->user_id = auth()->id();
                        $document->client_id = $client->id;
                        $document->save();

                        ActionableDocumentData::insert([
                            'actionable_document_id' => $file_activity->actionable_id,
                            'document_id' => $document->id,
                            'client_id' => $client->id,
                            'user_id' => auth()->id(),
                            'duration' => 120
                        ]);
                        break;
                    default:
                        //todo capture detaults
                        break;
                }

            endforeach;

            //$notification = $this->activityNotification($client, $log->id);

        }

        //Move process step to the next step if all activities completed



        if($load_next_step == true) {
            return redirect()->route('clients.stepprogress', ['client' => $client,'process'=>$request->input('process_id'), 'step' => $n_step]);
        }

        return redirect()->back()->with(['flash_success' => "Activity values successffully captured."]);
    }

    public function stepProgressAction(Request $request,$client_id, $process_id, $step_id, $action_id )
    {

        $client = Client::find($client_id);

        $process = Process::find($process_id);

        //$client_process = ClientProcess::where('client_id',$client->id)->where('process_id',$process->id)->first();

        $step = Step::find($step_id);

        $activity_step = $step;

        $parameters = $this->clientProcessProgress($client,$process,$step,$request,$activity_step, $action_id);

        return view('clients.stepprogressactions')->with($parameters);
    }

    public function storeActions(Client $client, Request $request)
    {

        if($request->has('step_id') && $request->input('step_id') != ''){
            $log = new Log;
            $log->client_id = $client->id;
            $log->user_id = auth()->id();
            $log->save();

            $id = $client->id;

            $step = Step::find($request->input('step_id'));
            $step->load(['activities.actionable.data' => function ($query) use ($id) {
                $query->where('client_id', $id);
            }]);

            $all_activities_completed = false;
            foreach ($step->activities as $activity) {
                if (is_null($request->input($activity->id))) {
                    if ($request->input('old_' . $activity->id) != $request->input($activity->id)) {

                        if (is_array($request->input($activity->id))) {

                            $old = explode(',', $request->input('old_' . $activity->id));
                            $diff = array_diff($old, $request->input($activity->id));
                            //dd($diff);

                            foreach ($request->input($activity->id) as $key => $value) {
                                $activity_log = new ActivityLog;
                                $activity_log->log_id = $log->id;
                                $activity_log->activity_id = $activity->id;
                                $activity_log->activity_name = $activity->name;
                                $activity_log->old_value = $request->input('old_' . $activity->id);
                                $activity_log->new_value = $value;
                                $activity_log->save();
                            }
                        } else {
                            $old = $request->input('old_' . $activity->id);

                            $activity_log = new ActivityLog;
                            $activity_log->log_id = $log->id;
                            $activity_log->activity_id = $activity->id;
                            $activity_log->activity_name = $activity->name;
                            $activity_log->old_value = $request->input('old_' . $activity->id);
                            $activity_log->new_value = $request->input($activity->id);
                            $activity_log->save();
                        }

                        switch ($activity->actionable_type) {
                            case 'App\ActionableBoolean':
                                ActionableBooleanData::where('actionable_boolean_id', $activity->actionable_id)->where('client_id', $client->id)->delete();

                                break;
                            case 'App\ActionableDate':
                                ActionableDateData::where('actionable_date_id', $activity->actionable_id)->where('client_id', $client->id)->delete();

                                break;
                            case 'App\ActionableText':
                                ActionableTextData::where('actionable_text_id', $activity->actionable_id)->where('client_id', $client->id)->delete();

                                break;
                            case 'App\ActionableDropdown':
                                ActionableDropdownData::where('actionable_dropdown_id', $activity->actionable_id)->where('client_id', $client->id)->delete();

                                break;
                            default:
                                //todo capture defaults
                                break;
                        }
                    }
                }

                if ($request->has($activity->id) && !is_null($request->input($activity->id))) {
                    //If value did not change, do not save it again or add it to log
                    if ($request->input('old_' . $activity->id) == $request->input($activity->id)) {
                        continue;
                    }
                    if (is_array($request->input($activity->id))) {

                        $old = explode(',', $request->input('old_' . $activity->id));
                        $diff = array_diff($old, $request->input($activity->id));
                        //dd($diff);

                        foreach ($request->input($activity->id) as $key => $value) {
                            $activity_log = new ActivityLog;
                            $activity_log->log_id = $log->id;
                            $activity_log->activity_id = $activity->id;
                            $activity_log->activity_name = $activity->name;
                            $activity_log->old_value = $request->input('old_' . $activity->id);
                            $activity_log->new_value = $value;
                            $activity_log->save();
                        }
                    } else {
                        $old = $request->input('old_' . $activity->id);

                        $activity_log = new ActivityLog;
                        $activity_log->log_id = $log->id;
                        $activity_log->activity_id = $activity->id;
                        $activity_log->activity_name = $activity->name;
                        $activity_log->old_value = $request->input('old_' . $activity->id);
                        $activity_log->new_value = $request->input($activity->id);
                        $activity_log->save();
                    }

                    //activity type hook
                    //dd($request);
                    switch ($activity->actionable_type) {
                        case 'App\ActionableBoolean':
                            ActionableBooleanData::where('client_id', $client->id)->where('actionable_boolean_id', $activity->actionable_id)->where('data', $old)->delete();

                            ActionableBooleanData::insert([
                                'data' => $request->input($activity->id),
                                'actionable_boolean_id' => $activity->actionable_id,
                                'client_id' => $client->id,
                                'user_id' => auth()->id(),
                                'duration' => 120,
                                'created_at' => now()
                            ]);
                            break;
                        case 'App\ActionableDate':
                            ActionableDateData::insert([
                                'data' => $request->input($activity->id),
                                'actionable_date_id' => $activity->actionable_id,
                                'client_id' => $client->id,
                                'user_id' => auth()->id(),
                                'duration' => 120,
                                'created_at' => now()
                            ]);
                            break;
                        case 'App\ActionableText':

                            ActionableTextData::insert([
                                'data' => $request->input($activity->id),
                                'actionable_text_id' => $activity->actionable_id,
                                'client_id' => $client->id,
                                'user_id' => auth()->id(),
                                'duration' => 120,
                                'created_at' => now()
                            ]);
                            break;
                        case 'App\ActionableDropdown':
                            foreach ($request->input($activity->id) as $key => $value) {
                                if (in_array($value, $old, true)) {

                                } else {
                                    ActionableDropdownData::insert([
                                        'actionable_dropdown_id' => $activity->actionable_id,
                                        'actionable_dropdown_item_id' => $value,
                                        'client_id' => $client->id,
                                        'user_id' => auth()->id(),
                                        'duration' => 120,
                                        'created_at' => now()
                                    ]);
                                }

                                if (!empty($diff)) {
                                    ActionableDropdownData::where('client_id', $client->id)->where('actionable_dropdown_id', $activity->actionable_id)->whereIn('actionable_dropdown_item_id', $diff)->delete();
                                }


                            }
                            break;
                        default:
                            //todo capture defaults
                            break;
                    }

                }
            }

            //Handle files
            foreach ($request->files as $key => $file):
                $file_activity = Activity::find($key);
                switch ($file_activity->actionable_type) {
                    case 'App\ActionableDocument':
                        $afile = $request->file($key);
                        $name = Carbon::now()->format('Y-m-d') . "-" . strtotime(Carbon::now()) . "." . $afile->getClientOriginalExtension();
                        $stored = $afile->storeAs('documents', $name);

                        $document = new Document;
                        $document->name = $file_activity->name;
                        $document->file = $name;
                        $document->user_id = auth()->id();
                        $document->client_id = $client->id;
                        $document->save();

                        ActionableDocumentData::insert([
                            'actionable_document_id' => $file_activity->actionable_id,
                            'document_id' => $document->id,
                            'client_id' => $client->id,
                            'user_id' => auth()->id(),
                            'duration' => 120
                        ]);
                        break;
                    default:
                        //todo capture detaults
                        break;
                }

            endforeach;

            //$notification = $this->activityNotification($client, $log->id);

        }

        return redirect()->back()->with(['flash_success' => "Client details captured."]);
    }

    public function activityProgress(Request $request, $client_id, $process_id, $step_id)
    {
        $client = Client::find($client_id);

        $process = Process::find($process_id);

        $client_process = ClientProcess::where('client_id',$client->id)->where('process_id',$process->id)->first();

        $step = Step::find($client_process->step_id);

        $activity_step = Step::find($step_id);

        $action_id = null;

        $parameters = $this->clientProcessProgress($client,$process,$step,$request,$activity_step,$action_id);

        return view('clients.activityprogress')->with($parameters);
    }

    public function actions(Request $request,$clientid, Step $step){

        $client = Client::withTrashed()->find($clientid);

        $client->with('process.office.area.region.division');

        $client_progress = $client->process->getStageHex(0);

        if($client->step_id == $step->id)
            $client_progress = $client->process->getStageHex(1);

        if($client->step_id > $step->id)
            $client_progress = $client->process->getStageHex(2);

        $steps = Step::where('process_id', $client->process_id)->orderBy('order','asc')->get();
        $c_step_order = Step::where('id',$client->step_id)->withTrashed()->first();

        $step_data = [];
        foreach ($steps as $a_step):
            $progress_color = $client->process->getStageHex(0);
            $step_stage = 0;

            if($c_step_order->order == $a_step->order) {
                $progress_color = $client->process->getStageHex(1);
                $step_stage = 1;
            }

            if($c_step_order->order > $a_step->order) {
                $progress_color = $client->process->getStageHex(2);
                $step_stage = 2;
            }


            $tmp_step = [
                'id' => $a_step->id,
                'name' => $a_step->name,
                'process_id' => $a_step->process_id,
                'progress_color' => $progress_color,
                'order' => $a_step->order,
                'stage' => $step_stage
            ];

            array_push($step_data, $tmp_step);

        endforeach;
        $max_step = Step::orderBy('order','desc')->where('process_id', $client->process_id)->first();

        //$n_step = Step::select('id')->orderBy('order','asc')->where('process_id', $client->process_id)->where('order','>',$step->order)->whereNull('deleted_at')->first();
        $next_step = $step->id;
        if($next_step == $max_step->id)
            $next_step = $max_step->id;
        else
            $next_step = (isset($n_step->id) ? $n_step->id : $step->id);
        $template_email_options = EmailTemplate::orderBy('name')->pluck('name', 'id');


        $activities_comments = DB::select("SELECT activity_id,COUNT(activity_id) as activity_count,`private` as pr,user_id FROM client_activities_comments where client_id = '".$client->id."' and deleted_at is null group by activity_id,private,user_id");
        $activity_comment = array();
        foreach ($activities_comments as $value) {
            if ((isset($value->pr) && $value->pr > 0) && (isset($value->user_id) && Auth::id() == $value->user_id)) {
                $activity_comment[$value->activity_id] = (isset($activity_comment[$value->activity_id]) ? $activity_comment[$value->activity_id] : 0) + $value->activity_count;
            } else {
                if (isset($value->pr) && $value->pr == 0){
                    $activity_comment[$value->activity_id] = (isset($activity_comment[$value->activity_id]) ? $activity_comment[$value->activity_id] : 0) + $value->activity_count;
                }
            }
        }

        /* Get the raw data for assigned activities */
        $result = ActionsAssigned::with('client')->whereHas('activity', function($q) use ($clientid){
            $q->where('status','0')
                ->where('client_id',$clientid);
        })->where('completed',0)->get();
        $configs = Config::first();

        /*  Separate out the collection into an array we can manipulate better in the template */
        $activities = [];

        $due_date = '';

        foreach($result as $activity) {

            // User IDs are comma-separated in the database
            $split_users = explode(',', $activity->users);
            $auser_array = array();
            if ($activity->client) {
                // List of Activities
                foreach ($activity->activity as $activity_id) {
                    foreach ($split_users as $user_id) {

                        // User Name
                        $user = User::where('id', trim($user_id))->first();

                        $user_name = $user["first_name"] . ' ' . $user["last_name"];

                        if(!in_array($user_name,$auser_array)) {
                            array_push($auser_array, $user_name);
                        }
                    }

                    if ($activity_id != null && $activity_id->status != 1) {
                        $clientid = $activity->client["id"];
                        $parent_activity = Activity::withTrashed()->with(['actionable.data' => function ($query) use ($clientid) {
                            $query->where('client_id', $clientid);
                        }])->where('id', $activity_id->activity_id)->first();

                        if(isset($activities[($activity->client["company"] != null && $activity->client["company"] != '' ? $activity->client["company"] : $activity->client["first_name"] . ' ' . $activity->client["last_name"])][$activity_id->activity_id]["due_date"]) && $activities[($activity->client["company"] != null && $activity->client["company"] != '' ? $activity->client["company"] : $activity->client["first_name"] . ' ' . $activity->client["last_name"])][$activity_id->activity_id]["due_date"] > $activity->due_date){
                            $due_date = $activities[($activity->client["company"] != null && $activity->client["company"] != '' ? $activity->client["company"] : $activity->client["first_name"] . ' ' . $activity->client["last_name"])][$activity_id->activity_id]["due_date"];
                        } else {
                            $due_date = $activity->due_date;
                        }

                        if (isset($parent_activity->actionable['data'][0])) {
                            //foreach ($parent_activity->actionable['data'] as $data) {

                            //Get the current timestamp.
                            $now = strtotime(now());

                            //Calculate the difference.
                            $difference = $now - strtotime($due_date);

                            //Convert seconds into days.
                            $days = floor($difference / (60 * 60 * 24));

                            if ($days < -$configs->action_threshold) {
                                $class = $activity->client->process->getStageHex(2);
                            } elseif ($days <= $configs->action_threshold) {
                                if (Carbon::parse(now()) > Carbon::parse($due_date)) {
                                    $class = $activity->client->process->getStageHex(0);
                                } elseif (Carbon::parse(now()) >= Carbon::parse($due_date)->subDay($configs->action_threshold)) {
                                    $class = $activity->client->process->getStageHex(1);
                                }
                            } elseif ($days > $configs->action_threshold) {
                                $class = $activity->client->process->getStageHex(0);
                            } else {
                                $class = $activity->client->process->getStageHex(0);
                            }

                            if (Auth::check() && Auth::user()->isNot("admin") && Auth::id() == $user_id) {
                                $activities[($activity->client["company"] != null && $activity->client["company"] != '' ? $activity->client["company"] : $activity->client["first_name"] . ' ' . $activity->client["last_name"])] [$activity_id->activity_id] = [
                                    'client_id' => $activity->client["id"],
                                    'step_id' => $activity->step_id,
                                    'action_id' => $activity->id,
                                    'user' => (isset($activities[($activity->client["company"] != null && $activity->client["company"] != '' ? $activity->client["company"] : $activity->client["first_name"] . ' ' . $activity->client["last_name"])][$activity_id->activity_id]["user"]) ? array_unique(array_merge($activities[($activity->client["company"] != null && $activity->client["company"] != '' ? $activity->client["company"] : $activity->client["first_name"] . ' ' . $activity->client["last_name"])][$activity_id->activity_id]["user"],$auser_array)) : $auser_array),
                                    'activity_id' => trim($activity_id->activity_id),
                                    'activity_name' => Activity::withTrashed()->where('id', trim($activity_id->activity_id))->first()->name,
                                    'due_date' => $due_date,
                                    'class' => $class];
                            } elseif (Auth::check() && Auth::user()->is("admin")) {
                                $activities[($activity->client["company"] != null && $activity->client["company"] != '' ? $activity->client["company"] : $activity->client["first_name"] . ' ' . $activity->client["last_name"])] [$activity_id->activity_id] = [
                                    'client_id' => $activity->client["id"],
                                    'step_id' => $activity->step_id,
                                    'action_id' => $activity->id,
                                    'user' => (isset($activities[($activity->client["company"] != null && $activity->client["company"] != '' ? $activity->client["company"] : $activity->client["first_name"] . ' ' . $activity->client["last_name"])][$activity_id->activity_id]["user"]) ? array_unique(array_merge($activities[($activity->client["company"] != null && $activity->client["company"] != '' ? $activity->client["company"] : $activity->client["first_name"] . ' ' . $activity->client["last_name"])][$activity_id->activity_id]["user"],$auser_array)) : $auser_array),
                                    'activity_id' => trim($activity_id->activity_id),
                                    'activity_name' => Activity::withTrashed()->where('id', trim($activity_id->activity_id))->first()->name,
                                    'due_date' => $due_date,
                                    'created_date' => '',
                                    'updated_date' => '',
                                    'class' => $class];
                            }
                        } else {

                            $now2 = strtotime(now());

                            //Calculate the difference.
                            $difference2 = $now2 - strtotime($due_date);

                            //Convert seconds into days.
                            $days2 = floor($difference2 / (60 * 60 * 24));

                            if ($days2 < -$configs->action_threshold) {
                                $class = $activity->client->process->getStageHex(2);
                            } elseif ($days2 <= $configs->action_threshold) {
                                if (Carbon::parse(now()) > Carbon::parse($due_date)) {
                                    $class = $activity->client->process->getStageHex(0);
                                } elseif (Carbon::parse(now()) >= Carbon::parse($due_date)->subDay($configs->action_threshold)) {
                                    $class = $activity->client->process->getStageHex(1);
                                }
                            } elseif ($days2 > $configs->action_threshold) {
                                $class = $activity->client->process->getStageHex(0);
                            } else {
                                $class = $activity->client->process->getStageHex(0);
                            }

                            if (Auth::check() && Auth::user()->isNot("admin") && Auth::id() == $user_id) {
                                $activities[($activity->client["company"] != null && $activity->client["company"] != '' ? $activity->client["company"] : $activity->client["first_name"] . ' ' . $activity->client["last_name"])] [$activity_id->activity_id] = [
                                    'client_id' => $activity->client["id"],
                                    'step_id' => $activity->step_id,
                                    'action_id' => $activity->id,
                                    'user' => (isset($activities[($activity->client["company"] != null && $activity->client["company"] != '' ? $activity->client["company"] : $activity->client["first_name"] . ' ' . $activity->client["last_name"])][$activity_id->activity_id]["user"]) ? array_unique(array_merge($activities[($activity->client["company"] != null && $activity->client["company"] != '' ? $activity->client["company"] : $activity->client["first_name"] . ' ' . $activity->client["last_name"])][$activity_id->activity_id]["user"],$auser_array)) : $auser_array),
                                    'activity_id' => trim($activity_id->activity_id),
                                    'activity_name' => Activity::withTrashed()->where('id', trim($activity_id->activity_id))->first()->name,
                                    'due_date' => $due_date,
                                    'created_date' => '',
                                    'updated_date' => '',
                                    'class' => $class];
                            } elseif (Auth::check() && Auth::user()->is("admin")) {
                                $activities[($activity->client["company"] != null && $activity->client["company"] != '' ? $activity->client["company"] : $activity->client["first_name"] . ' ' . $activity->client["last_name"])] [$activity_id->activity_id] = [
                                    'client_id' => $activity->client["id"],
                                    'step_id' => $activity->step_id,
                                    'action_id' => $activity->id,
                                    'user' => (isset($activities[($activity->client["company"] != null && $activity->client["company"] != '' ? $activity->client["company"] : $activity->client["first_name"] . ' ' . $activity->client["last_name"])][$activity_id->activity_id]["user"]) ? array_unique(array_merge($activities[($activity->client["company"] != null && $activity->client["company"] != '' ? $activity->client["company"] : $activity->client["first_name"] . ' ' . $activity->client["last_name"])][$activity_id->activity_id]["user"],$auser_array)) : $auser_array),
                                    'activity_id' => trim($activity_id->activity_id),
                                    'activity_name' => Activity::withTrashed()->where('id', trim($activity_id->activity_id))->first()->name,
                                    'due_date' => $due_date,
                                    'created_date' => '',
                                    'updated_date' => '',
                                    'class' => $class];
                            }
                        }
                    }
                }
            }
        }
        //dd($actions_data);
        $parameters = [

            'actions_data' => $activities,
            'actions' => Actions::where('status',1)->pluck('name','id')->prepend('Please select' , 0),
            'view_process_dropdown' => $client->clientProcessIfActivitiesExist(),
            'client' => $client,
            'activity_comment' => $activity_comment,
            'step' => $step,
            'active' => $step,
            'max_step' => $max_step->id,
            'next_step' => $next_step,
            'process_progress' => $client->getProcessStepProgress($step),
            'steps' => $step_data,
            //'steps' => Step::where('process_id', $client->process_id)->get(),
            'users_drop' => User::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id'),
            'documents' => Document::orderBy('name')->where('client_id', $client->id)->orWhere('client_id', null)->pluck('name', 'id'),
            'document_options' => Document::orderBy('name')->pluck('name', 'id'),
            'templates' => Template::where('template_type_id','2')->orderBy('name')->pluck('name', 'id'),
            'client_progress' => $client_progress,
            'template_email_options' => $template_email_options,
        ];

        return view('clients.actions.index')->with($parameters);
    }

    public function documents(Request $request, $client_id, $process_id, $step_id)
    {
        $client = Client::find($client_id);

        $process = Process::find($process_id);

        $client_process = ClientProcess::where('client_id',$client->id)->where('process_id',$process->id)->first();

        $step = Step::find($client_process->step_id);

        $activity_step = $step;

        $action_id = null;

        $parameters = $this->clientProcessProgress($client,$process,$step,$request,$activity_step, $action_id);

        return view('clients.documents')->with($parameters);
    }

    public function autocompleteClientProcess(Request $request, Client $clientid, Process $processid,$newprocess)
    {
        //autocomplete all entries for current process

            $steps = Step::with('activities.actionable.data')->where('process_id',$processid->id)->get();
            /*return $step;
            exit;*/
            $activities_auto_completed = [];
            //return null;
            //todo just change switch logic to assign fake data for all activities
        foreach($steps as $step) {
            foreach ($step->activities as $activity) {

                //Check if activity is not already set/completed
                if (!$clientid->isActivitieCompleted($activity)) {
                    //if(!isset($activity->actionable['data'][0])){
                    $found = false;
                    foreach ($activity->actionable['data'] as $datum) {
                        if ($datum->client_id == $clientid->id) {
                            $found = true;
                            break;
                        }
                    }

                    if (!$found) {
                        $activities_auto_completed[] = $activity->id;
                        $actionable_text_data = $activity->actionable_type . 'Data';
                        $actionable_data = new $actionable_text_data;
                        $actionable_data->client_id = $clientid->id;
                        $actionable_data->user_id = auth()->id();
                        $actionable_data->duration = 120;
                        switch ($activity->getTypeName()) {
                            case 'text':
                                $actionable_data->data = null;
                                $actionable_data->actionable_text_id = $activity->actionable_id;
                                $actionable_data->save();
                                break;
                            case 'template_email':
                                $actionable_data->template_id = 1;
                                $actionable_data->email = null;
                                $actionable_data->actionable_template_email_id = $activity->actionable_id;
                                $actionable_data->save();
                                break;
                            case 'document_email':
                                //return 'document_email';
                                break;
                            case 'document':
                                $actionable_data->actionable_document_id = $activity->actionable_id;
                                $actionable_data->document_id = 1;
                                $actionable_data->save();
                                break;
                            case 'dropdown':
                                $item = ActionableDropdownItem::where('actionable_dropdown_id', $activity->actionable_id)->take(1)->first();

                                $actionable_data->actionable_dropdown_id = $activity->actionable_id;
                                $actionable_data->actionable_dropdown_item_id = 0;
                                $actionable_data->save();
                                break;
                            case 'date':
                                $actionable_data->data = null;
                                $actionable_data->actionable_date_id = $activity->actionable_id;
                                $actionable_data->save();
                                break;
                            case 'boolean':
                                $actionable_data->data = null;
                                $actionable_data->actionable_boolean_id = $activity->actionable_id;
                                $actionable_data->save();
                                break;
                            case 'notification':
                                $notification = new Notification;
                                $notification->name = $clientid->company . ' has been updated: ' . $activity->name;
                                $notification->link = route('clients.progress', $clientid);
                                $notification->save();

                                $actionable_data->actionable_notification_id = $activity->actionable_id;
                                $actionable_data->notification_id = $notification->id;
                                $actionable_data->save();
                                break;
                            case 'multiple_attachment':
                                $actionable_data->template_id = 1;
                                $actionable_data->email = null;
                                $actionable_data->actionable_ma_id = $activity->actionable_id;
                                $actionable_data->save();
                                break;
                            default:
                                return 'error';
                                break;
                        }
                    }

                }
            }
        }

        $new_process_id = $newprocess;

        //check if selected process has any activities for this client
        $highest_step_id = Step::with(['activities.actionable.data' => function ($query) use ($clientid) {
            $query->where('client_id', $clientid->id);
        }])->where('process_id',$new_process_id)->get();

        $process_activities=array();
        if(count($highest_step_id) > 0) {

            foreach ($highest_step_id as $highest_step) {
                if($clientid->isStepActivitiesCompleted(Step::find($highest_step->id))) {
                    foreach ($highest_step->process->activities as $activity){
                        //get the step ids of the process if the client was previously in the selected process
                        foreach ($activity->actionable['data'] as $data) {
                            //push the step id into the array
                            array_push($process_activities,["step_id" => $highest_step->id,"name" => $highest_step->name,"order" => $highest_step->order]);
                        }
                    }
                }
            }


            //sort the array in descending order
            usort($process_activities, function ($item1, $item2) {
                return $item2['order'] <=> $item1['order'];
            });

        }

        $process_first_step = Step::where('process_id',$new_process_id)->orderBy('order','asc')->first();

        $new_step_id = ( isset($process_activities[0]) ? $process_activities[0]["step_id"] : $process_first_step->id );

        //update client with new process id and step id
        //$client = Client::find($clientid);
        $clientid->process_id = $new_process_id;
        $clientid->step_id = $new_step_id;
        $clientid->save();

        return response()->json();
    }

    public function keepClientProcess(Request $request, Client $clientid, Process $processid,$newprocess){
        $new_process_id = $newprocess;

        //check if selected process has any activities for this client
        $highest_step_id = Step::with(['activities.actionable.data' => function ($query) use ($clientid) {
            $query->where('client_id', $clientid->id);
        }])->where('process_id',$new_process_id)->get();

        $process_activities=array();
        if(count($highest_step_id) > 0) {

               foreach ($highest_step_id as $highest_step) {
                   if($clientid->isStepActivitiesCompleted(Step::find($highest_step->id))) {
                       foreach ($highest_step->process->activities as $activity){
                           //get the step ids of the process if the client was previously in the selected process
                           foreach ($activity->actionable['data'] as $data) {
                               //push the step id into the array

                               //return $data;
                               if($data["client_id"] != null && $data["data"] != null) {
                                   array_push($process_activities, ["step_id" => $highest_step->id, "name" => $highest_step->name, "order" => $highest_step->order]);
                               }
                           }
                       }
                   }
                }


            //sort the array in descending order
            usort($process_activities, function ($item1, $item2) {
                return $item2['order'] <=> $item1['order'];
            });

        }

        $process_first_step = Step::where('process_id',$new_process_id)->orderBy('order','asc')->first();

        $new_step_id = ( isset($process_activities[0]) && count($process_activities[0]) > 0 ? $process_activities[0]["step_id"] : $process_first_step->id );

        ClientProcess::where('client_id',$clientid->id)->where('process_id',$processid->id)->update(['active'=>0]);

        //update client with new process id and step id
        //$client = Client::find($clientid);
        $clientid->process_id = $new_process_id;
        $clientid->step_id = $new_step_id;
        $clientid->save();

        $check = ClientProcess::where('client_id',$clientid->id)->where('process_id',$processid->id)->get();

        if($check) {
            $client_new = new ClientProcess();
            $client_new->client_id = $clientid->id;
            $client_new->process_id = $new_process_id;
            $client_new->step_id = $new_step_id;
            $client_new->active = 1;
            $client_new->save();
        }

        return response()->json(['new_step_id'=>$new_step_id]);
    }

    public function follow(Client $client, StoreFollowRequest $request)
    {
        if ($request->input('follow')) {
            ClientUser::insert([
                'client_id' => $client->id,
                'user_id' => auth()->id(),
                'created_at' => now()
            ]);
        } else {
            ClientUser::where('client_id', $client->id)->where('user_id', auth()->id())->delete();
        }

        return redirect()->back()->with('flash_success', 'Follow status updated successfully.');
    }

    public function complete($client_id,$step_id,$newdate)
    {
        $client = Client::find($client_id);
        $client->completed_at = $newdate;
        $client->step_id = $step_id;
        $client->save();

        return response()->json();
    }

    public function uncomplete($client_id,$step_id)
    {
        $client = Client::find($client_id);
        $client->completed_at = null;
        $client->step_id = $step_id;
        $client->save();

        return response()->json();
    }

    public function changecomplete($client_id,$step_id,$newdate)
    {
        $client = Client::find($client_id);
        $client->completed_at = $newdate;
        $client->step_id = $step_id;
        $client->save();

        return response()->json();
    }

    public function viewTemplate(Client $client, Template $template)
    {

        $processed_template = $this->processTemplate($client, $template->id, $template->file, $template->name);

        return response()->download(storage_path('app/templates/' . $processed_template));

    }

    public function viewDocument(Client $client, Document $document)
    {

        $processed_document = $this->processDocument($client, $document->id, $document->file, $document->name);

        return response()->download(storage_path('app/documents/' . $processed_document));

    }

    public function sendTemplate(Client $client, Activity $activity, Request $request)
    {
        $template = Template::find($request->input('template_file'));

        $processed_templates = array();
        $processed_templates[0]['file'] = $this->processTemplate($client, $template->id, $template->file, $template->name);
        $processed_templates[0]['type'] = 'template';

        $actionable_template_email = $activity->actionable;

        ActionableTemplateEmailData::where('email',$client->email)->where('actionable_template_email_id',$actionable_template_email->id)->where('client_id',$client->id)->delete();

        ActionableTemplateEmailData::insert([
            'template_id' => $template->id,
            'email' => $client->email,
            'actionable_template_email_id' => $actionable_template_email->id,
            'client_id' => $client->id,
            'user_id' => auth()->id(),
            'duration' => 120,
            //'file' => $processed_template, To Do Add file name
        ]);

        if($request->session()->has('email_template') && $request->session()->get('email_template') != null) {
            $email_subject = $request->input('subject');
            $email_content = $request->session()->get('email_template');
        } else {
            $email_subject = $request->input('subject');

            if($request->has('email_content') && $request->input('email_content') != ""){
                $email_content = $request->input('email_content');
            } else {
                $email_content = EmailTemplate::where('id', $request->input('template_email'))->first()->email_content;
            }
        }

        $emails = explode(",", $request->input('email'));

        foreach ($emails as $email):
            Mail::to(trim($email))->send(new TemplateMail($client, $processed_templates, $email_subject, $email_content));

        endforeach;

        if($request->session()->has('email_template')){
            $request->session()->forget('email_template');
        }

        return response()->json(['success' => 'Template sent successfully.']);

    }

    public function sendDocument(Client $client, Activity $activity, Request $request)
    {
        $documents= Document::find($request->input('document_file'));

        $processed_documents= array();
        $processed_documents[0]['file'] = $this->processDocument($client, $documents->id, $documents->file, $documents->name);
        $processed_documents[0]['type'] = 'document';
//dd($this->processDocument($client, $documents->file));
        $actionable_documents_email = $activity->actionable;

        ActionableDocumentEmailData::where('email',$client->email)->where('actionable_document_email_id',$actionable_documents_email->id)->where('client_id',$client->id)->delete();

        ActionableDocumentEmailData::insert([
            'document_id' => $documents->id,
            'email' => $client->email,
            'actionable_document_email_id' => $actionable_documents_email->id,
            'client_id' => $client->id,
            'user_id' => auth()->id(),
            'duration' => 120,
            //'file' => $processed_template, To Do Add file name
        ]);

        if($request->session()->has('email_template') && $request->session()->get('email_template') != null) {
            $email_subject = $request->input('subject');
            $email_content = $request->session()->get('email_template');
        } else {
            $email_subject = $request->input('subject');

            if($request->has('email_content') && $request->input('email_content') != ""){
                $email_content = $request->input('email_content');
            } else {
                $email_content = EmailTemplate::where('id', $request->input('template_email'))->first()->email_content;
            }
        }

        $emails = explode(",", $request->input('email'));

        foreach ($emails as $email):
            Mail::to(trim($email))->send(new TemplateMail($client, $processed_documents, $email_subject, $email_content));
        endforeach;

        return response()->json(['success' => 'Template sent successfully.']);
    }

    public function sendDocuments(Client $client, Activity $activity, Request $request, Step $step)
    {
        //Todo
        $actionable_template_email = $activity->actionable;

        ActionableMultipleAttachmentData::where('email', $client->email)->where('actionable_ma_id', $actionable_template_email->id)->where('client_id', $client->id)->delete();
        //Send to all templates
        $processed_templates = Array();
        $counter = 0;
        if($request->input('templates')) {
            $templates = explode(',',$request->input('templates'));
            foreach ($templates as $template_id):
                if($template_id != null && $template_id != '' && $template_id > 0) {
                    $template = Template::find($template_id);

                    $processed_templates[$counter]['file'] = $this->processTemplate($client, $template->id, $template->file, $template->name);
                    $processed_templates[$counter]['type'] = 'template';

                    ActionableMultipleAttachmentData::insert([
                        'template_id' => $template->id,
                        'email' => $request->input('email'),
                        'actionable_ma_id' => $actionable_template_email->id,
                        'client_id' => $client->id,
                        'user_id' => auth()->id(),
                        'duration' => 120,
                        'attachment_type' => 'template'
                        //'file' => $processed_template, To Do Add file name
                    ]);
                    $counter++;
                }
            endforeach;
        }

        if($request->input('documents')) {
            $documents = explode(',',$request->input('documents'));
            foreach ($documents as $document_id):
                $document = Document::find($document_id);
                $processed_templates[$counter]['file'] = $this->processDocument($client, $document->id, $document->file, $document->name);
                $processed_templates[$counter]['type'] = 'document';

                ActionableMultipleAttachmentData::insert([
                    'template_id' => $document->id,
                    'email' => $request->input('email'),
                    'actionable_ma_id' => $actionable_template_email->id,
                    'client_id' => $client->id,
                    'user_id' => auth()->id(),
                    'duration' => 120,
                    'attachment_type' => 'document'
                    //'file' => $processed_template, To Do Add file name
                ]);
                $counter++;
            endforeach;
        }

        //$email_signature = EmailSignature::where('user_id','=',auth()->id())->get();

        if($request->session()->has('email_template') && $request->session()->get('email_template') != null) {
            $email_subject = $request->input('subject');
            $email_content = $request->session()->get('email_template');
        } else {
            $email_subject = $request->input('subject');
            if($request->has('email_content') && $request->input('email_content') != ""){
                $email_content = $request->input('email_content');
            } else {
                $email_content = EmailTemplate::where('id', $request->input('template_email'))->first()->email_content;
            }
        }


            /*$a = new ActionableMultipleAttachmentData();
            $a->template_id = $request->input('template_email');
            $a->email = $request->input('email');
            $a->actionable_ma_id = $activity->actionable_id;
            $a->client_id = $client->id;
            $a->user_id = auth()->id();
            $a->duration = '120';
            $a->save();*/
            //'file' => $processed_template, To Do Add file name

        $emails = explode(",", $request->input('email'));

        foreach ($emails as $email):
            //Mail::to(trim($email))->send(new TemplateMail($client, $processed_templates, $email_content,$email_signature));
            Mail::to(trim($email))->send(new TemplateMail($client, $processed_templates, $email_subject, $email_content));
        endforeach;

        $request->session()->forget('email_template');

        return response()->json(['success' => 'Documents sent successfully.', 'docs' => $processed_templates]);
    }

    /*
    * @param Client
    * @param template file
    * @return the new generated processed template
    * Use to process a docx document, if document type is not .docx, return as it is
    */
    public function processTemplate(Client $client, $template_id, $template_file, $template_name)
    {

        $filename = $template_name;
        $ext = pathinfo($template_file, PATHINFO_EXTENSION);

        //Only process docx files for now
        if ($ext == "docx") {
            return $this->processWordTemplate($client, $template_file, $template_name);
        } elseif($ext == "pptx"){
            return $this->processPowerpointTemplate($client->id,$client->process_id,$template_id);
        } elseif($ext == "pdf"){
            return $this->processPdfTemplate($client->id,$client->process_id,$template_id);
        } else {
            return $template_file;
        }
    }

    public function processWordTemplate(Client $client, $template_file, $template_name)
    {

        $filename = $template_name;
        $ext = pathinfo($template_file, PATHINFO_EXTENSION);

        $client->load('referrer', 'introducer', 'business_unit');

        $templateProcessor = new TemplateProcessor(storage_path('app/templates/' . $template_file));
        $templateProcessor->setValue('date', date("Y/m/d"));
        $templateProcessor->setValue(
            ['client.first_name', 'client.last_name', 'client.email', 'client.contact', 'client.company', 'client.email', 'client.id_number', 'client.company_registration_number', 'client.cif_code', 'client.business_unit','client.committee','client.project','client.trigger_type','client.case_number','client.qa_start_date','client.qa_end_date','client.instruction_date','client.assigned_date','client.completed_date','client.out_of_scope'],
            [$client->first_name, $client->last_name, $client->email, $client->contact, $client->company, $client->email, $client->id_number, $client->company_registration_number, $client->cif_code, ($client->business_unit_id > 0 ? $client->business_unit->name : ''), ($client->committee_id ? $client->committee->name : ''), ($client->project_id > 0 ? $client->project->name : ''), ($client->trigger_type_id > 0 ? $client->trigger->name : ''),$client->case_number,$client->qa_start_date,$client->qa_end_date,$client->instruction_date,$client->assigned_date,$client->completed_date,($client->out_of_scope == '1' ? 'Yes' : 'No')]
        );

        $templateProcessor->setValue(
            ['introducer.first_name', 'introducer.last_name', 'introducer.email', 'introducer.contact'],
            isset($client->introducer) ?
                [
                    $client->introducer->first_name,
                    $client->introducer->last_name,
                    $client->introducer->email,
                    $client->introducer->contact
                ] :
                ['', '', '', '']
        );

        $client_id = $client->id;
        $process_id = $client->process_id;
        $var_array = array();
        $value_array = array();

        $steps = Step::with(['activities.actionable.data'=>function ($q) use ($client_id){
            $q->where('client_id',$client_id);
        }])->where('process_id',$process_id)->get();

        foreach($steps as $step) {

            foreach ($step["activities"] as $activity) {
                $var = '';
                switch ($activity['actionable_type']){
                    case 'App\ActionableDropdown':
                        $var = 'activity.'.strtolower(str_replace(' ', '_', $activity->name));
                        array_push($var_array,$var);
                        break;

                    default:
                        $var = 'activity.'.strtolower(str_replace(' ', '_', $activity->name));
                        array_push($var_array, $var);
                        break;
                }

                if (isset($activity["actionable"]->data) && count($activity["actionable"]->data) > 0) {
                    foreach ($activity["actionable"]->data as $value) {

                        switch ($activity['actionable_type']){
                            case 'App\ActionableDropdown':

                                $data = ActionableDropdownItem::where('id',$value->actionable_dropdown_item_id)->first();
                                if($data){
                                    array_push($value_array, $data["name"]);
                                } else {
                                    array_push($value_array, '');
                                }
                                break;
                            case 'App\ActionableBoolean':
                                $items = ActionableBooleanData::where('client_id',$client_id)->where('actionable_boolean_id',$value->actionable_boolean_id)->first();

                                if($items){
                                    array_push($value_array, ($items->data == '0' ? 'No' : 'Yes'));
                                } else {
                                    array_push($value_array, '');
                                }

                                break;
                            default:

                                array_push($value_array, $value->data);
                                break;
                        }
                    }
                } else {

                    switch ($activity['actionable_type']){
                        case 'App\ActionableDropdown':
                            $items = ActionableDropdownItem::where('actionable_dropdown_id',$activity["actionable_id"])->get();
                            if($items){
                                foreach ($items as $item) {

                                    array_push($value_array, '');

                                }
                            } else {
                                array_push($value_array, '');
                            }

                            break;
                        default:

                            array_push($value_array, '');
                            break;
                    }
                }
            }
        }

        $templateProcessor->setValue(
            $var_array,$value_array
        );

        //Create directory to store processed templates, for future reference or to check what was sent to the client
        $processed_template_path = 'processedtemplates/' . date("Y") . DIRECTORY_SEPARATOR . date("m");
        if (!File::exists(storage_path('app/templates/' . $processed_template_path))) {
            Storage::makeDirectory('templates/' . $processed_template_path);
        }

        $processed_template = $processed_template_path . DIRECTORY_SEPARATOR . str_replace(' ','_',strtolower($filename)) . "_" . $client->id . "_" . date("Y_m_d_H_i_s") . "_" . uniqid() . ".docx";

        $templateProcessor->saveAs(storage_path('app/templates/' . $processed_template));

        return $processed_template;

    }

    public function processPowerpointTemplate($client_id,$process_id,$template_id)
    {
        // Grab the client
        $client = Client::where('id',$client_id);

        // Client details in an array
        $client = $client->first()->toArray();

        // What will eventually be sent to the report
        $output = [];
        $processData = [];

        // We have a client
        if($client) {
            // loop over steps to get the activity names, storing them in an assoc. array
            $steps = Step::with(['process'=> function($q) use ($process_id) {
                $q->where('id',$process_id);
            }])->orderBy('id')->get();

            foreach($steps as $step) {
                $activities = Activity::with(['actionable.data'=>function($query) use ($client){
                    $query->where('client_id',$client["id"])->orderBy('created_at','desc');
                }])->get();

                foreach($activities as $activity) {
                    if (strpos($activity['actionable_type'], 'Actionable') !== false) {
                        $completed_activity_clients_data = null;
                        switch ($activity['actionable_type']) {
                            case 'App\ActionableBoolean':
                                $filter_data = ['' => 'All', 0 => 'No', 1 => 'Yes', 2 => 'Not Completed'];
                                $completed_activity_clients_data = ActionableBooleanData::where('actionable_boolean_id', $activity['actionable_id'])
                                    ->select('client_id', 'data')
                                    ->distinct()
                                    ->pluck('data', 'client_id');
                                break;
                            case 'App\ActionableDate':
                                $filter_data = ['' => 'All', 1 => 'Completed', 0 => 'Not Completed'];
                                $completed_activity_clients_data = ActionableDateData::where('actionable_date_id', $activity['actionable_id'])
                                    ->select('client_id', 'data')
                                    ->distinct()
                                    ->pluck('data', 'client_id');
                                break;
                            case 'App\ActionableText':
                                $filter_data = ['' => 'All', 1 => 'Completed', 0 => 'Not Completed'];
                                $completed_activity_clients_data = ActionableTextData::where('actionable_text_id', $activity['actionable_id'])
                                    ->select('client_id', 'data')
                                    ->distinct()
                                    ->pluck('data', 'client_id');
                                break;
                            case 'App\ActionableTextarea':
                                $filter_data = ['' => 'All', 1 => 'Completed', 0 => 'Not Completed'];
                                $completed_activity_clients_data = ActionableTextareaData::where('actionable_textarea_id', $activity['actionable_id'])
                                    ->select('client_id', 'data')
                                    ->distinct()
                                    ->pluck('data', 'client_id');
                                break;
                            case 'App\ActionableDropdown':
                                $filter_data = ActionableDropdownItem::where('actionable_dropdown_id', $activity['actionable_id'])
                                    ->select('name', 'id')
                                    ->distinct()
                                    ->pluck('name', 'id')
                                    ->prepend('All', 0);

                                /*if ($request->has('activity') && $request->input('activity') != '') {*/
                                $completed_activity_clients_data = ActionableDropdownData::where('actionable_dropdown_id', $activity['actionable_id'])

                                    ->select('client_id', 'actionable_dropdown_item_id')
                                    ->distinct()
                                    //->get()->toArray();
                                    ->pluck('actionable_dropdown_item_id', 'client_id');
                                /*} else {
                                    $completed_activity_clients_data = ActionableDropdownData::where('actionable_dropdown_id', $activity['actionable_id'])
                                        ->select('client_id', 'actionable_dropdown_item_id')
                                        ->distinct()
                                        //->get()->toArray();
                                        ->pluck('actionable_dropdown_item_id', 'client_id');
                                }*/

                                // $tmp_filter_data2 = $filter_data->toArray();
                                // foreach($tmp_filter_data2 as $key=>$value):
                                //     array_push($tmp_filter_data, $key);
                                // endforeach;
                                break;
                            case 'App\ActionableDocument':
                                $filter_data = ['' => 'All', 1 => 'Completed', 0 => 'Not Completed'];
                                $completed_activity_clients_data = ActionableDocumentData::where('actionable_document_id', $activity['actionable_id'])
                                    ->select('client_id', 'actionable_document_id')
                                    ->distinct()
                                    ->pluck('actionable_document_id', 'client_id');
                                break;
                            case 'App\ActionableTemplateEmail':
                                $filter_data = Template::orderBy('name')
                                    ->select('name', 'id')
                                    ->distinct()
                                    ->pluck('name', 'id')
                                    ->prepend('All', 0);
                                $completed_activity_clients_data = ActionableTemplateEmailData::where('actionable_template_email_id', $activity['actionable_id'])
                                    ->select('client_id', 'template_id')
                                    ->distinct()
                                    ->pluck('template_id', 'client_id');
                                break;
                            case 'App\ActionableNotification':
                                $filter_data = ['' => 'All', 1 => 'Sent', 0 => 'Not Sent'];
                                $completed_activity_clients_data = ActionableNotificationData::where('actionable_notification_id', $activity['actionable_id'])
                                    ->select('client_id', 'actionable_notification_id')
                                    ->distinct()
                                    ->pluck('actionable_notification_id', 'client_id');
                                break;
                            case 'App\ActionableMultipleAttachment':
                                $filter_data = Template::orderBy('name')
                                    ->select('name', 'id')
                                    ->distinct()
                                    ->pluck('name', 'id')
                                    ->prepend('All', 0);
                                $completed_activity_clients_data = ActionableMultipleAttachmentData::where('actionable_ma_id', $activity['actionable_id'])
                                    ->select('client_id', 'template_id')
                                    ->distinct()
                                    ->pluck('template_id', 'client_id');
                                break;
                            default:
                                //todo capture defaults
                                break;
                        }

                        $data_value = isset($completed_activity_clients_data[$client['id']]) && trim($completed_activity_clients_data[$client['id']]) != '' ? $completed_activity_clients_data[$client['id']] : '';
                        $completed_value = '';
                        $selected_drop_down_names = '';

                        $data = '';
                        $yn_value = '';
                        switch ($activity['actionable_type']) {
                            case 'App\ActionableBoolean':
                                $completed_value = isset($completed_activity_clients_data[$client['id']]) ? 'Yes' : 'No';
                                $activity_value = isset($completed_activity_clients_data[$client['id']]) ? $completed_activity_clients_data[$client['id']] : 2;
                                if (isset($completed_activity_clients_data[$client['id']]) && $completed_activity_clients_data[$client['id']] == '1') {
                                    $yn_value = "Yes";
                                }
                                if (isset($completed_activity_clients_data[$client['id']]) && $completed_activity_clients_data[$client['id']] == '0') {
                                    $yn_value = "No";
                                }
                                break;
                            case 'App\ActionableDate':
                                $completed_value = isset($completed_activity_clients_data[$client['id']]) && trim($completed_activity_clients_data[$client['id']]) != '' ? $completed_activity_clients_data[$client['id']] : '';
                                $activity_value = isset($completed_activity_clients_data[$client['id']]) && trim($completed_activity_clients_data[$client['id']]) != '' ? 1 : 0;
                                break;
                            case 'App\ActionableText':
                                $completed_value = isset($completed_activity_clients_data[$client['id']]) && trim($completed_activity_clients_data[$client['id']]) != '' ? $completed_activity_clients_data[$client["id"]] : '';
                                $activity_value = isset($completed_activity_clients_data[$client['id']]) && trim($completed_activity_clients_data[$client['id']]) != '' ? 1 : 0;
                                break;
                            case 'App\ActionableTextarea':
                                $completed_value = isset($completed_activity_clients_data[$client['id']]) && trim($completed_activity_clients_data[$client['id']]) != '' ? $completed_activity_clients_data[$client["id"]] : '';
                                $activity_value = isset($completed_activity_clients_data[$client['id']]) && trim($completed_activity_clients_data[$client['id']]) != '' ? 1 : 0;
                                break;
                            case 'App\ActionableDropdown':
                                $data_value = '';
                                /*if ($request->has('s') && $request->input('s') != '') {
                                    $selected_drop_down_items = ActionableDropdownData::with('item')->where('actionable_dropdown_id', $activity['actionable_id'])
                                        ->where('client_id', $client['id'])
                                        ->select('actionable_dropdown_item_id')
                                        ->distinct()
                                        ->get()->toArray();

                                    foreach ($selected_drop_down_items as $key => $selected_drop_down_item):
                                        //dd($selected_drop_down_item);
                                        if (in_array($selected_drop_down_item['actionable_dropdown_item_id'], $tmp_filter_data)) {
                                            if ($key == sizeof($selected_drop_down_items) - 1)
                                                $data_value .= $selected_drop_down_item['item']['name'];
                                            else
                                                $data_value .= $selected_drop_down_item['item']['name'] . ', ';
                                        }
                                    endforeach;
                                }*/
                                $data = ActionableDropdownData::with('item')->where('client_id', $client['id'])->where('actionable_dropdown_id', $activity['actionable_id'])->first();
                                //dd($data->item->name);
                                $activity_data_value = isset($completed_activity_clients_data[$client['id']]) ? (isset($data->item->name) && $data->item->name != null ? $data->item->name : '') : '';
                                $completed_value = isset($completed_activity_clients_data[$client['id']]) ? (isset($data->item->name) && $data->item->name != null ? $data->item->name : '') : '';
                                $activity_value = isset($completed_activity_clients_data[$client['id']]) ? $completed_activity_clients_data[$client['id']] : 0;
                                break;
                            case 'App\ActionableDocument':
                                $completed_value = isset($completed_activity_clients_data[$client['id']]) && trim($completed_activity_clients_data[$client['id']]) != '' ? 'Completed' : 'Not Completed';

                                $activity_value = isset($completed_activity_clients_data[$client['id']]) ? $completed_activity_clients_data[$client['id']] : 0;
                                break;
                            case 'App\ActionableTemplateEmail':
                                $completed_value = isset($completed_activity_clients_data[$client['id']]) && trim($completed_activity_clients_data[$client['id']]) != '' ? 'Completed' : 'Not Completed';
                                $activity_value = isset($completed_activity_clients_data[$client['id']]) ? $completed_activity_clients_data[$client['id']] : 0;
                                break;
                            case 'App\ActionableNotification':
                                $completed_value = isset($completed_activity_clients_data[$client['id']]) && trim($completed_activity_clients_data[$client['id']]) != '' ? 'Completed' : 'Not Completed';
                                $activity_value = isset($completed_activity_clients_data[$client['id']]) && trim($completed_activity_clients_data[$client['id']]) != '' ? 1 : 0;
                                break;
                            case 'App\ActionableMultipleAttachment':
                                $completed_value = isset($completed_activity_clients_data[$client['id']]) && trim($completed_activity_clients_data[$client['id']]) != '' ? 'Completed' : 'Not Completed';
                                $activity_value = isset($completed_activity_clients_data[$client['id']]) ? $completed_activity_clients_data[$client['id']] : 0;
                                break;
                            default:
                                //todo capture defaults
                                break;

                        }


                        $processData[1] [] = ['name' => strtolower(str_replace(' ', '_', $activity['name'])),
                            'data' => $completed_value];


                    }
                }
            }
        }

        // Get the pptx template
        $template = Template::where('id', $template_id)->first();

        $presentation = new Presentation($template->file, ['client' => $client, 'activities' => $processData[1]]);

        // do whatevs
        $presentation->run();
        $downloadFile = $presentation->getDownloadPath();

        $headers = array(
            'Content-Type: application/vnd.ms-powerpoint',
        );

        //Create directory to store processed templates, for future reference or to check what was sent to the client
        $processed_template_path = 'processedtemplates/' . date("Y") . DIRECTORY_SEPARATOR . date("m");
        if (!File::exists(storage_path('app/templates/' . $processed_template_path))) {
            Storage::makeDirectory('templates/' . $processed_template_path);
        }

        $processed_template = $processed_template_path . DIRECTORY_SEPARATOR . str_replace(' ','_',strtolower($template->name)) . "_" . $client["id"] . "_" . date("Y_m_d_H_i_s") . "_" . uniqid() . ".pptx";

        $destinationPath= storage_path()."/app/templates/test.pptx";
        $success = \File::copy($downloadFile,storage_path('/app/templates/'.$processed_template));

        return $processed_template;
    }

    public function processDocument(Client $client, $template_id, $template_file, $document_name)
    {
        $filename = $document_name;
        $ext = pathinfo($template_file, PATHINFO_EXTENSION);

        //Only process docx files for now
        if ($ext == "docx") {
            return $this->processWordDocument($client, $template_file, $document_name);
        } elseif($ext == "pptx"){
            return $this->processPowerpointDocument($client->id,$client->process_id,$template_id);
        } else {
            return $template_file;
        }
    }

    public function processWordDocument(Client $client, $template_file, $document_name)
    {
        $filename = $document_name;
        $ext = pathinfo($template_file, PATHINFO_EXTENSION);

        //Only process docx files for now
        if ($ext != "docx") {
            return $template_file;
        }

        $client->load('referrer', 'introducer','business_unit');

        $templateProcessor = new TemplateProcessor(storage_path('app/documents/' . $template_file));
        $templateProcessor->setValue('date', date("Y/m/d"));
        $templateProcessor->setValue(
            ['client.first_name', 'client.last_name', 'client.email', 'client.contact', 'client.company', 'client.email', 'client.id_number', 'client.company_registration_number', 'client.cif_code', 'client.business_unit'],
            [$client->first_name, $client->last_name, $client->email, $client->contact, $client->company, $client->email, $client->id_number, $client->company_registration_number, $client->cif_code, $client->business_unit->name]
        );

        $templateProcessor->setValue(
            ['referrer.first_name', 'referrer.last_name', 'referrer.email', 'referrer.avatar'],
            isset($client->referrer) ?
                [
                    $client->referrer->first_name,
                    $client->referrer->last_name,
                    $client->referrer->email,
                    $client->referrer->contact
                ] :
                ['', '', '', '']
        );

        $templateProcessor->setValue(
            ['introducer.first_name', 'introducer.last_name', 'introducer.email', 'introducer.contact'],
            isset($client->introducer) ?
                [
                    $client->introducer->first_name,
                    $client->introducer->last_name,
                    $client->introducer->email,
                    $client->introducer->contact
                ] :
                ['', '', '', '']
        );

        $client_id = $client->id;
        $process_id = $client->process_id;
        $var_array = array();
        $value_array = array();

        $steps = Step::with(['activities.actionable.data'=>function ($q) use ($client_id){
            $q->where('client_id',$client_id);
        }])->where('process_id',$process_id)->get();

        foreach($steps as $step) {

            foreach ($step["activities"] as $activity) {
                $var = '';
                switch ($activity['actionable_type']){
                    case 'App\ActionableDropdown':
                        $var = 'activity.'.strtolower(str_replace(' ', '_', $activity->name));
                        array_push($var_array,$var);
                        break;

                    default:
                        $var = 'activity.'.strtolower(str_replace(' ', '_', $activity->name));
                        array_push($var_array, $var);
                        break;
                }

                if (isset($activity["actionable"]->data) && count($activity["actionable"]->data) > 0) {
                    foreach ($activity["actionable"]->data as $value) {

                        switch ($activity['actionable_type']){
                            case 'App\ActionableDropdown':

                                $data = ActionableDropdownItem::where('id',$value->actionable_dropdown_item_id)->first();
                                if($data){
                                    array_push($value_array, $data["name"]);
                                } else {
                                    array_push($value_array, '');
                                }
                                break;
                            case 'App\ActionableBoolean':
                                $items = ActionableBooleanData::where('client_id',$client_id)->where('actionable_boolean_id',$value->actionable_boolean_id)->first();

                                if($items){
                                    array_push($value_array, ($items->data == '0' ? 'No' : 'Yes'));
                                } else {
                                    array_push($value_array, '');
                                }

                                break;
                            default:

                                array_push($value_array, $value->data);
                                break;
                        }
                    }
                } else {

                    switch ($activity['actionable_type']){
                        case 'App\ActionableDropdown':
                            $items = ActionableDropdownItem::where('actionable_dropdown_id',$activity["actionable_id"])->get();
                            if($items){
                                foreach ($items as $item) {

                                    array_push($value_array, '');

                                }
                            } else {
                                array_push($value_array, '');
                            }

                            break;
                        default:

                            array_push($value_array, '');
                            break;
                    }
                }
            }
        }

        $templateProcessor->setValue(
            $var_array,$value_array
        );

        //Create directory to store processed templates, for future reference or to check what was sent to the client
        $processed_template_path = 'processeddocuments/' . date("Y") . DIRECTORY_SEPARATOR . date("m");
        if (!File::exists(storage_path('app/documents/' . $processed_template_path))) {
            Storage::makeDirectory('documents/' . $processed_template_path);
        }

        $processed_template = $processed_template_path . DIRECTORY_SEPARATOR . str_replace(' ','_',strtolower($filename)) . "_" . $client->id . "_" . date("Y_m_d_H_i_s") . "_" . uniqid() . ".docx";

        $templateProcessor->saveAs(storage_path('app/documents/' . $processed_template));

        return $processed_template;

    }

    public function processPowerpointDocument($client_id,$process_id,$template_id)
    {
        // Grab the client
        $client = Client::where('id',$client_id);

        // Client details in an array
        $client = $client->first()->toArray();

        // What will eventually be sent to the report
        $output = [];
        $processData = [];

        // We have a client
        if($client) {
            // loop over steps to get the activity names, storing them in an assoc. array
            $steps = Step::with(['process'=> function($q) use ($process_id) {
                $q->where('id',$process_id);
            }])->orderBy('id')->get();

            foreach($steps as $step) {
                $activities = Activity::with(['actionable.data'=>function($query) use ($client){
                    $query->where('client_id',$client["id"])->orderBy('created_at','desc');
                }])->get();

                foreach($activities as $activity) {
                    if (strpos($activity['actionable_type'], 'Actionable') !== false) {
                        $completed_activity_clients_data = null;
                        switch ($activity['actionable_type']) {
                            case 'App\ActionableBoolean':
                                $filter_data = ['' => 'All', 0 => 'No', 1 => 'Yes', 2 => 'Not Completed'];
                                $completed_activity_clients_data = ActionableBooleanData::where('actionable_boolean_id', $activity['actionable_id'])
                                    ->select('client_id', 'data')
                                    ->distinct()
                                    ->pluck('data', 'client_id');
                                break;
                            case 'App\ActionableDate':
                                $filter_data = ['' => 'All', 1 => 'Completed', 0 => 'Not Completed'];
                                $completed_activity_clients_data = ActionableDateData::where('actionable_date_id', $activity['actionable_id'])
                                    ->select('client_id', 'data')
                                    ->distinct()
                                    ->pluck('data', 'client_id');
                                break;
                            case 'App\ActionableText':
                                $filter_data = ['' => 'All', 1 => 'Completed', 0 => 'Not Completed'];
                                $completed_activity_clients_data = ActionableTextData::where('actionable_text_id', $activity['actionable_id'])
                                    ->select('client_id', 'data')
                                    ->distinct()
                                    ->pluck('data', 'client_id');
                                break;
                            case 'App\ActionableTextarea':
                                $filter_data = ['' => 'All', 1 => 'Completed', 0 => 'Not Completed'];
                                $completed_activity_clients_data = ActionableTextareaData::where('actionable_textarea_id', $activity['actionable_id'])
                                    ->select('client_id', 'data')
                                    ->distinct()
                                    ->pluck('data', 'client_id');
                                break;
                            case 'App\ActionableDropdown':
                                $filter_data = ActionableDropdownItem::where('actionable_dropdown_id', $activity['actionable_id'])
                                    ->select('name', 'id')
                                    ->distinct()
                                    ->pluck('name', 'id')
                                    ->prepend('All', 0);

                                /*if ($request->has('activity') && $request->input('activity') != '') {*/
                                $completed_activity_clients_data = ActionableDropdownData::where('actionable_dropdown_id', $activity['actionable_id'])

                                    ->select('client_id', 'actionable_dropdown_item_id')
                                    ->distinct()
                                    //->get()->toArray();
                                    ->pluck('actionable_dropdown_item_id', 'client_id');
                                /*} else {
                                    $completed_activity_clients_data = ActionableDropdownData::where('actionable_dropdown_id', $activity['actionable_id'])
                                        ->select('client_id', 'actionable_dropdown_item_id')
                                        ->distinct()
                                        //->get()->toArray();
                                        ->pluck('actionable_dropdown_item_id', 'client_id');
                                }*/

                                // $tmp_filter_data2 = $filter_data->toArray();
                                // foreach($tmp_filter_data2 as $key=>$value):
                                //     array_push($tmp_filter_data, $key);
                                // endforeach;
                                break;
                            case 'App\ActionableDocument':
                                $filter_data = ['' => 'All', 1 => 'Completed', 0 => 'Not Completed'];
                                $completed_activity_clients_data = ActionableDocumentData::where('actionable_document_id', $activity['actionable_id'])
                                    ->select('client_id', 'actionable_document_id')
                                    ->distinct()
                                    ->pluck('actionable_document_id', 'client_id');
                                break;
                            case 'App\ActionableTemplateEmail':
                                $filter_data = Template::orderBy('name')
                                    ->select('name', 'id')
                                    ->distinct()
                                    ->pluck('name', 'id')
                                    ->prepend('All', 0);
                                $completed_activity_clients_data = ActionableTemplateEmailData::where('actionable_template_email_id', $activity['actionable_id'])
                                    ->select('client_id', 'template_id')
                                    ->distinct()
                                    ->pluck('template_id', 'client_id');
                                break;
                            case 'App\ActionableNotification':
                                $filter_data = ['' => 'All', 1 => 'Sent', 0 => 'Not Sent'];
                                $completed_activity_clients_data = ActionableNotificationData::where('actionable_notification_id', $activity['actionable_id'])
                                    ->select('client_id', 'actionable_notification_id')
                                    ->distinct()
                                    ->pluck('actionable_notification_id', 'client_id');
                                break;
                            case 'App\ActionableMultipleAttachment':
                                $filter_data = Template::orderBy('name')
                                    ->select('name', 'id')
                                    ->distinct()
                                    ->pluck('name', 'id')
                                    ->prepend('All', 0);
                                $completed_activity_clients_data = ActionableMultipleAttachmentData::where('actionable_ma_id', $activity['actionable_id'])
                                    ->select('client_id', 'template_id')
                                    ->distinct()
                                    ->pluck('template_id', 'client_id');
                                break;
                            default:
                                //todo capture defaults
                                break;
                        }

                        $data_value = isset($completed_activity_clients_data[$client['id']]) && trim($completed_activity_clients_data[$client['id']]) != '' ? $completed_activity_clients_data[$client['id']] : '';
                        $completed_value = '';
                        $selected_drop_down_names = '';

                        $data = '';
                        $yn_value = '';
                        switch ($activity['actionable_type']) {
                            case 'App\ActionableBoolean':
                                $completed_value = isset($completed_activity_clients_data[$client['id']]) ? 'Yes' : 'No';
                                $activity_value = isset($completed_activity_clients_data[$client['id']]) ? $completed_activity_clients_data[$client['id']] : 2;
                                if (isset($completed_activity_clients_data[$client['id']]) && $completed_activity_clients_data[$client['id']] == '1') {
                                    $yn_value = "Yes";
                                }
                                if (isset($completed_activity_clients_data[$client['id']]) && $completed_activity_clients_data[$client['id']] == '0') {
                                    $yn_value = "No";
                                }
                                break;
                            case 'App\ActionableDate':
                                $completed_value = isset($completed_activity_clients_data[$client['id']]) && trim($completed_activity_clients_data[$client['id']]) != '' ? $completed_activity_clients_data[$client['id']] : '';
                                $activity_value = isset($completed_activity_clients_data[$client['id']]) && trim($completed_activity_clients_data[$client['id']]) != '' ? 1 : 0;
                                break;
                            case 'App\ActionableText':
                                $completed_value = isset($completed_activity_clients_data[$client['id']]) && trim($completed_activity_clients_data[$client['id']]) != '' ? $completed_activity_clients_data[$client["id"]] : '';
                                $activity_value = isset($completed_activity_clients_data[$client['id']]) && trim($completed_activity_clients_data[$client['id']]) != '' ? 1 : 0;
                                break;
                            case 'App\ActionableTextarea':
                                $completed_value = isset($completed_activity_clients_data[$client['id']]) && trim($completed_activity_clients_data[$client['id']]) != '' ? $completed_activity_clients_data[$client["id"]] : '';
                                $activity_value = isset($completed_activity_clients_data[$client['id']]) && trim($completed_activity_clients_data[$client['id']]) != '' ? 1 : 0;
                                break;
                            case 'App\ActionableDropdown':
                                $data_value = '';
                                /*if ($request->has('s') && $request->input('s') != '') {
                                    $selected_drop_down_items = ActionableDropdownData::with('item')->where('actionable_dropdown_id', $activity['actionable_id'])
                                        ->where('client_id', $client['id'])
                                        ->select('actionable_dropdown_item_id')
                                        ->distinct()
                                        ->get()->toArray();

                                    foreach ($selected_drop_down_items as $key => $selected_drop_down_item):
                                        //dd($selected_drop_down_item);
                                        if (in_array($selected_drop_down_item['actionable_dropdown_item_id'], $tmp_filter_data)) {
                                            if ($key == sizeof($selected_drop_down_items) - 1)
                                                $data_value .= $selected_drop_down_item['item']['name'];
                                            else
                                                $data_value .= $selected_drop_down_item['item']['name'] . ', ';
                                        }
                                    endforeach;
                                }*/
                                $data = ActionableDropdownData::with('item')->where('client_id', $client['id'])->where('actionable_dropdown_id', $activity['actionable_id'])->first();
                                //dd($data->item->name);
                                $activity_data_value = isset($completed_activity_clients_data[$client['id']]) ? (isset($data->item->name) && $data->item->name != null ? $data->item->name : '') : '';
                                $completed_value = isset($completed_activity_clients_data[$client['id']]) ? (isset($data->item->name) && $data->item->name != null ? $data->item->name : '') : '';
                                $activity_value = isset($completed_activity_clients_data[$client['id']]) ? $completed_activity_clients_data[$client['id']] : 0;
                                break;
                            case 'App\ActionableDocument':
                                $completed_value = isset($completed_activity_clients_data[$client['id']]) && trim($completed_activity_clients_data[$client['id']]) != '' ? 'Completed' : 'Not Completed';

                                $activity_value = isset($completed_activity_clients_data[$client['id']]) ? $completed_activity_clients_data[$client['id']] : 0;
                                break;
                            case 'App\ActionableTemplateEmail':
                                $completed_value = isset($completed_activity_clients_data[$client['id']]) && trim($completed_activity_clients_data[$client['id']]) != '' ? 'Completed' : 'Not Completed';
                                $activity_value = isset($completed_activity_clients_data[$client['id']]) ? $completed_activity_clients_data[$client['id']] : 0;
                                break;
                            case 'App\ActionableNotification':
                                $completed_value = isset($completed_activity_clients_data[$client['id']]) && trim($completed_activity_clients_data[$client['id']]) != '' ? 'Completed' : 'Not Completed';
                                $activity_value = isset($completed_activity_clients_data[$client['id']]) && trim($completed_activity_clients_data[$client['id']]) != '' ? 1 : 0;
                                break;
                            case 'App\ActionableMultipleAttachment':
                                $completed_value = isset($completed_activity_clients_data[$client['id']]) && trim($completed_activity_clients_data[$client['id']]) != '' ? 'Completed' : 'Not Completed';
                                $activity_value = isset($completed_activity_clients_data[$client['id']]) ? $completed_activity_clients_data[$client['id']] : 0;
                                break;
                            default:
                                //todo capture defaults
                                break;

                        }


                        $processData[1] [] = ['name' => strtolower(str_replace(' ', '_', $activity['name'])),
                            'data' => $completed_value];


                    }
                }
            }
        }

        // Get the pptx template
        $template = Document::where('id', $template_id)->first();

        $presentation = new Presentation($template->file, ['client' => $client, 'activities' => $processData[1]]);

        // do whatevs
        $presentation->run();
        $downloadFile = $presentation->getDownloadPath();

        $headers = array(
            'Content-Type: application/vnd.ms-powerpoint',
        );

        //Create directory to store processed templates, for future reference or to check what was sent to the client
        $processed_template_path = 'processeddocuments/' . date("Y") . DIRECTORY_SEPARATOR . date("m");
        if (!File::exists(storage_path('app/documents/' . $processed_template_path))) {
            Storage::makeDirectory('documents/' . $processed_template_path);
        }

        $processed_template = $processed_template_path . DIRECTORY_SEPARATOR . str_replace(' ','_',strtolower($template->name)) . "_" . $client["id"] . "_" . date("Y_m_d_H_i_s") . "_" . uniqid() . ".pptx";

        $success = \File::copy($downloadFile,storage_path('/app/documents/'.$processed_template));

        return $processed_template;
    }

    public function sendNotification(Client $client, Activity $activity, Request $request)
    {
        //Notification update
        $log = new Log;
        $log->client_id = $client->id;
        $log->user_id = auth()->id();
        $log->save();

        $client->load('users');

        $activity_log = new ActivityLog;
        $activity_log->log_id = $log->id;
        $activity_log->activity_id = $activity->id;
        $activity_log->activity_name = $activity->name;
        $activity_log->old_value = $request->input('old_'.$activity->id);
        $activity_log->new_value = implode(',',$request->input('notification_user'));
        $activity_log->save();


        $notification = new Notification;
        $notification->name = $client->company . ' has been updated: ' . $activity->name;
        $notification->link = route('activitieslog', $log->id);
        $notification->type = '1';
        //$notification->link = route('clients.progress', $client).'/1';
        $notification->save();

        $actionable_notification_data = new ActionableNotificationData;
        $actionable_notification_data->actionable_notification_id = $activity->actionable_id;
        $actionable_notification_data->notification_id = $notification->id;
        $actionable_notification_data->client_id = $client->id;
        $actionable_notification_data->user_id = auth()->id();
        $actionable_notification_data->duration = 120;
        $actionable_notification_data->save();

        $user_notifications = [];

        if (!is_null($client->introducer_id)) {
            array_push($user_notifications, [
                'user_id' => $client->introducer_id,
                'notification_id' => $notification->id
            ]);
        }

        if (!is_null($client->user_id)) {
            array_push($user_notifications, [
                'user_id' => $activity->user_id,
                'notification_id' => $notification->id
            ]);
        }

        if ( $request->has('notification_user') && (!empty($request->input('notification_user')))){
            $notification_users = $request->input('notification_user');
            foreach($notification_users as $notification_user):
                array_push($user_notifications, [
                    'user_id' => (int)$notification_user,
                    'notification_id' => $notification->id
                ]);
            endforeach;
        }

        NotificationEvent::dispatch($client->introducer_id, $notification);

        foreach ($client->users as $user) {
            array_push($user_notifications, [
                'user_id' => $user->id,
                'notification_id' => $notification->id
            ]);

            NotificationEvent::dispatch($user->id, $notification);
        }

        UserNotification::insert($user_notifications);

        return response()->json(['success' => 'Template sent successfully.']);
    }

    public function activityNotification(Client $client, $log_id)
    {
        $client->load('users');

        $notification = new Notification;
        $notification->name = $client->company . ' Notification for : ' . $client->company;
        $notification->link = route('activitieslog', $log_id);
        $notification->type = '1';
        $notification->save();

        $user_notifications = [];
        array_push($user_notifications, [
            'user_id' => $client->introducer_id,
            'notification_id' => $notification->id
        ]);

        NotificationEvent::dispatch($client->introducer_id, $notification);

        foreach ($client->users as $user) {
            array_push($user_notifications, [
                'user_id' => $user->id,
                'notification_id' => $notification->id
            ]);

            NotificationEvent::dispatch($user->id, $notification);
        }

        UserNotification::insert($user_notifications);

        return true;
    }

    public function storeComment(Client $client, Request $request)
    {
        $comment = new ClientComment;
        $comment->client_id = $client->id;
        $comment->user_id = auth()->id();
        $comment->comment = $request->input('comment');
        $comment->save();

        return redirect()->back()->with('flash_success', 'Comment added successfully');
    }

    public function createClientActivity($token, Request $request)
    {
        $client_activity = ClientActivity::where('token', $token)->firstOrFail();

        $client_activity->load('activity.actionable.data', 'client');

        $activity = [
            'name' => $client_activity->activity->name,
            'type' => $client_activity->activity->getTypeName(),
            'id' => $client_activity->activity_id,
            'value' => (isset($client_activity->activity->actionable['data'][0])) ? $client_activity->activity->actionable['data'][0] : ''
        ];

        $parameters = [
            'client' => $client_activity->client->load('introducer'),
            'activity' => $activity,
            'client_activity' => $client_activity
        ];

        return view('clients.clientactivity')->with($parameters);
    }

    public function storeClientActivity($token, Request $request)
    {
        $client_activity = ClientActivity::where('token', $token)->firstOrFail();

        $client_activity->load('client','activity');

        //activity type hook
        switch ($client_activity->activity->actionable_type) {
            case 'App\ActionableBoolean':
                ActionableBooleanData::insert([
                    'data' => $request->input($client_activity->activity->id),
                    'actionable_boolean_id' => $client_activity->activity->actionable_id,
                    'client_id' => $client_activity->client->id,
                    'user_id' => auth()->id(),
                    'duration' => 120
                ]);
                break;
            case 'App\ActionableDate':
                ActionableDateData::insert([
                    'data' => $request->input($client_activity->activity->id),
                    'actionable_date_id' => $client_activity->activity->actionable_id,
                    'client_id' => $client_activity->client->id,
                    'user_id' => auth()->id(),
                    'duration' => 120
                ]);
                break;
            case 'App\ActionableText':
                ActionableTextData::insert([
                    'data' => $request->input($client_activity->activity->id),
                    'actionable_text_id' => $client_activity->activity->actionable_id,
                    'client_id' => $client_activity->client->id,
                    'user_id' => auth()->id(),
                    'duration' => 120
                ]);
                break;
            case 'App\ActionableDocument':
                ActionableDocumentData::insert([
                    'actionable_document_id' => $client_activity->activity->actionable_id,
                    'document_id' => $request->input($client_activity->activity->id),
                    'client_id' => $client_activity->client->id,
                    'user_id' => auth()->id()
                ]);
                break;
            case 'App\ActionableDropdown':
                ActionableDropdownData::insert([
                    'actionable_dropdown_id' => $client_activity->activity->actionable_id,
                    'actionable_dropdown_item_id' => $request->input($client_activity->activity->id),
                    'client_id' => $client_activity->client->id,
                    'user_id' => auth()->id(),
                    'duration' => 120
                ]);
                break;
            default:
                //todo capture defaults
                break;
        }

        return redirect()->back()->with(['flash_success' => "Information updated successfully."]);
    }

    public function storeProgressing(Client $client){
        $client->is_progressing = !$client->is_progressing;
        if($client->is_progressing == 0) {
            $client->not_progressing_date = now();
        } else {
            $client->not_progressing_date = null;
        }

        $client->save();

        return redirect()->back()->with(['flash_info'=>'Client status updated successfully.']);
    }

    public function storeQA(Request $request,Client $client)
    {
        //$clientobj = Client::find($client->id);

        $related_parties_tree = RelatedPartiesTree::select('related_party_id')->withTrashed()->whereNotNull('deleted_at')->get();

        $related_parties_cnt = RelatedParty::select(DB::raw("count(id) as cnt"))->where('completed','!=','1')->where('client_id',$client->id)->whereNotIn('id',collect($related_parties_tree)->toArray())->first()->cnt;

        $rpc = $related_parties_cnt;

        if($rpc != 0) {
            return response()->json(['message' => 'Errorrp']);
        } else {


            if ($client->qa_start_date == null) {
                $client->qa_start_date = now();
                $client->qa_consultant = $request->input('userid');
            }
            $client->is_qa = 1;

            $client->save();

            if ($client->is_qa == 1) {
                $notification = new Notification;
                $notification->name = ($client->company != null ? $client->company : $client->first_name . ' ' . $client->last_name) . ' moved to QA.';
                $notification->link = route('clients.show', $client->id);
                $notification->type = '2';
                $notification->save();

                $user_notifications = [];

                /*$qauser = User::whereHas('roles', function ($q) {
                    $q->where('name', 'qa');
                })->get();*/
                $qauser = User::where('id', $request->input('userid'))->get();
                foreach ($qauser as $user) {
                    array_push($user_notifications, [
                        'user_id' => $user->id,
                        'notification_id' => $notification->id
                    ]);

                    NotificationEvent::dispatch($user->id, $notification);

                    Mail::to(trim($user->email))->send(new MoveToQANotify($client));
                }

                UserNotification::insert($user_notifications);

                return response()->json(['message' => 'Success']);
            }
        }
    }

    public function completeQA(Request $request,Client $client){
        //$client->is_qa = !$client->is_qa;
        if($client->is_qa == 1) {
            if ($client->qa_start_date != null) {
                $step = Step::where('process_id',12)->orderBy('order')->first()->id;

                $client->qa_end_date = now();
                $client->process_id = '12';
                $client->step_id = $step;
            }
            $client->save();

            $related_parties = RelatedParty::where('client_id',$client->id)->get();
            $rpstep = Step::where('process_id',14)->orderBy('order')->first()->id;

            foreach($related_parties as $related_party){
                $rp = RelatedParty::find($related_party->id);
                $rp->process_id = '14';
                $rp->step_id = $rpstep;
                $rp->save();
            }



            $notification = new Notification;
            $notification->name = 'QA completed for ' . ($client->company != null ? $client->company : $client->first_name . ' ' . $client->last_name);
            $notification->link = route('clients.show', $client->id);
            $notification->type = '1';
            $notification->save();

            $user_notifications = [];
            array_push($user_notifications, [
                'user_id' => $client->introducer_id,
                'notification_id' => $notification->id
            ]);

            NotificationEvent::dispatch($client->introducer_id, $notification);

            $user = User::where('id', $client->introducer_id)->first();

            Mail::to(trim($user->email))->send(new QACompleteNotify($client));

            $qauser = User::where('id', $request->input('userid'))->get();

            foreach ($qauser as $user) {
                array_push($user_notifications, [
                    'user_id' => $user->id,
                    'notification_id' => $notification->id
                ]);

                NotificationEvent::dispatch($user->id, $notification);
            }

            UserNotification::insert($user_notifications);

            return redirect()->back()->with(['flash_info' => 'QA successfully completed.']);
        } else {
            return redirect()->back()->with(['flash_info'=>'An Error occured while trying to complete the QA.']);
        }

}

    public function storeProd(Client $client){
        $client->is_qa = !$client->is_qa;

        $client->save();

        if($client->is_qa == 1) {
            /*$notification = new Notification;
            $notification->name = ($client->company != null ? $client->company : $client->first_name . ' ' . $client->last_name) . ' moved to QA.';
            $notification->link = route('clients.show', $client->id);
            $notification->type = '2';
            $notification->save();

            $user_notifications = [];

            $qauser = User::whereHas('roles', function ($q) {
                $q->where('name', 'qa');
            })->get();
            foreach ($qauser as $user) {
                array_push($user_notifications, [
                    'user_id' => $user->id,
                    'notification_id' => $notification->id
                ]);

                NotificationEvent::dispatch($user->id, $notification);
            }

            UserNotification::insert($user_notifications);

            Mail::to(trim('jaco@blackboardbs.com'))->send(new MoveToProdNotify($client));*/
        } else {
            $notification = new Notification;
            $notification->name = ($client->company != null ? $client->company : $client->first_name . ' ' . $client->last_name).' is available for amendments';
            $notification->link = route('clients.show', $client->id);
            $notification->type = '1';
            $notification->save();

            $user_notifications = [];
            array_push($user_notifications, [
                'user_id' => $client->introducer_id,
                'notification_id' => $notification->id
            ]);

            NotificationEvent::dispatch($client->introducer_id, $notification);

            UserNotification::insert($user_notifications);

            $user = User::where('id',$client->introducer_id)->first();

            Mail::to(trim($user->email))->send(new MoveToProdNotify($client));
        }

        return redirect()->back()->with(['flash_info'=>'Client successfully moved to production.']);
    }

    public function approval(Client $client, Request $request){
        $client->needs_approval = false;
        $client->save();

        if(!$request->input('status')){
            $client->delete();

            return redirect(route('clients.index'))->with(['flash_info'=>'Client declined successfully.']);
        }

        return redirect()->back()->with(['flash_info'=>'Client approved successfully.']);
    }

    public function completeStep(Client $client, Process $process, Step $step){
        $step->load('activities.actionable.data');
        //dd($step);
        $activities_auto_completed = [];
        //return null;
        //todo just change switch logic to assign fake data for all activities
        foreach($step->activities as $activity){

            //Check if activity is not already set/completed
            if(!$client->isActivitieCompleted($activity)){
                //if(!isset($activity->actionable['data'][0])){
                $found  = false;
                foreach ($activity->actionable['data'] as $datum){
                    if($datum->client_id == $client->id){
                        $found = true;
                        break;
                    }
                }

                if(!$found){
                    $activities_auto_completed[] = $activity->id;
                    $actionable_text_data = $activity->actionable_type.'Data';
                    $actionable_data = new $actionable_text_data;
                    $actionable_data->client_id = $client->id;
                    $actionable_data->user_id = auth()->id();
                    $actionable_data->duration = 120;
                    switch($activity->getTypeName()){
                        case 'text':
                            $actionable_data->data = null;
                            $actionable_data->actionable_text_id = $activity->actionable_id;
                            $actionable_data->save();
                            break;
                        case 'textarea':
                            $actionable_data->data = null;
                            $actionable_data->actionable_textarea_id = $activity->actionable_id;
                            $actionable_data->save();
                            break;
                        case 'template_email':
                            $actionable_data->template_id = 1;
                            $actionable_data->email = null;
                            $actionable_data->actionable_template_email_id = $activity->actionable_id;
                            $actionable_data->save();
                            break;
                        case 'document_email':
                            //return 'document_email';
                            break;
                        case 'document':
                            $actionable_data->actionable_document_id = $activity->actionable_id;
                            $actionable_data->document_id = 1;
                            $actionable_data->save();
                            break;
                        case 'dropdown':
                            $item = ActionableDropdownItem::where('actionable_dropdown_id', $activity->actionable_id)->take(1)->first();

                            $actionable_data->actionable_dropdown_id = $activity->actionable_id;
                            $actionable_data->actionable_dropdown_item_id = 0;
                            $actionable_data->save();
                            break;
                        case 'date':
                            $actionable_data->data = null;
                            $actionable_data->actionable_date_id = $activity->actionable_id;
                            $actionable_data->save();
                            break;
                        case 'boolean':
                            $actionable_data->data = null;
                            $actionable_data->actionable_boolean_id = $activity->actionable_id;
                            $actionable_data->save();
                            break;
                        case 'notification':
                            $notification = new Notification;
                            $notification->name = $client->company . ' has been updated: ' . $activity->name;
                            $notification->link = route('clients.progress', $client);
                            $notification->save();

                            $actionable_data->actionable_notification_id = $activity->actionable_id;
                            $actionable_data->notification_id = $notification->id;
                            $actionable_data->save();
                            break;
                        case 'multiple_attachment':
                            $actionable_data->template_id = 1;
                            $actionable_data->email = null;
                            $actionable_data->actionable_ma_id = $activity->actionable_id;
                            $actionable_data->save();
                            break;
                        default:
                            //return 'error';
                            break;
                    }
                }

            }
        }

        $client_process_progress = ClientProcess::where('client_id',$client->id)->where('process_id',$process->id)->first();

        //Move process step to the next step if all activities completed
        $max_step = Step::orderBy('order','desc')->where('process_id', $client->process_id)->first();

        $n_step = Step::select('id')->orderBy('order','asc')->where('process_id', $client->process_id)->where('order','>',$step->order)->whereNull('deleted_at')->first();

        if($client->isStepActivitiesCompleted($step) && $step->order < $max_step->order){
            $client = Client::find($client->id);
            //$client->step_id = $client->step_id + 1;
            $client->step_id = $n_step->id;
            $client->save();

            $cpp = ClientProcess::find($client_process_progress->id);
            $cpp->step_id = $n_step->id;;
            $cpp->save();
        }

        if(!$n_step) {
            if ($client->isStepActivitiesCompleted($step)) {
                $client = Client::find($client->id);
                //$client->step_id = $client->step_id + 1;
                $client->process_id = $process->id;
                $client->step_id = $max_step->id;
                $client->save();

                $cpp = ClientProcess::find($client_process_progress->id);
                $cpp->step_id = $max_step->id;
                $cpp->save();
            }
        }

        return response()->json(['success' => 'Template sent successfully.', 'activities_auto_completed' => $activities_auto_completed]);
    }

    public function forms(Request $request,Client $client)
    {
        if((strpos($request->headers->get('referer'),'reports') !== false) || (strpos($request->headers->get('referer'),'custom_report') !== false)) {
            $request->session()->put('path_route',$request->headers->get('referer'));
            $path = '1';
            $path_route = $request->session()->get('path_route');
        } else {
            $request->session()->forget('path_route');
            $path = '0';
            $path_route = '';
        }

        $step = Step::withTrashed()->find($client->step_id);
        $process_progress = $client->getProcessStepProgress($step);

        $client_progress = $client->process->getStageHex(0);

        if($client->step_id == $step->id)
            $client_progress = $client->process->getStageHex(1);

        if($client->step_id > $step->id)
            $client_progress = $client->process->getStageHex(2);

        $steps = Step::where('process_id', $client->process_id)->orderBy('order')->get();

        $step_data = [];
        foreach ($steps as $a_step):
            $progress_color = $client->process->getStageHex(0);

            if($client->step_id == $a_step->id)
                $progress_color = $client->process->getStageHex(1);

            if($client->step_id > $a_step->id)
                $progress_color = $client->process->getStageHex(2);


            $tmp_step = [
                'id' => $a_step->id,
                'name' => $a_step->name,
                'process_id' => $a_step->process_id,
                'progress_color' => $progress_color
            ];

            array_push($step_data, $tmp_step);
        endforeach;

        $clientforms = ClientForm::orderBy('id','desc')->get();
        $clientforms2 = ClientForm::orderBy('id','desc')->where('client_id',$client->id)->get();

        $clientcrfforms2 = ClientForm::where('name','CRF Form')->where('client_id',$client->id)->get();
        $clientcrfforms = ClientCRFForm::where('client_id',$client->id)->first();

        $r = $client->load('forms.user',"crfforms");
        //dd($clientforms2);
        return view('clients.forms.index')->with([
            'client' => $client->load('forms.user',"crfforms"),
            'process_progress' => $process_progress,
            'view_process_dropdown' => $client->clientProcessIfActivitiesExist(),
            'steps' => $step_data,
            'forms' => $clientforms,
            'crfform' => $clientcrfforms,
            'crf' => count(collect($clientcrfforms)->toArray()),
            'crf2' => count(collect($clientcrfforms2)->toArray()),
            'list' => $clientforms2,
            'path' => $path,
            'path_route' => $path_route
        ]);
    }

    public function uploadforms(Client $client){
        $step = Step::withTrashed()->find($client->step_id);
        $process_progress = $client->getProcessStepProgress($step);

        $client_progress = $client->process->getStageHex(0);

        if($client->step_id == $step->id)
            $client_progress = $client->process->getStageHex(1);

        if($client->step_id > $step->id)
            $client_progress = $client->process->getStageHex(2);

        $steps = Step::where('process_id', $client->process_id)->get();

        $step_data = [];
        foreach ($steps as $a_step):
            $progress_color = $client->process->getStageHex(0);

            if($client->step_id == $a_step->id)
                $progress_color = $client->process->getStageHex(1);

            if($client->step_id > $a_step->id)
                $progress_color = $client->process->getStageHex(2);


            $tmp_step = [
                'id' => $a_step->id,
                'name' => $a_step->name,
                'process_id' => $a_step->process_id,
                'progress_color' => $progress_color
            ];

            array_push($step_data, $tmp_step);
        endforeach;

        $clientforms = ClientForm::orderBy('id','desc')->get();
        $clientcrfforms2 = ClientForm::where('name','CRF Form')->where('client_id',$client->id)->get();
        $clientcrfforms = ClientCRFForm::where('client_id',$client->id)->first();



        return view('clients.forms.upload')->with([
            'client' => $client->load('forms.user'),
            'process_progress' => $process_progress,
            'steps' => $step_data,
            'forms' => $clientforms,
            'crfform' => $clientcrfforms,
            'crf' => count(collect($clientcrfforms)->toArray()),
            'crf2' => count(collect($clientcrfforms2)->toArray())
        ]);
    }

    public function storeuploadforms(StoreClientFormRequest $request,$clientid){

        if ($request->hasFile('file')) {
            $request->file('file')->store('crf');
        }

        $document = new ClientForm();
        if($request->input('form_type') == "CRF Form"){
            $document->name = "CRF Form";
            $document->form_type = "CRF Form";
        } else {
            $document->name = $request->input('name');
            $document->form_type = "Other";
        }
        $document->file = $request->file('file')->hashName();
        $document->user_id = auth()->id();
        $document->client_id = $clientid;

        $document->save();

        return redirect(route('clients.forms', $clientid))->with('flash_success', 'Form uploaded successfully');
    }

    public function edituploadforms(Client $client,$formid){
        $step = Step::withTrashed()->find($client->step_id);
        $process_progress = $client->getProcessStepProgress($step);

        $client_progress = $client->process->getStageHex(0);

        if($client->step_id == $step->id)
            $client_progress = $client->process->getStageHex(1);

        if($client->step_id > $step->id)
            $client_progress = $client->process->getStageHex(2);

        $steps = Step::where('process_id', $client->process_id)->get();

        $step_data = [];
        foreach ($steps as $a_step):
            $progress_color = $client->process->getStageHex(0);

            if($client->step_id == $a_step->id)
                $progress_color = $client->process->getStageHex(1);

            if($client->step_id > $a_step->id)
                $progress_color = $client->process->getStageHex(2);


            $tmp_step = [
                'id' => $a_step->id,
                'name' => $a_step->name,
                'process_id' => $a_step->process_id,
                'progress_color' => $progress_color
            ];

            array_push($step_data, $tmp_step);
        endforeach;

        $clientforms = ClientForm::where('id',$formid)->get();
        $clientcrfforms2 = ClientForm::where('name','CRF Form')->where('client_id',$client->id)->get();
        $clientcrfforms = ClientCRFForm::where('client_id',$client->id)->first();



        return view('clients.forms.editupload')->with([
            'client' => $client->load('forms.user'),
            'process_progress' => $process_progress,
            'steps' => $step_data,
            'forms' => $clientforms,
            'crfform' => $clientcrfforms,
            'crf' => count(collect($clientcrfforms)->toArray()),
            'crf2' => count(collect($clientcrfforms2)->toArray())
        ]);
    }

    public function updateuploadforms(Request $request,$client, $form){

        $document = ClientForm::find($form);

        if ($request->hasFile('file')) {
            $request->file('file')->store('crf');
            $document->file = $request->file('file')->hashName();
        }

        if($request->input('form_type') == "CRF Form"){
            $document->name = "CRF Form";
            $document->form_type = "CRF Form";
        } else {
            $document->name = $request->input('name');
            $document->form_type = "Other";
        }
        $document->user_id = auth()->id();

        $document->save();

            return redirect(route('clients.forms', $client))->with('flash_success', 'Form updated successfully');

    }

    public function searchClients($search){


        $results = collect();

        $clients = new Client();
        $clients->unHide();

        $results = $results->merge(
            $clients->select('id',DB::raw("IF(`hash_id_number` is null,'',CAST(AES_DECRYPT(`hash_id_number`, 'Qwfe345dgfdg') AS CHAR(50))) as `id_number`"),DB::raw('CAST(AES_DECRYPT(`hash_company`, "Qwfe345dgfdg") AS CHAR(50)) `name`'),DB::raw('CAST(AES_DECRYPT(`hash_company`, "Qwfe345dgfdg") AS CHAR(50)) human_company'))
                ->having(DB::raw('human_company'),'like', '%'.$search.'%')
                ->get()
                ->map(function ($item) {
                    return $item;
                })

        );

        $results = $results->merge(
            $clients->select('id',DB::raw("IF(`hash_id_number` is null,'',CAST(AES_DECRYPT(`hash_id_number`, 'Qwfe345dgfdg') AS CHAR(50))) as `id_number`"),DB::raw("CONCAT(CAST(AES_DECRYPT(`hash_first_name`, 'Qwfe345dgfdg') AS CHAR(50)),' ',CAST(AES_DECRYPT(`hash_last_name`, 'Qwfe345dgfdg') AS CHAR(50))) as `name`"),DB::raw('CAST(AES_DECRYPT(`hash_first_name`, "Qwfe345dgfdg") AS CHAR(50)) human_first_name'),DB::raw('CAST(AES_DECRYPT(`hash_last_name`, "Qwfe345dgfdg") AS CHAR(50)) human_last_name'))
                ->having(DB::raw('human_first_name'),'like', '%'.$search.'%')
                ->orHaving(DB::raw('human_last_name'),'like', '%'.$search.'%')
                ->get()
                ->map(function ($item) {
                    return $item;
                })

        );

        $results = $results->merge(
            $clients->select('id',DB::raw("IF(`hash_id_number` is null,'',CAST(AES_DECRYPT(`hash_id_number`, 'Qwfe345dgfdg') AS CHAR(50))) as `id_number`"),DB::raw("CONCAT(CAST(AES_DECRYPT(`hash_first_name`, 'Qwfe345dgfdg') AS CHAR(50)),' ',CAST(AES_DECRYPT(`hash_last_name`, 'Qwfe345dgfdg') AS CHAR(50))) as `name`"),DB::raw('CAST(AES_DECRYPT(`hash_first_name`, "Qwfe345dgfdg") AS CHAR(50)) human_first_name'),DB::raw('CAST(AES_DECRYPT(`hash_last_name`, "Qwfe345dgfdg") AS CHAR(50)) human_last_name'),DB::raw('CAST(AES_DECRYPT(`hash_cif_code`, "Qwfe345dgfdg") AS CHAR(50)) human_cif_code'))
                ->having(DB::raw('human_cif_code'),'like', '%'.$search.'%')
                ->get()
                ->map(function ($item) {
                    return $item;
                })

        );

        $data = array();

        foreach ($results as $client){
            array_push($data,[
                'id' => $client->id,
                'name' => $client->name,
                'id_number' => ''
            ]);
        }

        return $results;
    }

    public function getClients(){
        $clients = Client::where('is_progressing',1)->where('is_qa','0')->orderBy('first_name')->get();

        $data = array();

        foreach ($clients as $client){
            array_push($data,[
                'id' => $client->id,
                'name' => ($client->company != null && $client->company != 'N/A' && $client->company != 'n/a' ? $client->company : $client->first_name.' '.$client->last_name),
                'id_number' => ($client->id_number != null ? $client->id_number : '&nbsp;')
            ]);
        }

        return response()->json($data);
    }

    public function getClientDetail($clientid){
        $clients = Client::where('id',$clientid)->get();



        $users = User::select('id',DB::raw("CONCAT(CAST(AES_DECRYPT(`hash_first_name`, 'Qwfe345dgfdg') AS CHAR(50)),' ',CAST(AES_DECRYPT(`hash_last_name`, 'Qwfe345dgfdg') AS CHAR(50))) as full_name"))->groupBy('id')->groupBy('hash_first_name')->groupBy('hash_last_name')->get();

        foreach ($users as $user){
            $cnt = Client::select(DB::raw("count(id) as cnt"))->whereNull('qa_end_date')->where('is_qa',0)->where('consultant_id',$user->id)->first();
            $users_array[$user->id] = $user->full_name.' ( Open cases: '.$cnt->cnt.' )';
        }

        foreach ($clients as $client){
            $data = [
                'id' => $client->id,
                'qa' => $client->is_qa,
                'clname' => ($client->company != null && $client->company != 'N/A' && $client->company != 'n/a' ? $client->company : $client->first_name.' '.$client->last_name),
                'id_number' => ($client->id_number != null ? $client->id_number : '&nbsp;'),
                'consultant' => ($client->consultant_id != null ? User::select(DB::raw("CONCAT(CAST(AES_DECRYPT(`hash_first_name`, 'Qwfe345dgfdg') AS CHAR(50)),' ',CAST(AES_DECRYPT(`hash_last_name`, 'Qwfe345dgfdg') AS CHAR(50))) as full_name"))->where('id',$client->consultant_id)->first()->full_name : 0),
                'users' => $users_array
            ];
        }

        return response()->json($data);
    }

    public function storeConsultant(Request $request,$clientid){
        $client = Client::find($clientid);
        $client->consultant_id = $request->input('userid');
        $client->assigned_date = now();
        $client->save();

        if($client->id) {
            $log = new Log;
            $log->client_id = $client->id;
            $log->user_id = auth()->id();
            $log->save();

            $notification = new Notification;
            $notification->name = ($client->company != null ? $client->company : $client->first_name . ' ' . $client->last_name) .' has been assigned to you.';
            $notification->link = route('clients.show', $client->id);
            $notification->type = '3';
            $notification->save();

            $user_notifications = [];

            $qauser = User::where('id',$request->input('userid'))->get();
            foreach ($qauser as $user) {
                array_push($user_notifications, [
                    'user_id' => $user->id,
                    'notification_id' => $notification->id
                ]);

                NotificationEvent::dispatch($user->id, $notification);

                Mail::to(trim($user->email))->send(new AssignedConsultantNotify($client));
            }

            UserNotification::insert($user_notifications);

            return response()->json(['message' => 'Success', 'consultant' => ($client->consultant_id != null ? User::select(DB::raw("CONCAT(CAST(AES_DECRYPT(`hash_first_name`, 'Qwfe345dgfdg') AS CHAR(50)),' ',CAST(AES_DECRYPT(`hash_last_name`, 'Qwfe345dgfdg') AS CHAR(50))) as full_name"))->where('id', $client->consultant_id)->first()->full_name : 0), 'clname' => ($client->company != null && $client->company != 'N/A' && $client->company != 'n/a' ? $client->company : $client->first_name . ' ' . $client->last_name)]);
        } else {
            return response()->json(['message'=>'Error','consultant' => ($client->consultant_id != null ? User::select(DB::raw("CONCAT(CAST(AES_DECRYPT(`hash_first_name`, 'Qwfe345dgfdg') AS CHAR(50)),' ',CAST(AES_DECRYPT(`hash_last_name`, 'Qwfe345dgfdg') AS CHAR(50))) as full_name"))->where('id',$client->consultant_id)->first()->full_name : 0),'clname' => ($client->company != null && $client->company != 'N/A' && $client->company != 'n/a' ? $client->company : $client->first_name.' '.$client->last_name)]);
        }
    }

    public function checkClientActivities($client){

        $completed = false;

        $clientobj = Client::where('id',$client)->first();

        $steps = Step::where('process_id',$clientobj->process_id)->get();

        $i = 0;
        foreach ($steps as $step){
            $stepd = Step::where('id',$step->id)->first();
            if($clientobj->isStepActivitiesCompleted($stepd)){
                $i++;
            }
        }
//return $i;
        if(count($steps) == $i) {
            return response()->json(['message' => 'Success']);
        } else {
            return response()->json(['message' => 'Error']);
        }
    }

    public function completeClient($client){

            $c = Client::find($client);
            $c->completed_date = now();
            $c->completed_by = Auth::id();
            $c->completed = 1;
            $c->save();

            return response()->json(['message' => 'Success']);

    }

    public function WorkItemQA($id)
    {

        $client = Client::find($id);
        $client->work_item_qa = 1;
        $client->work_item_qa_date = now();
        $client->qa_consultant = auth()->id();
        $client->save();

        return response()->json(['data' => 'success']);
    }

    public function xlsExport(Request $request, Excel $excel)
    {

        $np = 0;
        $qa = 0;

        $c_array = array();
        $client_arr = array();

        $clients = new Client();
        $clients->unHide();

        $deleted_rps = RelatedPartiesTree::select('related_party_id')->get();

        $clients = $clients->with(['referrer', 'process.steps.activities.actionable.data', 'introducer', 'consultant', 'committee', 'project'])->select('*', DB::raw('CONCAT(first_name," ",COALESCE(`last_name`,"")) as full_name'),
            DB::raw('ROUND(DATEDIFF(completed_at, created_at), 0) as completed_days'),
            DB::raw('CAST(AES_DECRYPT(`hash_company`, "Qwfe345dgfdg") AS CHAR(50)) hash_company'),
            DB::raw('CAST(AES_DECRYPT(`hash_first_name`, "Qwfe345dgfdg") AS CHAR(50)) hash_first_name'),
            DB::raw('CAST(AES_DECRYPT(`hash_last_name`, "Qwfe345dgfdg") AS CHAR(50)) hash_last_name'),
            DB::raw('CAST(AES_DECRYPT(`hash_cif_code`, "Qwfe345dgfdg") AS CHAR(50)) hash_cif_code'),
            DB::raw('CAST(`case_number` AS CHAR(50)) AS case_number')
        );

        if ($request->has('q') && $request->input('q') != '') {

            $clients = $clients->having('hash_company', 'like', "%" . $request->input('q') . "%")
                ->orHaving('hash_first_name', 'like', "%" . $request->input('q') . "%")
                ->orHaving('hash_last_name', 'like', "%" . $request->input('q') . "%")
                ->orHaving('hash_cif_code', 'like', "%" . $request->input('q') . "%")
                ->orHaving('case_number', 'like', "%" . $request->input('q') . "%");
        }

        if ($request->has('trigger_type') && $request->input('trigger_type') != '') {
            $p = $request->input('trigger_type');
            $clients = $clients->whereTriggerTypeId($p);
        }

        if ($request->has('s') && $request->input('s') == 'unassigned') {
            $clients = $clients->whereNull('consultant_id');
        }

        if ($request->has('s') && $request->input('s') == 'assigned') {
            $clients = $clients->whereNotNull('consultant_id');
        }

        if($request->has('user') && $request->input('user') != null) {
            $clients = $clients->where('consultant_id',$request->input('user'));
        }

        if($request->has('c') && $request->input('c') == 'yes') {
            $clients = $clients->whereNotNull('completed_at');
        } elseif($request->has('c') && $request->input('c') == 'no') {
            $clients = $clients->whereNull('completed_at');
        }

        if ($request->has('review') && $request->input('review') == 'yes'){
            $clients = $clients->whereNull('qa_start_date')
                ->whereProcessId(8);
        }

        if ($request->has('qa') && $request->input('qa') == 'yes') {
            $clients = $clients->whereNotNull('qa_start_date')
                ->whereCompleted(1)
                ->whereNull('qa_end_date');
        }

        if ($request->has('qa') && $request->input('qa') == 'complete') {
            $clients = $clients->whereNotNull('qa_end_date')
                ->whereNotIn('process_id', [9]);
        }

        if ($request->has('p') && $request->input('p') == 12){
            $clients = $clients->whereNotNull('qa_end_date')
                ->whereNotNull('qa_start_date')
                ->whereNotIn('process_id', [8])
                ->whereProcessId(12);
        }

        if ($request->has('p') && $request->input('p') == 9){
            $clients = $clients->whereNotNull('qa_end_date')
                ->whereNotNull('qa_start_date')
                ->whereNotIn('process_id', [8])
                ->whereProcessId(9);
        }

        if ($request->has('p') && $request->input('p') == '8'){
            $clients = $clients->whereNull('qa_start_date')
                ->whereProcessId(8);
        }

        if ($request->has('f') && $request->input('f') != '') {
            $clients = $clients->where('instruction_date','>=',$request->input('f'));
        }

        if ($request->has('t') && $request->input('t') != '') {
            $clients = $clients->where('instruction_date','<=',$request->input('t'));
        }

        if ($request->has('step') && $request->input('step') != 'all' && $request->input('step') != '' && $request->input('step') != '1000') {
            $clients = $clients->whereStepId($request->input('step'));
        }

        $clients = $clients->get();

        foreach ($clients as $client) {

            $data = [];

            $current_work_queue = '';
            $p_committee_decision = '';

            if(!isset($client->qa_start_date) && $client->process_id == 8) $current_work_queue = "In Review";
            if(isset($client->qa_start_date) && $client->completed && !isset($client->qa_end_date)) $current_work_queue = "In QA";
            if(isset($client->qa_end_date)) $current_work_queue = "QA Complete";
            if(isset($client->completed_at)) $current_work_queue = "Case Completed";
            if(!isset($client->qa_end_date) && $client->process_id == 12) $current_work_queue = "In Exit Close Out";
            if($client->process_id == 9) $current_work_queue = "In Heightened Monitoring";

            $pd = ActionableDropdownData::with('item')->where('actionable_dropdown_id', 20)->whereClientId($client->id)->get();

            foreach ($pd as $key => $value):
                if(count($pd) > 1) {
                    $p_committee_decision .= $value["item"]["name"] . ', ';
                } else {
                    $p_committee_decision .= $value["item"]["name"];
                }
            endforeach;

            $client_arr[$client->id][] = [
                'type' => 'P',
                'company' => ($client->company != null && strtolower($client->hash_company) != 'n/a' && strtolower($client->company) != 'n/a'  ? $client->company : $client->first_name . ' ' . $client->last_name),
                'committee' => isset($client->committee)?$client->committee->name:null,
                'case_number' => (isset($client->case_number) ? $client->case_number : ''),
                'out_of_scope' => ($client->out_of_scope)?"Yes":"No",
                'cif_code' => $client->cif_code,
                'project' => isset($client->project)?$client->project->name:null,
                'instruction_date' => $client->instruction_date,
                'trigger_type' => isset($client->trigger_type_id) && $client->trigger_type_id > 0 ? $client->trigger->name:null,
                'investigation_completed_date' => $client->completed_date,
                'qa_end_date' => ($client->qa_end_date != null ? Carbon::parse($client->qa_end_date)->toDateString() : ''),
                'committee_date' => ActionableDateData::where('actionable_date_id', 41)->whereClientId($client->id)->first()->data??null,
                'exit_committee_decision' => $p_committee_decision,
                'completed_at'=> ($client->completed_at != null ? Carbon::parse($client->completed_at)->format('Y-m-d') : ''),
                'current_work_queue' => $current_work_queue,
                'consultant' => isset($client->consultant_id) ? $client->consultant->first_name.' '.$client->consultant->last_name : null
            ];

            //$total++;
        }




        $deleted_rps = RelatedPartiesTree::select('related_party_id')->get();

        $related_parties = RelatedParty::with(['client','referrer', 'process.steps2.activities.actionable.data', 'introducer', 'committee', 'project'])->select('*', DB::raw('CONCAT(first_name," ",COALESCE(`last_name`,"")) as full_name'),
            DB::raw('ROUND(DATEDIFF(completed_at, created_at), 0) as completed_days'),
            DB::raw('CAST(AES_DECRYPT(`hash_company`, "Qwfe345dgfdg") AS CHAR(50)) hash_company'),
            DB::raw('CAST(AES_DECRYPT(`hash_first_name`, "Qwfe345dgfdg") AS CHAR(50)) hash_first_name'),
            DB::raw('CAST(AES_DECRYPT(`hash_last_name`, "Qwfe345dgfdg") AS CHAR(50)) hash_last_name'),
            DB::raw('CAST(AES_DECRYPT(`hash_cif_code`, "Qwfe345dgfdg") AS CHAR(50)) hash_cif_code'),
            DB::raw('CAST(`case_number` AS CHAR(50)) AS case_number')
        )->whereHas('client', function ($q) use ($request){
            if ($request->has('s') && $request->input('s') == 'unassigned') {
                $q->whereNull('consultant_id');
            }

            if ($request->has('s') && $request->input('s') == 'assigned') {
                $q->whereNotNull('consultant_id');
            }

            if($request->has('user') && $request->input('user') != null) {
                $q->where('consultant_id',$request->input('user'));
            }

            if($request->has('c') && $request->input('c') == 'yes') {
                $q->whereNotNull('completed_at');
            } elseif($request->has('c') && $request->input('c') == 'no') {
                $q->whereNull('completed_at');
            }

            if ($request->has('review') && $request->input('review') == 'yes'){
                $q->whereNull('qa_start_date')
                    ->whereProcessId(8);
            }

            if ($request->has('qa') && $request->input('qa') == 'yes') {
                $q->whereNotNull('qa_start_date')
                    ->whereCompleted(1)
                    ->whereNull('qa_end_date');
            }

            if ($request->has('qa') && $request->input('qa') == 'complete') {
                $q->whereNotNull('qa_end_date')
                    ->whereNotIn('process_id', [9]);
            }
            if ($request->has('p') && $request->input('p') == 12){
                $q->whereNotNull('qa_end_date')
                    ->whereNotNull('qa_start_date')
                    ->whereNotIn('process_id', [8])
                    ->whereProcessId(12);
            }

            if ($request->has('p') && $request->input('p') == 9){
                $q->whereNotNull('qa_end_date')
                    ->whereNotNull('qa_start_date')
                    ->whereNotIn('process_id', [8])
                    ->whereProcessId(9);
            }

            if ($request->has('p') && $request->input('p') == '8'){
                $q->whereNull('qa_start_date')
                    ->whereProcessId(8);
            }
        })->whereIn('id',collect($deleted_rps)->toArray());

        if ($request->has('q') && $request->input('q') != '') {

            $related_parties = $related_parties->having('hash_company', 'like', "%" . $request->input('q') . "%")
                ->orHaving('hash_first_name', 'like', "%" . $request->input('q') . "%")
                ->orHaving('hash_last_name', 'like', "%" . $request->input('q') . "%")
                ->orHaving('hash_cif_code', 'like', "%" . $request->input('q') . "%")
                ->orHaving('case_number', 'like', "%" . $request->input('q') . "%");
        }

        if ($request->has('trigger_type') && $request->input('trigger_type') != '') {
            $related_parties = $related_parties->whereTriggerTypeId($request->input('trigger_type'));
        }

        if ($request->has('f') && $request->input('f') != '') {
            $related_parties = $related_parties->where('instruction_date','>=',$request->input('f'));
        }

        if ($request->has('t') && $request->input('t') != '') {
            $related_parties = $related_parties->where('instruction_date','<=',$request->input('t'));
        }

        if ($request->has('step') && $request->input('step') != 'all' && $request->input('step') != '' && $request->input('step') != '1000') {
            $step = StepsRelatedPartiesLink::wherePrimaryStep($request->input('step'))->first();
            $related_parties = $related_parties->whereStepId($step->related_step);
        }



        $related_parties = $related_parties->get();

        foreach ($related_parties as $related_party) {
            if($related_party){
                $rp_data = [];

                $current_work_queue = '';
                $rp_committee_decision = '';
                if(!isset($related_party->client->qa_start_date) && $related_party->process_id == 10) $current_work_queue = "In Review";
                if(isset($related_party->client->qa_start_date) && $related_party->client->completed && !isset($related_party->client->qa_end_date)) $current_work_queue = "In QA";
                if(isset($related_party->client->qa_end_date)) $current_work_queue = "QA Complete";
                if(isset($related_party->client->completed_at)) $current_work_queue = "Case Completed";
                if(!isset($related_party->client->qa_end_date) && $related_party->process_id == 14) $current_work_queue = "In Exit Close Out";
                if(!isset($related_party->client->completed_at) && $related_party->process_id == 13) $current_work_queue = "In Heightened Monitoring";

                $rpd = RelatedPartyDropdownData::with('item')->where('related_party_dropdown_id', 13)->whereRelatedPartyId($related_party->id)->get();

                foreach ($rpd as $key => $value):
                    if(count($rpd) > 1) {
                        $rp_committee_decision .= $value["item"]["name"] . ', ';
                    } else {
                        $rp_committee_decision .= $value["item"]["name"];
                    }
                endforeach;

                $committee_date = RelatedPartyDateData::where('related_party_date_id', 43)->whereRelatedPartyId($related_party->id)->first(['data']);

                $consultant = User::select(DB::raw("CONCAT(CAST(AES_DECRYPT(`hash_first_name`, 'Qwfe345dgfdg') AS CHAR(50)),' ',CAST(AES_DECRYPT(`hash_last_name`, 'Qwfe345dgfdg') AS CHAR(50))) as full_name"))->where('id',$related_party->client->consultant_id)->first();

                $client_arr[$related_party->client_id][] = [
                    'type' => 'R',
                    'company' => ($related_party->company != null ? $related_party->company : $related_party->first_name . ' ' . $related_party->last_name),
                    'committee' => isset($related_party->committee)?$related_party->committee->name:null,
                    'case_number' => $related_party->case_number,
                    'out_of_scope' => ($related_party->out_of_scope)?"Yes":"No",
                    'cif_code' => $related_party->cif_code,
                    'project' => isset($related_party->project)?$related_party->project->name:null,
                    'trigger_type' => isset($related_party->trigger_type_id) && $related_party->trigger_type_id > 0 ? $related_party->trigger->name:null,
                    'instruction_date' => $related_party->instruction_date,
                    'investigation_completed_date' => $related_party->completed_date,
                    'qa_end_date' => ($related_party->client->qa_end_date != null ? Carbon::parse($related_party->client->qa_end_date)->toDateString() : ''),
                    'committee_date' => ($committee_date != null ? $committee_date->data : ''),
                    'exit_committee_decision' => $rp_committee_decision,
                    'completed_at' => ($related_party->client->completed_at != null ? Carbon::parse($related_party->client->completed_at)->format('Y-m-d') : ''),
                    'current_work_queue' => $current_work_queue,
                    'consultant' => (isset($consultant) ? $consultant->full_name : '')
                ];
            }
            //$total++;
        }

        if ($request->has('step') && $request->input('step') != 'all' && $request->input('step') != '') {
            if ($request->input('step') == 1000) {
                if (($request->has('f') && $request->input('f') != '') && ($request->has('t') && $request->input('t') != '')){
                    return redirect('clients?c=yes&f='.$request->input('f').'&t='.$request->input('t'));
                } else {
                    return redirect('clients?c=yes');
                }
            }
            if ($request->input('step') == 1001) {
                return redirect('clients?qa=yes');
            }
        }


        foreach ($client_arr as $key => $value) {
            foreach ($value as $key2 => $value2) {
                array_push($c_array, $value2);
            }
        }

        if ($request->has('sort') && $request->input('sort') == 'investigation_completed_date') {
            if ($request->has('direction') && $request->input('direction') == 'asc') {
                $c_array = collect($c_array)->sortBy('investigation_completed_date')->values();
            }

            if ($request->has('direction') && $request->input('direction') == 'desc') {
                $c_array = collect($c_array)->sortByDesc('investigation_completed_date')->values();
            }
        }

        if ($request->has('sort') && $request->input('sort') == 'qa_end_date') {
            if ($request->has('direction') && $request->input('direction') == 'asc') {
                $c_array = collect($c_array)->sortBy('qa_end_date')->values();
            }

            if ($request->has('direction') && $request->input('direction') == 'desc') {
                $c_array = collect($c_array)->sortByDesc('qa_end_date')->values();
            }
        }

        if ($request->has('sort') && $request->input('sort') == 'committee_date') {
            if ($request->has('direction') && $request->input('direction') == 'asc') {
                $c_array = collect($c_array)->sortBy('committee_date')->values();
            }

            if ($request->has('direction') && $request->input('direction') == 'desc') {
                $c_array = collect($c_array)->sortByDesc('committee_date')->values();
            }
        }

        if ($request->has('sort') && $request->input('sort') == 'exit_committee_decision') {
            if ($request->has('direction') && $request->input('direction') == 'asc') {
                $c_array = collect($c_array)->sortBy('exit_committee_decision')->values();
            }

            if ($request->has('direction') && $request->input('direction') == 'desc') {
                $c_array = collect($c_array)->sortByDesc('exit_committee_decision')->values();
            }
        }

        if ($request->has('sort') && $request->input('sort') == 'current_work_queue') {
            if ($request->has('direction') && $request->input('direction') == 'asc') {
                $c_array = collect($c_array)->sortBy('current_work_queue')->values();
            }

            if ($request->has('direction') && $request->input('direction') == 'desc') {
                $c_array = collect($c_array)->sortByDesc('current_work_queue')->values();
            }
        }

        if ($request->has('sort') && $request->input('sort') == 'company') {
            if ($request->has('direction') && $request->input('direction') == 'asc') {
                $c_array = collect($c_array)->sortBy('company')->values();
            }

            if ($request->has('direction') && $request->input('direction') == 'desc') {
                $c_array = collect($c_array)->sortByDesc('company')->values();
            }
        }

        if ($request->has('sort') && $request->input('sort') == 'case_number') {
            if ($request->has('direction') && $request->input('direction') == 'asc') {
                $c_array = collect($c_array)->sortBy('case_number')->values();
            }

            if ($request->has('direction') && $request->input('direction') == 'desc') {
                $c_array = collect($c_array)->sortByDesc('case_number')->values();
            }
        }

        if ($request->has('sort') && $request->input('sort') == 'cif_code') {
            if ($request->has('direction') && $request->input('direction') == 'asc') {
                $c_array = collect($c_array)->sortBy('cif_code')->values();
            }

            if ($request->has('direction') && $request->input('direction') == 'desc') {
                $c_array = collect($c_array)->sortByDesc('cif_code')->values();
            }
        }

        if ($request->has('sort') && $request->input('sort') == 'committee') {
            if ($request->has('direction') && $request->input('direction') == 'asc') {
                $c_array = collect($c_array)->sortBy('committee')->values();
            }

            if ($request->has('direction') && $request->input('direction') == 'desc') {
                $c_array = collect($c_array)->sortByDesc('committee')->values();
            }
        }

        if ($request->has('sort') && $request->input('sort') == 'out_of_scope') {
            if ($request->has('direction') && $request->input('direction') == 'asc') {
                $c_array = collect($c_array)->sortBy('out_of_scope')->values();
            }

            if ($request->has('direction') && $request->input('direction') == 'desc') {
                $c_array = collect($c_array)->sortByDesc('out_of_scope')->values();
            }
        }

        if ($request->has('sort') && $request->input('sort') == 'completed_at') {
            if ($request->has('direction') && $request->input('direction') == 'asc') {
                $c_array = collect($c_array)->sortBy('completed_at')->values()->all();
            }

            if ($request->has('direction') && $request->input('direction') == 'desc') {
                $c_array = collect($c_array)->sortByDesc('completed_at')->values()->all();
            }
        }

        if ($request->has('sort') && $request->input('sort') == 'project') {
            if ($request->has('direction') && $request->input('direction') == 'asc') {
                $c_array = collect($c_array)->sortBy('project')->values();
            }

            if ($request->has('direction') && $request->input('direction') == 'desc') {
                $c_array = collect($c_array)->sortByDesc('project')->values();
            }
        }

        if ($request->has('sort') && $request->input('sort') == 'consultant') {
            if ($request->has('direction') && $request->input('direction') == 'asc') {
                $c_array = collect($c_array)->sortBy('consultant')->values();
            }

            if ($request->has('direction') && $request->input('direction') == 'desc') {
                $c_array = collect($c_array)->sortByDesc('consultant')->values();
            }
        }

        if ($request->has('sort') && $request->input('sort') == 'instruction_date') {
            if ($request->has('direction') && $request->input('direction') == 'asc') {
                $c_array = collect($c_array)->sortBy('instruction_date')->values();
            }

            if ($request->has('direction') && $request->input('direction') == 'desc') {
                $c_array = collect($c_array)->sortByDesc('instruction_date')->values();
            }
        }

        if ($request->has('sort') && $request->input('sort') == 'trigger_type') {
            if ($request->has('direction') && $request->input('direction') == 'asc') {
                $c_array = collect($c_array)->sortBy('trigger_type')->values();
            }

            if ($request->has('direction') && $request->input('direction') == 'desc') {
                $c_array = collect($c_array)->sortByDesc('trigger_type')->values();
            }
        }

        if ($request->has('committee_decision') && $request->committee_decision != ''){
            $c_array = collect($c_array)->filter(function($client) use($request){
                //return $client["exit_committee_decision"] >= $request->committee_decision;
                return false !== stristr($client["exit_committee_decision"], $request->committee_decision);
            })->values();
            //return collect($client_arr)->whereLike('exit_committee_decision', '%'.$request->committee_decision.'%')->values();
        }

        if ($request->has('committee_date_f') && $request->committee_date_f != ''){
            $c_array = collect($c_array)->filter(function($client) use($request){
                return $client["committee_date"] >= $request->committee_date_f && isset($client["committee_date"]);
            })->values();
        }

        if ($request->has('committee_date_t') && $request->committee_date_t != ''){
            $c_array = collect($c_array)->where('committee_date', '<=', $request->committee_date_t)->filter(function($client) use($request){
                return $client["committee_date"] <= $request->committee_date_t && isset($client["committee_date"]);
            })->values();
        }


        return $excel->download(new ClientExport(collect((isset($client_arr) ? (empty($c_array) ? $client_arr : $c_array) : []))), 'clients_'.date('Y_m_d_H_i_s').'.xlsx');
    }

    private function clientBucketActivityIds($steps, $client){
        $tmp_act = $steps->map(function ($step){
            return $step->activities->map(function ($activ){
                if($activ->client_bucket){
                    return  $activ->id;
                }
            });
        })->flatten()->toArray();

        $tmp_act = array_values(array_filter($tmp_act));

        $parent_activities_in_client_basket = ActivityInClientBasket::where('client_id', $client->id)->select('activity_id', 'in_client_basket')
            ->get();

        $flag = $parent_activities_in_client_basket->map(function ($activity_id){
            return $activity_id->activity_id;
        })->toArray();

        foreach ($tmp_act as $key => $item){
            if (in_array($item, $flag)) {
                unset($tmp_act[$key]);
            }
        }

        $active_activities = $parent_activities_in_client_basket->where('in_client_basket', 1)->map(function ($activity){
            return $activity->activity_id;
        })->values()->toArray();

        return array_merge($tmp_act, $active_activities);
    }

    public function clientProcessProgress(Client $client,Process $process, Step $step, Request $request, Step $activity_step, $action_id){
        //dd($step);
        if((strpos($request->headers->get('referer'),'reports') !== false) || (strpos($request->headers->get('referer'),'custom_report') !== false)) {
            $request->session()->put('path_route',$request->headers->get('referer'));
            $path = '1';
            $path_route = $request->session()->get('path_route');
        } else {
            $request->session()->forget('path_route');
            $path = '0';
            $path_route = '';
        }

        $client->withTrashed();

        if($client->viewed == 0 && $client->consultant_id == Auth::id()){
            $client->viewed = 1;
            $client->save();
        }

        $step->withTrashed();

        $process_progress = $client->getProcessStepProgress($step);

        $completed = "";
        $not_completed = "";

            $activity_progress_name = $activity_step->name;
            $activity_progress = $client->getProcessStepProgress($activity_step);

            $stepw = $activity_progress[0];
            //dd($step['order']);
            foreach ($stepw['activities'] as $activity):
                if (isset($activity['value'])) {
                    $completed .= '<li>' . $activity['name'] . '</li>';
                } else {
                    $not_completed .= '<li>' . $activity['name'] . '</li>';
                }
            endforeach;

        $client->load('referrer', 'office.area.region.division', 'users', 'comments.user','consultant');

        //get step times for graph
        $client_actual_times = [];
        foreach ($process->steps as $actual_time_step) {
            $client_actual_times[$actual_time_step->name] = 0;

            foreach ($actual_time_step->activities as $activity) {
                if (isset($activity->actionable->data[0])) {
                    $client_actual_times[$actual_time_step->name] += 1;
                }
            }
        }

        if($client->step_id == $step->id)
            $client_progress = $process->getStageHex(1);

        if($client->step_id > $step->id)
            $client_progress = $process->getStageHex(2);

        $client_process = ClientProcess::where('client_id',$client->id)->where('process_id',$process->id)->first();

        $steps = Step::where('process_id', $process->id)->orderBy('order','asc')->get();

        $c_step_order = Step::where('id', $client_process->step_id)->withTrashed()->first();

        $step_data = [];
        foreach ($steps as $a_step):
            if ($a_step->deleted_at == null) {
                $progress_color = $process->getStageHex(0);
                $step_stage = 0;

                if ($c_step_order->order == $a_step->order) {
                    $progress_color = $process->getStageHex(1);
                    $step_stage = 1;
                }

                if ($c_step_order->order > $a_step->order) {
                    $progress_color = $process->getStageHex(2);
                    $step_stage = 2;
                }


                $tmp_step = [
                    'id' => $a_step->id,
                    'name' => $a_step->name,
                    'progress_color' => $progress_color,
                    'process_id' => $a_step->process_id,
                    'order' => $a_step->order,
                    'stage' => $step_stage
                ];

                array_push($step_data, $tmp_step);
            }
        endforeach;
        $cnt2 = 0;
        $ns = true;
        foreach ($client->process->activities as $activity) {
            if (isset($activity->actionable['data'][0])) {
                foreach ($activity->actionable['data'] as $data) {
                    if ($data["client_id"] == $client->id && $data["data"] != null) {

                        $cnt2++;
                    }
                }
            } else {
                $ns = false;
            }

        }

        $max_step = Step::orderBy('order','desc')->where('process_id', $process->id)->first();

        //get next step id where order == current order+1
        $n_step = Step::select('id')->orderBy('order','asc')->where('process_id', $process->id)->where('order','>',$step->order)->whereNull('deleted_at')->first();
        //dd($step->id);
        $next_step = $step->id;
        //dd($step->id);
        if($next_step == $max_step->id)
            $next_step = $max_step->id;
        else
            $next_step = (isset($n_step->id) ? $n_step->id : $step->id);



        if($cnt2 > 1){
            $ns = true;
        }

        $status = null;
        $qa_complete = '';

        if($ns){
            $status = 'In Review';
            if($client->is_qa == 1){
                $status = 'In QA';
            }
            if($client->qa_end_date != null){
                $status = 'QA Complete';
            }
        } else {
            $status = 'Not Started';
        }


        $actions = ActionsAssigned::with('client')->whereHas('activity', function($q){
            $q->where('status',0);
        })->where('completed',0)->where('clients',$client->id)->orderBy('id','desc')->get();

        $configs = Config::first();

        $activities = [];
        $auser_array = [];
        $aduedate_array = array();


        //get assigned activities
        foreach ($actions as $activity) {
            $split_users = explode(',', $activity->users);

            if ($activity->client) {

                foreach ($activity->activity as $activity_id) {
                    //$auser_array = array();
                    foreach ($split_users as $user_id) {

                        // User Name
                        $user = User::where('id', trim($user_id))->first();

                        $user_name = $user["first_name"] . ' ' . $user["last_name"];

                        if(!in_array($user_name,$auser_array)) {
                            array_push($auser_array, $user_name);
                        }
                    }

                    if ($activity_id != null && $activity_id->status != 1) {

                        //Get the current timestamp.
                        $now = strtotime(now());

                        //Calculate the difference.
                        $difference = $now - strtotime($activity->due_date);

                        //Convert seconds into days.
                        $days = floor($difference / (60 * 60 * 24));

                        if ($days < -$configs->action_threshold) {
                            $class = $activity->client->process->getStageHex(2);
                        } elseif ($days <= $configs->action_threshold) {
                            if (Carbon::parse(now()) >= Carbon::parse($activity->due_date)->subDay($configs->action_threshold)) {
                                $class = $activity->client->process->getStageHex(2);
                            } else {
                                $class = $activity->client->process->getStageHex(1);
                            }
                        } elseif ($days > $configs->action_threshold) {
                            $class = $activity->client->process->getStageHex(0);
                        } else {
                            $class = $activity->client->process->getStageHex(0);
                        }

                        if (Auth::check() && Auth::user()->isNot("manager") && Auth::id() == $user_id) {
                            $activities[$activity_id->activity_id] = [
                                'step_id' => $activity->step_id,
                                'action_id' => $activity->id,
                                'user' => (isset($activities[$activity_id->activity_id]["user"]) ? array_unique(array_merge($activities[$activity_id->activity_id]["user"],$auser_array)) : $auser_array),
                                'activity_id' => trim($activity_id->activity_id),
                                'due_date' => $activity->due_date,
                                'class' => $class];
                        } elseif (Auth::check() && Auth::user()->is("manager")) {
                            $activities[$activity_id->activity_id] = [
                                'step_id' => $activity->step_id,
                                'action_id' => $activity->id,
                                'user' => (isset($activities[$activity_id->activity_id]["user"]) ? array_unique(array_merge($activities[$activity_id->activity_id]["user"],$auser_array)) : $auser_array),
                                'activity_id' => trim($activity_id->activity_id),
                                'due_date' => $activity->due_date,
                                'class' => $class];
                        }
                    }
                }
            }
        }

        /*$activities_comments = DB::select("SELECT activity_id,COUNT(activity_id) as activity_count,`private` as pr,user_id FROM client_activities_comments where client_id = '".$client->id."' and deleted_at is null group by activity_id,private,user_id");

        $activity_comment = array();


        foreach ($activities_comments as $value) {

            if ((isset($value->pr) && $value->pr > 0) && (isset($value->user_id) && Auth::id() == $value->user_id)) {
                $activity_comment[$value->activity_id] = (isset($activity_comment[$value->activity_id]) ? $activity_comment[$value->activity_id] : 0) + $value->activity_count;
            } else {
                if (isset($value->pr) && $value->pr == 0){
                    $activity_comment[$value->activity_id] = (isset($activity_comment[$value->activity_id]) ? $activity_comment[$value->activity_id] : 0) + $value->activity_count;
                }
            }

        }



        $qa_complete = '';

        if($client->qa_end_date != null){
            //$qa_complete = 'disabled';
        }

        if($client->is_qa == 1 && Auth::user()->is('consultant')){
            $qa_complete = 'disabled';
            if(Auth::user()->is('qa')){
                $qa_complete = '';
            }
        }*/


        $has_permission = true;

        if($client->introducer_id == Auth::id() || $client->consultant_id == Auth::id()){
            $has_permission = true;
        }

        if(Auth::user()->is('manager') || Auth::user()->is('admin')){
            $has_permission = true;
        }

        if(Auth::user()->is('qa') && $client->is_qa == 1 && $client->consultant_id == Auth::id()){
            $has_permission = true;
        }

        //get client basket
        $cp = ClientProcess::select('process_id')->where('client_id',$client->id)->get()->toArray();


        $activities_in_client_basket = $this->clientBucketActivityIds($steps, $client);

        $client_basket_activities = $steps->keyBy('name')->map(function ($step) use ($activities_in_client_basket){
            return $step->activities->filter(function ($activ) use ($activities_in_client_basket){
                return in_array($activ->id, array_unique($activities_in_client_basket));
            })->values();
        });

        $form = Forms::find(2);

        if($form) {
            $forms = $form->getClientDetailsInputValues($client->id, $form->id);

            $helper = new HelperFunction();
            $cb = $helper->clientBucketActivityIds($steps, $client, $client->process_id);
        } else {
            $forms = [];
            $cb = [];
        }

        $data = [
            'config'=>Config::first(),
            'activities' => $activities,
            'step_dropdown' => Step::where('process_id',$client->process_id)->pluck('name','id'),
            'view_process_dropdown' => $client->clientProcessIfActivitiesExist(),
            'process' => Process::whereHas('steps')->where('process_type_id','1')->whereNotIn('id',$cp)->orderBy('name','asc')->pluck('name','id')->prepend('Please Select','0'),
            'users' => User::select('id',DB::raw("CONCAT(CAST(AES_DECRYPT(`hash_first_name`, 'Qwfe345dgfdg') AS CHAR(50)),' ',CAST(AES_DECRYPT(`hash_last_name`, 'Qwfe345dgfdg') AS CHAR(50))) as full_name"))->orderBy('first_name')->orderBy('last_name')->pluck('full_name', 'id'),
            /*'activity_comment' => $activity_comment,*/
            'completed' => $completed,
            'not_completed' => $not_completed,
            'client' => $client,
            'process_id'=>$process->id,
            'process_progress' => $process_progress,
            'process_progress2' => $activity_progress,
            'steps' => $step_data,
            'step' => $step,
            'active' => $step,
            'activity_progress_name'=> $activity_progress_name,
            'max_step' => $max_step->id,
            'next_step' => $next_step,
            'client_actual_times' => $client_actual_times,
            'documents' => Document::orderBy('name')->where('client_id', $client->id)->orWhere('client_id', null)->pluck('name', 'id'),
            'document_options' => Document::orderBy('name')->where('client_id', $client->id)->orWhere('client_id', null)->pluck('name', 'id'),
            'templates' => Template::where('template_type_id','2')->orderBy('name')->pluck('name', 'id'),
            'template_email_options' => EmailTemplate::orderBy('name')->pluck('name', 'id'),
            'path' => $path,
            'path_route' => $path_route,
            'status' =>$status,
            'max_group' => ($client->groupCompletedActivities($step,$client->id) > 0 ? $client->groupCompletedActivities($step,$client->id) :1),
            'qa_complete' => $qa_complete,
            'user_has_permission' => $has_permission,
            'client_basket_activities' => $client_basket_activities,
            'grid_count' => count(Activity::select('position')->where('step_id',$step->id)->groupBy('position')->orderBy('position','desc')->get()),
            'forms' => $forms,
            'in_basket' => $cb
        ];

        return $data;
    }
}