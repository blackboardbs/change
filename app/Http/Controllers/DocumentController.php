<?php

namespace App\Http\Controllers;

use App\Document;
use App\Http\Requests\StoreDocumentRequest;
use App\Http\Requests\UpdateDocumentRequest;
use Illuminate\Http\Request;
use Carbon\Carbon;

class DocumentController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $documents = Document::where('client_id', null)->with('user');

        if ($request->has('q')) {
            $documents->where('name', 'LIKE', "%" . $request->input('q') . "%");
        }

        return view('documents.index')->with(['documents' => $documents->get()]);
    }

    public function create()
    {
        return view('documents.create');
    }

    public function store(StoreDocumentRequest $request)
    {
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $name = Carbon::now()->format('Y-m-d')."-".strtotime(Carbon::now()).".".$file->getClientOriginalExtension();
            $stored = $file->storeAs('documents', $name);
        }

        $document = new Document;
        $document->name = $request->input('name');
        $document->file = $name;
        $document->user_id = auth()->id();

        if ($request->has('client')) {
            $document->client_id = $request->input('client');
        }

        $document->save();

        if ($request->has('client')) {
            return redirect(route('clients.documents', $request->input('client')))->with('flash_success', 'Document uploaded successfully');
        }

        return redirect(route('documents.index'))->with('flash_success', 'Document uploaded successfully');
    }

    public function show($id)
    {
        //
    }

    public function edit(Document $document)
    {
        return view('documents.edit')->with(['document'=> $document]);
    }

    public function update(UpdateDocumentRequest $request, Document $document)
    {
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $name = Carbon::now()->format('Y-m-d')."-".strtotime(Carbon::now()).".".$file->getClientOriginalExtension();
            $stored = $file->storeAs('documents', $name);
            $document->file = $name;
        }

        $document->name = $request->input('name');
        $document->user_id = auth()->id();

        if ($request->has('client')) {
            $document->client_id = $request->input('client');
        }

        $document->save();

        if ($request->has('client')) {
            return redirect(route('clients.documents', $request->input('client')))->with('flash_success', 'Document updated successfully');
        }

        return redirect(route('documents.index'))->with('flash_success', 'Document updated successfully');
    }

    public function destroy($id,$client_id)
    {
        Document::destroy($id);
        //File::delete($request->input('q'));
        if ($client_id != 0) {
            return redirect(route('clients.documents', $client_id))->with('flash_success', 'Document deleted successfully');
        }

        return redirect(route('documents.index'))->with('flash_success', 'Document deleted successfully');
    }
}
