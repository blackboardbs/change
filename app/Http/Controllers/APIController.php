<?php

namespace App\Http\Controllers;

use App\UserNotification;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Client;

class APIController extends Controller
{
    /**
     * Authenticates via OAUTH to the API, sends a JSON string to it, then parses the result and 
     * returns a PDF download.
     * 
     * @param Request $request
     * @return Illuminate\Http\Response
     */
    public function index(Request $request, $clientId) {

        // OAuth2 Token exchange
        $auth = $this->authorise();
        
        // Get the Client details from the DB, in the format expected by the API endpoint
        $input = $this->getSourceData($clientId);

        // Get the document
        $apiReturn = json_decode($this->getDocument($auth, json_encode($input)));

        $base = $apiReturn;

        $dateTime = date('m-d Hi');
        $filename = $input['initials_last_name'].' '.$dateTime.'.pdf';
        
        $file = public_path() . "/" . $filename;
        
        // Temp fix before insert to database, or otherwise convert to pdf from string.    
        file_put_contents($file, base64_decode($base->DocumentData));
       
        $headers = array(
            'Content-Type: application/pdf',
        );  

        return \Response::download($file, $filename, $headers);
    }

    /**
     * Do the needed database queries to retrieve the data to be passed on to the API
     *
     * @return array
     */
    private function getSourceData( int $clientId ) {
        // Load the client
        $client = Client::find($clientId);

        // Transpose into the format expected by DocFusion
        $returnArray = [    'initials_last_name' => $client->initials ." ". $client->last_name,
                            'first_name' => $client->first_name,
                            'maiden_name' => "",
                            'id_number' => $client->id_number,
                            'date_of_birth' => "1983-04-05",
                            'sex' => "M",
                            'marital_status' => "",
                            'marital_contract' => "",
                            'contact_number' => $client->contact,
                            'email_address' => $client->email,
        ];
        
        return $returnArray;
    }


    private function authorise() {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://dr.docfusion-paas.com:44331/core/connect/token?Client_ID=AttoohClient&Client_secret=@tT0o%2523357",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "client_id=AttoohClient&client_secret=@tT0o%23357&grant_type=client_credentials&scope=DocFusion&resource=DocFusion",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/x-www-form-urlencoded"
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        $decoded = json_decode($response, true);
    
        return "Authorization: Bearer ". $decoded['access_token'];
    }

    public function getDocument( $authString, $inputJson ) {
        $curl = curl_init();

        $data = base64_encode($inputJson);

        // For /GenerateDocument
        // $postFields = [     
        //         "BusinessUnitGuid"  => "5ba96b19-5402-4ff9-8dd6-2990365eca81",
        //         "TemplateGuid" => "673f9231-48ca-4e2b-873e-0304317fd203",
        //         "TemplateVersion" => null,
        //         "ChainGuid" => "72f1976c-d7de-4b0b-9e0f-576eab7753e1",
        //         "ChainVersion" => null,
        //         "ProcessAsync" => false,
        //         "RequestData" => $data,
        //         "TemplateData" => "",
        //         "TimeoutMilliseconds" => 60000,
        //         "Parameters" => "",
        // ];

        // For /GenerateDocumentFull
        $postFields =   "{
            \r\n    \"Source\": \"Atooh Test\",
            \r\n    \"JobReference\": \"Atooh Hello World\",
            \r\n    \"Timeout\": \"00:01:00\",
            \r\n    \"Process\": {
            \r\n        \"ProcessType\": 0,
            \r\n        \"BusinessUnitGuid\" : \"5ba96b19-5402-4ff9-8dd6-2990365eca81\",
            \r\n        \"TemplateGuid\": \"673f9231-48ca-4e2b-873e-0304317fd203\",
            \r\n        \"TemplateVersion\": null,
            \r\n        \"ChainGuid\": \"72f1976c-d7de-4b0b-9e0f-576eab7753e1\",
            \r\n        \"ChainVersion\": null,
            \r\n        \"ProcessAsync\": false
            \r\n    \t\t   },
            \r\n    \"Data\": {
            \r\n        \"DataType\": 3,
            \r\n        \"Data\": \"$data\",
            \r\n        \"DataProcessor\": null,
            \r\n        \"ProcessorProfile\": null
            \r\n    },
            \r\n    \"Parameters\": {}\r\n
        }";
        
        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://dr.docfusion-paas.com/api/DocFusionV2/GenerateDocumentFull",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $postFields,
        CURLOPT_HTTPHEADER => array(
            "$authString",
            "Content-Type: application/json"
        ),
        ));
        
        $curlOutput = curl_exec($curl);
      
        curl_close($curl);
        return $curlOutput;
    }
}
