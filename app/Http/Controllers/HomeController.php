<?php

namespace App\Http\Controllers;

use App\ActionableBooleanData;
use App\ActionableDateData;
use App\ActionableDocumentEmailData;
use App\ActionableTemplateEmailData;
use App\ActionableDropdownData;
use App\ActionableDropdownItem;
use App\ActionableTextData;
use App\ActionableTextareaData;
use App\Activity;
use App\Client;
use App\ClientProcess;
use App\Committee;
use App\Config;
use App\Document;
use App\EmailLogs;
use App\HelperFunction;
use App\Process;
use App\Referrer;
use App\RelatedPartiesTree;
use App\RelatedParty;
use App\Step;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\BusinessUnits;

class HomeController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth');
    }

    public function index()
    {
        if (auth()->check()) {
            return redirect(route('clients.index'));
        } else {
            return view('welcome');
        }
    }

    public function recents()
    {
        $clients = ClientProcess::with('client','process','step')->orderBy('created_at','DESC')->take(30)->get();

        //dd($clients);

        $parameters = [
            'clients' => $clients
            /*'referrers' => Referrer::orderBy('created_at','DESC')->take(5)->get(),
            'documents' => Document::orderBy('created_at','DESC')->take(5)->get(),
            'emails' => EmailLogs::orderBy('date','DESC')->take(5)->get()*/
        ];

        return view('recents')->with($parameters);
    }

    public function dashboard(Request $request)
    {

        $config = Config::first();

        if ($request->has('f') && $request->input('f')) {
            $from = Carbon::parse(now())->subMonth(3)->format('Y-m-d');
        } else {
            $from = Carbon::parse(now())->subMonth(3)->format('Y-m-d');
        }

        if ($request->has('t') && $request->input('t')) {
            $to = Carbon::parse($request->input('t'))->format('Y-m-d');
        } else {
            $to = Carbon::parse(now())->format('Y-m-d');
        }

        if ((!$request->has('r') || !$request->has('f') || !$request->has('t') || !$request->has('p'))) {
            return redirect(route('dashboard', ['r' => 'week', 'f' => Carbon::parse(now())->startOfYear()->format("Y-m-d"), 't' => Carbon::now()->toDateString(), 'p' => $config->dashboard_process]));
        }

        if (($request->has('p') && $request->input('p') == '0')) {
            $mi1 = $this->investigation($from,$to);
            $totals1 = $this->investigationTotals($mi1,$from,$to);
            $mi2 = $this->committee($from,$to);
            $totals2 = $this->committeeTotals($mi2);
            $mi3 = $this->closure($from,$to);
            $totals3 = $this->closureTotals($mi3);

            $mi = array();

            /*foreach($mi1['EB'] as $key => $value){
                array
            }

            foreach($mi2['EB'] as $key => $value){
                $mi['EB'] = [$key => $value];
            }*/
        }

        if (($request->has('p') && $request->input('p') == '0_1')) {
            $mi = $this->investigation($from,$to);
            $totals = $this->investigationTotals($mi,$from,$to);
        }

        if (($request->has('p') && $request->input('p') == '0_2')) {
            $mi = $this->committee($from,$to);
            $totals = $this->committeeTotals($mi);
        }

        if (($request->has('p') && $request->input('p') == '0_3')) {
            $mi = $this->closure($from,$to);
            $totals = $this->closureTotals($mi);
        }

        if (($request->has('p') && $request->input('p') == '0_4')) {
            $mi = $this->sla();
            $totals = 0;
        }

        if ($request->has('p') && $request->input('p') != 0  && $request->input('p') != '0_1' && $request->input('p') != '0_2'  && $request->input('p') != '0_3'  && $request->input('p') != '0_4' ) {


            $processes = Process::orderBy('id','asc')->pluck('name', 'id');

            $dashboard_regions = explode(',',$config->dashboard_regions);

            if ($request->has('r')) {
                $range = $request->input('r');
            } else {
                $range = 'week';
            }
            if ($request->has('f')) {
                $from = Carbon::parse($request->input('f'));
            } else {
                $from = Carbon::createFromFormat('Y-m-d', '2010-01-01');
            }

            if ($request->has('t')) {
                $to = Carbon::parse($request->input('t'));
            } else {
                $to = Carbon::now();
            }

            $to->addHours(23)->addMinutes(59);

            $stepSelect = Step::where('process_id', $config->dashboard_process)->orderBy('order')->pluck('name', 'id')->toArray();

            $process = Process::where('id', $request->input('p'))->with('steps.activities.actionable.data')->first();
            $client_step_counts = $this->getClientStepCounts($process, $from, $to);
            $client_converted_counts = $this->getConvertedCount($process, $from, $to);
            $client_onboard_times = $this->getClientOnboardTimes($process, $from, $to);
            $client_onboards = $this->getCompletedClients($process, $from, $to, $range);
            $process_average_times = $this->getProcessAverageTimes($process, $from, $to);
            $process_outstanding_activities = $this->getOutstandingActivities($process->id,$config->dashboard_outstanding_step);

            $outstanding_step_name = Step::where('id',$config->dashboard_outstanding_step)->first();
            $regions = Step::whereIn('id',$dashboard_regions)->orderBy('order','asc')->get()->toArray();
        } else {
            $stepSelect = [];
            $from = '';
            $to = '';
            $processes = [];
            $client_step_counts = [];
            $client_converted_counts = [];
            $client_onboard_times = [];
            $client_onboards = [];
            $process_average_times = [];
            $process_outstanding_activities = [];

            $outstanding_step_name = [];
            $regions = [];
        }

        // Steps formatted for a select control


        $parameters = [
            'client_step_counts' => $client_step_counts,
            'client_converted_count' => $client_converted_counts,
            'client_onboard_times' => $client_onboard_times,
            'client_onboards' => $client_onboards,
            'process_average_times' => $process_average_times,
            'process_outstanding_activities' => $process_outstanding_activities,
            'outstanding_activity_name' => $outstanding_step_name,
            'outstanding_activity_select' => $stepSelect,
            'from' => $from,
            'to' => $to,
            'config' => $config,
            'processes' => $processes,
            'regions' => $regions,
            /*'clients' => Client::select(DB::raw("CONCAT(first_name,' ',COALESCE(`last_name`,'')) AS full_name"), 'id')->orderBy('first_name')->pluck('full_name', 'id'),*/
            'todate' => Carbon::parse($request->input('t'))->format('d-M-Y'),
            'mi'=>(isset($mi) ? $mi : []),
            'totals'=>(isset($totals) ? $totals : []),
            'mi1'=>(isset($mi1) ? $mi1 : []),
            'totals1'=>(isset($totals1) ? $totals1 : []),
            'mi2'=>(isset($mi2) ? $mi2 : []),
            'totals2'=>(isset($totals2) ? $totals2 : []),
            'mi3'=>(isset($mi3) ? $mi3 : []),
            'totals3'=>(isset($totals3) ? $totals3 : [])
        ];

        return view('dashboard')->with($parameters);
    }

    public function sla(){
        $helper = new HelperFunction();

        $clients = Client::with('related_parties', 'process')
            ->whereNotNull('instruction_date')
            ->get(['id', 'instruction_date', 'viewed', 'consultant_id', 'is_qa', 'process_id', 'qa_end_date', 'completed']);

        $clients_not_started = $helper->sla_helper($clients, ['viewed', 'is_qa'], [0, 0]);
        $clients_in_progress = $helper->sla_helper($clients, ['viewed', 'is_qa'], [1, 0]);
        $clients_in_qa = $helper->sla_helper($clients, ['is_qa', 'qa_end_date'], [1, null]);
        $clients_exit_closeout = $helper->sla_helper($clients, ['process_id', 'completed'], [12, 1]);

        return [
            'not_started_30_days' => $clients_not_started->sum('thirty_days'),
            'not_started_60_days' => $clients_not_started->sum('sixty_days'),
            'not_started_90_days' => $clients_not_started->sum('ninety_days'),
            'not_started_90_plus_days' => $clients_not_started->sum('ninety_days_plus'),
            'in_progress_30_days' => $clients_in_progress->sum('thirty_days'),
            'in_progress_60_days' => $clients_in_progress->sum('sixty_days'),
            'in_progress_90_days' => $clients_in_progress->sum('ninety_days'),
            'in_progress_90_plus_days' => $clients_in_progress->sum('ninety_days_plus'),
            'in_qa_30_days' => $clients_in_qa->sum('thirty_days'),
            'in_qa_60_days' => $clients_in_qa->sum('sixty_days'),
            'in_qa_90_days' => $clients_in_qa->sum('ninety_days'),
            'in_qa_90_plus_days' => $clients_in_qa->sum('ninety_days_plus'),
            'exit_closeout_30_days' => $clients_exit_closeout->sum('thirty_days'),
            'exit_closeout_60_days' => $clients_exit_closeout->sum('sixty_days'),
            'exit_closeout_90_days' => $clients_exit_closeout->sum('ninety_days'),
            'exit_closeout_90_plus_days' => $clients_exit_closeout->sum('ninety_days_plus'),
        ];
    }

    public function investigation($from,$to){

        for ($i = 0; $i <= Carbon::parse($to)->diffInMonths(Carbon::parse($from)); $i++) {

            $instruction_captured = 0;
            $out_of_scope = 0;
            $not_started = 0;
            $started = 0;
            $not_submitted = 0;
            $submitted = 0;

            if (Carbon::parse($from)->addMonth($i)->format('Y-m') <= '2019-10') {
                $mi['EB']['July - Oct'] = [
                    'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                    'instruction_date' => $instruction_captured + 145,
                    'out_of_scope' => $out_of_scope + 6,
                    'not_started' => $not_started,
                    'started' => $started,
                    'notsubmitted' => $not_submitted + 34,
                    'submitted' => $submitted + 105,
                ];
                $mi['RBB']['July - Oct'] = [
                    'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                    'instruction_date' => $instruction_captured + 251,
                    'out_of_scope' => $out_of_scope + 30,
                    'not_started' => $not_started,
                    'started' => $started,
                    'notsubmitted' => $not_submitted + 41,
                    'submitted' => $submitted + 180,
                ];
                $mi['RB']['July - Oct'] = [
                    'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                    'instruction_date' => $instruction_captured + 682,
                    'out_of_scope' => $out_of_scope + 81,
                    'not_started' => $not_started,
                    'started' => $started,
                    'notsubmitted' => $not_submitted + 136,
                    'submitted' => $submitted + 465,
                ];

            }

            if (Carbon::parse($from)->addMonth($i)->format('Y-m') == '2019-11') {
                $mi['EB']['Nov'] = [
                    'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                    'instruction_date' => $instruction_captured + 6,
                    'out_of_scope' => $out_of_scope,
                    'not_started' => $not_started,
                    'started' => $started,
                    'notsubmitted' => $not_submitted,
                    'submitted' => $submitted + 6,
                ];
                $mi['RBB']['Nov'] = [
                    'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                    'instruction_date' => $instruction_captured + 59,
                    'out_of_scope' => $out_of_scope + 13,
                    'not_started' => $not_started,
                    'started' => $started,
                    'notsubmitted' => $not_submitted + 21,
                    'submitted' => $submitted + 25,
                ];
                $mi['RB']['Nov'] = [
                    'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                    'instruction_date' => $instruction_captured + 83,
                    'out_of_scope' => $out_of_scope + 2,
                    'not_started' => $not_started,
                    'started' => $started,
                    'notsubmitted' => $not_submitted + 10,
                    'submitted' => $submitted + 71,
                ];

            }
            if (Carbon::parse($from)->addMonth($i)->format('Y-m') == '2019-12') {
                $mi['EB']['Dec'] = [
                    'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                    'instruction_date' => $instruction_captured + 3,
                    'out_of_scope' => $out_of_scope,
                    'not_started' => $not_started,
                    'started' => $started,
                    'notsubmitted' => $not_submitted+1,
                    'submitted' => $submitted + 2,
                ];

                $mi['RBB']['Dec'] = [
                    'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                    'instruction_date' => $instruction_captured + 45,
                    'out_of_scope' => $out_of_scope + 8,
                    'not_started' => $not_started,
                    'started' => $started,
                    'notsubmitted' => $not_submitted + 18,
                    'submitted' => $submitted + 19,
                ];
                $mi['RB']['Dec'] = [
                    'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                    'instruction_date' => $instruction_captured + 41,
                    'out_of_scope' => $out_of_scope + 5,
                    'not_started' => $not_started,
                    'started' => $started,
                    'notsubmitted' => $not_submitted + 26,
                    'submitted' => $submitted + 10,
                ];
            }

            if (Carbon::parse($from)->addMonth($i)->format('Y-m') == '2020-01') {

                $mi['EB']['Jan'] = [
                    'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                    'instruction_date' => $instruction_captured + 3,
                    'out_of_scope' => $out_of_scope,
                    'not_started' => $not_started,
                    'started' => $started+1,
                    'notsubmitted' => $not_submitted + 1,
                    'submitted' => $submitted + 1,
                ];
                $mi['RBB']['Jan'] = [
                    'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                    'instruction_date' => $instruction_captured + 25,
                    'out_of_scope' => $out_of_scope+1,
                    'not_started' => $not_started,
                    'started' => $started + 10,
                    'notsubmitted' => $not_submitted + 14,
                    'submitted' => $submitted,
                ];
                $mi['RB']['Jan'] = [
                    'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                    'instruction_date' => $instruction_captured + 68,
                    'out_of_scope' => $out_of_scope,
                    'not_started' => $not_started,
                    'started' => $started,
                    'notsubmitted' => $not_submitted + 56,
                    'submitted' => $submitted + 12,
                ];

            }

            if (Carbon::parse($from)->addMonth($i)->format('Y-m') == '2020-02') {

                $mi['EB']['Feb'] = [
                    'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                    'instruction_date' => $instruction_captured,
                    'out_of_scope' => $out_of_scope,
                    'not_started' => $not_started,
                    'started' => $started,
                    'notsubmitted' => $not_submitted,
                    'submitted' => $submitted,
                ];
                $mi['RBB']['Feb'] = [
                    'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                    'instruction_date' => $instruction_captured,
                    'out_of_scope' => $out_of_scope,
                    'not_started' => $not_started,
                    'started' => $started,
                    'notsubmitted' => $not_submitted,
                    'submitted' => $submitted,
                ];
                $mi['RB']['Feb'] = [
                    'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                    'instruction_date' => $instruction_captured,
                    'out_of_scope' => $out_of_scope,
                    'not_started' => $not_started,
                    'started' => $started,
                    'notsubmitted' => $not_submitted,
                    'submitted' => $submitted,
                ];

            }

            //out of scope
            $clients = Client::with('process.steps.activities.actionable.data')->whereDate('instruction_date', '>=', Carbon::parse($from)->addMonth($i)->startOfMonth()->format('Y-m-d'))->whereDate('instruction_date', '<=', Carbon::parse($from)->addMonth($i)->endOfMonth()->format('Y-m-d'))->get();


            $business_units = Committee::all();

            foreach ($business_units as $business_unit) {

                if (in_array($business_unit->id, array('1', '2', '3'))) {
                    if ($business_unit->id == '1') {
                        $bu = 'RBB';
                    }
                    if ($business_unit->id == '3') {
                        $bu = 'EB';
                    }
                    if ($business_unit->id == '2') {
                        $bu = 'RB';
                    }


                    if (!isset($mi[$bu][Carbon::parse($from)->addMonth($i)->format('M')])) {
                        $mi[$bu][Carbon::parse($from)->addMonth($i)->format('M')] = [
                            'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                            'instruction_date' => $instruction_captured,
                            'out_of_scope' => $out_of_scope,
                            'not_started' => $not_started,
                            'started' => $started,
                            'notsubmitted' => $not_submitted,
                            'submitted' => $submitted,
                        ];
                    }
                    $clients = Client::with('process.steps.activities.actionable.data')->where('committee_id', $business_unit->id)->whereDate('instruction_date', '>=', Carbon::parse($from)->addMonth($i)->startOfMonth()->format('Y-m-d'))->whereDate('instruction_date', '<=', Carbon::parse($from)->addMonth($i)->endOfMonth()->format('Y-m-d'))->get();

                    //out of scope
                    $oos = Client::select(DB::raw("count(committee_id) as cnt"))->where('committee_id', $business_unit->id)->where('out_of_scope', '1')->where('instruction_date', '>=', Carbon::parse($from)->addMonth($i)->startOfMonth()->format('Y-m-d'))->where('instruction_date', '<=', Carbon::parse($from)->addMonth($i)->endOfMonth()->format('Y-m-d'))->first();

                    foreach ($clients as $client) {
                        $cnsubmitted = 0;
                        $csubmitted = 0;
                        $hasstarted = 0;
                        $activities = Activity::with('actionable.data')->orWhere('id', '545')->get();

                        if ($client->completed == '1' && $client->qa_end_date != null) {
                            $csubmitted = 1;
                        }


                        if ($client->completed == '1' && $client->is_qa == '1' &&  !isset($client->qa_end_date)) {
                            $cnsubmitted = 1;
                        }

                        if ($client->is_qa == '0' && !isset($client->qa_end_date)) {

                            $hasstarted = 1;
                        }


                        if ($business_unit->id != null) {
                            if (Carbon::parse($from)->addMonth($i)->format('Y-m') <= '2019-10') {
                                /*$mi[$bu]['July - Oct'] = [
                                    'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                                    'instruction_date' => ($client->instruction_date != null ? $mi[$bu]['July - Oct']['instruction_date'] + 1 : $mi[$bu]['July - Oct']['instruction_date']),
                                    'out_of_scope' => $mi[$bu]['July - Oct']['out_of_scope'],
                                    'not_started' => ($client->instruction_date != null && $client->consultant_id == null && $client->committee_id != '4' ? $mi[$bu]['July - Oct']['not_started'] + 1 : $mi[$bu]['July - Oct']['not_started']),
                                    'started' => ($client->instruction_date != null && $client->consultant_id != null && $client->viewed == '1' && $client->committee_id != '4' ? $mi[$bu]['July - Oct']['started'] + 1 : $mi[$bu]['July - Oct']['started']),
                                    'notsubmitted' => ($cnsubmitted > 0 ? $mi[$bu]['July - Oct']['notsubmitted'] + 1 : $mi[$bu]['July - Oct']['notsubmitted']),
                                    'submitted' => ($csubmitted > 0 ? $mi[$bu]['July - Oct']['submitted'] + 1 : $mi[$bu]['July - Oct']['submitted']),
                                ];*/
                            } elseif (Carbon::parse($from)->addMonth($i)->format('Y-m') == '2019-11') {
                                /*$mi[$bu]['Nov'] = [
                                    'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                                    'instruction_date' => ($client->instruction_date != null ? $mi[$bu]['Nov']['instruction_date'] + 1 : $mi[$bu]['Nov']['instruction_date']),
                                    'out_of_scope' => $mi[$bu]['Nov']['out_of_scope'],
                                    'not_started' => ($client->instruction_date != null && $client->consultant_id == null && $client->committee_id != '4' ? $mi[$bu]['Nov']['not_started'] + 1 : $mi[$bu]['Nov']['not_started']),
                                    'started' => ($client->instruction_date != null && $client->consultant_id != null && $client->viewed == '1' && $client->committee_id != '4' ? $mi[$bu]['Nov']['started'] + 1 : $mi[$bu]['Nov']['started']),
                                    'notsubmitted' => ($cnsubmitted > 0 ? $mi[$bu]['Nov']['notsubmitted'] + 1 : $mi[$bu]['Nov']['notsubmitted']),
                                    'submitted' => ($csubmitted > 0 ? $mi[$bu]['Nov']['submitted'] + 1 : $mi[$bu]['Nov']['submitted']),
                                ];*/
                            } elseif (Carbon::parse($from)->addMonth($i)->format('Y-m') == '2019-12') {
                                /*$mi[$bu]['Dec'] = [
                                    'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                                    'instruction_date' => ($client->instruction_date != null ? $mi[$bu]['Dec']['instruction_date'] + 1 : $mi[$bu]['Dec']['instruction_date']),
                                    'out_of_scope' => $oos->cnt,
                                    'not_started' => ($client->instruction_date != null && $client->consultant_id == null && $client->committee_id != '4' ? $mi[$bu]['Dec']['not_started'] + 1 : $mi[$bu]['Dec']['not_started']),
                                    'started' => ($client->instruction_date != null && $client->consultant_id != null && $client->viewed == '1' && $client->committee_id != '4' ? $mi[$bu]['Dec']['started'] + 1 : $mi[$bu]['Dec']['started']),
                                    'notsubmitted' => ($cnsubmitted > 0 ? $mi[$bu]['Dec']['notsubmitted'] + 1 : $mi[$bu]['Dec']['notsubmitted']),
                                    'submitted' => ($csubmitted > 0 ? $mi[$bu]['Dec']['submitted'] + 1 : $mi[$bu]['Dec']['submitted']),
                                ];*/
                            } elseif (Carbon::parse($from)->addMonth($i)->format('Y-m') == '2020-01') {
                                /*$mi[$bu]['Jan'] = [
                                    'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                                    'instruction_date' => ($client->instruction_date != null ? $mi[$bu]['Jan']['instruction_date'] + 1 : $mi[$bu]['Jan']['instruction_date']),
                                    'out_of_scope' => $oos->cnt,
                                    'not_started' => ($client->instruction_date != null && $client->consultant_id == null && $client->committee_id != '4' ? $mi[$bu]['Jan']['not_started'] + 1 : $mi[$bu]['Jan']['not_started']),
                                    'started' => ($client->instruction_date != null && $client->consultant_id != null && $client->viewed == '1' && $client->committee_id != '4' ? $mi[$bu]['Jan']['started'] + 1 : $mi[$bu]['Jan']['started']),
                                    'notsubmitted' => ($cnsubmitted > 0 ? $mi[$bu]['Jan']['notsubmitted'] + 1 : $mi[$bu]['Jan']['notsubmitted']),
                                    'submitted' => ($csubmitted > 0 ? $mi[$bu]['Jan']['submitted'] + 1 : $mi[$bu]['Jan']['submitted']),
                                ];*/
                            } else {
                                $mi[$bu][Carbon::parse($from)->addMonth($i)->format('M')] = [
                                    'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                                    'instruction_date' => ($client->instruction_date != null ? $mi[$bu][Carbon::parse($from)->addMonth($i)->format('M')]['instruction_date'] + 1 : $mi[$bu][Carbon::parse($from)->addMonth($i)->format('M')]['instruction_date']),
                                    'out_of_scope' => $oos->cnt,
                                    'not_started' => ($client->instruction_date != null && $client->viewed == '0' && $client->out_of_scope != 1 && $client->completed == '0' ? $mi[$bu][Carbon::parse($from)->addMonth($i)->format('M')]['not_started'] + 1 : $mi[$bu][Carbon::parse($from)->addMonth($i)->format('M')]['not_started']),
                                    'started' => ($client->instruction_date != null && $client->consultant_id != null && $client->viewed == '1' && $client->out_of_scope != 1 && $hasstarted == '1' ? $mi[$bu][Carbon::parse($from)->addMonth($i)->format('M')]['started'] + 1 : $mi[$bu][Carbon::parse($from)->addMonth($i)->format('M')]['started']),
                                    'notsubmitted' => ($cnsubmitted >= 1 && $client->out_of_scope != 1 ? $mi[$bu][Carbon::parse($from)->addMonth($i)->format('M')]['notsubmitted'] + 1 : $mi[$bu][Carbon::parse($from)->addMonth($i)->format('M')]['notsubmitted']),
                                    'submitted' => ($csubmitted >= 1 && $client->out_of_scope != 1 ? $mi[$bu][Carbon::parse($from)->addMonth($i)->format('M')]['submitted'] + 1 : $mi[$bu][Carbon::parse($from)->addMonth($i)->format('M')]['submitted']),
                                ];
                            }
                        }
                    }

                    $related_party_tree = RelatedPartiesTree::select('related_party_id')->get();

                    $related_parties = RelatedParty::where('committee_id', $business_unit->id)->where('instruction_date', '>=', Carbon::parse($from)->addMonth($i)->startOfMonth()->format('Y-m-d'))->where('instruction_date', '<=', Carbon::parse($from)->addMonth($i)->endOfMonth()->format('Y-m-d'))->whereIn('id',collect($related_party_tree)->toArray())->get();

                    //out of scope
                    $rpoos = RelatedParty::select(DB::raw("count(committee_id) as cnt"))->where('committee_id', $business_unit->id)->where('out_of_scope', 1)->where('instruction_date', '>=', Carbon::parse($from)->addMonth($i)->startOfMonth()->format('Y-m-d'))->where('instruction_date', '<=', Carbon::parse($from)->addMonth($i)->endOfMonth()->format('Y-m-d'))->first();

                    if (isset($mi[$bu][Carbon::parse($from)->addMonth($i)->format('M')]['out_of_scope'])) {
                        $rpooscnt = $rpoos->cnt + $mi[$bu][Carbon::parse($from)->addMonth($i)->format('M')]['out_of_scope'];
                    } else {
                        $rpooscnt = $rpoos->cnt;
                    }
                    foreach ($related_parties as $rp) {

                        $cnsubmitted2 = 0;
                        $csubmitted2 = 0;
                        $hasstarted = 0;

                        $rpclient = Client::where('id', $rp->client_id)->first();

                        $consultant_id = $rpclient->consultant_id;

                        if ($rp->completed == '1' && $rpclient->qa_end_date != null) {
                            $csubmitted2 = 1;
                        }


                        if ($rp->completed == '1' && $rpclient->is_qa == '1' && !isset($rpclient->qa_end_date)) {

                            $cnsubmitted2 = 1;
                        }

                        if ($rpclient->is_qa == '0' && !isset($rpclient->qa_end_date)) {

                            $hasstarted = 1;
                        }


                        //$instruction_captured = ($rp->instruction_date != null ? $instruction_captured+1 : $instruction_captured);
                        if (Carbon::parse($from)->addMonth($i)->format('Y-m') <= '2019-10') {
                            /*$mi[$bu]['July - Oct'] = [
                                'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                                'instruction_date' => ($rp->instruction_date != null ? $mi[$bu]['July - Oct']['instruction_date'] + 1 : $mi[$bu]['July - Oct']['instruction_date']),
                                'out_of_scope' => $mi[$bu]['July - Oct']['out_of_scope'],
                                'not_started' => ($rp->instruction_date != null && $consultant_id == null && $rp->viewed == '0' ? $mi[$bu]['July - Oct']['not_started'] + 1 : $mi[$bu]['July - Oct']['not_started']),
                                'started' => ($rp->instruction_date != null && $consultant_id != null && $rp->viewed == '1' ? $mi[$bu]['July - Oct']['started'] + 1 : $mi[$bu]['July - Oct']['started']),
                                'notsubmitted' => ($cnsubmitted2 > 0 ? $mi[$bu]['July - Oct']['notsubmitted'] + 1 : $mi[$bu]['July - Oct']['notsubmitted']),
                                'submitted' => ($csubmitted2 > 0 ? $mi[$bu]['July - Oct']['submitted'] + 1 : $mi[$bu]['July - Oct']['submitted']),
                            ];*/
                        } elseif (Carbon::parse($from)->addMonth($i)->format('Y-m') == '2019-11') {
                            /*$mi[$bu]['Nov'] = [
                                'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                                'instruction_date' => ($rp->instruction_date != null ? $mi[$bu]['Nov']['instruction_date'] + 1 : $mi[$bu]['Nov']['instruction_date']),
                                'out_of_scope' => $mi[$bu]['Nov']['out_of_scope'],
                                'not_started' => ($rp->instruction_date != null && $consultant_id == null && $rp->viewed == '0' ? $mi[$bu]['Nov']['not_started'] + 1 : $mi[$bu]['Nov']['not_started']),
                                'started' => ($rp->instruction_date != null && $consultant_id != null && $rp->viewed == '1' ? $mi[$bu]['Nov']['started'] + 1 : $mi[$bu]['Nov']['started']),
                                'notsubmitted' => ($cnsubmitted2 > 0 ? $mi[$bu]['Nov']['notsubmitted'] + 1 : $mi[$bu]['Nov']['notsubmitted']),
                                'submitted' => ($csubmitted2 > 0 ? $mi[$bu]['Nov']['submitted'] + 1 : $mi[$bu]['Nov']['submitted']),
                            ];*/
                        } elseif (Carbon::parse($from)->addMonth($i)->format('Y-m') == '2019-12') {
                            /*$mi[$bu]['Dec'] = [
                                'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                                'instruction_date' => ($rp->instruction_date != null ? $mi[$bu]['Dec']['instruction_date'] + 1 : $mi[$bu]['Dec']['instruction_date']),
                                'out_of_scope' => $mi[$bu]['Dec']['out_of_scope'],
                                'not_started' => ($rp->instruction_date != null && $consultant_id == null && $rp->viewed == '0' ? $mi[$bu]['Dec']['not_started'] + 1 : $mi[$bu]['Dec']['not_started']),
                                'started' => ($rp->instruction_date != null && $consultant_id != null && $rp->viewed == '1' ? $mi[$bu]['Dec']['started'] + 1 : $mi[$bu]['Dec']['started']),
                                'notsubmitted' => ($cnsubmitted2 > 0 ? $mi[$bu]['Dec']['notsubmitted'] + 1 : $mi[$bu]['Dec']['notsubmitted']),
                                'submitted' => ($csubmitted2 > 0 ? $mi[$bu]['Dec']['submitted'] + 1 : $mi[$bu]['Dec']['submitted']),
                            ];*/
                        } elseif (Carbon::parse($from)->addMonth($i)->format('Y-m') == '2020-01') {
                            /*$mi[$bu]['Jan'] = [
                                'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                                'instruction_date' => ($rp->instruction_date != null ? $mi[$bu]['Jan']['instruction_date'] + 1 : $mi[$bu]['Jan']['instruction_date']),
                                'out_of_scope' => $mi[$bu]['Jan']['out_of_scope'],
                                'not_started' => ($rp->instruction_date != null && $consultant_id == null && $rp->viewed == '0' ? $mi[$bu]['Jan']['not_started'] + 1 : $mi[$bu]['Jan']['not_started']),
                                'started' => ($rp->instruction_date != null && $consultant_id != null && $rp->viewed == '1' ? $mi[$bu]['Jan']['started'] + 1 : $mi[$bu]['Jan']['started']),
                                'notsubmitted' => ($cnsubmitted2 > 0 ? $mi[$bu]['Jan']['notsubmitted'] + 1 : $mi[$bu]['Jan']['notsubmitted']),
                                'submitted' => ($csubmitted2 > 0 ? $mi[$bu]['Jan']['submitted'] + 1 : $mi[$bu]['Jan']['submitted']),
                            ];*/
                        } else {
                            $mi[$bu][Carbon::parse($from)->addMonth($i)->format('M')] = [
                                'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                                'instruction_date' => ($rp->instruction_date != null ? $mi[$bu][Carbon::parse($from)->addMonth($i)->format('M')]['instruction_date'] + 1 : $mi[$bu][Carbon::parse($from)->addMonth($i)->format('M')]['instruction_date']),
                                'out_of_scope' => $rpooscnt,
                                'not_started' => ($rp->instruction_date != null && $rp->viewed == '0' && $rp->out_of_scope != 1 && $rp->completed == '0' ? $mi[$bu][Carbon::parse($from)->addMonth($i)->format('M')]['not_started'] + 1: $mi[$bu][Carbon::parse($from)->addMonth($i)->format('M')]['not_started']),
                                'started' => ($rp->instruction_date != null && $consultant_id != null && $rp->viewed == '1' && $rp->out_of_scope != 1 && $hasstarted == '1' ? $mi[$bu][Carbon::parse($from)->addMonth($i)->format('M')]['started'] + 1 : $mi[$bu][Carbon::parse($from)->addMonth($i)->format('M')]['started']),
                                'notsubmitted' => ($cnsubmitted2 >= 1 && $rp->out_of_scope != 1 ? $mi[$bu][Carbon::parse($from)->addMonth($i)->format('M')]['notsubmitted'] + 1 : $mi[$bu][Carbon::parse($from)->addMonth($i)->format('M')]['notsubmitted']),
                                'submitted' => ($csubmitted2 >= 1 && $rp->out_of_scope != 1 ? $mi[$bu][Carbon::parse($from)->addMonth($i)->format('M')]['submitted'] + 1 : $mi[$bu][Carbon::parse($from)->addMonth($i)->format('M')]['submitted']),
                            ];
                        }

                    }


                }
            }
        }

        return $mi;
    }

    public function investigationTotals($mi,$from,$to){

        $cnt = 0;

        for ($i = 0; $i <= Carbon::parse($to)->diffInMonths(Carbon::parse($from)); $i++) {
            $oos = Client::select(DB::raw("count(committee_id) as cnt"))->where('committee_id', '4')->where('instruction_date', '>=', Carbon::parse($from)->addMonth($i)->startOfMonth()->format('Y-m-d'))->where('instruction_date', '<=', Carbon::parse($from)->addMonth($i)->endOfMonth()->format('Y-m-d'))->first();
            $rpoos = RelatedParty::select(DB::raw("count(committee_id) as cnt"))->where('committee_id', '4')->where('instruction_date', '>=', Carbon::parse($from)->addMonth($i)->startOfMonth()->format('Y-m-d'))->where('instruction_date', '<=', Carbon::parse($from)->addMonth($i)->endOfMonth()->format('Y-m-d'))->first();

            $cnt = $cnt+$oos->cnt+$rpoos->cnt;
        }

        /*$totals['instruction_date'] = 348;
        $totals['out_of_scope'] = (28+$cnt);
        $totals['not_started'] = 21;
        $totals['started'] = 16;
        $totals['notsubmitted'] = 140;
        $totals['submitted'] = 143;*/
        foreach($mi as $key => $value){
            foreach($value as $key2 => $value3){
                foreach($value3 as $key3 => $value2){
                    if($key3 != 'year') {
                        if (!isset($totals[$key3])) {
                            $totals[$key3] = $value2;
                        } else {
                            $totals[$key3] += $value2;
                        }
                    }
                }
            }
        }

        return $totals;
    }

    public function committee($from,$to){
        for ($i = 0; $i <= Carbon::parse($to)->diffInMonths(Carbon::parse($from)); $i++) {

            $notmade = 0;
            $made = 0;
            $approve = 0;
            $decline = 0;
            $retain = 0;
            $exit = 0;
            $watchlist = 0;

            if (Carbon::parse($from)->addMonth($i)->format('Y-m') <= '2019-10') {
                $mi['EB']['July - Oct'] = [
                    'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                    'notmade' => $notmade+43,
                    'made' => $made+62,
                    'approve' => $approve+8,
                    'decline' => $decline+1,
                    'retain' => $retain+33,
                    'exit' => $exit+13,
                    'watchlist' => $watchlist+7,
                ];
                $mi['RBB']['July - Oct'] = [
                    'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                    'notmade' => $notmade+133,
                    'made' => $made+47,
                    'approve' => $approve+3,
                    'decline' => $decline,
                    'retain' => $retain+2,
                    'exit' => $exit+18,
                    'watchlist' => $watchlist+24,
                ];
                $mi['RB']['July - Oct'] = [
                    'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                    'notmade' => $notmade+198,
                    'made' => $made+267,
                    'approve' => $approve+14,
                    'decline' => $decline+8,
                    'retain' => $retain+152,
                    'exit' => $exit+68,
                    'watchlist' => $watchlist+25,
                ];

            }

            if (Carbon::parse($from)->addMonth($i)->format('Y-m') == '2019-11') {
                $mi['EB']['Nov'] = [
                    'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                    'notmade' => $notmade+4,
                    'made' => $made+2,
                    'approve' => $approve,
                    'decline' => $decline,
                    'retain' => $retain+1,
                    'exit' => $exit+1,
                    'watchlist' => $watchlist,
                ];
                $mi['RBB']['Nov'] = [
                    'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                    'notmade' => $notmade+22,
                    'made' => $made+3,
                    'approve' => $approve,
                    'decline' => $decline,
                    'retain' => $retain+2,
                    'exit' => $exit+1,
                    'watchlist' => $watchlist,
                ];
                $mi['RB']['Nov'] = [
                    'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                    'notmade' => $notmade+65,
                    'made' => $made+6,
                    'approve' => $approve+1,
                    'decline' => $decline+1,
                    'retain' => $retain,
                    'exit' => $exit+4,
                    'watchlist' => $watchlist,
                ];

            }

            if (Carbon::parse($from)->addMonth($i)->format('Y-m') == '2019-12') {
                $mi['EB']['Dec'] = [
                    'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                    'notmade' => $notmade,
                    'made' => $made+2,
                    'approve' => $approve+1,
                    'decline' => $decline,
                    'retain' => $retain+1,
                    'exit' => $exit,
                    'watchlist' => $watchlist,
                ];
                $mi['RBB']['Dec'] = [
                    'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                    'notmade' => $notmade+18,
                    'made' => $made+1,
                    'approve' => $approve+1,
                    'decline' => $decline,
                    'retain' => $retain,
                    'exit' => $exit,
                    'watchlist' => $watchlist,
                ];
                $mi['RB']['Dec'] = [
                    'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                    'notmade' => $notmade+10,
                    'made' => $made,
                    'approve' => $approve,
                    'decline' => $decline,
                    'retain' => $retain,
                    'exit' => $exit,
                    'watchlist' => $watchlist,
                ];

            }

            if (Carbon::parse($from)->addMonth($i)->format('Y-m') == '2020-01') {
                $mi['EB']['Jan'] = [
                    'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                    'notmade' => $notmade+1,
                    'made' => $made,
                    'approve' => $approve,
                    'decline' => $decline,
                    'retain' => $retain,
                    'exit' => $exit,
                    'watchlist' => $watchlist,
                ];
                $mi['RBB']['Jan'] = [
                    'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                    'notmade' => $notmade,
                    'made' => $made,
                    'approve' => $approve,
                    'decline' => $decline,
                    'retain' => $retain,
                    'exit' => $exit,
                    'watchlist' => $watchlist,
                ];
                $mi['RB']['Jan'] = [
                    'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                    'notmade' => $notmade+12,
                    'made' => $made,
                    'approve' => $approve,
                    'decline' => $decline,
                    'retain' => $retain,
                    'exit' => $exit,
                    'watchlist' => $watchlist,
                ];

            }

            if (Carbon::parse($from)->addMonth($i)->format('Y-m') == '2020-02') {
                $mi['EB']['Feb'] = [
                    'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                    'notmade' => $notmade,
                    'made' => $made,
                    'approve' => $approve,
                    'decline' => $decline,
                    'retain' => $retain,
                    'exit' => $exit,
                    'watchlist' => $watchlist,
                ];
                $mi['RBB']['Feb'] = [
                    'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                    'notmade' => $notmade,
                    'made' => $made,
                    'approve' => $approve,
                    'decline' => $decline,
                    'retain' => $retain,
                    'exit' => $exit,
                    'watchlist' => $watchlist,
                ];
                $mi['RB']['Feb'] = [
                    'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                    'notmade' => $notmade,
                    'made' => $made,
                    'approve' => $approve,
                    'decline' => $decline,
                    'retain' => $retain,
                    'exit' => $exit,
                    'watchlist' => $watchlist,
                ];

            }


            $business_units = Committee::all();

            foreach($business_units as $business_unit) {
                if(!isset($mi[$business_unit->name][Carbon::parse($from)->addMonth($i)->format('M')])) {
                    $mi[$business_unit->name][Carbon::parse($from)->addMonth($i)->format('M')] = [
                        'notmade' => $notmade,
                        'made' => $made,
                        'approve' => $approve,
                        'decline' => $decline,
                        'retain' => $retain,
                        'exit' => $exit,
                        'watchlist' => $watchlist,
                    ];
                }
                $clients = Client::with('process.steps.activities.actionable.data','related_parties')->where('committee_id',$business_unit->id)->whereDate('instruction_date', '>=', Carbon::parse($from)->addMonth($i)->startOfMonth()->format('Y-m-d'))->whereDate('instruction_date', '<=', Carbon::parse($from)->addMonth($i)->endOfMonth()->format('Y-m-d'))->get();

                foreach ($clients as $client) {

                    $nodecision = 0;
                    $decisionmade = 0;
                    $a = 0;
                    $d = 0;
                    $r = 0;
                    $e = 0;
                    $w = 0;

                    $activities = Activity::with('actionable.data')->where('id', '545')->get();

                    foreach ($activities as $activity) {
                        if (isset($activity['actionable']['data'])) {
                            foreach ($activity['actionable']['data'] as $data) {
                                if ($data['client_id'] == $client->id && isset($data)) {
                                    if ($data['client_id'] == $client->id && $activity['id'] == '545' && in_array($data['actionable_dropdown_item_id'], array('123','124','125','126','127','128','129','130','131','132','133','134','135','136','137','138','139','140','141','142','143','144','145','146','147','148'))) {

                                        $decisionmade = 1;

                                    }

                                    if ($data['client_id'] == $client->id && $activity['id'] == '545' && $data['actionable_dropdown_item_id'] == '123') {
                                        $a = 1;
                                    }

                                    if ($data['client_id'] == $client->id && $activity['id'] == '545' && $data['actionable_dropdown_item_id'] == '129') {
                                        $d = 1;
                                    }

                                    if ($data['client_id'] == $client->id && $activity['id'] == '545' && ($data['actionable_dropdown_item_id'] == '138' || $data['actionable_dropdown_item_id'] == '139' || $data['actionable_dropdown_item_id'] == '140' || $data['actionable_dropdown_item_id'] == '141' || $data['actionable_dropdown_item_id'] == '142' || $data['actionable_dropdown_item_id'] == '148')) {
                                        $r = 1;
                                    }

                                    if ($data['client_id'] == $client->id && $activity['id'] == '545' && in_array($data['actionable_dropdown_item_id'], array('131', '133'))) {
                                        $e = 1;
                                    }

                                    if ($data['client_id'] == $client->id && $activity['id'] == '545' && in_array($data['actionable_dropdown_item_id'], array('144'))) {
                                        $w = 1;
                                    }
                                }
                            }

                        } else {
                            $nodecision = 1;
                        }

                    }
                    if ($business_unit->name != '') {
                        if (Carbon::parse($from)->addMonth($i)->format('Y-m') <= '2019-10') {
                            /*$mi[$business_unit->name]['July - Oct'] = [
                                'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                                'notmade' => ($nodecision > 0 ? $mi[$business_unit->name]['July - Oct']['notmade'] + 1 : $mi[$business_unit->name]['July - Oct']['notmade']),
                                'made' => ($decisionmade > 0 ? $mi[$business_unit->name]['July - Oct']['made'] + 1 : $mi[$business_unit->name]['July - Oct']['made']),
                                'approve' => ($a > 0 ? $mi[$business_unit->name]['July - Oct']['approve'] + 1 : $mi[$business_unit->name]['July - Oct']['approve']),
                                'decline' => ($d > 0 ? $mi[$business_unit->name]['July - Oct']['decline'] + 1 : $mi[$business_unit->name]['July - Oct']['decline']),
                                'retain' => ($r > 0 ? $mi[$business_unit->name]['July - Oct']['retain'] + 1 : $mi[$business_unit->name]['July - Oct']['retain']),
                                'exit' => ($e > 0 ? $mi[$business_unit->name]['July - Oct']['exit'] + 1 : $mi[$business_unit->name]['July - Oct']['exit']),
                                'watchlist' => ($w > 0 ? $mi[$business_unit->name]['July - Oct']['watchlist'] + 1 : $mi[$business_unit->name]['July - Oct']['watchlist']),
                            ];*/
                        } elseif (Carbon::parse($from)->addMonth($i)->format('Y-m') == '2019-11') {
                            /*$mi[$business_unit->name]['Nov'] = [
                                'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                                'notmade' => ($nodecision > 0 ? $mi[$business_unit->name]['Nov']['notmade'] + 1 : $mi[$business_unit->name]['Nov']['notmade']),
                                'made' => ($decisionmade > 0 ? $mi[$business_unit->name]['Nov']['made'] + 1 : $mi[$business_unit->name]['Nov']['made']),
                                'approve' => ($a > 0 ? $mi[$business_unit->name]['Nov']['approve'] + 1 : $mi[$business_unit->name]['Nov']['approve']),
                                'decline' => ($d > 0 ? $mi[$business_unit->name]['Nov']['decline'] + 1 : $mi[$business_unit->name]['Nov']['decline']),
                                'retain' => ($r > 0 ? $mi[$business_unit->name]['Nov']['retain'] + 1 : $mi[$business_unit->name]['Nov']['retain']),
                                'exit' => ($e > 0 ? $mi[$business_unit->name]['Nov']['exit'] + 1 : $mi[$business_unit->name]['Nov']['exit']),
                                'watchlist' => ($w > 0 ? $mi[$business_unit->name]['Nov']['watchlist'] + 1 : $mi[$business_unit->name]['Nov']['watchlist']),
                            ];*/
                        } elseif (Carbon::parse($from)->addMonth($i)->format('Y-m') == '2019-12') {
                            /*$mi[$business_unit->name]['Dec'] = [
                                'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                                'notmade' => ($nodecision > 0 ? $mi[$business_unit->name]['Dec']['notmade'] + 1 : $mi[$business_unit->name]['Dec']['notmade']),
                                'made' => ($decisionmade > 0 ? $mi[$business_unit->name]['Dec']['made'] + 1 : $mi[$business_unit->name]['Dec']['made']),
                                'approve' => ($a > 0 ? $mi[$business_unit->name]['Dec']['approve'] + 1 : $mi[$business_unit->name]['Dec']['approve']),
                                'decline' => ($d > 0 ? $mi[$business_unit->name]['Dec']['decline'] + 1 : $mi[$business_unit->name]['Dec']['decline']),
                                'retain' => ($r > 0 ? $mi[$business_unit->name]['Dec']['retain'] + 1 : $mi[$business_unit->name]['Dec']['retain']),
                                'exit' => ($e > 0 ? $mi[$business_unit->name]['Dec']['exit'] + 1 : $mi[$business_unit->name]['Dec']['exit']),
                                'watchlist' => ($w > 0 ? $mi[$business_unit->name]['Dec']['watchlist'] + 1 : $mi[$business_unit->name]['Dec']['watchlist']),
                            ];*/
                        } elseif (Carbon::parse($from)->addMonth($i)->format('Y-m') == '2020-01') {
                            /*$mi[$business_unit->name]['Jan'] = [
                                'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                                'notmade' => ($nodecision > 0 ? $mi[$business_unit->name]['Jan']['notmade'] + 1 : $mi[$business_unit->name]['Jan']['notmade']),
                                'made' => ($decisionmade > 0 ? $mi[$business_unit->name]['Jan']['made'] + 1 : $mi[$business_unit->name]['Jan']['made']),
                                'approve' => ($a > 0 ? $mi[$business_unit->name]['Jan']['approve'] + 1 : $mi[$business_unit->name]['Jan']['approve']),
                                'decline' => ($d > 0 ? $mi[$business_unit->name]['Jan']['decline'] + 1 : $mi[$business_unit->name]['Jan']['decline']),
                                'retain' => ($r > 0 ? $mi[$business_unit->name]['Jan']['retain'] + 1 : $mi[$business_unit->name]['Jan']['retain']),
                                'exit' => ($e > 0 ? $mi[$business_unit->name]['Jan']['exit'] + 1 : $mi[$business_unit->name]['Jan']['exit']),
                                'watchlist' => ($w > 0 ? $mi[$business_unit->name]['Jan']['watchlist'] + 1 : $mi[$business_unit->name]['Jan']['watchlist']),
                            ];*/
                        } else {
                            $mi[$business_unit->name][Carbon::parse($from)->addMonth($i)->format('M')] = [
                                'notmade' => ($nodecision > 0 ? $mi[$business_unit->name][Carbon::parse($from)->addMonth($i)->format('M')]['notmade'] + 1 : $mi[$business_unit->name][Carbon::parse($from)->addMonth($i)->format('M')]['notmade']),
                                'made' => ($decisionmade > 0 ? $mi[$business_unit->name][Carbon::parse($from)->addMonth($i)->format('M')]['made'] + 1 : $mi[$business_unit->name][Carbon::parse($from)->addMonth($i)->format('M')]['made']),
                                'approve' => ($a > 0 ? $mi[$business_unit->name][Carbon::parse($from)->addMonth($i)->format('M')]['approve'] + 1 : $mi[$business_unit->name][Carbon::parse($from)->addMonth($i)->format('M')]['approve']),
                                'decline' => ($d > 0 ? $mi[$business_unit->name][Carbon::parse($from)->addMonth($i)->format('M')]['decline'] + 1 : $mi[$business_unit->name][Carbon::parse($from)->addMonth($i)->format('M')]['decline']),
                                'retain' => ($r > 0 ? $mi[$business_unit->name][Carbon::parse($from)->addMonth($i)->format('M')]['retain'] + 1 : $mi[$business_unit->name][Carbon::parse($from)->addMonth($i)->format('M')]['retain']),
                                'exit' => ($e > 0 ? $mi[$business_unit->name][Carbon::parse($from)->addMonth($i)->format('M')]['exit'] + 1 : $mi[$business_unit->name][Carbon::parse($from)->addMonth($i)->format('M')]['exit']),
                                'watchlist' => ($w > 0 ? $mi[$business_unit->name][Carbon::parse($from)->addMonth($i)->format('M')]['watchlist'] + 1 : $mi[$business_unit->name][Carbon::parse($from)->addMonth($i)->format('M')]['watchlist']),
                            ];
                        }
                    }
                }

                $related_parties = RelatedParty::where('committee_id',$business_unit->id)->where('instruction_date', '>=', Carbon::parse($from)->addMonth($i)->startOfMonth()->format('Y-m-d'))->where('instruction_date', '<=', Carbon::parse($from)->addMonth($i)->endOfMonth()->format('Y-m-d'))->get();

                foreach ($related_parties as $rp) {

                    $nodecision = 0;
                    $decisionmade = 0;
                    $a = 0;
                    $d = 0;
                    $r = 0;
                    $e = 0;
                    $w = 0;

                    $activities = Activity::with('actionable.data')->where('id', '598')->get();
                    //dd($activities);

                    foreach ($activities as $activity) {
                        if (isset($activity['actionable']['data'])) {
                            foreach ($activity['actionable']['data'] as $data) {
                                if ($data['related_party_id'] == $rp->id && isset($data)) {
                                    if ($data['related_party_id'] == $rp->id && $activity['id'] == '598' && in_array($data['related_party_dropdown_item_id'], array('48','49','50','51','52','53','54','55','56','57','58','59','60','61','62','63','64','65','66','67','68','69','70','71','72','73'))) {

                                        $decisionmade = 1;

                                    }

                                    if ($data['related_party_id'] == $rp->id && $activity['id'] == '598' && $data['related_party_dropdown_item_id'] == '48') {
                                        $a = 1;
                                    }

                                    if ($data['related_party_id'] == $rp->id && $activity['id'] == '598' && $data['related_party_dropdown_item_id'] == '54') {
                                        $d = 1;
                                    }

                                    if ($data['related_party_id'] == $rp->id && $activity['id'] == '598' && ($data['related_party_dropdown_item_id'] == '63' || $data['related_party_dropdown_item_id'] == '64' || $data['related_party_dropdown_item_id'] == '65' || $data['related_party_dropdown_item_id'] == '66' || $data['related_party_dropdown_item_id'] == '67' || $data['related_party_dropdown_item_id'] == '73')) {
                                        $r = 1;
                                    }

                                    if ($data['related_party_id'] == $rp->id && $activity['id'] == '598' && in_array($data['related_party_dropdown_item_id'], array('56', '58'))) {
                                        $e = 1;
                                    }

                                    if ($data['related_party_id'] == $rp->id && $activity['id'] == '598' && in_array($data['related_party_dropdown_item_id'], array('69'))) {
                                        $w = 1;
                                    }
                                }
                            }

                        } else {
                            $nodecision = 1;
                        }

                    }
                    if ($business_unit->name != '') {
                        if (Carbon::parse($from)->addMonth($i)->format('Y-m') <= '2019-10') {
                            /*$mi[$business_unit->name]['July - Oct'] = [
                                'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                                'notmade' => ($nodecision > 0 ? $mi[$business_unit->name]['July - Oct']['notmade'] + 1 : $mi[$business_unit->name]['July - Oct']['notmade']),
                                'made' => ($decisionmade > 0 ? $mi[$business_unit->name]['July - Oct']['made'] + 1 : $mi[$business_unit->name]['July - Oct']['made']),
                                'approve' => ($a > 0 ? $mi[$business_unit->name]['July - Oct']['approve'] + 1 : $mi[$business_unit->name]['July - Oct']['approve']),
                                'decline' => ($d > 0 ? $mi[$business_unit->name]['July - Oct']['decline'] + 1 : $mi[$business_unit->name]['July - Oct']['decline']),
                                'retain' => ($r > 0 ? $mi[$business_unit->name]['July - Oct']['retain'] + 1 : $mi[$business_unit->name]['July - Oct']['retain']),
                                'exit' => ($e > 0 ? $mi[$business_unit->name]['July - Oct']['exit'] + 1 : $mi[$business_unit->name]['July - Oct']['exit']),
                                'watchlist' => ($w > 0 ? $mi[$business_unit->name]['July - Oct']['watchlist'] + 1 : $mi[$business_unit->name]['July - Oct']['watchlist']),
                            ];*/
                        } elseif (Carbon::parse($from)->addMonth($i)->format('Y-m') == '2019-11') {
                            /*$mi[$business_unit->name]['Nov'] = [
                                'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                                'notmade' => ($nodecision > 0 ? $mi[$business_unit->name]['Nov']['notmade'] + 1 : $mi[$business_unit->name]['Nov']['notmade']),
                                'made' => ($decisionmade > 0 ? $mi[$business_unit->name]['Nov']['made'] + 1 : $mi[$business_unit->name]['Nov']['made']),
                                'approve' => ($a > 0 ? $mi[$business_unit->name]['Nov']['approve'] + 1 : $mi[$business_unit->name]['Nov']['approve']),
                                'decline' => ($d > 0 ? $mi[$business_unit->name]['Nov']['decline'] + 1 : $mi[$business_unit->name]['Nov']['decline']),
                                'retain' => ($r > 0 ? $mi[$business_unit->name]['Nov']['retain'] + 1 : $mi[$business_unit->name]['Nov']['retain']),
                                'exit' => ($e > 0 ? $mi[$business_unit->name]['Nov']['exit'] + 1 : $mi[$business_unit->name]['Nov']['exit']),
                                'watchlist' => ($w > 0 ? $mi[$business_unit->name]['Nov']['watchlist'] + 1 : $mi[$business_unit->name]['Nov']['watchlist']),
                            ];*/
                        } elseif (Carbon::parse($from)->addMonth($i)->format('Y-m') == '2019-12') {
                            /*$mi[$business_unit->name]['Dec'] = [
                                'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                                'notmade' => ($nodecision > 0 ? $mi[$business_unit->name]['Dec']['notmade'] + 1 : $mi[$business_unit->name]['Dec']['notmade']),
                                'made' => ($decisionmade > 0 ? $mi[$business_unit->name]['Dec']['made'] + 1 : $mi[$business_unit->name]['Dec']['made']),
                                'approve' => ($a > 0 ? $mi[$business_unit->name]['Dec']['approve'] + 1 : $mi[$business_unit->name]['Dec']['approve']),
                                'decline' => ($d > 0 ? $mi[$business_unit->name]['Dec']['decline'] + 1 : $mi[$business_unit->name]['Dec']['decline']),
                                'retain' => ($r > 0 ? $mi[$business_unit->name]['Dec']['retain'] + 1 : $mi[$business_unit->name]['Dec']['retain']),
                                'exit' => ($e > 0 ? $mi[$business_unit->name]['Dec']['exit'] + 1 : $mi[$business_unit->name]['Dec']['exit']),
                                'watchlist' => ($w > 0 ? $mi[$business_unit->name]['Dec']['watchlist'] + 1 : $mi[$business_unit->name]['Dec']['watchlist']),
                            ];*/
                        } elseif (Carbon::parse($from)->addMonth($i)->format('Y-m') == '2020-01') {
                            /*$mi[$business_unit->name]['Jan'] = [
                                'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                                'notmade' => ($nodecision > 0 ? $mi[$business_unit->name]['Jan']['notmade'] + 1 : $mi[$business_unit->name]['Jan']['notmade']),
                                'made' => ($decisionmade > 0 ? $mi[$business_unit->name]['Jan']['made'] + 1 : $mi[$business_unit->name]['Jan']['made']),
                                'approve' => ($a > 0 ? $mi[$business_unit->name]['Jan']['approve'] + 1 : $mi[$business_unit->name]['Jan']['approve']),
                                'decline' => ($d > 0 ? $mi[$business_unit->name]['Jan']['decline'] + 1 : $mi[$business_unit->name]['Jan']['decline']),
                                'retain' => ($r > 0 ? $mi[$business_unit->name]['Jan']['retain'] + 1 : $mi[$business_unit->name]['Jan']['retain']),
                                'exit' => ($e > 0 ? $mi[$business_unit->name]['Jan']['exit'] + 1 : $mi[$business_unit->name]['Jan']['exit']),
                                'watchlist' => ($w > 0 ? $mi[$business_unit->name]['Jan']['watchlist'] + 1 : $mi[$business_unit->name]['Jan']['watchlist']),
                            ];*/
                        } else {
                            $mi[$business_unit->name][Carbon::parse($from)->addMonth($i)->format('M')] = [
                                'notmade' => ($nodecision > 0 ? $mi[$business_unit->name][Carbon::parse($from)->addMonth($i)->format('M')]['notmade'] + 1 : $mi[$business_unit->name][Carbon::parse($from)->addMonth($i)->format('M')]['notmade']),
                                'made' => ($decisionmade > 0 ? $mi[$business_unit->name][Carbon::parse($from)->addMonth($i)->format('M')]['made'] + 1 : $mi[$business_unit->name][Carbon::parse($from)->addMonth($i)->format('M')]['made']),
                                'approve' => ($a > 0 ? $mi[$business_unit->name][Carbon::parse($from)->addMonth($i)->format('M')]['approve'] + 1 : $mi[$business_unit->name][Carbon::parse($from)->addMonth($i)->format('M')]['approve']),
                                'decline' => ($d > 0 ? $mi[$business_unit->name][Carbon::parse($from)->addMonth($i)->format('M')]['decline'] + 1 : $mi[$business_unit->name][Carbon::parse($from)->addMonth($i)->format('M')]['decline']),
                                'retain' => ($r > 0 ? $mi[$business_unit->name][Carbon::parse($from)->addMonth($i)->format('M')]['retain'] + 1 : $mi[$business_unit->name][Carbon::parse($from)->addMonth($i)->format('M')]['retain']),
                                'exit' => ($e > 0 ? $mi[$business_unit->name][Carbon::parse($from)->addMonth($i)->format('M')]['exit'] + 1 : $mi[$business_unit->name][Carbon::parse($from)->addMonth($i)->format('M')]['exit']),
                                'watchlist' => ($w > 0 ? $mi[$business_unit->name][Carbon::parse($from)->addMonth($i)->format('M')]['watchlist'] + 1 : $mi[$business_unit->name][Carbon::parse($from)->addMonth($i)->format('M')]['watchlist']),
                            ];
                        }
                    }
                }
            }
        }

        return $mi;
    }

    public function committeeTotals($mi){
        /*$totals['notmade'] = 137;
        $totals['made'] = 6;
        $totals['approve'] = 0;
        $totals['decline'] = 1;
        $totals['retain'] = 0;
        $totals['exit'] = 5;
        $totals['watchlist'] = 0;*/
        foreach($mi as $key => $value){
            foreach($value as $key2 => $value3){
                foreach($value3 as $key3 => $value2){
                    if($key3 != 'year') {
                        if (!isset($totals[$key3])) {
                            $totals[$key3] = $value2;
                        } else {
                            $totals[$key3] += $value2;
                        }
                    }
                }
            }
        }

        return $totals;
    }
    public function closure($from,$to){
        for ($i = 0; $i <= Carbon::parse($to)->diffInMonths(Carbon::parse($from)); $i++) {

            $watchlist = 0;
            $letter = 0;
            $unclaimed = 0;
            $collection = 0;
            $outside = 0;

            if (Carbon::parse($from)->addMonth($i)->format('Y-m') <= '2019-10') {
                $mi['EB']['July - Oct'] = [
                    'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                    'watchlist' => $watchlist + 21,
                    'letter' => $letter + 13,
                    'unclaimed' => $unclaimed + 5,
                    'collection' => $collection + 4,
                    'outside' => $outside
                ];
                $mi['RBB']['July - Oct'] = [
                    'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                    'watchlist' => $watchlist + 42,
                    'letter' => $letter + 18,
                    'unclaimed' => $unclaimed + 2,
                    'collection' => $collection + 11,
                    'outside' => $outside
                ];
                $mi['RB']['July - Oct'] = [
                    'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                    'watchlist' => $watchlist + 101,
                    'letter' => $letter + 68,
                    'unclaimed' => $unclaimed + 1,
                    'collection' => $collection + 28,
                    'outside' => $outside
                ];
            }

            if (Carbon::parse($from)->addMonth($i)->format('Y-m') == '2019-11') {
                $mi['EB']['Nov'] = [
                    'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                    'watchlist' => $watchlist+1,
                    'letter' => $letter+1,
                    'unclaimed' => $unclaimed,
                    'collection' => $collection,
                    'outside' => $outside
                ];
                $mi['RBB']['Nov'] = [
                    'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                    'watchlist' => $watchlist + 1,
                    'letter' => $letter + 1,
                    'unclaimed' => $unclaimed,
                    'collection' => $collection,
                    'outside' => $outside
                ];
                $mi['RB']['Nov'] = [
                    'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                    'watchlist' => $watchlist + 5,
                    'letter' => $letter + 4,
                    'unclaimed' => $unclaimed,
                    'collection' => $collection + 1,
                    'outside' => $outside
                ];
            }

            if (Carbon::parse($from)->addMonth($i)->format('Y-m') == '2019-12') {
                $mi['EB']['Dec'] = [
                    'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                    'watchlist' => $watchlist,
                    'letter' => $letter,
                    'unclaimed' => $unclaimed,
                    'collection' => $collection,
                    'outside' => $outside
                ];
                $mi['RBB']['Dec'] = [
                    'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                    'watchlist' => $watchlist,
                    'letter' => $letter,
                    'unclaimed' => $unclaimed,
                    'collection' => $collection,
                    'outside' => $outside
                ];
                $mi['RB']['Dec'] = [
                    'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                    'watchlist' => $watchlist,
                    'letter' => $letter,
                    'unclaimed' => $unclaimed,
                    'collection' => $collection,
                    'outside' => $outside
                ];
            }

            if (Carbon::parse($from)->addMonth($i)->format('Y-m') == '2020-01') {
                $mi['EB']['Jan'] = [
                    'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                    'watchlist' => $watchlist,
                    'letter' => $letter,
                    'unclaimed' => $unclaimed,
                    'collection' => $collection,
                    'outside' => $outside
                ];
                $mi['RBB']['Jan'] = [
                    'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                    'watchlist' => $watchlist,
                    'letter' => $letter,
                    'unclaimed' => $unclaimed,
                    'collection' => $collection,
                    'outside' => $outside
                ];
                $mi['RB']['Jan'] = [
                    'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                    'watchlist' => $watchlist,
                    'letter' => $letter,
                    'unclaimed' => $unclaimed,
                    'collection' => $collection,
                    'outside' => $outside
                ];

            }

            if (Carbon::parse($from)->addMonth($i)->format('Y-m') == '2020-02') {
                $mi['EB']['Feb'] = [
                    'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                    'watchlist' => $watchlist,
                    'letter' => $letter,
                    'unclaimed' => $unclaimed,
                    'collection' => $collection,
                    'outside' => $outside
                ];
                $mi['RBB']['Feb'] = [
                    'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                    'watchlist' => $watchlist,
                    'letter' => $letter,
                    'unclaimed' => $unclaimed,
                    'collection' => $collection,
                    'outside' => $outside
                ];
                $mi['RB']['Feb'] = [
                    'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                    'watchlist' => $watchlist,
                    'letter' => $letter,
                    'unclaimed' => $unclaimed,
                    'collection' => $collection,
                    'outside' => $outside
                ];

            }


            $business_units = Committee::all();

            foreach($business_units as $business_unit) {
                if(!isset($mi[$business_unit->name][Carbon::parse($from)->addMonth($i)->format('M')])) {
                    $mi[$business_unit->name][Carbon::parse($from)->addMonth($i)->format('M')] = [
                        'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                        'watchlist' => $watchlist,
                        'letter' => $letter,
                        'unclaimed' => $unclaimed,
                        'collection' => $collection,
                        'outside' => $outside
                    ];
                }

                $clients = Client::with('process.steps.activities.actionable.data')->where('committee_id',$business_unit->id)->whereDate('instruction_date', '>=', Carbon::parse($from)->addMonth($i)->startOfMonth()->format('Y-m-d'))->whereDate('instruction_date', '<=', Carbon::parse($from)->addMonth($i)->endOfMonth()->format('Y-m-d'))->get();

                foreach ($clients as $client) {

                    $w =0;
                    $l =0;
                    $u =0;
                    $c =0;
                    $o =0;

                    $activities = Activity::with('actionable.data')
                        ->where('id','545')
                        ->orWhere('id','647')
                        ->orWhere('id','645')
                        ->orWhere('id','231')
                        ->orWhere('id','788')
                        ->orWhere('id','663')
                        ->get();
                    //dd($activities);

                    $start = $client->instruction_date;
                    $end = $client->completed_at;

                    if(Carbon::parse($start)->diffInDays(Carbon::parse($end)) >= 90){
                        $o = 1;
                    }

                    foreach($activities as $activity) {
                        //dd($activity);
                        if(isset($activity['actionable']['data'])){
                            foreach ($activity['actionable']['data'] as $data) {
                                if ($data['client_id'] == $client->id && isset($data)) {
                                    if ($data['client_id'] == $client->id && $activity['id'] == '647') {
                                        /*if (isset($data)) {*/
                                        $l = 1;
                                        /*}*/
                                    }

                                    if ($data['client_id'] == $client->id && $activity['id'] == '788') {
                                        /*if (isset($data)) {*/
                                        $u = 1;
                                        /*}*/
                                    }

                                    if ($data['client_id'] == $client->id && $activity['id'] == '663') {
                                        /*if (isset($data)) {*/
                                        $c = 1;
                                        /*}*/
                                    }

                                    if($data['client_id'] == $client->id && $activity['id'] == '645'){
                                        $w = 1;
                                    }
                                }}

                        }

                    }


                    if($business_unit->name != '') {
                        if (Carbon::parse($from)->addMonth($i)->format('Y-m') <= '2019-10') {
                            /*$mi[$business_unit->name]['July - Oct'] = [
                                'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                                'watchlist' => ($w > 0 ? $mi[$business_unit->name]['July - Oct']['watchlist']+1 : $mi[$business_unit->name]['July - Oct']['watchlist']),
                                'letter' => ($l > 0 ? $mi[$business_unit->name]['July - Oct']['letter']+1 : $mi[$business_unit->name]['July - Oct']['letter']),
                                'unclaimed' => ($u > 0 ? $mi[$business_unit->name]['July - Oct']['unclaimed']+1 : $mi[$business_unit->name]['July - Oct']['unclaimed']),
                                'collection' => ($c > 0 ? $mi[$business_unit->name]['July - Oct']['collection']+1 : $mi[$business_unit->name]['July - Oct']['collection']),
                                'outside' => ($o > 0 ? $mi[$business_unit->name]['July - Oct']['outside']+1 : $mi[$business_unit->name]['July - Oct']['outside'])
                            ];*/
                        } elseif (Carbon::parse($from)->addMonth($i)->format('Y-m') == '2019-11') {
                            /*$mi[$business_unit->name]['Nov'] = [
                                'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                                'watchlist' => ($w > 0 ? $mi[$business_unit->name]['Nov']['watchlist']+1 : $mi[$business_unit->name]['Nov']['watchlist']),
                                'letter' => ($l > 0 ? $mi[$business_unit->name]['Nov']['letter']+1 : $mi[$business_unit->name]['Nov']['letter']),
                                'unclaimed' => ($u > 0 ? $mi[$business_unit->name]['Nov']['unclaimed']+1 : $mi[$business_unit->name]['Nov']['unclaimed']),
                                'collection' => ($c > 0 ? $mi[$business_unit->name]['Nov']['collection']+1 : $mi[$business_unit->name]['Nov']['collection']),
                                'outside' => ($o > 0 ? $mi[$business_unit->name]['Nov']['outside']+1 : $mi[$business_unit->name]['Nov']['outside'])
                            ];*/
                        } elseif (Carbon::parse($from)->addMonth($i)->format('Y-m') == '2019-12') {
                            /*$mi[$business_unit->name]['Dec'] = [
                                'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                                'watchlist' => ($w > 0 ? $mi[$business_unit->name]['Dec']['watchlist']+1 : $mi[$business_unit->name]['Dec']['watchlist']),
                                'letter' => ($l > 0 ? $mi[$business_unit->name]['Dec']['letter']+1 : $mi[$business_unit->name]['Dec']['letter']),
                                'unclaimed' => ($u > 0 ? $mi[$business_unit->name]['Dec']['unclaimed']+1 : $mi[$business_unit->name]['Dec']['unclaimed']),
                                'collection' => ($c > 0 ? $mi[$business_unit->name]['Dec']['collection']+1 : $mi[$business_unit->name]['Dec']['collection']),
                                'outside' => ($o > 0 ? $mi[$business_unit->name]['Dec']['outside']+1 : $mi[$business_unit->name]['Dec']['outside'])
                            ];*/
                        } elseif (Carbon::parse($from)->addMonth($i)->format('Y-m') == '2020-01') {
                            /*$mi[$business_unit->name]['Jan'] = [
                                'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                                'watchlist' => ($w > 0 ? $mi[$business_unit->name]['Jan']['watchlist']+1 : $mi[$business_unit->name]['Jan']['watchlist']),
                                'letter' => ($l > 0 ? $mi[$business_unit->name]['Jan']['letter']+1 : $mi[$business_unit->name]['Jan']['letter']),
                                'unclaimed' => ($u > 0 ? $mi[$business_unit->name]['Jan']['unclaimed']+1 : $mi[$business_unit->name]['Jan']['unclaimed']),
                                'collection' => ($c > 0 ? $mi[$business_unit->name]['Jan']['collection']+1 : $mi[$business_unit->name]['Jan']['collection']),
                                'outside' => ($o > 0 ? $mi[$business_unit->name]['Jan']['outside']+1 : $mi[$business_unit->name]['Jan']['outside'])
                            ];*/
                        } else {
                            $mi[$business_unit->name][Carbon::parse($from)->addMonth($i)->format('M')] = [
                                'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                                'watchlist' => ($w > 0 ? $mi[$business_unit->name][Carbon::parse($from)->addMonth($i)->format('M')]['watchlist']+1 : $mi[$business_unit->name][Carbon::parse($from)->addMonth($i)->format('M')]['watchlist']),
                                'letter' => ($l > 0 ? $mi[$business_unit->name][Carbon::parse($from)->addMonth($i)->format('M')]['letter']+1 : $mi[$business_unit->name][Carbon::parse($from)->addMonth($i)->format('M')]['letter']),
                                'unclaimed' => ($u > 0 ? $mi[$business_unit->name][Carbon::parse($from)->addMonth($i)->format('M')]['unclaimed']+1 : $mi[$business_unit->name][Carbon::parse($from)->addMonth($i)->format('M')]['unclaimed']),
                                'collection' => ($c > 0 ? $mi[$business_unit->name][Carbon::parse($from)->addMonth($i)->format('M')]['collection']+1 : $mi[$business_unit->name][Carbon::parse($from)->addMonth($i)->format('M')]['collection']),
                                'outside' => ($o > 0 ? $mi[$business_unit->name][Carbon::parse($from)->addMonth($i)->format('M')]['outside']+1 : $mi[$business_unit->name][Carbon::parse($from)->addMonth($i)->format('M')]['outside'])
                            ];
                        }
                    }

                }

                $related_parties = RelatedParty::where('committee_id',$business_unit->id)->whereDate('instruction_date', '>=', Carbon::parse($from)->addMonth($i)->startOfMonth()->format('Y-m-d'))->whereDate('instruction_date', '<=', Carbon::parse($from)->addMonth($i)->endOfMonth()->format('Y-m-d'))->get();

                foreach ($related_parties as $rp) {

                    $w =0;
                    $l =0;
                    $u =0;
                    $c =0;
                    $o =0;

                    $activities = Activity::with('actionable.data')
                        ->where('id','692')
                        ->orWhere('id','694')
                        ->orWhere('id','758')
                        ->orWhere('id','710')
                        ->get();
                    //dd($activities);

                    $start = $rp->instruction_date;
                    $end = $rp->completed_at;

                    if(Carbon::parse($start)->diffInDays(Carbon::parse($end)) >= 90){
                        $o = 1;
                    }

                    foreach($activities as $activity) {
                        //dd($activity);
                        if(isset($activity['actionable']['data'])){
                            foreach ($activity['actionable']['data'] as $data) {
                                if ($data['related_party_id'] == $rp->id && isset($data)) {
                                    if ($data['related_party_id'] == $rp->id && $activity['id'] == '694') {
                                        /*if (isset($data)) {*/
                                        $l = 1;
                                        /*}*/
                                    }

                                    if ($data['related_party_id'] == $rp->id && $activity['id'] == '758') {
                                        /*if (isset($data)) {*/
                                        $u = 1;
                                        /*}*/
                                    }

                                    if ($data['related_party_id'] == $rp->id && $activity['id'] == '710') {
                                        /*if (isset($data)) {*/
                                        $c = 1;
                                        /*}*/
                                    }

                                    if ($data['related_party_id'] == $rp->id && $activity['id'] == '692') {
                                        $w = 1;
                                    }
                                }}

                        }

                    }


                    if($business_unit->name != '') {
                        if (Carbon::parse($from)->addMonth($i)->format('Y-m') <= '2019-10') {
                            /*$mi[$business_unit->name]['July - Oct'] = [
                                'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                                'watchlist' => ($w > 0 ? $mi[$business_unit->name]['July - Oct']['watchlist']+1 : $mi[$business_unit->name]['July - Oct']['watchlist']),
                                'letter' => ($l > 0 ? $mi[$business_unit->name]['July - Oct']['letter']+1 : $mi[$business_unit->name]['July - Oct']['letter']),
                                'unclaimed' => ($u > 0 ? $mi[$business_unit->name]['July - Oct']['unclaimed']+1 : $mi[$business_unit->name]['July - Oct']['unclaimed']),
                                'collection' => ($c > 0 ? $mi[$business_unit->name]['July - Oct']['collection']+1 : $mi[$business_unit->name]['July - Oct']['collection']),
                                'outside' => ($o > 0 ? $mi[$business_unit->name]['July - Oct']['outside']+1 : $mi[$business_unit->name]['July - Oct']['outside'])
                            ];*/
                        } elseif (Carbon::parse($from)->addMonth($i)->format('Y-m') == '2019-11') {
                            /*$mi[$business_unit->name]['Nov'] = [
                                'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                                'watchlist' => ($w > 0 ? $mi[$business_unit->name]['Nov']['watchlist']+1 : $mi[$business_unit->name]['Nov']['watchlist']),
                                'letter' => ($l > 0 ? $mi[$business_unit->name]['Nov']['letter']+1 : $mi[$business_unit->name]['Nov']['letter']),
                                'unclaimed' => ($u > 0 ? $mi[$business_unit->name]['Nov']['unclaimed']+1 : $mi[$business_unit->name]['Nov']['unclaimed']),
                                'collection' => ($c > 0 ? $mi[$business_unit->name]['Nov']['collection']+1 : $mi[$business_unit->name]['Nov']['collection']),
                                'outside' => ($o > 0 ? $mi[$business_unit->name]['Nov']['outside']+1 : $mi[$business_unit->name]['Nov']['outside'])
                            ];*/
                        } elseif (Carbon::parse($from)->addMonth($i)->format('Y-m') == '2019-12') {
                            /*$mi[$business_unit->name]['Dec'] = [
                                'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                                'watchlist' => ($w > 0 ? $mi[$business_unit->name]['Dec']['watchlist']+1 : $mi[$business_unit->name]['Dec']['watchlist']),
                                'letter' => ($l > 0 ? $mi[$business_unit->name]['Dec']['letter']+1 : $mi[$business_unit->name]['Dec']['letter']),
                                'unclaimed' => ($u > 0 ? $mi[$business_unit->name]['Dec']['unclaimed']+1 : $mi[$business_unit->name]['Dec']['unclaimed']),
                                'collection' => ($c > 0 ? $mi[$business_unit->name]['Dec']['collection']+1 : $mi[$business_unit->name]['Dec']['collection']),
                                'outside' => ($o > 0 ? $mi[$business_unit->name]['Dec']['outside']+1 : $mi[$business_unit->name]['Dec']['outside'])
                            ];*/
                        } elseif (Carbon::parse($from)->addMonth($i)->format('Y-m') == '2020-01') {
                            /*$mi[$business_unit->name]['Jan'] = [
                                'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                                'watchlist' => ($w > 0 ? $mi[$business_unit->name]['Jan']['watchlist']+1 : $mi[$business_unit->name]['Jan']['watchlist']),
                                'letter' => ($l > 0 ? $mi[$business_unit->name]['Jan']['letter']+1 : $mi[$business_unit->name]['Jan']['letter']),
                                'unclaimed' => ($u > 0 ? $mi[$business_unit->name]['Jan']['unclaimed']+1 : $mi[$business_unit->name]['Jan']['unclaimed']),
                                'collection' => ($c > 0 ? $mi[$business_unit->name]['Jan']['collection']+1 : $mi[$business_unit->name]['Jan']['collection']),
                                'outside' => ($o > 0 ? $mi[$business_unit->name]['Jan']['outside']+1 : $mi[$business_unit->name]['Jan']['outside'])
                            ];*/
                        } else {
                            $mi[$business_unit->name][Carbon::parse($from)->addMonth($i)->format('M')] = [
                                'year' => Carbon::parse($from)->addMonth($i)->format('y'),
                                'watchlist' => ($w > 0 ? $mi[$business_unit->name][Carbon::parse($from)->addMonth($i)->format('M')]['watchlist']+1 : $mi[$business_unit->name][Carbon::parse($from)->addMonth($i)->format('M')]['watchlist']),
                                'letter' => ($l > 0 ? $mi[$business_unit->name][Carbon::parse($from)->addMonth($i)->format('M')]['letter']+1 : $mi[$business_unit->name][Carbon::parse($from)->addMonth($i)->format('M')]['letter']),
                                'unclaimed' => ($u > 0 ? $mi[$business_unit->name][Carbon::parse($from)->addMonth($i)->format('M')]['unclaimed']+1 : $mi[$business_unit->name][Carbon::parse($from)->addMonth($i)->format('M')]['unclaimed']),
                                'collection' => ($c > 0 ? $mi[$business_unit->name][Carbon::parse($from)->addMonth($i)->format('M')]['collection']+1 : $mi[$business_unit->name][Carbon::parse($from)->addMonth($i)->format('M')]['collection']),
                                'outside' => ($o > 0 ? $mi[$business_unit->name][Carbon::parse($from)->addMonth($i)->format('M')]['outside']+1 : $mi[$business_unit->name][Carbon::parse($from)->addMonth($i)->format('M')]['outside'])
                            ];
                        }
                    }

                }
            }
        }


        return $mi;
    }

    public function closureTotals($mi){
        foreach($mi as $key => $value){
            foreach($value as $key2 => $value3){
                foreach($value3 as $key3 => $value2){
                    if($key3 != 'year') {
                        if (!isset($totals[$key3])) {
                            $totals[$key3] = $value2;
                        } else {
                            $totals[$key3] += $value2;
                        }
                    }
                }
            }
        }

        return $totals;
    }

    public function getConvertedCount(Process $process, Carbon $from, Carbon $to)
    {
        $client_step_counts = Client::where('process_id', $process->id)->where('is_progressing', '=', 1)->where('is_qa','0')->where('updated_at', '!=', 'completed_at')->where('completed_at','!=', null)->where('completed_at','>=',$from)->where('completed_at','<=',$to)->count();


        return $client_step_counts;
    }

    public function getClientStepCounts(Process $process, Carbon $from, Carbon $to)
    {

        $clients = Client::with([
            'process.steps.activities' => function ($query) {
                $query->where('kpi', true)
                    ->with('actionable.data');
            }
        ])
        ->where('process_id', $process->id)
        ->where('is_progressing', '=', 1)
        ->where('is_qa','0')
        ->where(function ($query) use ($from) {
            $query->where('created_at', '>=', $from)
                ->orWhere('completed_at', '>=', $from)
                ->orWhere('updated_at', '>=', $from);
        })
        ->where(function ($query) use ($to) {
            $query->where('created_at', '<=', $to)
                ->orWhere('completed_at', '<=', $to)
                ->orWhere('updated_at', '<=', $to);
        })->get();


        $client_step_counts = [];

       foreach ($clients as $res) {

           if($res->step_id == '5'){
               $client_step_counts[$res->step_id] = Client::where('process_id',$process->id)->where('is_progressing','=',1)->where('updated_at','>=',$from)->where('updated_at','<=',$to)->where('step_id',$res->step_id)->where('completed_at',null)->count();
           } else {
               $client_step_counts[$res->step_id] = Client::where('process_id', $process->id)->where('is_progressing', '=', 1)->where('step_id', $res->step_id)->where('completed_at', null)->count();
           }
       }

        return $client_step_counts;
    }

    /**
     * Returns an array of minimum, average and maximum Client onboarding times for
     * a given process and date range.
     * 
     * Used for dashboard graphing
     *
     * @param Process $process
     * @param Carbon $from
     * @param Carbon $to
     * @return array
     */
    public function getClientOnboardTimes(Process $process, Carbon $from, Carbon $to)
    {
        $config = Config::first();
        $step = $config->dashboard_activities_step_for_age;

        $clients = Client::with('process.activities.actionable.data')
                        ->where('is_progressing','1')
                        ->where('is_qa','0')
                        ->where('process_id',$process->id)
                        ->where('completed_at', '>=', $from)
                        ->where('completed_at', '<=', $to)
                        ->whereNotNull('created_at')
                        ->get();

        $client_array = array();
        $client_array["days"] = array();

        $cnt = 0;

        foreach ($clients as $client){

            foreach ($client->process->activities as $activity){

                if($activity->step_id == $step) {
                    foreach ($activity->actionable['data'] as $data) {
                        $max = 0;
                        if ($data["created_at"] != null) {

                            $max = $max + Carbon::parse($data["created_at"])->diffInDays($client->completed_at);
                            array_push($client_array["days"], $max);

                        }

                        $cnt++;
                    }
                }
            }
        }

        sort($client_array['days']);
        $min = (isset($client_array['days'][0]) ? $client_array['days'][0] : 0);

        rsort($client_array['days']);
        $max = (isset($client_array['days'][0]) ? $client_array['days'][0] : 0);

        $avg = ($cnt > 0 ? round(array_sum($client_array['days']) / $cnt,0) : 0);

        return ['minimum' => $min, 'average' => $avg, 'maximum' => $max];
    }


    /**
     * Completed clients, between dates $from and $to, aggregated 
     * by range (day, week, month, year)
     *
     * @param Process $process
     * @param Carbon $from
     * @param Carbon $to
     * @param String $range  
     * @return array
     */
    public function getCompletedClients(Process $process, Carbon $from, Carbon $to, String $range)
    {

        $last_step = Step::where('process_id',$process->id)->orderBy('order','desc')->get();
        //dd($last_step);

        switch ($range) {
            default:
            case 'day':
                $date_diff = $from->diffInDays($to);

                $client_query = Client::where('step_id',$last_step[0]->id)->where('process_id', $process->id)->where('is_progressing', '=', 1)->where('is_qa','0')->whereDate('completed_at','>=',$from)->whereDate('completed_at','<=',$to)->where('completed_at','!=', null)->select(DB::raw('DATE_FORMAT(completed_at, "%e %M %Y") as date'), DB::raw('count(*) as onboarded'))->groupBy('date')->pluck('onboarded', 'date')->toArray();

                $client_onboards = [];
                for ($i = 0; $i <= $date_diff; $i++) {
                    $working_date = $from->copy()->addDays($i)->format('j F Y');
                    if (isset($client_query[$working_date])) {
                        $client_onboards[$working_date] = $client_query[$working_date];
                    } else {
                        $client_onboards[$working_date] = 0;
                    }
                }

                break;
            case 'week':
                $date_diff = $from->diffInWeeks($to->addDay(1));

                $client_query = Client::where('step_id',$last_step[0]->id)->where('process_id', $process->id)->where('is_progressing', '=', 1)->whereDate('completed_at','>=',$from)->whereDate('completed_at','<=',$to)->where('completed_at','!=', null)->select(DB::raw('DATE_FORMAT(completed_at, "%u %x") as date'), DB::raw('count(*) as onboarded'))->groupBy('date')->pluck('onboarded', 'date')->toArray();
            
                $client_onboards = [];
                for ($i = 0; $i <= $date_diff; $i++) {

                    $readable_date = $from->copy()->startOfWeek()->addWeeks($i)->format('j F Y');
                    $working_date = $from->copy()->startOfWeek()->addWeeks($i)->format('W Y');
                    if (isset($client_query[$working_date])) {
                        $client_onboards[$readable_date] = $client_query[$working_date];
                    } else {
                        $client_onboards[$readable_date] = 0;
                    }
                }

                break;
            case 'month':
                $date_diff = $from->diffInMonths($to);

                $client_query = Client::where('step_id',$last_step[0]->id)->where('process_id', $process->id)->where('is_progressing', '=', 1)->whereDate('completed_at','>=',$from)->whereDate('completed_at','<=',$to)->where('completed_at','!=', null)->select(DB::raw('DATE_FORMAT(updated_at, "%e %M %Y") as date'), DB::raw('count(*) as onboarded'))->groupBy('date')->pluck('onboarded', 'date')->toArray();

                $client_onboards = [];
                for ($i = 0; $i <= $date_diff; $i++) {
                    $working_date = $from->copy()->addMonths($i)->format('F Y');
                    if (isset($client_query[$working_date])) {
                        $client_onboards[$working_date] = $client_query[$working_date];
                    } else {
                        $client_onboards[$working_date] = 0;
                    }
                }

                break;
            case 'year':
                $date_diff = $from->diffInYears($to);

                $client_query = Client::where('step_id',$last_step[0]->id)->where('process_id', $process->id)->where('is_progressing', '=', 1)->whereDate('completed_at','>=',$from)->whereDate('completed_at','<=',$to)->where('completed_at','!=', null)->select(DB::raw('DATE_FORMAT(updated_at, "%e %M %Y") as date'), DB::raw('count(*) as onboarded'))->groupBy('date')->pluck('onboarded', 'date')->toArray();

                $client_onboards = [];
                for ($i = 0; $i <= $date_diff; $i++) {
                    $working_date = $from->copy()->addYears($i)->format('Y');
                    if (isset($client_query[$working_date])) {
                        $client_onboards[$working_date] = $client_query[$working_date];
                    } else {
                        $client_onboards[$working_date] = 0;
                    }
                }
                break;
        }
        
        return $client_onboards;
    }

    /**
     * Undocumented function
     *
     * @param Process $process
     * @param Carbon $from
     * @param Carbon $to
     * @return void
     */
    public function getProcessAverageTimes(Process $process, Carbon $from, Carbon $to)
    {
        $configs = Config::first();

        $step_ids = explode(',',$configs->dashboard_avg_step);

        $client_array = new Collection();

        $clients = Client::select('id','created_at')
            ->where('is_progressing','1')
            ->where('is_qa','0')
            ->where('process_id', $process->id)
            //->whereNotNull('completed_at')
            ->where(function ($query) use ($from) {
                $query->where('created_at', '>=', $from)
                    ->orWhere('updated_at', '>=', $from)
                    ->orWhere('completed_at', '>=', $from);
            })
            ->where(function ($query) use ($to) {
                $query->where('created_at', '<=', $to)
                    ->orWhere('updated_at', '<=', $to)
                    ->orWhere('completed_at', '<=', $to);
            })->get()->toArray();
//dd($clients);
        foreach($clients as $client){
            $client_array->push([
                'id' => $client["id"],
                'created_at' => $client["created_at"]
            ]);
        }
        //dd($client_array);
        $process_average_times = [];
        foreach ($process->steps as $step) {
            if(in_array($step->id,$step_ids)) {
                $process_average_times[$step->name] = 0;
                $step_duration = 0;
                $data_count = 0;

                $cnt = 0;
                $activity_array = collect($step->activities)->toArray();

                //remove array values where created_at = null
                foreach ($activity_array as $key => $value){
                    if(empty($activity_array[$key]['created_at'])){
                        unset($activity_array[$key]);
                    }
                }

                foreach ($step->activities as $activity) {

                    $cnt++;

                    if (isset($activity->actionable['data'])) {
                        foreach($activity->actionable['data'] as $key => $value) {
                            if (empty($activity->actionable['data'][$key]['created_at'])) {
                                unset($activity->actionable['data'][$key]);
                            }
                        }


                        foreach ($activity->actionable['data'] as $data) {

                            if (isset($data["created_at"]) && $data["created_at"] >= $from && $data["created_at"] <= $to) {

                                $search = array();

                                foreach ($client_array as $client) {
                                    if ($client['id'] == $data["client_id"]) {

                                        array_push($search, $client['created_at']);
                                    }
                                }

                                if (count($search) > 0) {

                                    $step_duration += (isset($data['created_at']) ? Carbon::parse($search[0])->diffInDays(Carbon::parse($data['created_at'])) : 0);
                                    $data_count++;

                                } else {

                                    $step_duration += 0;
                                    $data_count++;

                                }
                            }
                        }
                    }
                }
                $process_average_times[$step->name] = round($step_duration / (($data_count > 0) ? $data_count : 1));
            }
        }
        return $process_average_times;
    }

    /**
     * Returns count of outsanding activities
     *
     * @param int $process_id
     * @param int $step_id
     * @return void
     */
    public function getOutstandingActivities($process_id, $step_id)
    {
        $configs = Config::first();

        $activity_ids = explode(',',$configs->dashboard_outstanding_activities);

        $process = Process::where('id',$configs->dashboard_process)->first();

        $clients = Client::where('is_progressing',1)->where('is_qa','0')->where('step_id',$configs->dashboard_outstanding_step)->where('process_id', $process->id)->pluck('id');

        $outstanding_activities = [];
        foreach ($process->steps as $step) {

                foreach ($step->activities as $activity) {
                    if(($key = array_search($activity->id, $activity_ids)) === false) {

                    } else {
                        
                        if ($activity->step_id == $configs->dashboard_outstanding_step) {

                            $outstanding_activities[$activity->name] = [
                                //'client' => 0,
                                'user' => 0
                            ];
                            foreach ($clients as $client_id) {
                                $has_data = false;
                                if (isset($activity->actionable['data'][0])) {

                                    foreach ($activity->actionable['data'] as $data) {

                                        if ($data->client_id == $client_id) {
                                            if (isset($data->actionable_boolean_id)) {
                                                if ($data->actionable_boolean_id > 0) {
                                                    $data2 = ActionableBooleanData::where('client_id',$data->client_id)->where('actionable_boolean_id', $data->actionable_boolean_id)->orderBy('id','desc')->take(1)->first();

                                                    if ($data2->data == "1") {
                                                        $has_data = true;
                                                    }
                                                } else {
                                                    $has_data = false;
                                                }
                                            }

                                            if (isset($data->actionable_dropdown_id)) {
                                                if (isset($data->actionable_dropdown_item_id) && $data->actionable_dropdown_item_id > 0) {
                                                    $data2 = ActionableDropdownItem::where('id', $data->actionable_dropdown_item_id)->first();

                                                    if ($data2->name) {
                                                        $has_data = true;
                                                    }
                                                } else {
                                                    $has_data = false;
                                                }
                                            }

                                            if (isset($data->actionable_text_id)) {
                                                if (isset($data->actionable_text_id) && $data->actionable_text_id > 0) {
                                                    $data2 = ActionableTextData::where('client_id',$data->client_id)->where('actionable_text_id', $data->actionable_text_id)->first();

                                                    if ($data2->data) {
                                                        $has_data = true;
                                                    }
                                                } else {
                                                    $has_data = false;
                                                }
                                            }

                                            if (isset($data->actionable_textarea_id)) {
                                                if (isset($data->actionable_textarea_id) && $data->actionable_textarea_id > 0) {
                                                    $data2 = ActionableTextareaData::where('client_id',$data->client_id)->where('actionable_textarea_id', $data->actionable_textarea_id)->first();

                                                    if ($data2->data) {
                                                        $has_data = true;
                                                    }
                                                } else {
                                                    $has_data = false;
                                                }
                                            }

                                            if (isset($data->actionable_document_email_id)) {
                                                if (isset($data->actionable_document_email_id) && $data->actionable_document_email_id > 0) {
                                                    $data2 = ActionableDocumentEmailData::where('client_id',$data->client_id)->where('actionable_document_email_id', $data->actionable_document_email_id)->first();

                                                    if ($data2->data) {
                                                        $has_data = true;
                                                    }
                                                } else {
                                                    $has_data = false;
                                                }
                                            }

                                            if (isset($data->actionable_template_email_id)) {
                                                if (isset($data->actionable_template_email_id) && $data->actionable_template_email_id > 0) {
                                                    $data2 = ActionableTemplateEmailData::where('client_id',$data->client_id)->where('actionable_template_email_id', $data->actionable_template_email_id)->first();

                                                    if ($data2->data) {
                                                        $has_data = true;
                                                    }
                                                } else {
                                                    $has_data = false;
                                                }
                                            }

                                            if (isset($data->actionable_date_id)) {
                                                if (isset($data->actionable_date_id) && $data->actionable_date_id > 0) {
                                                    $data2 = ActionableDateData::where('client_id',$data->client_id)->where('actionable_date_id', $data->actionable_date_id)->first();

                                                    if ($data2->data != null) {
                                                        $has_data = true;
                                                    }
                                                } else {
                                                    $has_data = false;
                                                }
                                            }
                                        }
                                    }
                                }

                                if (!$has_data) {
                                    if ($activity->client_activity) {
                                        //$outstanding_activities[$activity->name]['client']++;
                                    } else {
                                        $outstanding_activities[$activity->name]['user']++;
                                    }
                                   
                                }
                            }
                        }
                    }
                }
        }

        return $outstanding_activities;
    }

    public function calendar()
    {
        return view('calendar');
    }

    /**
     * XHR Endpoint for Outstanding Activity graph
     * TEMPORARY COPY OF getOutstandingActivities()
     *
     * @return String JSON Array
     */
    public function getOutstandingActivitiesAjax(Request $request) {
        
        $process_id = ($request->has('process_id')) ? $request->get('process_id') : 0;
        $step_id = ($request->has('step_id')) ? $request->get('step_id') : 0;

        $configs = Config::first();

        $activity_ids = explode(',',$configs->dashboard_outstanding_activities);

        $process = Process::where('id',$configs->dashboard_process)->first();

        $clients = Client::where('is_progressing',1)->where('is_qa','0')->where('step_id',$step_id)->where('process_id', $process->id)->pluck('id');
        
        $outstanding_activities = [];
        foreach ($process->steps as $step) {
                
            foreach ($step->activities as $activity) {
                    
                if ($activity->step_id == $step_id) {

                    $outstanding_activities[$activity->name] = [
                        'user' => 0
                    ];
                    foreach ($clients as $client_id) {
                        $has_data = false;
                        if (isset($activity->actionable['data'][0])) {

                            foreach ($activity->actionable['data'] as $data) {
                                if ($data->client_id == $client_id) {
                                    if (isset($data->actionable_boolean_id)) {
                                        if ($data->actionable_boolean_id > 0) {
                                            $data2 = ActionableBooleanData::where('client_id',$data->client_id)->where('actionable_boolean_id', $data->actionable_boolean_id)->orderBy('id','desc')->take(1)->first();

                                            if ($data2->data == "1") {
                                                $has_data = true;
                                            }
                                        } else {
                                            $has_data = false;
                                        }
                                    }

                                    if (isset($data->actionable_dropdown_id)) {
                                        if (isset($data->actionable_dropdown_item_id) && $data->actionable_dropdown_item_id > 0) {
                                            $data2 = ActionableDropdownItem::where('id', $data->actionable_dropdown_item_id)->first();

                                            if ($data2->name != null) {
                                                $has_data = true;
                                            }
                                        } else {
                                            $has_data = false;
                                        }
                                    }

                                    if (isset($data->actionable_text_id)) {
                                        if (isset($data->actionable_text_id) && $data->actionable_text_id > 0) {
                                            $data2 = ActionableTextData::where('client_id',$data->client_id)->where('actionable_text_id', $data->actionable_text_id)->first();

                                            if ($data2->data) {
                                                $has_data = true;
                                            }
                                        } else {
                                            $has_data = false;
                                        }
                                    }

                                    if (isset($data->actionable_textarea_id)) {
                                        if (isset($data->actionable_textarea_id) && $data->actionable_textarea_id > 0) {
                                            $data2 = ActionableTextareaData::where('client_id',$data->client_id)->where('actionable_textarea_id', $data->actionable_textarea_id)->first();

                                            if ($data2->data) {
                                                $has_data = true;
                                            }
                                        } else {
                                            $has_data = false;
                                        }
                                    }

                                    if (isset($data->actionable_document_email_id)) {
                                        if (isset($data->actionable_document_email_id) && $data->actionable_document_email_id > 0) {
                                            $data2 = ActionableDocumentEmailData::where('client_id',$data->client_id)->where('actionable_document_email_id', $data->actionable_document_email_id)->first();

                                            if ($data2->data) {
                                                $has_data = true;
                                            }
                                        } else {
                                            $has_data = false;
                                        }
                                    }

                                    if (isset($data->actionable_template_email_id)) {
                                        if (isset($data->actionable_template_email_id) && $data->actionable_template_email_id > 0) {
                                            $data2 = ActionableTemplateEmailData::where('client_id',$data->client_id)->where('actionable_template_email_id', $data->actionable_template_email_id)->first();

                                            if ($data2->data) {
                                                $has_data = true;
                                            }
                                        } else {
                                            $has_data = false;
                                        }
                                    }

                                    if (isset($data->actionable_date_id)) {
                                        if (isset($data->actionable_date_id) && $data->actionable_date_id > 0) {
                                            $data2 = ActionableDateData::where('client_id',$data->client_id)->where('actionable_date_id', $data->actionable_date_id)->first();

                                            if ($data2->data != null) {
                                                $has_data = true;
                                            }
                                        } else {
                                            $has_data = false;
                                        }
                                    }
                                }
                            }
                        }

                        if (!$has_data) {
                            $outstanding_activities[$activity->name]['user']++;
                        }
                    }
                }
            }
        }

        return json_encode($outstanding_activities);
    }

    public function getCompletedClientsAjax(Request $request)
    {
        $process_id = ($request->has('process_id')) ? $request->get('process_id') : 0;
        $process = Process::find($process_id);

        $range = ($request->has('range')) ? $request->get('range') : 'day';
        $from_string = ($request->has('from')) ? $request->get('from') : '';
        $to_string = ($request->has('to')) ? $request->get('to') : '';

        $from = Carbon::parse($from_string);
        $to = Carbon::parse($to_string);

        switch ($range) {
            default:
            case 'day':
                $date_diff = $from->diffInDays($to);

                $client_query = Client::where('step_id',5)->where('process_id', $process->id)->where('is_progressing', '=', 1)->where('is_qa','0')->whereDate('completed_at','>=',$from)->whereDate('completed_at','<=',$to)->where('completed_at','!=', null)->select(DB::raw('DATE_FORMAT(completed_at, "%e %M %Y") as date'), DB::raw('count(*) as onboarded'))->groupBy('date')->pluck('onboarded', 'date')->toArray();

                $client_onboards = [];
                for ($i = 0; $i <= $date_diff; $i++) {
                    $working_date = $from->copy()->addDays($i)->format('j F Y');
                    if (isset($client_query[$working_date])) {
                        $client_onboards[$working_date] = $client_query[$working_date];
                    } else {
                        $client_onboards[$working_date] = 0;
                    }
                }

                break;
            case 'week':
                $date_diff = $from->diffInWeeks($to->addDay(1));

                $client_query = Client::where('step_id',5)->where('process_id', $process->id)->where('is_progressing', '=', 1)->where('is_qa','0')->whereDate('completed_at','>=',$from)->whereDate('completed_at','<=',$to)->where('completed_at','!=', null)->select(DB::raw('DATE_FORMAT(completed_at, "%u %x") as date'), DB::raw('count(*) as onboarded'))->groupBy('date')->pluck('onboarded', 'date')->toArray();
            
                $client_onboards = [];
                for ($i = 0; $i <= $date_diff; $i++) {

                    $readable_date = $from->copy()->startOfWeek()->addWeeks($i)->format('j F Y');
                    $working_date = $from->copy()->startOfWeek()->addWeeks($i)->format('W Y');
                    if (isset($client_query[$working_date])) {
                        $client_onboards[$readable_date] = $client_query[$working_date];
                    } else {
                        $client_onboards[$readable_date] = 0;
                    }
                }

                break;
            case 'month':
                $date_diff = $from->diffInMonths($to);

                $client_query = Client::where('step_id',5)->where('process_id', $process->id)->where('is_progressing', '=', 1)->where('is_qa','0')->whereDate('completed_at','>=',$from)->whereDate('completed_at','<=',$to)->where('completed_at','!=', null)->select(DB::raw('DATE_FORMAT(updated_at, "%e %M %Y") as date'), DB::raw('count(*) as onboarded'))->groupBy('date')->pluck('onboarded', 'date')->toArray();

                $client_onboards = [];
                for ($i = 0; $i <= $date_diff; $i++) {
                    $working_date = $from->copy()->addMonths($i)->format('F Y');
                    if (isset($client_query[$working_date])) {
                        $client_onboards[$working_date] = $client_query[$working_date];
                    } else {
                        $client_onboards[$working_date] = 0;
                    }
                }

                break;
            case 'year':
                $date_diff = $from->diffInYears($to);

                $client_query = Client::where('step_id',5)->where('process_id', $process->id)->where('is_progressing', '=', 1)->where('is_qa','0')->whereDate('completed_at','>=',$from)->whereDate('completed_at','<=',$to)->where('completed_at','!=', null)->select(DB::raw('DATE_FORMAT(updated_at, "%e %M %Y") as date'), DB::raw('count(*) as onboarded'))->groupBy('date')->pluck('onboarded', 'date')->toArray();

                $client_onboards = [];
                for ($i = 0; $i <= $date_diff; $i++) {
                    $working_date = $from->copy()->addYears($i)->format('Y');
                    if (isset($client_query[$working_date])) {
                        $client_onboards[$working_date] = $client_query[$working_date];
                    } else {
                        $client_onboards[$working_date] = 0;
                    }
                }
                break;
        }
        
        return json_encode($client_onboards);
    }
}
