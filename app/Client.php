<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;
use AustinHeap\Database\Encryption\Traits\HasEncryptedAttributes;

class Client extends Model
{
    use SoftDeletes;
    use Sortable;
    use HasEncryptedAttributes;

    protected $encrypted = ['company','first_name','last_name','email','contact','cif_code','id_number','çompany_registration_number'];
    protected $dates = ['completed_at','deleted_at'];

    /*public $hidden = ['hash_first_name','hash_last_name','hash_company','hash_id_number','hash_email','hash_contact','hash_company_registration_number'];*/
    public $sortable = ['cif_code','contact','case_number','instruction_date','process_id','completed_days','created_at','step_id', 'consultant_id'];


    /*public function __construct(array $attributes = array(), bool $unHide = false)
    {
        parent::__construct($attributes);
        if ($unHide) {
            $this->hidden = [];
        }
    }*/

    public function unHide(){
        $this->hidden = [];
    }


    /**
     * Returns the company name.
     * 
     * Should the company name field be empty,
     * return the concatenated first and last name.
     *
     * @return string
     */
    public function company_name() {
        $company = $this->company;

        return ($company) ?: $this->first_name." ".$this->last_name;
    }

    public function referrer()
    {
        return $this->belongsTo('App\Referrer', 'referrer_id');
    }

    public function trigger()
    {
        return $this->belongsTo('App\TriggerType', 'trigger_type_id');
    }

    public function introducer()
    {
        return $this->belongsTo(User::class, 'introducer_id')->withTrashed();
    }

    public function consultant()
    {
        return $this->belongsTo(User::class, 'consultant_id')->withTrashed();
    }

    public function completedby()
    {
        return $this->belongsTo(User::class, 'completed_by')->withTrashed();
    }

    public function committee()
    {
        return $this->belongsTo(Committee::class, 'committee_id')->withTrashed();
    }

    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id')->withTrashed();
    }

    public function office()
    {
        return $this->belongsTo(Office::class);
    }

    public function step()
    {
        return $this->belongsTo(Step::class, 'step_id')->withTrashed();
    }

    public function process()
    {
        return $this->belongsTo(Process::class);
    }

    public function processes()
    {
        return $this->hasMany('App\ClientProcess','client_id','id');
    }

    public function documents()
    {
        return $this->hasMany('App\Document');
    }

    public function forms(){
        return $this->hasMany('App\ClientForm');
    }

    public function crfforms(){
        return $this->hasMany('App\ClientCRFForm');
    }

    public function client_forms()
    {
        return $this->hasMany('App\ClientForm');
    }

    public function comments()
    {
        return $this->hasMany('App\ClientComment');
    }

    public function business_unit()
    {
        return $this->hasOne('App\BusinessUnits', 'id', 'business_unit_id');
    }

    public function related_parties(){
        return $this->hasMany('App\RelatedParty','client_id','id');
    }

    public function getProcessProgress(Step $step)
    {
        $id = $this->id;

        $step->load(['activities.actionable.data' => function ($query) use ($id) {
            $query->where('client_id', $id);
            $query->orderBy('created_at','asc');
        }]);

        $process_progress = [];

        $step_array = [
            'id' => $step->id,
            'name' => $step->name,
            'order' => $step->order,
            'stage' => 0,
            'activities' => []
        ];

        array_push($process_progress, $step_array);

        foreach ($step->activities /*$step->activities*/ as $activity) {
            $activity_array = [
                'id' => $activity->id,
                'name' => $activity->name,
                'order' => $activity->order,
                'type' => $activity->getTypeName(),
                'type_display' => $activity->getTypeDisplayName(),
                'stage' => 0,
                'due_date' => 0,
                'dependant_activity_id' => $activity->dependant_activity_id,
                'kpi' => $activity->kpi,
                'avalue' => $activity->value,
                'procedure' => $activity->procedure,
                'client_bucket' => $activity->client_bucket
            ];

            if ($activity_array['type'] == 'dropdown') {
                $activity_array['dropdown_items'] = $activity->actionable->items->pluck('name', 'id')->toArray();
                $activity_array['dropdown_values'] = $activity->actionable->valuess->where('client_id',$id)->pluck('actionable_dropdown_item_id', 'id')->toArray();
            }

            /*if (is_null($activity->dependant_activity_id)) {
                $activity_array['stage'] = 1;
            }*/

            if ($step->id <= $this->step_id) {
                $activity_array['stage'] = 1;
            }

            if (isset($activity->actionable['data'][0])) {
                $activity_array['stage'] = 2;
                $data_index = count($activity->actionable['data']) -1;
                //dd($activity->actionable['data']);
                switch ($activity_array['type']) {
                    //get last not zero
                    case 'boolean':
                        $activity_array['value'] = $activity->actionable['data'][$data_index]->data;
                        $activity_array['crdate'] = Carbon::parse($activity->actionable['data'][$data_index]->created_at)->format('Y-m-d');
                    case 'notification':
                        $activity_array['value'] = $activity->actionable['data'][$data_index]->data;
                        $activity_array['crdate'] = Carbon::parse($activity->actionable['data'][$data_index]->created_at)->format('Y-m-d');
                    case 'date':
                        $activity_array['value'] = $activity->actionable['data'][$data_index]->data;
                        $activity_array['crdate'] = Carbon::parse($activity->actionable['data'][$data_index]->created_at)->format('Y-m-d');
                    case 'text':
                        $activity_array['value'] = $activity->actionable['data'][$data_index]->data;
                        $activity_array['crdate'] = Carbon::parse($activity->actionable['data'][$data_index]->created_at)->format('Y-m-d');
                        break;
                    case 'template_email':
                        $activity_array['value'] = $activity->actionable['data'][$data_index]->template_id;
                        $activity_array['email'] = $activity->actionable['data'][$data_index]->email;
                        $activity_array['crdate'] = Carbon::parse($activity->actionable['data'][$data_index]->created_at)->format('Y-m-d');
                        break;
                    case 'document_email':
                        $activity_array['value'] = $activity->actionable['data'][$data_index]->document_id;
                        $activity_array['email'] = $activity->actionable['data'][$data_index]->email;
                        $activity_array['crdate'] = Carbon::parse($activity->actionable['data'][$data_index]->created_at)->format('Y-m-d');
                        break;
                    case 'document':
                        $activity_array['value'] = $activity->actionable['data'][$data_index]->document_id;
                        $activity_array['crdate'] = Carbon::parse($activity->actionable['data'][$data_index]->created_at)->format('Y-m-d');
                        break;
                    case 'dropdown':
                        $activity_array['value'] = $activity->actionable['data'][$data_index]->actionable_dropdown_item_id;
                        $activity_array['crdate'] = Carbon::parse($activity->actionable['data'][$data_index]->created_at)->format('Y-m-d');
                        break;
                }
            }
            $process_progress[0]['activities'][$activity->id] = $activity_array;
        }

        foreach ($step->activities/*$step['activities']*/ as $activity_key => $activity) {
            if (!is_null($activity['dependant_activity_id']) && $activity['stage'] == 0) {
                $working_dependancy_id = $activity['dependant_activity_id'];
                foreach ($process_progress as $searched_step_key => $searched_step) {
                    if (isset($searched_step['activities'][$working_dependancy_id])) {
                        if ($searched_step['activities'][$working_dependancy_id]['stage'] == 2) {
                            $process_progress[$step->id]['activities'][$activity_key]['stage'] = 1;
                        } else {
                            if (!is_null($searched_step['activities'][$working_dependancy_id]['dependant_activity_id'])) {
                                $working_dependancy_id = $searched_step['activities'][$working_dependancy_id]['dependant_activity_id'];
                            }
                        }
                    }
                }
            }
        }

        $completed = true;
        $started = false;
        foreach ($step->activities/*$step['activities']*/ as $activity_key => $activity) {
            /*if ($activity['stage'] == 1) {
                $started = true;
            }*/

            if ($step->id == $this->step_id) {
                $started = true;
                //dd($activity);
            }

            if ($activity['kpi'] && $activity['stage'] != 2) {
                $completed = false;
            }

        }

        if ($started) {
            $process_progress[0]['stage'] = 1;
        }

        if ($completed) {
            $process_progress[0]['stage'] = 2;
            //dd($activity);
        }

        return $process_progress;
    }

    public function getClientActivities()
    {
        $id = $this->id;


//dd($client_basket_activities);
        $steps = Step::with('activities')->get();

        //dd($steps);
        $client_basket = [];

        foreach ($steps as $step) {
            $process_progress = [];

            $step_array = [
                'id' => $step->id,
                'name' => $step->name,
                'order' => $step->order,
                'stage' => 0,
                'activities' => []
            ];

            array_push($process_progress, $step_array);

            foreach ($step->activities /*$step->activities*/ as $activity) {

                $activity_array = [
                    'id' => $activity->id,
                    'name' => $activity->name,
                    'order' => $activity->order,
                    'type' => $activity->getTypeName(),
                    'type_display' => $activity->getTypeDisplayName(),
                    'stage' => 0,
                    'due_date' => 0,
                    'dependant_activity_id' => $activity->dependant_activity_id,
                    'kpi' => $activity->kpi,
                    'avalue' => $activity->value,
                    'procedure' => $activity->procedure,
                    'client_bucket' => $activity->client_bucket,
                    'grouped' => $activity->grouped,
                    'tooltip' => $activity->tooltip
                ];

                if ($activity_array['type'] == 'dropdown') {
                    $activity_array['dropdown_items'] = $activity->actionable->items->pluck('name', 'id')->toArray();
                    $activity_array['dropdown_values'] = $activity->actionable->valuess->where('client_id', $id)->pluck('actionable_dropdown_item_id', 'id')->toArray();
                }

                /*if (is_null($activity->dependant_activity_id)) {
                    $activity_array['stage'] = 1;
                }*/

                if ($step->id <= $this->step_id) {
                    $activity_array['stage'] = 1;
                }

                if (isset($activity->actionable['data'][0])) {
                    $activity_array['stage'] = 2;
                    $data_index = count($activity->actionable['data']) - 1;
                    //dd($activity->actionable['data']);
                    switch ($activity_array['type']) {
                        //get last not zero
                        case 'boolean':
                            $activity_array['value'] = $activity->actionable['data'][$data_index]->data;
                            $activity_array['crdate'] = Carbon::parse($activity->actionable['data'][$data_index]->created_at)->format('Y-m-d');
                        case 'notification':
                            $activity_array['value'] = $activity->actionable['data'][$data_index]->data;
                            $activity_array['crdate'] = Carbon::parse($activity->actionable['data'][$data_index]->created_at)->format('Y-m-d');
                        case 'date':
                            $activity_array['value'] = $activity->actionable['data'][$data_index]->data;
                            $activity_array['crdate'] = Carbon::parse($activity->actionable['data'][$data_index]->created_at)->format('Y-m-d');
                        case 'text':
                            $activity_array['value'] = $activity->actionable['data'][$data_index]->data;
                            $activity_array['crdate'] = Carbon::parse($activity->actionable['data'][$data_index]->created_at)->format('Y-m-d');
                            break;
                        case 'template_email':
                            $activity_array['value'] = $activity->actionable['data'][$data_index]->template_id;
                            $activity_array['email'] = $activity->actionable['data'][$data_index]->email;
                            $activity_array['crdate'] = Carbon::parse($activity->actionable['data'][$data_index]->created_at)->format('Y-m-d');
                            break;
                        case 'document_email':
                            $activity_array['value'] = $activity->actionable['data'][$data_index]->document_id;
                            $activity_array['email'] = $activity->actionable['data'][$data_index]->email;
                            $activity_array['crdate'] = Carbon::parse($activity->actionable['data'][$data_index]->created_at)->format('Y-m-d');
                            break;
                        case 'document':
                            $activity_array['value'] = $activity->actionable['data'][$data_index]->document_id;
                            $activity_array['crdate'] = Carbon::parse($activity->actionable['data'][$data_index]->created_at)->format('Y-m-d');
                            break;
                        case 'dropdown':
                            $activity_array['value'] = $activity->actionable['data'][$data_index]->actionable_dropdown_item_id;
                            $activity_array['crdate'] = Carbon::parse($activity->actionable['data'][$data_index]->created_at)->format('Y-m-d');
                            break;
                    }
                }
                $process_progress[0]['activities'][$activity->id] = $activity_array;
            }

            foreach ($step->activities/*$step['activities']*/ as $activity_key => $activity) {
                if (!is_null($activity['dependant_activity_id']) && $activity['stage'] == 0) {
                    $working_dependancy_id = $activity['dependant_activity_id'];
                    foreach ($process_progress as $searched_step_key => $searched_step) {
                        if (isset($searched_step['activities'][$working_dependancy_id])) {
                            if ($searched_step['activities'][$working_dependancy_id]['stage'] == 2) {
                                $process_progress[$step->id]['activities'][$activity_key]['stage'] = 1;
                            } else {
                                if (!is_null($searched_step['activities'][$working_dependancy_id]['dependant_activity_id'])) {
                                    $working_dependancy_id = $searched_step['activities'][$working_dependancy_id]['dependant_activity_id'];
                                }
                            }
                        }
                    }
                }
            }

            $completed = true;
            $started = false;
            foreach ($step->activities/*$step['activities']*/ as $activity_key => $activity) {
                /*if ($activity['stage'] == 1) {
                    $started = true;
                }*/

                if ($step->id == $this->step_id) {
                    $started = true;
                    //dd($activity);
                }

                if ($activity['kpi'] && $activity['stage'] != 2) {
                    $completed = false;
                }
            }

            if ($started) {
                $process_progress[0]['stage'] = 1;
            }

            if ($completed) {
                $process_progress[0]['stage'] = 2;
                //dd($activity);
            }

            array_push($client_basket,$process_progress);
        }

        return $client_basket;
    }

    public function getClientBasketActivities($process_id)
    {
        $id = $this->id;


//dd($client_basket_activities);
        $steps = Step::with(['activities.actionable.data' => function ($query) use ($id) {
            $query->where('client_id', $id);
            $query->orderBy('created_at','asc');
        }])->where('process_id',$process_id)->get();

        //dd($steps);
        $client_basket = [];

        foreach ($steps as $step) {
            $process_progress = [];

            $step_array = [
                'id' => $step->id,
                'name' => $step->name,
                'order' => $step->order,
                'stage' => 0,
                'activities' => []
            ];

            array_push($process_progress,$step_array);

            foreach ($step->activities /*$step->activities*/ as $activity) {

                $activity_array = [
                    'id' => $activity->id,
                    'name' => $activity->name,
                    'order' => $activity->order,
                    'type' => $activity->getTypeName(),
                    'type_display' => $activity->getTypeDisplayName(),
                    'stage' => 0,
                    'due_date' => 0,
                    'dependant_activity_id' => $activity->dependant_activity_id,
                    'kpi' => $activity->kpi,
                    'avalue' => $activity->value,
                    'procedure' => $activity->procedure,
                    'client_bucket' => $activity->client_bucket,
                    'grouped' => $activity->grouped,
                    'tooltip' => $activity->tooltip
                ];

                if ($activity_array['type'] == 'dropdown') {
                    $activity_array['dropdown_items'] = $activity->actionable->items->pluck('name', 'id')->toArray();
                    $activity_array['dropdown_values'] = $activity->actionable->valuess->where('client_id', $id)->pluck('actionable_dropdown_item_id', 'id')->toArray();
                }

                /*if (is_null($activity->dependant_activity_id)) {
                    $activity_array['stage'] = 1;
                }*/

                if ($step->id <= $this->step_id) {
                    $activity_array['stage'] = 1;
                }

                if (isset($activity->actionable['data'][0])) {
                    $activity_array['stage'] = 2;
                    $data_index = count($activity->actionable['data']) - 1;
                    //dd($activity->actionable['data']);
                    switch ($activity_array['type']) {
                        //get last not zero
                        case 'boolean':
                            $activity_array['value'] = $activity->actionable['data'][$data_index]->data;
                            $activity_array['crdate'] = Carbon::parse($activity->actionable['data'][$data_index]->created_at)->format('Y-m-d');
                        case 'notification':
                            $activity_array['value'] = $activity->actionable['data'][$data_index]->data;
                            $activity_array['crdate'] = Carbon::parse($activity->actionable['data'][$data_index]->created_at)->format('Y-m-d');
                        case 'date':
                            $activity_array['value'] = $activity->actionable['data'][$data_index]->data;
                            $activity_array['crdate'] = Carbon::parse($activity->actionable['data'][$data_index]->created_at)->format('Y-m-d');
                        case 'text':
                            $activity_array['value'] = $activity->actionable['data'][$data_index]->data;
                            $activity_array['crdate'] = Carbon::parse($activity->actionable['data'][$data_index]->created_at)->format('Y-m-d');
                            break;
                        case 'template_email':
                            $activity_array['value'] = $activity->actionable['data'][$data_index]->template_id;
                            $activity_array['email'] = $activity->actionable['data'][$data_index]->email;
                            $activity_array['crdate'] = Carbon::parse($activity->actionable['data'][$data_index]->created_at)->format('Y-m-d');
                            break;
                        case 'document_email':
                            $activity_array['value'] = $activity->actionable['data'][$data_index]->document_id;
                            $activity_array['email'] = $activity->actionable['data'][$data_index]->email;
                            $activity_array['crdate'] = Carbon::parse($activity->actionable['data'][$data_index]->created_at)->format('Y-m-d');
                            break;
                        case 'document':
                            $activity_array['value'] = $activity->actionable['data'][$data_index]->document_id;
                            $activity_array['crdate'] = Carbon::parse($activity->actionable['data'][$data_index]->created_at)->format('Y-m-d');
                            break;
                        case 'dropdown':
                            $activity_array['value'] = $activity->actionable['data'][$data_index]->actionable_dropdown_item_id;
                            $activity_array['crdate'] = Carbon::parse($activity->actionable['data'][$data_index]->created_at)->format('Y-m-d');
                            break;
                    }
                }
                $process_progress[0]['activities'][$activity->id] = $activity_array;
            }

            foreach ($step->activities/*$step['activities']*/ as $activity_key => $activity) {
                if (!is_null($activity['dependant_activity_id']) && $activity['stage'] == 0) {
                    $working_dependancy_id = $activity['dependant_activity_id'];
                    foreach ($process_progress as $searched_step_key => $searched_step) {
                        if (isset($searched_step['activities'][$working_dependancy_id])) {
                            if ($searched_step['activities'][$working_dependancy_id]['stage'] == 2) {
                                $process_progress[$step->id]['activities'][$activity_key]['stage'] = 1;
                            } else {
                                if (!is_null($searched_step['activities'][$working_dependancy_id]['dependant_activity_id'])) {
                                    $working_dependancy_id = $searched_step['activities'][$working_dependancy_id]['dependant_activity_id'];
                                }
                            }
                        }
                    }
                }
            }

            $completed = true;
            $started = false;
            foreach ($step->activities/*$step['activities']*/ as $activity_key => $activity) {
                /*if ($activity['stage'] == 1) {
                    $started = true;
                }*/

                if ($step->id == $this->step_id) {
                    $started = true;
                    //dd($activity);
                }

                if ($activity['kpi'] && $activity['stage'] != 2) {
                    $completed = false;
                }
            }

            if ($started) {
                $process_progress[0]['stage'] = 1;
            }

            if ($completed) {
                $process_progress[0]['stage'] = 2;
                //dd($activity);
            }

            $client_basket[$step->id][$step->name] =$process_progress;
        }

        return $client_basket;
    }

    public function getClientBasketActivitiesClientView($process_id,$activities)
    {
        $id = $this->id;


//dd($client_basket_activities);
        $steps = Step::with('activities')->where('process_id',$process_id)->get();

        //dd($steps);
        $client_basket = [];

        foreach ($steps as $step) {
            $i = 0;
            $process_progress = [];

            $step_array = [
                'id' => $step->id,
                'name' => $step->name,
                'order' => $step->order,
                'stage' => 0,
                'activities' => []
            ];

            array_push($process_progress, $step_array);

            foreach ($step->activities /*$step->activities*/ as $activity) {
                if (in_array($activity->id, $activities))
                    $i++;
                    $activity_array = [
                        'id' => $activity->id,
                        'name' => $activity->name,
                        'order' => $activity->order,
                        'type' => $activity->getTypeName(),
                        'type_display' => $activity->getTypeDisplayName(),
                        'stage' => 0,
                        'due_date' => 0,
                        'dependant_activity_id' => $activity->dependant_activity_id,
                        'kpi' => $activity->kpi,
                        'avalue' => $activity->value,
                        'procedure' => $activity->procedure,
                        'client_bucket' => $activity->client_bucket,
                        'grouped' => $activity->grouped,
                        'tooltip' => $activity->tooltip
                    ];

                if ($activity_array['type'] == 'dropdown') {
                    $activity_array['dropdown_items'] = $activity->actionable->items->pluck('name', 'id')->toArray();
                    $activity_array['dropdown_values'] = $activity->actionable->valuess->where('client_id', $id)->pluck('actionable_dropdown_item_id', 'id')->toArray();
                }

                /*if (is_null($activity->dependant_activity_id)) {
                    $activity_array['stage'] = 1;
                }*/

                if ($step->id <= $this->step_id) {
                    $activity_array['stage'] = 1;
                }

                if (isset($activity->actionable['data'][0])) {
                    $activity_array['stage'] = 2;
                    $data_index = count($activity->actionable['data']) - 1;
                    //dd($activity->actionable['data']);
                    switch ($activity_array['type']) {
                        //get last not zero
                        case 'boolean':
                            $activity_array['value'] = $activity->actionable['data'][$data_index]->data;
                            $activity_array['crdate'] = Carbon::parse($activity->actionable['data'][$data_index]->created_at)->format('Y-m-d');
                        case 'notification':
                            $activity_array['value'] = $activity->actionable['data'][$data_index]->data;
                            $activity_array['crdate'] = Carbon::parse($activity->actionable['data'][$data_index]->created_at)->format('Y-m-d');
                        case 'date':
                            $activity_array['value'] = $activity->actionable['data'][$data_index]->data;
                            $activity_array['crdate'] = Carbon::parse($activity->actionable['data'][$data_index]->created_at)->format('Y-m-d');
                        case 'text':
                            $activity_array['value'] = $activity->actionable['data'][$data_index]->data;
                            $activity_array['crdate'] = Carbon::parse($activity->actionable['data'][$data_index]->created_at)->format('Y-m-d');
                            break;
                        case 'template_email':
                            $activity_array['value'] = $activity->actionable['data'][$data_index]->template_id;
                            $activity_array['email'] = $activity->actionable['data'][$data_index]->email;
                            $activity_array['crdate'] = Carbon::parse($activity->actionable['data'][$data_index]->created_at)->format('Y-m-d');
                            break;
                        case 'document_email':
                            $activity_array['value'] = $activity->actionable['data'][$data_index]->document_id;
                            $activity_array['email'] = $activity->actionable['data'][$data_index]->email;
                            $activity_array['crdate'] = Carbon::parse($activity->actionable['data'][$data_index]->created_at)->format('Y-m-d');
                            break;
                        case 'document':
                            $activity_array['value'] = $activity->actionable['data'][$data_index]->document_id;
                            $activity_array['crdate'] = Carbon::parse($activity->actionable['data'][$data_index]->created_at)->format('Y-m-d');
                            break;
                        case 'dropdown':
                            $activity_array['value'] = $activity->actionable['data'][$data_index]->actionable_dropdown_item_id;
                            $activity_array['crdate'] = Carbon::parse($activity->actionable['data'][$data_index]->created_at)->format('Y-m-d');
                            break;
                    }
                }
                $process_progress[0]['activities'][$activity->id] = $activity_array;
            }
        }

            foreach ($step->activities/*$step['activities']*/ as $activity_key => $activity) {
                if (!is_null($activity['dependant_activity_id']) && $activity['stage'] == 0) {
                    $working_dependancy_id = $activity['dependant_activity_id'];
                    foreach ($process_progress as $searched_step_key => $searched_step) {
                        if (isset($searched_step['activities'][$working_dependancy_id])) {
                            if ($searched_step['activities'][$working_dependancy_id]['stage'] == 2) {
                                $process_progress[$step->id]['activities'][$activity_key]['stage'] = 1;
                            } else {
                                if (!is_null($searched_step['activities'][$working_dependancy_id]['dependant_activity_id'])) {
                                    $working_dependancy_id = $searched_step['activities'][$working_dependancy_id]['dependant_activity_id'];
                                }
                            }
                        }
                    }
                }
            }

            $completed = true;
            $started = false;
            foreach ($step->activities/*$step['activities']*/ as $activity_key => $activity) {
                /*if ($activity['stage'] == 1) {
                    $started = true;
                }*/

                if ($step->id == $this->step_id) {
                    $started = true;
                    //dd($activity);
                }

                if ($activity['kpi'] && $activity['stage'] != 2) {
                    $completed = false;
                }
            }

            if ($started) {
                $process_progress[0]['stage'] = 1;
            }

            if ($completed) {
                $process_progress[0]['stage'] = 2;
                //dd($activity);
            }

            if($i == 0) {
                $client_basket[$step->name] = $process_progress;
            }


        return $client_basket;
    }

    public function getRelatedPartyProcessProgress(Step $step,$related_party_id)
    {
        $id = $this->id;

        $step->load(['activities.actionable.data' => function ($query) use ($id) {
            $query->where('client_id', $id);
            $query->orderBy('created_at','asc');
        }]);

        $process_progress = [];

        $step_array = [
            'id' => $step->id,
            'name' => $step->name,
            'order' => $step->order,
            'stage' => 0,
            'activities' => []
        ];

        array_push($process_progress, $step_array);

        foreach ($step->activities /*$step->activities*/ as $activity) {
            $activity_array = [
                'id' => $activity->id,
                'name' => $activity->name,
                'order' => $activity->order,
                'type' => $activity->getTypeName(),
                'type_display' => $activity->getTypeDisplayName(),
                'stage' => 0,
                'due_date' => 0,
                'dependant_activity_id' => $activity->dependant_activity_id,
                'kpi' => $activity->kpi,
                'avalue' => $activity->value,
                'procedure' => $activity->procedure,
                'client_bucket' => $activity->client_bucket
            ];

            if ($activity_array['type'] == 'dropdown') {
                $activity_array['dropdown_items'] = $activity->actionable->items->pluck('name', 'id')->toArray();
                $activity_array['dropdown_values'] = $activity->actionable->valuess->where('client_id',$id)->pluck('actionable_dropdown_item_id', 'id')->toArray();
            }

            /*if (is_null($activity->dependant_activity_id)) {
                $activity_array['stage'] = 1;
            }*/

            if ($step->id <= $this->step_id) {
                $activity_array['stage'] = 1;
            }

            if (isset($activity->actionable['data'][0])) {
                $activity_array['stage'] = 2;
                $data_index = count($activity->actionable['data']) -1;
                //dd($activity->actionable['data']);
                switch ($activity_array['type']) {
                    //get last not zero
                    case 'boolean':
                        $activity_array['value'] = $activity->actionable['data'][$data_index]->data;
                        $activity_array['crdate'] = Carbon::parse($activity->actionable['data'][$data_index]->created_at)->format('Y-m-d');
                    case 'notification':
                        $activity_array['value'] = $activity->actionable['data'][$data_index]->data;
                        $activity_array['crdate'] = Carbon::parse($activity->actionable['data'][$data_index]->created_at)->format('Y-m-d');
                    case 'date':
                        $activity_array['value'] = $activity->actionable['data'][$data_index]->data;
                        $activity_array['crdate'] = Carbon::parse($activity->actionable['data'][$data_index]->created_at)->format('Y-m-d');
                    case 'text':
                        $activity_array['value'] = $activity->actionable['data'][$data_index]->data;
                        $activity_array['crdate'] = Carbon::parse($activity->actionable['data'][$data_index]->created_at)->format('Y-m-d');
                        break;
                    case 'template_email':
                        $activity_array['value'] = $activity->actionable['data'][$data_index]->template_id;
                        $activity_array['email'] = $activity->actionable['data'][$data_index]->email;
                        $activity_array['crdate'] = Carbon::parse($activity->actionable['data'][$data_index]->created_at)->format('Y-m-d');
                        break;
                    case 'document_email':
                        $activity_array['value'] = $activity->actionable['data'][$data_index]->document_id;
                        $activity_array['email'] = $activity->actionable['data'][$data_index]->email;
                        $activity_array['crdate'] = Carbon::parse($activity->actionable['data'][$data_index]->created_at)->format('Y-m-d');
                        break;
                    case 'document':
                        $activity_array['value'] = $activity->actionable['data'][$data_index]->document_id;
                        $activity_array['crdate'] = Carbon::parse($activity->actionable['data'][$data_index]->created_at)->format('Y-m-d');
                        break;
                    case 'dropdown':
                        $activity_array['value'] = $activity->actionable['data'][$data_index]->actionable_dropdown_item_id;
                        $activity_array['crdate'] = Carbon::parse($activity->actionable['data'][$data_index]->created_at)->format('Y-m-d');
                        break;
                }
            }
            $process_progress[0]['activities'][$activity->id] = $activity_array;
        }

        foreach ($step->activities/*$step['activities']*/ as $activity_key => $activity) {
            if (!is_null($activity['dependant_activity_id']) && $activity['stage'] == 0) {
                $working_dependancy_id = $activity['dependant_activity_id'];
                foreach ($process_progress as $searched_step_key => $searched_step) {
                    if (isset($searched_step['activities'][$working_dependancy_id])) {
                        if ($searched_step['activities'][$working_dependancy_id]['stage'] == 2) {
                            $process_progress[$step->id]['activities'][$activity_key]['stage'] = 1;
                        } else {
                            if (!is_null($searched_step['activities'][$working_dependancy_id]['dependant_activity_id'])) {
                                $working_dependancy_id = $searched_step['activities'][$working_dependancy_id]['dependant_activity_id'];
                            }
                        }
                    }
                }
            }
        }

        $completed = true;
        $started = false;
        foreach ($step->activities/*$step['activities']*/ as $activity_key => $activity) {
            /*if ($activity['stage'] == 1) {
                $started = true;
            }*/

            if ($step->id == $this->step_id) {
                $started = true;
                //dd($activity);
            }

            if ($activity['kpi'] && $activity['stage'] != 2) {
                $completed = false;
            }

        }

        if ($started) {
            $process_progress[0]['stage'] = 1;
        }

        if ($completed) {
            $process_progress[0]['stage'] = 2;
            //dd($activity);
        }

        return $process_progress;
    }

    public function getProcessStepProgress(Step $step)
    {
        $id = $this->id;

        $step->load(['activities.actionable.data' => function ($query) use ($id) {
            $query->where('client_id', $id);
        }]);

        $process_progress = [];

        $step_array = [
            'id' => $step->id,
            'name' => $step->name,
            'order' => $step->order,
            'stage' => 0,
            'group' => $step->group,
            'activities' => []
        ];

        array_push($process_progress, $step_array);

        foreach ($step->activities /*$step->activities*/ as $activity) {
            $activity_array = [
                'id' => $activity->id,
                'name' => $activity->name,
                'order' => $activity->order,
                'type' => $activity->getTypeName(),
                'type_display' => $activity->getTypeDisplayName(),
                'stage' => 0,
                'dependant_activity_id' => $activity->dependant_activity_id,
                'kpi' => $activity->kpi,
                'comment' => $activity->comment,
                'avalue' => $activity->value,
                'tooltip' => $activity->tooltip,
                'procedure' => $activity->procedure,
                'grouped' => $activity->grouped,
                'grouping' => $activity->grouping,
                'default_value' => $activity->default_value,
                'client_bucket' => $activity->client_bucket,
                'position' => ($activity->position != null ? $activity->position : '1'),
            ];

            if ($activity_array['type'] == 'dropdown') {
                $activity_array['dropdown_items'] = $activity->actionable->items->pluck('name', 'id')->toArray();
                $activity_array['dropdown_values'] = $activity->actionable->valuess->where('client_id',$id)->pluck('actionable_dropdown_item_id', 'id')->toArray();
            }

            /*if (is_null($activity->dependant_activity_id)) {
                $activity_array['stage'] = 1;
            }*/

            if ($step->id <= $this->step_id) {
                $activity_array['stage'] = 1;
            }

            if (isset($activity->actionable['data'][0])) {
                $activity_array['stage'] = 2;
                $data_index = count($activity->actionable['data']) -1;
                switch ($activity_array['type']) {
                    //get last not zero
                    case 'boolean':
                        $activity_array['value'] = $activity->actionable['data'][$data_index]->data;
                        $activity_array['crdate'] = Carbon::parse($activity->actionable['data'][$data_index]->created_at)->format('Y-m-d');
                    case 'notification':
                        $activity_array['value'] = $activity->actionable['data'][$data_index]->data;
                        $activity_array['crdate'] = Carbon::parse($activity->actionable['data'][$data_index]->created_at)->format('Y-m-d');
                    case 'date':
                        $activity_array['value'] = $activity->actionable['data'][$data_index]->data;
                        $activity_array['crdate'] = Carbon::parse($activity->actionable['data'][$data_index]->created_at)->format('Y-m-d');
                    case 'text':
                        $activity_array['value'] = $activity->actionable['data'][$data_index]->data;
                        $activity_array['crdate'] = Carbon::parse($activity->actionable['data'][$data_index]->created_at)->format('Y-m-d');
                        break;
                    case 'textarea':
                        $activity_array['value'] = $activity->actionable['data'][$data_index]->data;
                        $activity_array['crdate'] = Carbon::parse($activity->actionable['data'][$data_index]->created_at)->format('Y-m-d');
                        break;
                    case 'template_email':
                        $activity_array['value'] = $activity->actionable['data'][$data_index]->template_id;
                        $activity_array['email'] = $activity->actionable['data'][$data_index]->email;
                        $activity_array['crdate'] = Carbon::parse($activity->actionable['data'][$data_index]->created_at)->format('Y-m-d');
                        break;
                    case 'document_email':
                        $activity_array['value'] = $activity->actionable['data'][$data_index]->document_id;
                        $activity_array['email'] = $activity->actionable['data'][$data_index]->email;
                        $activity_array['crdate'] = Carbon::parse($activity->actionable['data'][$data_index]->created_at)->format('Y-m-d');
                        break;
                    case 'document':
                        $activity_array['value'] = $activity->actionable['data'][$data_index]->document_id;
                        $activity_array['crdate'] = Carbon::parse($activity->actionable['data'][$data_index]->created_at)->format('Y-m-d');
                        break;
                    case 'dropdown':
                        $activity_array['value'] = $activity->actionable['data'][$data_index]->actionable_dropdown_item_id;
                        $activity_array['crdate'] = Carbon::parse($activity->actionable['data'][$data_index]->created_at)->format('Y-m-d');
                        break;
                }
            }
            $process_progress[0]['activities'][$activity->id] = $activity_array;

        }

        foreach ($step->activities/*$step['activities']*/ as $activity_key => $activity) {
            if (!is_null($activity['dependant_activity_id']) && $activity['stage'] == 0) {
                $working_dependancy_id = $activity['dependant_activity_id'];
                foreach ($process_progress as $searched_step_key => $searched_step) {
                    if (isset($searched_step['activities'][$working_dependancy_id])) {
                        if ($searched_step['activities'][$working_dependancy_id]['stage'] == 2) {
                            $process_progress[$step->id]['activities'][$activity_key]['stage'] = 1;
                        } else {
                            if (!is_null($searched_step['activities'][$working_dependancy_id]['dependant_activity_id'])) {
                                $working_dependancy_id = $searched_step['activities'][$working_dependancy_id]['dependant_activity_id'];
                            }
                        }
                    }
                }
            }
        }

        $completed = true;
        $started = false;
        foreach ($step->activities/*$step['activities']*/ as $activity_key => $activity) {
            /*if ($activity['stage'] == 1) {
                $started = true;
            }*/

            if ($step->id == $this->step_id) {
                $started = true;
                //dd($activity);
            }

            if ($activity['kpi'] && $activity['stage'] != 2) {
                $completed = false;
            }

        }

        if ($started) {
            $process_progress[0]['stage'] = 1;
        }

        if ($completed) {
            $process_progress[0]['stage'] = 2;
            //dd($activity);
        }

        return $process_progress;
    }

    public function getRelatedPartyProcessStepProgress(Step $step,$related_party_id)
    {
        $id = $this->id;

        $step->load(['activities.actionable.data' => function ($query) use ($id,$related_party_id) {
            $query->where('client_id', $id)->where('related_party_id',$related_party_id);
        }]);

        $process_progress = [];

        $step_array = [
            'id' => $step->id,
            'name' => $step->name,
            'order' => $step->order,
            'group' => $step->group,
            'process_id' => $step->process_id,
            'stage' => 0,
            'activities' => []
        ];

        array_push($process_progress, $step_array);

        foreach ($step->activities /*$step->activities*/ as $activity) {
            $activity_array = [
                'id' => $activity->id,
                'name' => $activity->name,
                'order' => $activity->order,
                'type' => $activity->getRelatedPartyTypeName(),
                'type_display' => $activity->getRelatedPartyTypeDisplayName(),
                'stage' => 0,
                'dependant_activity_id' => $activity->dependant_activity_id,
                'kpi' => $activity->kpi,
                'comment' => $activity->comment,
                'avalue' => $activity->value,
                'procedure' => $activity->procedure,
                'tooltip' => $activity->tooltip,
                'grouped' => $activity->grouped,
                'grouping' => $activity->grouping,
                'default_value' => $activity->default_value,
                'client_bucket' => $activity->client_bucket
            ];

            if ($activity_array['type'] == 'dropdown') {
                $activity_array['dropdown_items'] = $activity->actionable->items->pluck('name', 'id')->toArray();
                $activity_array['dropdown_values'] = $activity->actionable->valuess->where('client_id',$id)->where('related_party_id',$related_party_id)->pluck('related_party_dropdown_item_id', 'id')->toArray();
            }

            /*if (is_null($activity->dependant_activity_id)) {
                $activity_array['stage'] = 1;
            }*/

            if ($step->id <= $this->step_id) {
                $activity_array['stage'] = 1;
            }

            if (isset($activity->actionable['data'][0])) {
                $activity_array['stage'] = 2;
                $data_index = count($activity->actionable['data']) -1;
                switch ($activity_array['type']) {
                    //get last not zero
                    case 'boolean':
                        $activity_array['value'] = $activity->actionable['data'][$data_index]->data;
                        $activity_array['crdate'] = Carbon::parse($activity->actionable['data'][$data_index]->created_at)->format('Y-m-d');
                    case 'notification':
                        $activity_array['value'] = $activity->actionable['data'][$data_index]->data;
                        $activity_array['crdate'] = Carbon::parse($activity->actionable['data'][$data_index]->created_at)->format('Y-m-d');
                    case 'date':
                        $activity_array['value'] = $activity->actionable['data'][$data_index]->data;
                        $activity_array['crdate'] = Carbon::parse($activity->actionable['data'][$data_index]->created_at)->format('Y-m-d');
                    case 'text':
                        $activity_array['value'] = $activity->actionable['data'][$data_index]->data;
                        $activity_array['crdate'] = Carbon::parse($activity->actionable['data'][$data_index]->created_at)->format('Y-m-d');
                        break;
                    case 'textarea':
                        $activity_array['value'] = $activity->actionable['data'][$data_index]->data;
                        $activity_array['crdate'] = Carbon::parse($activity->actionable['data'][$data_index]->created_at)->format('Y-m-d');
                        break;
                    case 'template_email':
                        $activity_array['value'] = $activity->actionable['data'][$data_index]->template_id;
                        $activity_array['email'] = $activity->actionable['data'][$data_index]->email;
                        $activity_array['crdate'] = Carbon::parse($activity->actionable['data'][$data_index]->created_at)->format('Y-m-d');
                        break;
                    case 'document_email':
                        $activity_array['value'] = $activity->actionable['data'][$data_index]->document_id;
                        $activity_array['email'] = $activity->actionable['data'][$data_index]->email;
                        $activity_array['crdate'] = Carbon::parse($activity->actionable['data'][$data_index]->created_at)->format('Y-m-d');
                        break;
                    case 'document':
                        $activity_array['value'] = $activity->actionable['data'][$data_index]->document_id;
                        $activity_array['crdate'] = Carbon::parse($activity->actionable['data'][$data_index]->created_at)->format('Y-m-d');
                        break;
                    case 'dropdown':
                        $activity_array['value'] = $activity->actionable['data'][$data_index]->related_party_dropdown_item_id;
                        $activity_array['crdate'] = Carbon::parse($activity->actionable['data'][$data_index]->created_at)->format('Y-m-d');
                        break;
                }
            }
            $process_progress[0]['activities'][$activity->id] = $activity_array;

        }

        foreach ($step->activities/*$step['activities']*/ as $activity_key => $activity) {
            if (!is_null($activity['dependant_activity_id']) && $activity['stage'] == 0) {
                $working_dependancy_id = $activity['dependant_activity_id'];
                foreach ($process_progress as $searched_step_key => $searched_step) {
                    if (isset($searched_step['activities'][$working_dependancy_id])) {
                        if ($searched_step['activities'][$working_dependancy_id]['stage'] == 2) {
                            $process_progress[$step->id]['activities'][$activity_key]['stage'] = 1;
                        } else {
                            if (!is_null($searched_step['activities'][$working_dependancy_id]['dependant_activity_id'])) {
                                $working_dependancy_id = $searched_step['activities'][$working_dependancy_id]['dependant_activity_id'];
                            }
                        }
                    }
                }
            }
        }

        $completed = true;
        $started = false;
        foreach ($step->activities/*$step['activities']*/ as $activity_key => $activity) {
            /*if ($activity['stage'] == 1) {
                $started = true;
            }*/

            if ($step->id == $this->step_id) {
                $started = true;
                //dd($activity);
            }

            if ($activity['kpi'] && $activity['stage'] != 2) {
                $completed = false;
            }

        }

        if ($started) {
            $process_progress[0]['stage'] = 1;
        }

        if ($completed) {
            $process_progress[0]['stage'] = 2;
            //dd($activity);
        }

        return $process_progress;
    }

    function getClientHighestStep(Process $process){

        $id = $this->id;
//dd($process);
        $highest_step_id = Step::with(['activities.actionable.data' => function ($query) use ($id) {
            $query->where('client_id', $id);
        }])->where('process_id',$process->id)->get();
//dd($highest_step_id);
        $process_activities=array();
        foreach ($highest_step_id as $highest_step) {
            if($this->isStepActivitiesCompleted(Step::find($highest_step->id))) {
                foreach ($highest_step->process->activities as $activity){
                    //get the step ids of the process if the client was previously in the selected process
                    foreach ($activity->actionable['data'] as $data) {
                        //push the step id into the array
                        array_push($process_activities, ["step_id" => $highest_step->id,"name" => $highest_step->name,"order" => $highest_step->order]);
                    }
                }
            }
        }
        //sort the array in descending order
        rsort($process_activities);
        return $process_activities[0]["step_id"];
        //return (isset($process_activities[0]) ? $process_activities[0] : []);
    }

    function getClientHighestStepOrder(Process $process){

        $id = $this->id;
//dd($process);
        $highest_step_id = Step::with(['activities.actionable.data' => function ($query) use ($id) {
            $query->where('client_id', $id);
        }])->where('process_id',$process->id)->get();

        $process_activities=array();
        if(count($highest_step_id) > 0) {

            foreach ($highest_step_id as $highest_step) {
                if($this->isStepActivitiesCompleted(Step::find($highest_step->id))) {
                    foreach ($highest_step->process->activities as $activity){
                        //get the step ids of the process if the client was previously in the selected process
                        foreach ($activity->actionable['data'] as $data) {
                            //push the step id into the array
                            array_push($process_activities,["step_id" => $highest_step->id,"name" => $highest_step->name,"order" => $highest_step->order]);
                        }
                    }
                }
            }


            //sort the array in descending order
            usort($process_activities, function ($item1, $item2) {
                return $item2['order'] <=> $item1['order'];
            });

        }
        $process_first_step = Step::where('process_id',$process->id)->orderBy('order','asc')->first();

        return ( isset($process_activities[0]) ? $process_activities[0]["order"] : $process_first_step->order );
        //return (isset($process_activities[0]) ? $process_activities[0] : []);
    }

    function getCurrentStep()
    {
        if ($this->completed_at != null) {
            $step = new Step();
            $step->name = "Converted";
            return $step;
        }

        $step = Step::withTrashed()->find($this->step_id);

        return $step;
    }

    function getCurrentStepToBeRemoved()
    {
        if ($this->completed_at != null) {
            $step = new Step();
            $step->name = "Converted";
            return $step;

            //return $this->process->steps->last();
        }

        foreach ($this->process->steps as $step) {
            foreach ($step->activities as $activity) {
                if ($activity->kpi == 1) {
                    if (isset($activity->actionable['data'][0])) {
                        $found = false;
                        foreach ($activity->actionable['data'] as $datum) {
                            if ($datum->client_id == $this->id) {
                                $found = true;
                                break;
                            }
                        }
                        if(!$found){
                            return $step;
                        }
                    } else {
                        return $step;
                    }
                }
            }
        }
    }

    function isStepActivitiesCompleted(Step $step)
    {
        //$step = Step::find($request->input('step_id'));
        $id = $this->id;
        $step->load(['activities.actionable.data' => function ($query) use ($id) {
            $query->where('client_id', $id);
        }]);

        $found = false;
        foreach ($step->activities as $activity) {
            $found = false;
            if ($activity->kpi == 1) {
                if (isset($activity->actionable['data'][0])) {
                    foreach ($activity->actionable['data'] as $datum) {
                        if ($datum->client_id == $this->id) {
                            $found = true;
                            break;
                        }
                    }
                    if(!$found){
                        return $found;
                    }
                } else {
                    return $found;
                }
            }
            $found = true;
        }
        return $found;
    }

    function isRelatedPartyStepActivitiesCompleted(Step $step,$related_party_id)
    {
        //$step = Step::find($request->input('step_id'));
        $id = $this->id;
        $step->load(['activities.actionable.data' => function ($query) use ($id,$related_party_id) {
            $query->where('related_party_id',$related_party_id);
        }]);

        $found = false;
        foreach ($step->activities as $activity) {
            $found = false;
            if ($activity->kpi == 1) {
                if (isset($activity->actionable['data'][0])) {
                    foreach ($activity->actionable['data'] as $datum) {
                        if ($datum->related_party_id == $related_party_id) {
                            $found = true;
                            break;
                        }
                    }
                    if(!$found){
                        return $found;
                    }
                } else {
                    return $found;
                }
            }
            $found = true;
        }
        return $found;
    }

    function isRelatedPartiesStepActivitiesCompleted(Step $step,$related_party_id)
    {
        //$step = Step::find($request->input('step_id'));
        $id = $this->id;
        $step->load(['activities.actionable.data' => function ($query) use ($id,$related_party_id) {
            $query->where('related_party_id',$related_party_id);
        }]);
//dd($step);
        $found = false;
        foreach ($step->activities as $activity) {
            $found = false;
            if ($activity->kpi == 1) {
                if (isset($activity->actionable['data'][0])) {

                    foreach ($activity->actionable['data'] as $datum) {
                        if ($datum->related_party_id == $related_party_id) {

                            $found = true;
                            break;
                        }
                    }
                    if(!$found){

                        return $found;
                    }
                } else {
                    return $found;
                }
            }
            $found = true;
        }
        return $found;
    }

    function isActivitieCompleted(Activity $activity)
    {
        $found = false;
        if ($activity->kpi == 1) {
            if (isset($activity->actionable['data'][0])) {
                foreach ($activity->actionable['data'] as $datum) {
                    if ($datum->client_id == $this->id) {
                        $found = true;
                        break;
                    }
                }
                if(!$found){
                    return $found;
                }
            } else {
                return $found;
            }
        }

        return $found;
    }

    function isRelatedPartiesActivitieCompleted(Activity $activity,$related_party_id)
    {
        $found = false;
        if ($activity->kpi == 1) {

            if (isset($activity->actionable['data'][0])) {
                foreach ($activity->actionable['data'] as $datum) {
                    if ($datum->related_party_id == $related_party_id) {
                        $found = true;
                        break;
                    }
                }
                if(!$found){
                    return $found;
                }
            } else {
                return $found;
            }
        }

        return $found;
    }

    //Check if activities for this client exists
    function clientProcessIfActivitiesExist()
    {
        $id = $this->id;

        /*$steps = Step::withTrashed()->with(['activities.actionable.data' => function ($query) use ($id) {
            $query->where('client_id', $id);
        }])->whereHas('process',function($q){
            $q->where('process_type_id','1');
        })->get();*/
        $processes = [];
        array_push($processes, [
            "id" => '0',
            "name" => 'Please Select'
        ]);

        $cps = ClientProcess::with('process')->where('client_id',$id)->get();

        foreach($cps as $cp){
            array_push($processes, [
                "id" => $cp->process_id,
                "name" => $cp->process->name,
            ]);
        }
        /*foreach ($steps as $step) {
            foreach ($step->activities as $activity) {
                //dd($activity->actionable->data[0]);
                if ($activity->actionable->data != null) {
                    foreach ($activity->actionable->data as $datum) {
                        if ($datum->client_id == $id) {
                            if (!in_array($step->process_id, array_column($processes,'id'))) {
                                $process_name = Process::withTrashed()->select('name')->where('id',$step->process_id)->first();
                                if($process_name != null) {
                                    array_push($processes, [
                                        "id" => $step->process_id,
                                        "name" => $process_name->name
                                    ]);
                                }
                            }
                        }
                    }
                }
            }

        }*/
        return $processes;
    }

    //Move process step to the next step if all activities completed
    function moveClientToNextStepIfActivitiesCompleted(Step $step){
        $load_next_step = false;

        $max_step = Step::orderBy('order','desc')->where('process_id', $this->process_id)->first();

        $c_step_order = Step::where('id',$this->step_id)->first();

        $n_step = Step::select('id')->orderBy('order','asc')->where('process_id', $this->process_id)->where('order','>',$step->order)->whereNull('deleted_at')->first();

        if($this->isStepActivitiesCompleted($step) && $step->order < $max_step->order && $step->order == $c_step_order->order){
            $this->step_id = $n_step->id;
            $this->save();
            $load_next_step = true;
        }

        if($this->isStepActivitiesCompleted($step) && $step->order == $max_step->order && $step->order == $c_step_order->order){
            $this->step_id = $step->id;
            $this->save();
        }

        if($this->isStepActivitiesCompleted($step) && $step->order < $c_step_order->order){
            $load_next_step = true;
        }

        return $load_next_step;
    }

    function users(){
        return $this->hasManyThrough('App\User','App\ClientUser', 'client_id', 'id', 'id', 'user_id');
    }

    function groupCompletedActivities(Step $step,$client_id){

        $step->load(['activities.actionable.data' => function ($query) use ($client_id) {
            $query->where('client_id', $client_id);
        }]);

        $group = 1;
        foreach($step["activities"] as $activity) {
            //dd($activity);
            if(isset($activity["actionable"]["data"][0])){
                if($activity["actionable"]["data"][0]->data != null) {
                    $group = $activity->grouping;
                }
            }
        }

        return $group;
    }

    function groupRelatedPartyCompletedActivities(Step $step,$client_id){

        $step->load(['activities.actionable.data' => function ($query) use ($client_id) {
            $query->where('related_party_id', $client_id);
        }]);

        $group = 1;
        foreach($step["activities"] as $activity) {
            //dd($activity);
            if(isset($activity["actionable"]["data"][0])){
                if($activity["actionable"]["data"][0]->data != null) {
                    $group = $activity->grouping;
                }
            }
        }

        return $group;
    }
}