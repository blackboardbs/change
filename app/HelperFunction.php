<?php


namespace App;


use Carbon\Carbon;

class HelperFunction
{
    public function array_find($needle, array $haystack, $column = null) {

        if(is_array($haystack[0]) === true) { // check for multidimentional array


            foreach (array_column($haystack, $column) as $key => $value) {
                if (strpos(strtolower($value), strtolower($needle)) !== false) {
                    return $key;
                }
            }

        } else {
            foreach ($haystack as $key => $value) { // for normal array
                if (strpos(strtolower($value), strtolower($needle)) !== false) {
                    return $key;
                }
            }
        }
        return false;
    }

    public function clientBucketActivityIds($steps, $client, $process_id){
        $tmp_act = $steps->where('process_id', $process_id)->map(function ($step){
            return $step->activities->map(function ($activ){
                if($activ->client_bucket){
                    return  $activ->id;
                }
            });
        })->flatten()->toArray();

        $tmp_act = array_values(array_filter($tmp_act));

        $parent_activities_in_client_basket = ActivityInClientBasket::where('client_id', $client->id)->select('activity_id', 'in_client_basket')
            ->get();

        $flag = $parent_activities_in_client_basket->map(function ($activity_id){
            return $activity_id->activity_id;
        })->toArray();

        foreach ($tmp_act as $key => $item){
            if (in_array($item, $flag)) {
                unset($tmp_act[$key]);
            }
        }

        $active_activities = $parent_activities_in_client_basket->where('in_client_basket', 1)->map(function ($activity){
            return $activity->activity_id;
        })->values()->toArray();

        return array_merge($tmp_act, $active_activities);
    }

}